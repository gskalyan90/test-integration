$(document).ready(function() {
	
	airlineId = $("#airlineId").val();
	getRules();

});

function getRules() {

	$.ajax({
				url : "http://localhost:8080/apis/airlines/"+airlineId+"/rule/getRules",
				type : "GET",
				contentType : "application/json",
				dataType : "json",
				processData : false,
				success : function(result) {
					console.log("List of Rules", result);
					$(".tableRes").show();					
					var dynamicHTML = "";
					for (var i = 0; i < result.length; i++) {
						var ruleId = result[i]['ruleId'];
						dynamicHTML += "<tr id='" + result[i]["ruleId"] + "'>";
						dynamicHTML += "<td>" + result[i]["ruleName"] + "</td>";
						dynamicHTML += "<td><span><button class=\"listingbtn\" onClick=\"previewRule('"+ ruleId + "')\" type=\"button\" class=\"btn btn-info btn-lg\" data-toggle=\"modal\" data-target=\"#myModal\">View</button></span>&nbsp; &nbsp;<span><button class=\"listing-delete-btn\" onClick=\"deleteRules('"+ ruleId + "')\">Delete</button> </span> &nbsp;&nbsp;<span id=\"deleteMsg\" style=\"display:none\">Deleting...<b></b></span></td>";
						dynamicHTML += "</tr>";
					}
					$("#dynamicRow").html(dynamicHTML);
				},
				error : function(error) {
					console.log("error occured", error);
				}
			})
}

function deleteRules(ruleId) {
	$.ajax({
		url : "http://localhost:8080/apis/rule/deleteRule/" + ruleId,
		type : "DELETE",
		contentType : "application/json",
		dataType : "json",
		processData : false,
		success : function(result) {
			console.log("List of Rules", result);
			location.reload();
		},
		error : function(error) {
			console.log("error occured", error);
		}
	})
}


function previewRule(ruleId)
{
	$('#myModal').show();
	$('#myModal').addClass("in").css({"overflow":"auto"});	
	
	$.ajax({
		url : "http://localhost:8080/apis/rule/getRule/"+ruleId,
		type : "GET",
		contentType : "application/json",
		dataType : "json",
		processData : false,
		success : function(data) {
			console.log(" Rule object ", data);
			
			$('#tableDataContainer').html("");
			console.log(data);
			console.log(data.ruleName);
			$('#ruleName').css("text-align","left");
			$('#ruleName').html("<p> <b>Rule Name:</b>  "+ data.ruleName +"</p>");
			$('#ruleName').append("<p> <b>Configuration Name:</b>  "+ data.configurationName +"</p>");
			$('#ruleName').append(" <p> <b>Content Type Name:</b>  "+ data.ruleContentTypeMappingDTO.contentTypeName+"</p>");
			//for(var i=0; i < data.ruleContentTypeMappingDTO.contentTypeField.length; i++){
				var tableInit = '<table class="table table-bordered"><thead><tr><th>Field Name</th><th>Hidden</th><th>Translatable</th><th>MaxChar</th></tr></thead> <tbody  class="">'
				var trList = "";
				for(var j=0; j < data.ruleContentTypeMappingDTO.contentTypeField.length;j++)
				{
					var liData = data.ruleContentTypeMappingDTO.contentTypeField[j];
					trList += '<tr class=""><td>'+liData.fieldName+'</td><td style="text-align:left">'+liData.validatorDTO.hidden+'</td><td >'+liData.validatorDTO.translatable+'</td><td >'+liData.validatorDTO.maxChar+'</td></tr>';
				}
				tableInit = tableInit + trList + '</tbody></table>';
				$('#tableDataContainer').append(tableInit);
			//}
			
			//location.reload();
		},
		error : function(error) {
			console.log("error occured", error);
		}
	})	
}