var flag = true;
$(document).ready(function() {
	
	airlineId = $("#airlineId").val();
	
    $(document).on('click', '#submitValidation', function(event) {
    	var configText = $("#configDropdown0").text();
    	var contentTypeText = $("#configDropdown0").text();
    	var ruleName = $("#ruleName").val();
    	if(ruleName == "")
    		{
    			alert("Please enter Rule Name");
    			return;
    		}
    	if(configText =="Please Select Configuration" || contentTypeText =="Please Select Content Type")
    		{
    			alert("Please select the values from both dropdowns.");
    			return;
    		}
        var coreObj = {
        	"configurationName": contentTypeText,	
            "ruleId": "",
            "ruleName": "",
            "ruleContentTypeMappingDTO": {}
        };

        var contentTypeRepeatedObj = {
            "contentTypeId": "",
        };

       var tempArr =[];
        var allTables = $("#dynamic-dropdwon-area .tableRes");
        var secondPartObj ={};
        for (var i = 0; i < allTables.length; i++) {
              
            contentTypeRepeatedObj.contentTypeId = allTables[i]["id"];
            var fieldVal = $("#dynamic-dropdwon-area .tableRes #dynamicRow tr");
            var fieldCount = $("#dynamic-dropdwon-area .tableRes #dynamicRow tr").length;
            
            
            for (var j = 0; j < fieldCount; j++) {
              var trArrayDom = $("#dynamic-dropdwon-area .tableRes #dynamicRow tr")[j];
              var hiddenChkBox = $(trArrayDom).find("#hiddenType").is(':checked');
              var translatableChkBox = $(trArrayDom).find("#translatable").is(':checked');
              var maxCharLen = $(trArrayDom).find("#maxChar").val();
              var contentTypeRepeatedObj1 ={};
              //contentTypeRepeatedObj.fielId.push()
              //contentTypeRepeatedObj1.fieldId = $(fieldVal[j]).attr("id");
              contentTypeRepeatedObj1.fieldName = $(fieldVal[j]).text();
              contentTypeRepeatedObj1.validatorDTO = {"hidden": hiddenChkBox,"translatable": translatableChkBox,"maxChar": maxCharLen};
              tempArr.push(contentTypeRepeatedObj1);
              console.log(contentTypeRepeatedObj);
              contentTypeRepeatedObj.contentTypeField = tempArr;
              
            }
        }
        
      
        coreObj.ruleId = "string";
        coreObj.ruleName = $("#ruleName").val();
        //coreObj.ruleContentTypeMappingDTO.push(contentTypeRepeatedObj);
        coreObj.ruleContentTypeMappingDTO = contentTypeRepeatedObj;
        console.log("coreObj>>>>>>>>>>>>>>>>>",coreObj);
        saveRule(coreObj);
        
        
    });

    // code for dynamic dropdown add         
    if (flag) {
        var dynamicHTML = addDynamicDropdown(0);
        addDynamicConfigDropdown(0);
    }
    flag = false;
    $(document).on('click', '#addDropdown', function(event) {
        var allExistingDropdown = $("#dynamic-dropdwon-area .dropdown").length;
        var newItemId = allExistingDropdown + 1;
        var dynamicHTML = addDynamicDropdown(newItemId);
    });
});


function populateContentType(id) {

    $.ajax({
        url: "http://localhost:8080/apis/rule/contentTypeList",
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        processData: false,
        success: function(result) {
            console.log("Added success", result);
            var dynamicHTML = "";
            var listContentTypeLen = result.length;
            for (var i = 0; i < listContentTypeLen; i++) {
                var currentContentTypeName = result[i].contentTypeName;
                var currentContentTypeId = result[i].contentTypeId;
                dynamicHTML += "<li><a href='#' id='" + currentContentTypeId + "'>" + currentContentTypeName + "</a></li>";
                //console.log(">>dynamicHTML>>",dynamicHTML);
            }
            $("#" + id).append(dynamicHTML);

        },
        error: function(error) {
            console.log("error occured ", error);
        }
    })
}


function addDynamicDropdown(id) {
    var dynamicDropDownId = "dropdown" + id;
    var ddID = "ddValidation" + id;
    var ruleDropdown = "ruleDropdown" + id;
    var dynamicHTML = "";
    dynamicHTML += "<div class='dropdown' id=" + dynamicDropDownId + ">";
    dynamicHTML += "<button class='btn btn-primary dropdown-toggle thales-dropdown' type='button' data-toggle='dropdown' id=" + ruleDropdown + ">Please Select Content Type";
    dynamicHTML += '<span class="caret"></span></button>';
    dynamicHTML += "<ul class='dropdown-menu' id=" + ddID + ">";
    dynamicHTML += '</ul>';
    //dynamicHTML += '<span ><button type="button" class="btn btn-primary confirm-btn" style="margin-left: 5px;" id="addDropdown">Add</button></span>';
    dynamicHTML += '</div>';
    dynamicHTML += "<p>&nbsp;</p>";
    $("#dynamic-dropdwon-area").append(dynamicHTML);
    populateContentType(dynamicDropDownId + " " + "#" + ddID);
    var currentId = '#ddValidation' + id + " li";

    setTimeout(function() {
        console.log("event bind for ", currentId);

        $(document).on('click', currentId, function(event) {
            createBootstrapDropdown(this, ruleDropdown, ddID);
            getSelectedContentType(this);

        });
    }, 500);


}

function createBootstrapDropdown(currentElement, buttonId, dropdownlistId) {
    var idSelectorForButton = "#" + buttonId;
    var idSelectorForDropdown = "#" + dropdownlistId + " li a";
    var getCurrntListItemText = $(currentElement).find("a").text();
    $(idSelectorForButton).html(getCurrntListItemText + '<span class="caret"></span>');
    $(idSelectorForDropdown).removeClass("isSelected");
    $($(currentElement).find('a')).addClass("isSelected")
}




function getSelectedContentType(currentElem) {
    var contentTypeId = $(currentElem).context.firstChild.id;
    console.log(contentTypeId);
    getFieldsByContentTypeId(contentTypeId, currentElem);
    //console.log("http://localhost:8080/ECMS_6/apis/getFieldByContentTypeId?contentTypeId="+contentTypeId);    
}


function getFieldsByContentTypeId(contentTypeId, currentElem) {
    $.ajax({
        url: "http://localhost:8080/apis/rule/getFieldByContentTypeId/" + contentTypeId,
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        processData: false,
        success: function(result) {
            //console.log("Added success", result);
            var tableDomId = $(currentElem).parent().parent().attr("id");
            var nextElemntLength = $("#" + tableDomId).next(".tableRes");
            if (nextElemntLength) {
                nextElemntLength.remove();
            }

            createDynamicTable(currentElem, contentTypeId);
            var dynamicHTML = "";
            for (var i = 0; i < result.length; i++) {
                dynamicHTML += "<tr id='" + result[i]["fielId"] + "'>";
                dynamicHTML += "<td id='" + result[i]["fielId"] + "'>" + result[i]["fieldName"] + "</td>";
                dynamicHTML += '<td><input id="hiddenType" type="checkbox" name="hiddenType"></td>';
                dynamicHTML += '<td><input id="translatable" type="checkbox" name="translatable"></td>';
                dynamicHTML += '<td><input id="maxChar" type="text" name="maxChar" class="txtBox" placeholder="Max Char"></td>';
                dynamicHTML += "</tr>";
            }
            $("#" + tableDomId).next().find("#dynamicRow").html("").html(dynamicHTML);
            $("#submitValidation").show();
            $(".tableRes").show();

        },
        error: function(error) {
            console.log("error occured", error);
        }
    })
}

function createDynamicTable(currentElem, contentTypeId) {
    var currentRootElement = $(currentElem).parent().parent();
    var dynamicHTML = "";
    dynamicHTML += '<div class="tableRes" id=' + contentTypeId + ' style="display:none;margin-top:5px;">';
    dynamicHTML += '<table class="table table-bordered">';
    dynamicHTML += '<thead>';
    dynamicHTML += '<tr>';
    dynamicHTML += '<th>Field Name</th>';
    dynamicHTML += '<th>Hidden</th>';
    dynamicHTML += '<th>Translatable</th>';
    dynamicHTML += '<th>MaxChar</th>';
    dynamicHTML += '</tr>';
    dynamicHTML += '</thead>';
    dynamicHTML += '<tbody id="dynamicRow">';
    dynamicHTML += '</tbody>';
    dynamicHTML += '</table>';
    dynamicHTML += '</div>';
    $(dynamicHTML).insertAfter($(currentRootElement));
    $(".tableRes").show();
}


function getRules() {

	$.ajax({
		url : "http://localhost:8080/apis/rule/getRules",
		type : "GET",
		contentType : "application/json",
		dataType : "json",
		processData : false,
		success : function(result) {
			console.log("Added success", result);
			$(".tableRes").show();
			var dynamicHTML = "";
			for (var i = 0; i < result.length; i++) {
				dynamicHTML += "<tr id='" + result[i]["ruleId"] + "'>";
				dynamicHTML += "<td>" + result[i]["ruleName"] + "</td>";
				dynamicHTML += "</tr>";
			}
			$("#dynamicRow").html(dynamicHTML);
		},
		error : function(error) {
			console.log("error occured", error);
		}
	})
}


function saveRule(coreObject) {
	$("#submitValidation").html("Submitting....");
    console.log("json = ", coreObject);
    $.ajax({
           url : "http://localhost:8080/apis/airlines/"+airlineId+"/rule/addRule",
           type : "POST",
           contentType : "application/json",
           dataType : "json",
           processData : false,
           data : JSON.stringify(coreObject),
           success : function(result) {
                  console.log("Added success", result);
                  alert("Rule successfully created..");
                  $("#submitValidation").html("Submit");
                  location.reload();
           },
           error : function(error) {
                  console.log("error occured", error);
           }
    })
}

function populateConfiguration(id){
	$.ajax({
        url: "http://localhost:8080/apis/configuration/configurationList/"+airlineId,
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        processData: false,
        success: function(data) {
            console.log("Added success", data);
            var dynamicHTML = "";
            var configLength = data.length;
            for (var i = 0; i < configLength; i++) {
                var configName = data[i].configurationName;
                var configId = data[i].configurationId;
                dynamicHTML += "<li><a href='#' id='" + configId + "'>" + configName + "</a></li>";
                //console.log(">>dynamicHTML>>",dynamicHTML);
            }
            $("#" + id).append(dynamicHTML);

        },
        error: function(error) {
            console.log("error occured ", error);
        }
    })
	
	 //var data = configData;
   
	
}
function addDynamicConfigDropdown(id) {
    var dynamicDropDownId = "ConfigDropdown" + id;
    var ddID = "ddConfigValidation" + id;
    var configDropdown = "configDropdown" + id;
    var dynamicHTML = "";
    dynamicHTML += "<div class='dropdown' id=" + dynamicDropDownId + ">";
    dynamicHTML += "<button class='btn btn-primary dropdown-toggle thales-dropdown' type='button' data-toggle='dropdown' id=" + configDropdown + ">Please Select Configuration";
    dynamicHTML += '<span class="caret"></span></button>';
    dynamicHTML += "<ul class='dropdown-menu' id=" + ddID + ">";
    dynamicHTML += '</ul>';
    dynamicHTML += '</div>';
    dynamicHTML += "<br/>";
    $("#dynamic-ConfigDropdwon-area").append(dynamicHTML);
    populateConfiguration(ddID)
    var currentId = '#ddConfigValidation' + id + " li";

    setTimeout(function() {
        console.log("event bind for ", currentId);

        $(document).on('click', currentId, function(event) {
            createBootstrapDropdown(this, configDropdown, ddID);
           // getSelectedContentType(this);

        });
    }, 500);


}
