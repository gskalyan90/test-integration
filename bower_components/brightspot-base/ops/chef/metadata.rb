name             'brightspot-base'
maintainer       '${maintainer}'
maintainer_email '${maintainer_email}'
description      'Installs/Configures brightspot-base project'
long_description 'Installs/Configures brightspot-base project'
version          '1.0.0'

depends 'brightspot', '~> 2.0.0'
depends 'perfectsense-brightspot', '~> 1.0.0'
