export interface IOrder {
	configureDTOSet:IConfigureDTOSet[]
	displayGroupList:IDisplayGroup[];
	orderDescription:string;
	orderEndDate:number;
	orderId:string;
	orderName:string;
	orderStartDate:number;
	lastModifiedDate:string;
	orderStatus:string;

	dirty?:boolean;
}

export interface IConfigureDTOSet {
	configurationId:string;
	configurationName:string;
	configurationStatus:string;
	packagingType:string;
	titleList:ITitle[];
}

export interface IListItem {
	label:string;
	id:string;
	data?:any;
	selected:false;
}

export interface ITitle{
	contentInternalId:string;
	contentName:string;
	contentStartDate:number;
	contentendDate:number;
	contentStatus:string;
	priority:string;
	destinationList:string[]
	configurationName?:string;
	configurationId?:string;
}

export interface IDisplayGroup{
	endDate:number;
	month:string;
	startDate:number;
}

export interface Airline{
	airlineInternalId:string;
	airlineName:string;
	website:string;
	companyName:string;
	airlineStatus:string;
	icaoCode:string;
	iataCode:string;
	codeShare:string;
}


export interface IRule {
	ruleContentTypeMappingDTO:IRuleContentTypeMappingDTOSet;
	ruleId:string;
	ruleName:string;
	
}
export interface IRuleContentTypeMappingDTOSet
{
	contentTypeId:string;
	contentTypeName:string;
	contentTypeField: IContentTypeFieldSet[];
}
export interface IContentTypeFieldSet
{
	fieldName:string;
	validatorDTO:IValidatorDTO;
}
export interface IValidatorDTO
{
	maxChar:string;
	required:boolean;
	translatable:boolean;
}



export interface IContentTypeListForRule{
	contentTypeId:string;
	contentTypeName:string;
	fieldId:string;
	fieldName:string;
	translatable:boolean;
	maxChar:string;
}
export interface IContentTypeForProfile
{
	contentTypeName:string;
	fields:IConetntTypeFields[];

}
export interface IConetntTypeFields
{
	displayName:string;
	fieldId:string;
	fieldType:string;
	selectionListName:string;
}


export interface IContentType{
	fieldId:string;
	fieldName:string;
	translatable:string;
	maxChar:string;	
}

export interface IMultiSelectOption{
	label:string;
	value:string;
	data?:any;
	id?:string

}

export interface IBundle{
	seatingClassPriceMappings:ISeatingClassPriceMappings[];
	bundleId:string;
	bundleName:string;
	
}
export interface ISeatingClassPriceMappings
{
	ppa:string;
	ppv:string;
	seatingClassId:string;
}
export interface ISeatingClass
{
	seatingClassInternalId:string;
	name:string;
}

