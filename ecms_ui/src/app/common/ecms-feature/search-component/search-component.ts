import { AfterViewChecked } from '@angular/core/src/metadata/lifecycle_hooks';
import {Component, EventEmitter, Output, AfterViewInit} from '@angular/core';


@Component({
  selector: 'search-component',
  templateUrl: './search-component.html',
  styleUrls: ['./search-component.scss']
})
export class SearchComponent implements AfterViewInit
{
	
	fields:any;
	flag:boolean=false;

	@Output() onSearch = new EventEmitter();

    constructor(){
    	this.fields = {orderName:"", orderStartDate:"", orderEndDate:"",lastModifiedDate:"", orderStatus:"", orderDescription:""};
    }

	hidePopup(){
	
		this.flag = false;
	}
	
	openSearchPopup = function()
	{
		this.flag = true;
	}
	
	serachResults = function()
	{
		this.onSearch.emit(this.fields);
		this.flag = false;
	}

	ngAfterViewInit(){
		this.onSearch.emit(this.fields);
	}

	parseDatetoTime(date:any):number{
		return (new Date(date.year,date.month,date.day)).getTime();
	}
}
