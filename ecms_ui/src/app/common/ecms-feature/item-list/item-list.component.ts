import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

	@Input() itemsProvider;

	@Output() itemClicked = new EventEmitter();

	itemClickedEvent:ItemClickedEvent;

  	constructor() { }
  	
  	ngOnInit() {
  		this.itemClickedEvent = {
  			item:null,
			selectedItemsList:[]
  		}  		
  	}

  	itemClickedHandler(item){  		
      item.selected = !item.selected;
      this.itemClickedEvent.item = item;  		
      if(item.selected){
  			this.itemClickedEvent.selectedItemsList.push(item);
  		}else{
  			this.itemClickedEvent.selectedItemsList = this.itemClickedEvent.selectedItemsList.filter((i)=>{
  				return i.id != item.id;
  			})
  		}
  		this.itemClicked.emit(this.itemClickedEvent);
  	}

}

interface Item{
	id:string,
	label:string,
	selected:boolean
}

export interface ItemClickedEvent{
	item:any;
	selectedItemsList:any[]
}