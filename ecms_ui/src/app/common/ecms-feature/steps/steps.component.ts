import { Component, OnInit, QueryList, Input, AfterContentInit, ContentChildren, Output, EventEmitter } from '@angular/core';
//import { IOrder } from '../../interfaces';


@Component({
	template:`
		<div [hidden]="!active" class="pane">
	      <ng-content></ng-content>
	    </div>
	`,
	selector:'step',
	styles: [`
	    .pane{
	      padding: 1em;
	    }
 	`,'./steps.component.scss']
})
export class Step {
	@Input('title') title: string;
  @Input() active = false;
}


@Component({
  selector: 'steps',
  template: `
    <ul class="steps">
    	<li *ngFor="let step of step" [class.active]="step.active" (click)="selectStep(step)">{{step.title}}<span class="toe-top"></span> <span class="toe-bottom"></span></li>
    </ul>    
    <div class="step-container"><ng-content></ng-content></div>
    <div class="button-container">      
      <button class="btn btn-default btn-sm" (click)="handlerPre()" [disabled]="isFirstIndex"><i class="fa fa-angle-left" aria-hidden="true"></i> Prev</button>          
      <button class="btn btn-primary btn-submit" (click)="handlerSubmit()" [disabled]="!isLastIndex">Submit</button>
      <button class="btn btn-default btn-sm" (click)="handlerNext()" [disabled]="isLastIndex">Next <i class="fa fa-angle-right" aria-hidden="true"></i></button>      
    </div>
	`,
  styleUrls: ['./steps.component.scss']
})
export class Steps implements OnInit, AfterContentInit {

  @ContentChildren(Step) step: QueryList<Step>;

  isLastIndex:boolean;
  isFirstIndex:boolean;

  @Output('submitClicked') submitClicked = new EventEmitter();
  @Output('saveClicked') saveClicked = new EventEmitter();



 /* @Input()
  get orderModel():IOrder{
    return 
  }*/

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit(){
  	// get all active tabs
  	let activeTabs = this.step.filter((step)=>step.active);
    
    // if there is no active step set, activate the first
    if(activeTabs.length === 0) {
      this.selectStep(this.step.first);      
    }
    this.manageButtons(this.getCurrentIndex());
  }

  selectStep(step: Step){
    // deactivate all steps
    this.step.toArray().forEach(step => step.active = false);
    
    // activate the step the user has clicked on.
    step.active = true;
    this.manageButtons(this.getCurrentIndex());
  }

  handlerPre(){
    let currentIndex = this.getCurrentIndex();    
    this.step.toArray()[currentIndex]['active'] = false
    this.step.toArray()[--currentIndex]['active'] = true;
    this.manageButtons(currentIndex);
  }

  handlerNext(){
    let currentIndex = this.getCurrentIndex();    
    this.step.toArray()[currentIndex]['active'] = false
    this.step.toArray()[++currentIndex]['active'] = true;
    this.manageButtons(currentIndex);
  }

  getCurrentIndex():number{    
    return this.step.toArray().findIndex(x=>x.active);
  }

  manageButtons(currentIndex){
    this.isLastIndex = false;
    this.isFirstIndex = false;
    if(currentIndex==0){
      this.isFirstIndex = true;
    }else if(currentIndex==this.step.toArray().length-1){
      this.isLastIndex = true;
    }
  }

  handlerSubmit(){
    this.submitClicked.emit();
  }

  handlerSave(){
    this.saveClicked.emit(); 
  }
}
