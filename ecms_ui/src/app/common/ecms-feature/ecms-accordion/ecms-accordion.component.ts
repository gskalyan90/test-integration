import { Component, OnInit, Input, AfterViewInit } from '@angular/core';


@Component({
  selector:'ecms-accordion',
  template:`
    <div class="accordion">
      <div class="panel panel defualt callout header" (click)="toggleContent()">
       <h5 class="ecms-sub-head">{{title}}</h5>
        <span [hidden]="!isOpen"><i class="fa fa-plus" aria-hidden="true"></i></span>
        <span [hidden]="isOpen"><i class="fa fa-minus" aria-hidden="true"></i></span>
      </div>
      <div class="body callout" [hidden]="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styleUrls: ['./ecms-accordion.component.scss']
})
export class Accordion {
  @Input('title') title: string;
  isOpen:boolean = false;
  toggleContent(){    
    this.isOpen = !this.isOpen;
  }
}

@Component({
  selector: 'ecms-accordion-container',
  template: `
    <div class="accordion-container">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./ecms-accordion.component.scss']
})
export class EcmsAccordionComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(){

  }
}

