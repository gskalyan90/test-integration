import { DayTemplateContext } from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker-day-template-context';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'date-picker',
  template: `
    <form class="form-inline">
        <div class="form-group">
            <div class="input-group">
                <input class="form-control" placeholder="yyyy-mm-dd" name="dp" [(ngModel)]="dateModel" (ngModelChange)="onChange($event)" ngbDatepicker #startD="ngbDatepicker">
                <div class="input-group-addon" (click)="startD.toggle()">
                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>                            
                </div>
            </div>
        </div>
    </form>
  `
})
export class DatePickerComponent implements OnChanges {

  dateModel: DateModel;

  @Output() dateChange = new EventEmitter();

  @Input() date: number;

  ngOnChanges(val) {
    console.log('ngchanges-', this.date, val);
    if (val) {
      this.dateModel = this.parseDate(this.date);
    }
  }

  onChange(date) {    
    this.date = this.parseDatetoNumber(date);
    this.dateChange.emit(this.date);    
  }

  parseDate(timestamp: number): DateModel {
    let d = new Date(timestamp);
    let dm: DateModel = {} as DateModel;
    dm.year = d.getFullYear();
    dm.month = d.getMonth()+1;
    dm.day = d.getDate();
    return dm;
  }

  parseDatetoNumber(dt: DateModel): any {
    if(!dt){
      return "";
    }
    return (new Date(dt.year, dt.month-1, dt.day)).getTime();
  }

}

export class DateModel {
  year: number;
  month: number;
  day: number;
}