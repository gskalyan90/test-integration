import {Component, Input, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'switchbox',
  template: `
  	<div class="switch medium">
	    <input (click)="handlerClick()" class="switch-input" id="{{switchId}}" type="checkbox" name="exampleSwitch" [checked]="isOn">
	    <label class="switch-paddle" for="{{switchId}}">
	    	<span class="show-for-sr">Part of package</span>
	        <span class="switch-active" aria-hidden="true">Yes</span>
	        <span class="switch-inactive" aria-hidden="true">No</span>
	    </label>
	</div>
  `,
  styleUrls: ['./switchbox.scss']
})
export class SwitchBoxComponent{
	isOn: boolean;

	switchId: string;

	@Output()switchOnChange = new EventEmitter();

	@Input()
	get switchOn():boolean{
		return this.isOn
	}

	set switchOn(val:boolean){
		this.isOn = val
		this.switchOnChange.emit(this.isOn);
	}

	@Input()
	set switchFor(val:string){
		this.switchId = val;
	}

	handlerClick(){
		this.switchOn = !this.switchOn
	}
}
