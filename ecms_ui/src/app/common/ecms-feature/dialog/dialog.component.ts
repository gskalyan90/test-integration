/**
 * Created by Alok on 1/19/2017.
 */
 import { AppService } from '../../../app-service.service'; 
import { Component, OnInit, Input, Output, OnChanges, EventEmitter, trigger, state, style, animate, transition } from '@angular/core';

@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.less'],
    animations: [
        trigger('dialog', [
            transition('void => *', [
                style({ transform: 'scale3d(.3, .3, .3)' }),
                animate(100)
            ]),
            transition('* => void', [
                animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
            ])
        ])
    ]
})
export class DialogComponent implements OnInit {
    @Input() closable = true;
    @Input() visible: boolean;
    @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() clickOutside = new EventEmitter();
    constructor(private appService:AppService) { }

    ngOnInit() { }
    close() {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    }
    onClickOutSide(value:string){
      if(this.closable){
          this.close();
      }else{
          this.clickOutside.emit();
      }
    }

   

}
