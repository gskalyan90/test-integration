import { Injectable } from '@angular/core';
import { Airline } from './common/interfaces';
import { Response } from '@angular/http';
import { HttpService } from './ecmsservice.service';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {

  constructor(private http:HttpService) { }

  getAirline():Airline{
    if(localStorage.getItem('airline')){
      return JSON.parse(localStorage.getItem('airline'));
    }
    return null
  }

  setAirline(airline:Airline){
    localStorage.setItem("airline",JSON.stringify(airline));    
  }

  getAirlines():Observable<Airline[]>{
    return this.http.get("apis/airlines")
      .map((res: Response) => res.json())
  }
}
