import {Component, OnChanges, OnInit} from '@angular/core';
import {Ingestion} from '../models/ingestion';
import {IngestionService} from '../services/ingestion.service';
import { FileUploader } from 'ng2-file-upload';
import {IngestionSearchComponent} from '../ingestion-search-component/ingestion-search-component'

import {IngestionViewService} from '../services/ingestion-view.service'
import {AppService} from "../../app-service.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'ingestion',
  templateUrl: 'ingestion.component.html',
  styleUrls: ['ingestion.component.less']
})
export class IngestionCmp implements OnInit{

  ingestionData: Ingestion[];

  constructor(private ingestionService: IngestionService, private ingestionViewService:IngestionViewService, private appService:AppService) { }

  ngOnInit(): void {
    this.ingestionViewService.getJobList(this.appService.getAirline().airlineInternalId);
    }
}

