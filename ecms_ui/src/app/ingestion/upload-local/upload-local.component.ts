import {Component, Output, Input, EventEmitter, OnChanges, OnInit} from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

import {ChangeDetectorRef} from '@angular/core';
import {AppService} from '../../app-service.service'
import {IngestionService} from '../services/ingestion.service'
// const URL = '/api/';


@Component({
  selector: 'app-upload-local',
  templateUrl: './upload-local.component.html',
  styleUrls: ['upload-local.component.less']
})
export class UploadLocalComponent implements  OnChanges,OnInit{
  @Input() testUrl: string;
  public URL: string = 'http://localhost:8080/apis/metadata/uploadfiles';
  public uploader:FileUploader = new FileUploader({url: this.URL});
  public hasBaseDropZoneOver:boolean = false;
  //public hasAnotherDropZoneOver:boolean = false;

  @Output() sendUploadedData = new EventEmitter<any>();
  @Output() public closeDialog: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() uploadSuccessEvent = new EventEmitter();

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  sendUploadedFiles() {
    this.sendUploadedData.emit(this.uploader);
  }

  ngOnInit(){

  }

  constructor(private changeDetectorRef: ChangeDetectorRef, private appService:AppService, private ingestionService:IngestionService) {

    // this.uploader.onSuccessItem = (item: any, response: any, status: any, headers: any) => {
    //   console.log(response);
    //   alert('Upload Complete');
    // }

    let airlineId=this.appService.getAirline().airlineInternalId;

    this.URL+='/'+airlineId+'/';

  }


  removeRow(rowNumber:number){
    this.uploader.queue.splice(rowNumber, 1);
    this.changeDetectorRef.detectChanges();
  }

  fileValidator(item){

    let fileName:string;
    let fileType:string;
    fileName = item.file.name;
    let n:number;
    n=fileName.lastIndexOf(".");
    fileType=fileName.substr(n+1,fileName.length);
    console.log(fileType);
    if(fileType!=('xlsx'||'csv')) {
      alert("File format not supported!");
      return;
    }
    // } else {
    //
    // }

  }


  closeDialogBox(item: any, rowNumber:number) {
    //For removing repeatative propery lines
    this.uploader.queue.splice(rowNumber, 1);
    this.changeDetectorRef.detectChanges();
    //File Validator
    //console.log(item.file.name);
    let fileName:string;
    let fileType:string;
    fileName = item.file.name;
    let n:number;
    n=fileName.lastIndexOf(".");
    fileType=fileName.substr(n+1,fileName.length);
    //console.log(fileType);
    if(fileType!=('xlsx'||'csv')){
      alert("File format not supported!")
      return;
    }

    this.sendUploadedData.emit(item);
    this.closeDialog.emit(false);   // (its actually spitting out the value 'false') but this.closeDialog !== false;
    //console.log(this);
    //document.getElementById("try").style.display="none";
  }

  ngOnChanges(changes: any) {
    //console.log(changes.selectedValue);
    //console.log('value of on changes', changes);
    this.URL+=changes.testUrl.currentValue;
    //console.log(this.URL);
    this.uploader = new FileUploader({url: this.URL});

    this.uploader.onSuccessItem=(item: any, response: any, status: any, headers: any)=>{
      console.log('Onchanges '+ response);

      this.uploadSuccessEvent.emit(null);
    }
  }

}
