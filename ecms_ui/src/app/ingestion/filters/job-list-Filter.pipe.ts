import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jobListFilter'
})
export class JobListFilter implements PipeTransform {

  transform(value: any, name: string, startDate: number, endDate: number, status: string): any {
    name = name ? name.toLowerCase() : "";
    status = status ? status.toLowerCase() : "";
    return value && value.filter((value) => {
        let fileName = value.fileName ? value.fileName.toLowerCase():"";
        let status = value.status ? value.status.toLowerCase():"";
        //console.log(status);
        //let orderDescription = value.orderDescription ? value.orderDescription.toLowerCase():"";
        if (fileName.indexOf(name) >= 0 &&
          status.indexOf(status) >= 0) {
          if (startDate) {
            if ((new Date(value.ingestedDate).toLocaleDateString()) != (new Date(startDate).toLocaleDateString()))
              //console.log("I am in");
              return false;
          }
          return true;
        }
        return false;
      });
  }
}
