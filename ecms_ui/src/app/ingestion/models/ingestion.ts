export class Ingestion {
  id: number;
  name: string;
  runtime: number;
  progress: number;
  description: string;
}
