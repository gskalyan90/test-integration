import { Injectable } from '@angular/core';
import { Ingestion } from '../models/ingestion';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Response} from '@angular/http'
import {Observable} from 'rxjs/Rx';

@Injectable()
export class IngestionService {

  configValue:any;
  constructor(private http:Http){}

	getConfig(airlineId){
    return this.http.get("apis/order/configurationByAirline/" + airlineId)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || "Server Error"));
  }

  // postSelectedConfig(value){
	//   this.configValue= value;
	//   //return configValue;
  //   //console.log("CHECK "+this.configValue);
  // }
  //
  // getSelectedConfig(){
  //   console.log("Get CONFIG " +this.configValue);
  //   return this.configValue;
  // }

  //POST
  importJob(user:any){
    const body = JSON.stringify(user);
    const headers= new Headers(); //optional
    headers.append("Content-type",'application/json'); //optional
    return this.http.post('http://localhost:8080/apis/metadata/importcontenttype',body,
      {headers:headers}
    )
      .map((data:Response) => data.json());
  }
}

