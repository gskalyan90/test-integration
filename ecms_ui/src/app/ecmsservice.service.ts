import { Injectable } from '@angular/core';
import { ToastyService } from 'ng2-toasty';


import {
  Http,
  ConnectionBackend,
  RequestOptions,
  RequestOptionsArgs,
  Response,
  Request
} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class ToastService {
  public static toast: ToastyService;

  setToastInstance(instance:ToastyService){
    ToastService.toast = instance;
  }

  getToastInstance():ToastyService{
    if(!ToastService.toast){
      throw new Error("instance not available yet");
    }
    return ToastService.toast;
  }
}


@Injectable()
export class HttpService extends Http {
  readonly API_ENDPOINT:string = "http://localhost:8080/";

  constructor(
    backend: ConnectionBackend,
    defaultOptions: RequestOptions,
    private toastService: ToastService
  ) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {    
    return super.request(url, options).catch(res => {            
      this.errorInterceptor(res.json());
      return Observable.throw(res);
    });
  }

  private errorInterceptor(res) {
    this.toastService.getToastInstance().error({ title: 'Error', msg: res.errorMessage });
  }
  
  /*get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    console.log('get...');
    return super.get(`${this.API_ENDPOINT}/${url}`, options).catch(res => {
      // do something
      console.log("Error", res);
      return Observable.throw(res);
    });
  }*/
}
