import { WorkflowModule } from './workflow/workflow.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { EcmsFeatureModule } from './common/ecms-feature/ecms-feature.module';
import { OrderManagementModule } from './order-management/order-management.module';
import { PackagingModule } from './packaging/packaging.module';
import { IngestionModule } from './ingestion/ingestion.module';
import { ProfileModule } from './profile/profile.module';
//import { ProgressBar, ProgressBarModule } from "angular2-progressbar";
import { HttpService, ToastService } from './ecmsservice.service'
import { ToastyModule } from 'ng2-toasty';
import { AppService } from './app-service.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


export function httpServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions, toast: ToastService) {
  return new HttpService(backend, defaultOptions, toast);
}


@NgModule({
  declarations: [
    AppComponent
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    EcmsFeatureModule,
    OrderManagementModule,
    PackagingModule,
    IngestionModule,
    ProfileModule,
    WorkflowModule,
    ToastyModule.forRoot(),
    NgbModule.forRoot()
  ],
  providers: [AppService, {
    provide: HttpService,
    useFactory: httpServiceFactory,
    deps: [XHRBackend, RequestOptions, ToastService]
  }, ToastService],
  bootstrap: [AppComponent]
 
})
export class AppModule { }
