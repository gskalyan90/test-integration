import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterOrder'
})
export class FilterOrderPipe implements PipeTransform {

  transform(value: any, name: string, startDate: number, endDate: number, description: string, status: string): any {
    name = name ? name.toLowerCase() : "";
    status = status ? status.toLowerCase() : "";
    description = description ? description.toLowerCase() : "";
    return value && value.filter((value) => {
      let orderName = value.orderName ? value.orderName.toLowerCase():"";
      let orderStatus = value.orderStatus ? value.orderStatus.toLowerCase():"";
      let orderDescription = value.orderDescription ? value.orderDescription.toLowerCase():"";
      if (orderName.indexOf(name) >= 0 &&
        orderStatus.indexOf(status) >= 0 &&
        orderDescription.indexOf(description) >= 0
      ) {
        if (startDate && endDate) {
          if ((new Date(value.orderStartDate).toLocaleDateString()) != (new Date(startDate).toLocaleDateString()) ||
            (new Date(value.orderEndDate).toLocaleDateString()) != (new Date(endDate).toLocaleDateString())) {
            return false;
          }
        } else if (startDate) {
          if ((new Date(value.orderStartDate).toLocaleDateString()) != (new Date(startDate).toLocaleDateString()))
            return false;
        } else if (endDate) {
          if ((new Date(value.orderEndDate).toLocaleDateString()) != (new Date(endDate).toLocaleDateString()))
            return false;
        }
        return true
      }
      return false;
    });
  }
}
