import { Component, Input } from '@angular/core';
import { IOrder, ITitle } from '../../../common/interfaces';

@Component({
  selector: 'view-title',
  templateUrl: './view-title.html',
  styleUrls: ['./view-title.scss']
})
export class ViewTitle {
  @Input() order: IOrder;

  configurations: ITitle[];

  ngOnInit() {
    console.log(this.order);
  }

  ngOnChanges() {
    if (this.order && this.order.configureDTOSet) {      
      const config = this.order.configureDTOSet.map(config => {
        if(config && config.titleList){
          config.titleList.forEach(x=>{
            x.configurationId = config.configurationId;
            x.configurationName = config.configurationName;
          })
          return config.titleList;
        }
        return [];
      })
      if(config.length){
        this.configurations = config.reduce((a, b) => {
          return a.concat(b);
        })
      }
    }
  }

}