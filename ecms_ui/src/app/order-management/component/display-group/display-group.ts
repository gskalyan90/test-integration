import { Component, OnInit, Input } from '@angular/core';
import {IOrder} from '../../../common/interfaces';
@Component({
  selector: 'display-group',
  templateUrl: './display-group.html',
  styleUrls: ['./display-group.scss']
})
export class DisplayGroup {
  @Input() order:IOrder;

  constructor(){
  }
}
