
import { Component, ElementRef, Pipe, PipeTransform, Injectable, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { IConfigureDTOSet, IListItem, IMultiSelectOption, IOrder, ITitle } from '../../../common/interfaces';

@Component({
	selector: 'add-title',
	templateUrl: './add-title.html',
	styleUrls: ['./add-title.scss']
})
export class AddTitle {
	_order: IOrder;

	titles: ITitle[];
	searchTitleName: string;

	configurationSelected: IConfigureDTOSet

	@Output() orderChange = new EventEmitter();

	@Input()
	get order(): IOrder {
		return this._order;
	}

	set order(val: IOrder) {
		this._order = val;
	}

	priorities: any[] = [{ value: "High", label: "High" }, { value: "Medium", label: "Medium" }, { value: "Low", label: "Low" }];

	destinations: any[] = [{ value: "DSU", label: "DSU" },
	{ value: "DSUC", label: "DSUC" },
	{ value: "AVCD", label: "AVCD" },
	{ value: "SVDU", label: "SVDU" },
	{ value: "SVDU5", label: "SVDU5" },
	{ value: "TPMU2", label: "TPMU2" },
	{ value: "TPMU", label: "TPMU" },
	{ value: "OVH", label: "OVH" }];

	years: any[] = [{ value: "January", label: "January" },
	{ value: "February", label: "February" },
	{ value: "March", label: "March" },
	{ value: "April", label: "April" },
	{ value: "May", label: "May" },
	{ value: "June", label: "June" },
	{ value: "July", label: "July" },
	{ value: "August", label: "August" },
	{ value: "September", label: "September" },
	{ value: "Octuber", label: "Octuber" },
	{ value: "November", label: "November" },
	{ value: "December", label: "December" }];


	configurationChanged() {
		this.configurationSelected.titleList.forEach(obj => {
			obj['destinationList'] = [];
		})
	}

	onDestinationChange = function (data, object) {
		if (object.selected) {
			this.configurationSelected.titleList.filter(obj => {
				return obj.selected
			}).forEach(x => {
				x.destinationList = data;
			})
		}

	}
	onPriorityChange = function (event, object) {
		if (object.selected) {
			this.configurationSelected.titleList.filter(obj => {
				return obj.selected
			}).forEach(x => {
				x.priority = object.priority;
			})
		}
	}
	onYearChange = function (event, object) {
		if (object.selected) {
			this.configurationSelected.titleList.filter(obj => {
				return obj.selected
			}).forEach(x => {
				x.activationMonth = object.activationMonth;
			})
		}
	}


}
