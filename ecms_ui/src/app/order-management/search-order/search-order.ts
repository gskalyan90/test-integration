import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { OrderManagementService } from '../order-management.service';
import { IOrder } from '../../common/interfaces';
import { AppService } from '../../app-service.service';


@Component({
	selector: 'searchOrder',
	templateUrl: './search-order.html',
	styleUrls: ['search-order.scss']
})
export class searchOrderCmp implements OnInit {
	sortField = "";
	orders: any[] = [];
	filter: any = {};

	constructor(private orderService: OrderManagementService, private appService: AppService) { }

	ngOnInit() {
		if (this.appService.getAirline()) {
			this.orderService.getOrders(this.appService.getAirline().airlineInternalId).subscribe(res => this.orders = res);
		}
	}

	handlerSearch(fields) {
		//this.filter = fields;
		//debugger;
		this.orderService.searchOrder(fields as IOrder, this.appService.getAirline().airlineInternalId).subscribe(res=>this.orders = res);
	}

	changeSortField(field) {
		if (this.sortField.indexOf(field) == -1) {
			this.sortField = field;
		}else{
			if(this.sortField.indexOf('!')==-1)
				this.sortField = '!'+field;
			else
				this.sortField = field 
		}
	}
}
