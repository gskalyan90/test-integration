import { NgModule } from '@angular/core';
import { CanDeactivate, RouterModule, Routes } from '@angular/router';
import { OrderManagement, Order } from './order-management.component';
import { CreateOrder } from './create-order/create-order';
import { ViewOrderComponent } from './view-order/view-order';
import { searchOrderCmp } from './search-order/search-order';


export class ConfirmDeactivateGuard implements CanDeactivate<CreateOrder>{
	canDeactivate(target: CreateOrder):any {
		if (target.hasChanges()) {
			return target.openConfirmDialog().then(res=>{
				return true;
			}).catch(rej=>{
				return false;
			});			
		}
		return true;
	}
}


const routes: Routes = [
	{
		path: 'order-management',
		component: Order,
		children: [{
			path: '',
			component: OrderManagement,
			children: [{
				path: '',
				redirectTo: 'order',
				pathMatch: 'full'
			}, {
				path: 'search',
				component: searchOrderCmp,
				data: {
					breadcrumb: "Search"
				}
			}, {
				path: 'order',
				component: CreateOrder,
				data: {
					breadcrumb: "Create"
				},
				canDeactivate: [ConfirmDeactivateGuard]
			}, {
				path: 'edit/:orderId',
				component: CreateOrder,
				data: {
					breadcrumb: "Edit"
				},
				canDeactivate: [ConfirmDeactivateGuard]
			}, {
				path: 'copy/:orderId',
				component: CreateOrder,
				data: {
					breadcrumb: "Copy Order"
				},
				canDeactivate: [ConfirmDeactivateGuard]
			}


			],
			data: {
				breadcrumb: "Management"
			}
		}, {
			path: 'view/:orderId',
			component: ViewOrderComponent,
			data: {
				breadcrumb: "View Order"
			}
		}


		],
		data: {
			breadcrumb: "Order"
		}
	},

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: [ConfirmDeactivateGuard]
})
export class OrderManagementRoutingModule { }




