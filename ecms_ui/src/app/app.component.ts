import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { ToastService } from './ecmsservice.service';
import { AppService } from './app-service.service';
import { Airline } from './common/interfaces';
import { Router } from '@angular/router';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
	showNotification: boolean;
	isCloudActive: boolean;
	airlineList: Airline[];
	selectedAirline: Airline;
	isClosable:boolean = false;
	airlineModel:string = "default";

	@ViewChild("airlineModal") airlineModal: ElementRef;

	constructor(private modalService: NgbModal,  private toasty: ToastyService, private toastyConfig: ToastyConfig, private toast: ToastService, private appService: AppService, private _router: Router) {
		this.showNotification = false;
		this.isCloudActive = false;		
		this.toastyConfig.theme = 'default';
		this.toastyConfig.position = "top-center";
	}
 
	ngOnInit() {
		if (!this.appService.getAirline()) {			
			this.appService.getAirlines().subscribe(res => {
				this.airlineList = res
			});
			this.openAirlineModal();
			this.isClosable = false;
			
		}
		else{
			this.isClosable = true;
			this.airlineModel = this.appService.getAirline().airlineInternalId;
			this.selectedAirline = this.appService.getAirline();
		}
		
		this.toast.setToastInstance(this.toasty);
	}

	openAirlineModal(){
		const options:NgbModalOptions = {
			backdrop:"static"
		};
		this.modalService.open(this.airlineModal, options).result.then((result) => {
			//this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			//this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});		
	}

	toggleNotification() {
		//this.showNotification = !this.showNotification;
	}

	changeAirline() {
		this.appService.getAirlines().subscribe(res => {
			this.airlineList = res
		});
		this.openAirlineModal();
	}
	onAirlineSelect() {
		const index = this.airlineList.findIndex(x=>x.airlineInternalId===this.airlineModel);
	    this.appService.setAirline(this.airlineList[index]);
		this.selectedAirline = this.appService.getAirline();		
		this._router.navigate(['order-management/order']);
		this.isClosable = true;
	}
}
