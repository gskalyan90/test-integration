import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagingRoutingModule } from './packaging-routing.module';
import {EcmsFeatureModule} from '../common/ecms-feature/ecms-feature.module';
import { PackagingComponent, PackagingRoot } from './packaging.component';
import { ViewPackagingComponent} from './view-packaging/view-packaging';
import { ViewPackageInfoComponent} from './view-packaging/info/info.component';
import { ViewPackageDefComponent} from './view-packaging/definitions/definitions.component';
import { ViewPackageContentListComponent} from './view-packaging/content-list/content-list.component';

@NgModule({
  imports: [
    CommonModule,
    PackagingRoutingModule,
    EcmsFeatureModule
  ],
  declarations: [PackagingRoot,
  					PackagingComponent,
  					ViewPackagingComponent,
  					ViewPackageInfoComponent,
  					ViewPackageDefComponent,
  					ViewPackageContentListComponent ]
})
export class PackagingModule { }
