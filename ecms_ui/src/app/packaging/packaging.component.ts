import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-packaging',
  templateUrl: './packaging.component.html',
  styleUrls: ['./packaging.component.scss']
})
export class PackagingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}


@Component({
  selector: 'packaging-root',
  template: '<router-outlet></router-outlet>'
})
export class PackagingRoot implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}