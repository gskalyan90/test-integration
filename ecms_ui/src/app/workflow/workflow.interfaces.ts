export interface IWFMetadata {
    createTime: number;
    description: string;
    name: string;
    schemaVersion: 2;
    tasks: {
        name: string;
        startDelay: number;
        taskReferenceName: string;
        type: string;
    }[];
    version: number;
    outputParameters?: any;
}

export interface ITask {
    createTime: number;
    description: string;
    name: string;
    responseTimeoutSeconds: number;
    retryCount: number;
    retryDelaySeconds: number;
    retryLogic: string;
    timeoutPolicy: string;
    timeoutSeconds: number
}