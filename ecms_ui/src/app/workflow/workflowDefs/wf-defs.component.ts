import { Component, OnInit } from '@angular/core';
import { WFService } from '../workflow.service';


@Component({
    templateUrl: './wf-defs.component.html'
})


export class WFDefs {
    wfDefsList: any[];
    

    constructor(private _wfService: WFService) {
       _wfService.getWFMetadata().subscribe(res => {
            this.wfDefsList = res;

             console.log(this.wfDefsList);

        });
       

    }

}