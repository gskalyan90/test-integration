import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { HttpService } from '../ecmsservice.service';
import { Injectable } from '@angular/core';

@Injectable()
export class WFService {
    constructor(private http: HttpService) { }

    getWFMetadata(): Observable<any> {
        return this.http.get('api/metadata/workflow').map((res: Response) => res.json())
    }

    searchWorkflow(query): Observable<any> {
        let freeText = [];
        if (query.freeText) {
            freeText.push(query.freeText);
        } else {
            freeText.push('*');
        }

        let h = '-1';
        if (query.h) {
            h = query.h;
        }
        if (h != '-1') {
            freeText.push('startTime:[now-' + h + 'h TO now]');
        }
        let q = "";
        if (query.workflowTypes.length && query.status.length) {
            q = 'workflowType IN (' + query.workflowTypes.join() + ') AND status IN (' + query.status.join() + ')';
        } else if (query.status.length) {
            q = 'status IN (' + query.status.join() + ')'
        } else if (query.workflowTypes.length) {
            q = 'workflowType IN (' + query.workflowTypes.join() + ')'
        }
        return this.http.get('api/workflow/search?start=0&size=100&sort=startTime:DESC&freeText=' + freeText.join(' AND ') + '&query=' + q).map(res => res.json())
    }

    getWorkflowDetail(id: string): Observable<any> {
        return this.http.get('api/workflow/' + id).map(res => res.json())
    }

    getWFMetaDefinition(type: string, version: number): Observable<any> {
        return this.http.get('api/metadata/workflow/' + type + '?version=' + version).map(res=>res.json())
    }
    
    getWFTaskMetadata():Observable<any>{
        return this.http.get('api/metadata/taskdefs').map((res:Response)=>res.json())
    }
   
}