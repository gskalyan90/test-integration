import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterTask'
})
export class FilterTaskPipe implements PipeTransform {

  transform(value: any, name: string, timeoutPolicy: string,timeoutSecond: number, retryCount: number, retryLogic: string): any {
    name = name ? name.toLowerCase() : "";
    retryLogic = retryLogic ? retryLogic.toLowerCase() : "";
    timeoutPolicy = timeoutPolicy ? timeoutPolicy.toLowerCase() : "";
    return value && value.filter((value) => {
      let taskName = value.taskName ? value.taskName.toLowerCase():"";
      let timeoutPolicy = value.timeoutPolicy ? value.timeoutPolicy.toLowerCase():"";
      let orderDescription = value.orderDescription ? value.orderDescription.toLowerCase():"";
      if (orderName.indexOf(name) >= 0 &&
        orderStatus.indexOf(status) >= 0 &&
        orderDescription.indexOf(description) >= 0
      ) {
        if (startDate && endDate) {
          if ((new Date(value.orderStartDate).toLocaleDateString()) != (new Date(startDate).toLocaleDateString()) ||
            (new Date(value.orderEndDate).toLocaleDateString()) != (new Date(endDate).toLocaleDateString())) {
            return false;
          }
        } else if (startDate) {
          if ((new Date(value.orderStartDate).toLocaleDateString()) != (new Date(startDate).toLocaleDateString()))
            return false;
        } else if (endDate) {
          if ((new Date(value.orderEndDate).toLocaleDateString()) != (new Date(endDate).toLocaleDateString()))
            return false;
        }
        return true
      }
      return false;
    });
  }
}
