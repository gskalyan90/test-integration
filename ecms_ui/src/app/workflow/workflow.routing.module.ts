import { RouterOutletComponent } from '../common/ecms-feature/ecms-feature.module';
import { WFDetail } from './wf-detail/workflow-detail.component';
import { Workflows } from './workflows/workflows.component';
import { WFDefs } from './workflowDefs/wf-defs.component';
import { Tasks } from './tasks/tasks.component';
import { WorkflowComponent, WorkflowHome } from './workflow.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const detailRoute: Routes = [
    { path: "", redirectTo: "wfe", pathMatch: "full" },
    { path: "wfe", component: Workflows },
    { path: "detail/:id", component: WFDetail }
]

const route: Routes = [
    {
        path: "workflow", component: WorkflowComponent, children: [
            { path: "", redirectTo: "home", pathMatch: "full" },
            { path: "home", component: WorkflowHome },
            {
                path: "all", component: RouterOutletComponent, data: {
                    breadcrumb: "All"
                }, children: detailRoute
            },
            {
                path: "running", component: RouterOutletComponent, data: {
                    breadcrumb: "Running"
                }, children: detailRoute
            },
            {
                path: "failed", component: RouterOutletComponent, data: {
                    breadcrumb: "Failed"
                }, children: detailRoute
            },
            {
                path: "timed-out", component: RouterOutletComponent, data: {
                    breadcrumb: "Timed Out"
                }, children: detailRoute
            },
            {
                path: "terminated", component: RouterOutletComponent, data: {
                    breadcrumb: "Terminated"
                }, children: detailRoute
            },
            {
                path: "completed", component: RouterOutletComponent, data: {
                    breadcrumb: "Completed"
                }, children: detailRoute
            },
            {
                path: "wf-defs", component: WFDefs, data: {
                    breadcrumb: "Workflow Defs"
                }
            },
            {
                path: "tasks", component: Tasks, data: {
                    breadcrumb: "Tasks"
                }
            }
        ], data: {
            breadcrumb: "Workflow"
        }
    }
]

@NgModule({
    imports: [RouterModule.forChild(route)],
    exports: [RouterModule]
})
export class WorkflowRoute { }