import {NgbPopover} from '@ng-bootstrap/ng-bootstrap';
import { Component, ViewChild } from '@angular/core';
import { WFService } from '../workflow.service';


@Component({
    templateUrl:'./tasks.component.html',
   styleUrls:['./task.component.scss']    
})
export class Tasks{

	wfTaskList: any[];
    greeting:string = "Shakir";
    selectedTask:any;
    customPopover:any = {'top':'100px','left':'100px'};
    prePopupIndex:number = -1;

    constructor(private _wfService: WFService) {
       

    }

    ngOnInit(){
    	this._wfService.getWFTaskMetadata().subscribe(res => {
            this.wfTaskList = res;
             this.selectedTask = this.wfTaskList[0];

        });
       
    }

    @ViewChild('p') public popover: NgbPopover;
   
  public changeGreeting(greeting: any,index:number,e:any): void {

  	this.customPopover =  {'top': e.pageY+'px','left': e.pageX+30 +'px'}
  	this.selectedTask = this.wfTaskList[index];
  	
    const isOpen = this.popover.isOpen();
    if(isOpen && this.prePopupIndex == index){
      this.popover.close();	
    }
    else{
    	this.popover.close();	
    	this.popover.open(this.selectedTask.name);
    }
     this.prePopupIndex = index;
    
   
  }
}

