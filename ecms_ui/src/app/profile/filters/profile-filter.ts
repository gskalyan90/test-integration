import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterBundle'
})
export class FilterBundlePipe implements PipeTransform {

  transform(value: any, bundleName: string): any {
    bundleName = bundleName ? bundleName.toLowerCase() : "";
    return value && value.filter((value) => {
      if (value.bundleName.toLowerCase().indexOf(bundleName) >= 0) {
        return true
      }
      return false;
    });

  }
}

@Pipe({
  name: 'filterRule'
})
export class FilterRulePipe implements PipeTransform {

  transform(value: any, ruleName: string): any {
    ruleName = ruleName ? ruleName.toLowerCase() : "";
    return value && value.filter((value) => {
      if (value.ruleName.toLowerCase().indexOf(ruleName) >= 0) {
        return true
      }
      return false;
    });

  }
}

@Pipe({
  name: 'filterConfiguration'
})
export class FilterConfigurationPipe implements PipeTransform {

  transform(value: any, configurationName: string): any {
    configurationName = configurationName ? configurationName.toLowerCase() : "";
    return value && value.filter((value) => {
      if (value.configurationName.toLowerCase().indexOf(configurationName) >= 0) {
        return true
      }
      return false;
    });

  }
}

@Pipe({
  name: 'FilterContentType'
})
export class FilterContentTypePipe implements PipeTransform {

  transform(value: any, contentTypeName: string): any {
    contentTypeName = contentTypeName ? contentTypeName.toLowerCase() : "";
    return value && value.filter((value) => {
      if (value.contentTypeName.toLowerCase().indexOf(contentTypeName) >= 0) {
        return true
      }
      return false;
    });

  }
}


