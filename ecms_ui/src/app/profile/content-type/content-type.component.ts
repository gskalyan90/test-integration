import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'content-type',
  templateUrl: './content-type.component.html',
  styleUrls: ['content-type.component.scss']
})
export class ContentType {

contentTypeList:any[];
filterContentType:any;
   constructor(private _router: Router,private _profileService:ProfileService)
  {
  	_profileService.getContentTypesList().subscribe(res=>{
      this.contentTypeList = res;

     // console.log(this.contentTypeList);
    });
  }
  ngOnInit() {
      
  }
  
  viewContent = function(){
   this._router.navigate(['profile/content']);
  }
  createContent = function()
  {
  	this._router.navigate(['profile/create-content']);
  }
  deleteContentType = function(index)
  {
  
    let contentTypeId = this.contentTypeList[index].contentTypeInternalId;    
      this._profileService.deleteContentType(contentTypeId).subscribe(res => {
          console.log(this.contentTypeList.splice(index,1));
          this.contentTypeList.splice(index,1);
         window.location.reload();
      });
  }
  editContentType = function(index)
  {
    let contentTypeId = this.contentTypeList[index].contentTypeInternalId;    
    this._router.navigate(['profile/content-type/view-content-type',contentTypeId]);
  }
}

