import {Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {Validators,FormGroup,FormArray,FormBuilder} from '@angular/forms';
import {ProfileService} from '../../profile.service';
import {IContentTypeForProfile,IConetntTypeFields} from '../../../common/interfaces';

@Component({
    selector: 'edit-contentType',
    templateUrl: './edit-content-type.html',
    styleUrls: ['./edit-content-type.scss']
})

export class EditContentType implements OnInit {

    contentTypeList:IContentTypeForProfile = {} as IContentTypeForProfile;// = {contentTypeName:"",fields:[this.contentTypeFields()]};

    /*contentTypeFields():IConetntTypeFields
    {
        return {displayName:"",fieldId:"",fieldType:"",selectionListName:""}
    }*/
   

    fieldTypes:string[];
    private subscribe:any;
    contentTypeId:any;

    constructor(private _router: Router, private profileService: ProfileService,private activatedRoute:ActivatedRoute) { }

    ngOnInit() {
        this.profileService.getFieldTypes().subscribe(res=>{
            this.fieldTypes = res;
        });
         this.subscribe = this.activatedRoute.params.subscribe(params => {          
           this.contentTypeId = params['contentTypeId'];       
           if(this.contentTypeId){
               this.profileService.getGenericContentType( this.contentTypeId).subscribe(res=>{               
                    this.contentTypeList  = res;                   
                });             
           } 
        });    
       // this.contentTypeList.fields = this.contentTypeList.fields || [];
       // this.contentTypeList.fields.push({} as IConetntTypeFields); 
     }

    addBaseFields = function() {          
        this.contentTypeList.fields.push({} as IConetntTypeFields);
    }

    deleteBaseFields = function(index) {
         this.contentTypeList.fields.splice(index,1);
    }
    submitContentType =function(){
        console.log(this.contentTypeList);
        // this.profileService.addGenericContentType(this.contentTypeList).subscribe(response=>{

            // this._router.navigate(['profile/content-type/content-type-list'], { relativeTo: this.route });
       // });
    }
    goBack = function(){
        this._router.navigate(['profile/content-type/content-type-list'], { relativeTo: this.route });
    }

}