import { ServerGroup } from './server-group/server-group.component';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import {ProfileService} from '../../profile.service';
import { AppService } from '../../../app-service.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'create-configuration',
  templateUrl: './create-configuration.html',
  styleUrls: ['create-configuration.scss']
})
export class CreateConfiguration {
  
lopaOptions: any[] = [];
lruOptions: any[] = [];
lruTypeOptions: any[] = [];
contentTypeOptions: any[] = [];
ruleOptions: any[] = [];
interfacelanguageOptions: any[] = [];

configForm:FormGroup;

submitlabel:string;

constructor(private _router:Router,private route: ActivatedRoute, private _fb: FormBuilder, private profileService:ProfileService, private appService:AppService){
    this.initFormConfig();

    this.submitlabel = this.route.snapshot.params["configurationId"] ? "Submit" : "Create Configuration";
}

initFormConfig(){

  // fetching lopaOptions
  this.profileService.getLopas().subscribe(res => {
      //console.log("lopas loaded: ", res);
    res.forEach(element => {
      this.lopaOptions.push({value:element.lopaInternalId, label:element.lopaName})
    });
  });

  // fetching lruOptions
  this.profileService.getLrus().subscribe(res => {
      //console.log("LRUs loaded: ", res);
    res.forEach(element => {
      this.lruOptions.push({value:element.lruInternalId, label:element.lruName})
    });
  });

  // fetching lruTypesOptions
  this.profileService.getLruTypes().subscribe(res => {
      //console.log("LruTypes loaded: ", res);
    res.forEach(element => {
      this.lruTypeOptions.push({value:element.lruTypeInternalId, label:element.lruTypeName})
    });
  });
  // fetching contentTypeOptions
  this.profileService.getContentTypes().subscribe(res => {
      //console.log("ContentTypes loaded: ", res);
    res.forEach(element => {
      this.contentTypeOptions.push({value:element.contentTypeId, label:element.contentTypeName})
    });
  });
  
  // fetching ruleOptions
  if (this.appService.getAirline()) {
    this.profileService.getRules(this.appService.getAirline().airlineInternalId)
    .subscribe(res => {
      //console.log("Rules loaded: ", res);
      res.forEach(element => {
        this.ruleOptions.push({value:element.ruleId, label:element.ruleName})
      });
    });
  }

  // fetching interfacelanguageOptions
  this.profileService.getPaxguilanguages().subscribe(res => {
    //console.log("Paxguilanguages loaded: ", res);
    res.forEach(element => {
      this.interfacelanguageOptions.push({value:element.paxguiLanguageId, label:element.paxguiLanguageName})
    });
  });
}

ngOnInit() {
    this.configForm = this._fb.group({
        configurationId:'',
        configurationName:['', Validators.required],
        configurationStatus:['', Validators.required],
        packagingType:['', Validators.required],
        platform:['', Validators.required],
        version:['', Validators.required],
        interfaceLanguagesId:[],
        lopaIdList:[],
        lrusId:[],
        lruTypesId:[],
        contentTypeIdList:[],
        rulesId:[],
        titleList: [],
        serverGroupList: this._fb.array([])
    });

    if (this.route.snapshot.params["configurationId"]) {
      this.profileService.getConfigurationById(this.route.snapshot.params["configurationId"]).subscribe(res => {
        console.log("Loaded for Edit", res);
        this.configForm.setValue(res);
      });
    }else{
      this.addServerGroup();
    }
}

initServerGroup() {
        // initialize our address
        return this._fb.group({
            name: ['', Validators.required],
            lrus: [],
            state:{}
        });
    }

addServerGroup() {
    // add serverGroup to the list
    const control = <FormArray>this.configForm.controls['serverGroupList'];
    control.push(this.initServerGroup());
}

removeServerGroup(index:number){
    // remove serverGroup at the index from the list
    const control = <FormArray>this.configForm.controls['serverGroupList'];
    control.removeAt(index);
}

handleFormSubmit(){
  
    let data = this.configForm.value;
    let configurationId = this.route.snapshot.params["configurationId"];

    console.log("handleFormSubmit:", data);

    if (configurationId) {
      this.profileService.updateConfiguration(configurationId, data).subscribe(res=>{
        this._router.navigate(['profile/configuration/configuration-list'], { relativeTo: this.route });
      });
    }else{
      this.profileService.createConfiguration(data).subscribe(res=>{
       this._router.navigate(['profile/configuration/configuration-list'], { relativeTo: this.route });
      });
    }


}

}

