import { ProfileService } from '../../../profile.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    moduleId: module.id,
  selector: 'server-group',
  templateUrl: './server-group.component.html',
  styleUrls: ['server-group.component.scss']
})
export class ServerGroup {
    
    @Input('data')
    public serverGroupForm: FormGroup;

    //_data:any = {name:"", lrus:[]};
    
    lruOptions: any[] = []


    @Output() dataChange = new EventEmitter();
    @Output() deleteClick = new EventEmitter();

    /*@Input() 
    get data():any{
        return this._data;
    }
    set data(val){
        this._data = val;
        this.dataChange.emit(this._data);
    };*/

    constructor(private profileService:ProfileService){
        // fetching lruOptions
        this.profileService.getLrus().subscribe(res => {
            console.log("LRUs loaded: ", res);
            res.forEach(element => {
            this.lruOptions.push({value:element.lruInternalId, label:element.lruName})
            });
        });
    }

    deletePress(){
        this.deleteClick.emit();
    }

  
}

