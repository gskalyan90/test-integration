import { Component, OnInit,ElementRef, OnDestroy} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ProfileService } from '../../profile.service';
import { Observable } from 'rxjs/Rx';
import { IRule,IRuleContentTypeMappingDTOSet,IContentTypeFieldSet,IValidatorDTO} from '../../../common/interfaces';
import { AppService } from '../../../app-service.service';

@Component({
  selector: 'edit-rule',
  templateUrl: './edit-rule.html',
  styleUrls: ['./edit-rule.scss']
})
export class EditRule implements OnInit {  
  rule:IRule= {ruleContentTypeMappingDTO:this.ruleContentTypeMappings(),ruleId:"",ruleName:""};
  ruleContentTypeMappings():IRuleContentTypeMappingDTOSet{
    return {contentTypeId:"",contentTypeName:"",contentTypeField:[this.contentTypeFields()]};

  }
  contentTypeFields():IContentTypeFieldSet{
    return { fieldName:"",validatorDTO:this.IValidatorDTOs()};
  }
  IValidatorDTOs():IValidatorDTO{
    return { maxChar:"",required:false,translatable:false};
  }

  ruleId:string
  //responseDTOSet:any;
  resposeDTOFileds:any;
  private subscribe:any;
  contentTypes:any;
  allFields:any;

  constructor(private _router: Router,
              private activatedRoute:ActivatedRoute, 
              private profileService:ProfileService, 
              private appService:AppService) { }

  ngOnInit()
  {
    this.subscribe = this.activatedRoute.params.subscribe(params => {          
       this.ruleId = params['ruleId'];
       
       if(this.ruleId)
       {

          this.profileService.getRule(this.ruleId, this.appService.getAirline().airlineInternalId).subscribe(response=>{        
            this.rule = response;        

          }); 
       } 
    });
     this.profileService.getContentTypesList().subscribe(res=>{
       debugger;
        this.contentTypes = res;
         this.setcontentType();
      }); 
  }
 

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  } 
  goBack()
  {
    this._router.navigate(['profile/rule']);
  }
  setcontentType = function()
  {       
    debugger;
    this.isVisibile = true;    
    this.profileService.getContentType(this.rule.ruleContentTypeMappingDTO.contentTypeId).subscribe(res=>{
    this.allFields = res;
    const index = this.contentTypes.findIndex(x => x.contentTypeInternalId == this.rule.ruleContentTypeMappingDTO.contentTypeId);
    this.rule.ruleContentTypeMappingDTO.contentTypeName = this.contentTypes[index].contentTypeName;        
  });
   
  } 
  editBundle = function()  {
      
      this.subscribe = this.activatedRoute.params.subscribe(params => 
      {          
        this.ruleId = params['ruleId'];       
        if(this.ruleId)
        {
          console.log(this.rule)
          this.profileService.updateRule(this.rule, this.appService.getAirline().airlineInternalId).subscribe(response => {
            console.log("done............");
            this._router.navigate(['profile/rule/rule-list'], { relativeTo: this.route });
          });
        }
    });
  } 
}


