import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from '../../profile.service';
import { Observable } from 'rxjs/Rx';
import { IContentTypeListForRule,IContentType,IRule, IConfigureDTOSet, IRuleContentTypeMappingDTOSet } from '../../../common/interfaces';
import { OrderManagementService } from '../../../order-management/order-management.service';
import { AppService } from '../../../app-service.service';


@Component({
  selector: 'create-rule',
  templateUrl: './create-rule.html',
  styleUrls: ['./create-rule.scss']
})
export class CreateRule implements OnInit{  
  private subscribe :any;  
  rule:IRule;
  contentTypes:IContentTypeListForRule[];
  allFields:IContentType[];
  isVisibile:boolean= false; 
  ruleName:string;
  constructor(private _router: Router, 
              private profileService:ProfileService, 
              private orderService:OrderManagementService,
              private appService:AppService){
    this.rule = {} as IRule;
    this.rule.ruleContentTypeMappingDTO = {} as IRuleContentTypeMappingDTOSet;
  }

  ngOnInit(){ 
         this.profileService.getContentTypesList().subscribe(res=>{
         this.contentTypes = res;
      });         
  }

  goBack = function(){
    this._router.navigate(['profile/rule']);
  }

  setcontentType = function(evt)
  {     
    this.isVisibile = true;    
    this.profileService.getContentType(this.rule.ruleContentTypeMappingDTO.contentTypeId).subscribe(res=>{
     
      this.allFields = res;  
      const index = this.contentTypes.findIndex(x => x.contentTypeInternalId == this.rule.ruleContentTypeMappingDTO.contentTypeId);
      this.rule.ruleContentTypeMappingDTO.contentTypeName = this.contentTypes[index].contentTypeName;      
    });
   
  }
  
  submitRule = function(){   
    debugger;
   this.profileService.addRule(this.setUpRuleDTO(this.rule), this.appService.getAirline().airlineInternalId).subscribe(response=>{
        
          this._router.navigate(['profile/rule/rule-list'], { relativeTo: this.route });
    });
  }
  setUpRuleDTO = function(rule:IRule):any
  {
    let _rule:any = {};
    _rule = rule;
    _rule.ruleName = this.ruleName;
    _rule.ruleContentTypeMappingDTO.contentTypeField = [];     
     if(this.allFields){
      this.allFields.forEach(con=>{
         _rule.ruleContentTypeMappingDTO.contentTypeField.push({fieldName:con.fieldName,validatorDTO:{maxChar:con.maxChar,required:con.required,translatable:con.translatable}});  
      })  
    }
    return _rule;
  }
  
  
}

