import { RouterOutletComponent } from '../common/ecms-feature/ecms-feature.module';
import { CreateConfiguration } from './configuration/create-configuration/create-configuration';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Profile } from './profile.component';
import { ContentType } from './content-type/content-type.component';
import { ViewContentType } from './content-type/view-content-type/view-content-type';
import { EditContentType } from './content-type/edit-content-type/edit-content-type';
import { CreateContentType } from './content-type/create-content/create-content';
import { Configuration } from './configuration/configuration.component';
import { Rule } from './rule/rule.component';
import { CreateRule } from './rule/create-rule/create-rule';
import { ViewRule } from './rule/view-rule/view-rule';
import { Bundle, BundleManagement, } from './bundle-management/bundle-management';
import { CreateBundle } from './bundle-management/create-bundle/create-bundle';
import { ViewBundle } from './bundle-management/view-bundle/view-bundle';
import { EditBundle } from './bundle-management/edit-bundle/edit-bundle';
import { EditRule } from './rule/edit-rule/edit-rule';


const routes: Routes = [
	{
		path: 'profile', component: Profile, children: [
			{
				path: '', redirectTo: 'content-type', pathMatch: 'full' },
	
			{
				path: 'content-type', component: RouterOutletComponent,
				children:[
					{path:'', redirectTo:'content-type-list', pathMatch:'full'},
					{path:'content-type-list', component:ContentType, data: {
						breadcrumb: "List"
					}},
					{path:'create-content', component:CreateContentType, data: {
						breadcrumb: "Create Content"
					}},
					{path:'view-content-type/:contentTypeId', component:ViewContentType, data: {
						breadcrumb: "View Content Type"
					}},
					{path:'edit-content-type/:contentTypeId', component:EditContentType, data: {
						breadcrumb: "Edit Content Type"
					}}
		           ],
				data: {
					breadcrumb: "Content Type"
				}
			},
			{
				path: 'configuration', component: RouterOutletComponent,
				children:[
					{path:'', redirectTo:'configuration-list', pathMatch:'full'},
					{path:'configuration-list', component:Configuration, data: {
						breadcrumb: "List"
					}},
					{path:'create', component: CreateConfiguration, data: {
						breadcrumb: "Create"
					}},
					{path:'edit/:configurationId', component: CreateConfiguration, data: {
						breadcrumb: "Edit"
					}}
				],
				data: {
					breadcrumb: "Configuration"
				}
			},
			{
			path: 'rule', component: RouterOutletComponent,
				children:[
				{path:'', redirectTo:'rule-list', pathMatch:'full'},
				{
					path: 'rule-list', component: Rule, data: {
						breadcrumb: "List"
					}
				},
				{
					path: 'create-rule', component: CreateRule, data: {
						breadcrumb: "Create Rule"
					}
				},
				{
					path: 'edit-rule/:ruleId', component: EditRule, data: {
						breadcrumb: "Edit Rule"
					}
				},
				{
					path: 'view-rule/:ruleId', component: ViewRule, data: {
						breadcrumb: "View Rule"
					}
				}],data: {
					breadcrumb: "Rules"
				}
			},
			{
			path: 'bundle', component: RouterOutletComponent,
				children:[
				{path:'', redirectTo:'bundle-list', pathMatch:'full'},
			{
				path: 'bundle-list', component: Bundle, data: {
					breadcrumb: "List"
				}
			},
			{
				path: 'edit-bundle/:bundleId', component: EditBundle, data: {
					breadcrumb: "Edit Bundle"
				}
				
			},
			{
				path: 'create-bundle', component: CreateBundle, data: {
					breadcrumb: "Create Bundle"
				},
				
			},{
				path: 'view-bundle/:bundleId', component: ViewBundle, data: {
					breadcrumb: "View Bundle"
				}}],data: {
					breadcrumb: "Bundle"
				}}		
	
		],
		data:{
			breadcrumb:"Profile Management"
		}
	}

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class ProfileRoutingModule { }