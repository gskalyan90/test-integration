import { ServerGroup } from './configuration/create-configuration/server-group/server-group.component';
import { CreateConfiguration } from './configuration/create-configuration/create-configuration';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EcmsFeatureModule } from '../common/ecms-feature/ecms-feature.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileService } from './profile.service';
import { Profile } from './profile.component';
import { ContentType } from './content-type/content-type.component';
import { CreateContentType } from './content-type/create-content/create-content';
import { Configuration } from './configuration/configuration.component';
import { Rule } from './rule/rule.component';
import { CreateRule } from './rule/create-rule/create-rule';
import { ViewRule } from './rule/view-rule/view-rule';
import { Bundle, BundleManagement } from './bundle-management/bundle-management';
import { CreateBundle } from './bundle-management/create-bundle/create-bundle';
import { ViewBundle } from './bundle-management/view-bundle/view-bundle';
import { EditBundle } from './bundle-management/edit-bundle/edit-bundle';
import { EditRule } from './rule/edit-rule/edit-rule';
import { ViewContentType } from './content-type/view-content-type/view-content-type';
import { EditContentType } from './content-type/edit-content-type/edit-content-type';
import {FilterBundlePipe,FilterRulePipe,FilterConfigurationPipe,FilterContentTypePipe} from './filters/profile-filter'


@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    EcmsFeatureModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [ProfileService],
  declarations: [CreateContentType,ViewContentType,EditContentType, Profile, ContentType, Configuration, Rule,CreateRule,ViewRule, CreateConfiguration, ServerGroup,Bundle, BundleManagement,CreateBundle,ViewBundle,EditBundle,EditRule,FilterBundlePipe,FilterRulePipe,FilterConfigurationPipe,FilterContentTypePipe]
})
export class ProfileModule { }
