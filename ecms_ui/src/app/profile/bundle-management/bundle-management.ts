import { Component,OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { IBundle,ISeatingClass,ISeatingClassPriceMappings} from '../../common/interfaces';
import { ProfileService } from '../profile.service';
import { Observable } from 'rxjs/Rx';
import { AppService } from '../../app-service.service';


@Component({
	selector: 'bundle-management',
	templateUrl: 'bundle-management.html',
	styleUrls: ['bundle-management.scss']
})
export class Bundle implements OnInit{
	bundles:any;
	searchBundleName:any;	
	constructor(private _router: Router,private profileService: ProfileService, private appService: AppService){}	
	ngOnInit()
	{
	  if (this.appService.getAirline()) {	  	
      	this.profileService.getBundles(this.appService.getAirline().airlineInternalId).subscribe(res => 
      		{      			
      			this.bundles = res;

      		});
       }
	}
	createBundle = function(){
		this._router.navigate(['profile/create-bundle']);
	}
	deleteBundle = function(index){		
		let bundleId = this.bundles[index].bundleId;		
			this.profileService.deleteBundle(bundleId,this.appService.getAirline().airlineInternalId).subscribe(res => {
				//this.bundles = res;  
				this.bundles.splice(index,1);
			});
		
	}
}


@Component({
	template:`<router-outlet></router-outlet>`
})
export class BundleManagement  {}