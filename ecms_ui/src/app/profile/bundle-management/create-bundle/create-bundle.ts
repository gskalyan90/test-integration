import { Component,OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { IBundle,ISeatingClass,ISeatingClassPriceMappings} from '../../../common/interfaces';
import { ProfileService } from '../../profile.service';
import { Observable } from 'rxjs/Rx';
import { AppService } from '../../../app-service.service';

@Component({
	selector: 'create-bundle',
	templateUrl: './create-bundle.html',
	styleUrls: ['./create-bundle.scss']
})
export class CreateBundle implements OnInit{
	seatingClasses:any;
	bundle:IBundle = {seatingClassPriceMappings:[this.initSeatingClassPriceMappings()],bundleId:"",bundleName:""};
	
	initSeatingClassPriceMappings():ISeatingClassPriceMappings{
		return {ppa:"", ppv:"", seatingClassId:""};
	}	
	
	constructor(private _router: Router,private profileService:ProfileService,private appService:AppService){		
		
	}
	ngOnInit()
	{
		this.profileService.getSeatingClass(this.appService.getAirline().airlineInternalId).subscribe(response=>{ 
       	 	this.seatingClasses = response;
        }); 
	}
	addSeatingClassPriceMappings(){
		this.bundle.seatingClassPriceMappings.push(this.initSeatingClassPriceMappings());
	}
	removeSeatingClassPriceMappings = function(index)
	{
		this.bundle.seatingClassPriceMappings.splice(index,1);
	}
	saveBundle = function()
	{
		let airlineId = this.appService.getAirline().airlineInternalId;		
		this.profileService.addBundle(this.bundle, airlineId).subscribe(response=>{			
			this._router.navigate(['profile/bundle/bundle-list'], { relativeTo: this.route });	
		});
	      
	}
}
