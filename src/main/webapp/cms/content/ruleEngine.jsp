<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.psddev.cms.db.Site"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Rule Management</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../css/demo.css">
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.js"></script>
<script src="../../js/ruleListing.js"></script>
<link type="text/css" rel="stylesheet"
	href="/assets/css/less/svod/thalesHCL/landingAirlinePortal.css" />
<script></script>
<%
	ToolPageContext toolPageContext = new ToolPageContext(pageContext);
    Site airline = toolPageContext.getSite();
    String airlineId = null;
    if(airline != null){
    	airlineId = airline.getId().toString();
    }
%>
</head>
<body><%@ page import="com.psddev.cms.tool.ToolPageContext"%>
<%
	ToolPageContext wp = new ToolPageContext(pageContext);
	wp.writeHeader();
%>
<link type="text/css" rel="stylesheet"
	href="/assets/css/less/svod/thalesHCL/landingAirlinePortal.css" />
<body>

<!-- Using this to pass the airlineId to rule.js. Will pass null if the Site is Global -->
<input type = "hidden" id="airlineId" value="<%=airlineId%>">

<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">Rule Management</div>
					<div class="panel-body">
						<form action="">
							<!-- Trigger the modal with a button -->
							<div>
								<ul class="thalesNavBar">
								<li id="createAnOrder"><a href="ruleEngine.jsp">List
											Rules</a></li>
									<li id="createAnOrder"><a href="createRule.jsp">Create
											Rule</a></li>
								</ul>
							</div>

							<!-- Modal Ends -->
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-heading">List of Rules</div>
					<div class="panel-body">
						<div class="dropdown">
							<div id="responseMessage"></div>
							 <br />
							<div class="tableRes" style="display: none">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Field Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody id="dynamicRow">

									</tbody>
								</table>
							</div>
						</div>
						<br />
					</div>
				</div>
			</div>
			<div class="row">
			<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" id="clickModal" data-target="#myModal">Open Modal</button> -->
				<!-- Modal -->
				<div id="myModal" class="modal fade" role="dialog" style="display:none;">
				  <div class="modal-dialog">
				
				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Preview Rule Details</h4>
				      </div>
				      <div class="modal-body" id="modalContent">
				        <p id="ruleName"></p>
				        <div id="tableDataContainer"></div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default listingbtn" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				
				  </div>
				</div>
		</div>
		</div>
</body>
</html>

<%
	wp.writeFooter();
%>