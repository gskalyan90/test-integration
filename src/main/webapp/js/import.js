	$(document).ready(function() {
		var file = $('[name="file"]');
	 

		$('#btnUpload').on('click', function() {
			var filename = $.trim(file.val());

			$.ajax({
				url : 'http://localhost:8080/ECMS_6/apis/file/uploadfiles',
				type : "POST",
				data : new FormData(document.getElementById("fileForm")),
				enctype : 'multipart/form-data',
				processData : false,
				contentType : false
			}).done(function(response) {
			  var divData;
			  var html = "<tr><td>File Name</td><td>Status</td><td>Reason</td><td>Action</td></tr>";
		      $('#myTable').append(html);
		      
			   $.each (response, function (key, value) {
			     divData = value; //gives me the value of div 1
			     var html = "<tr><td>" + divData.fileName + "</td><td>" + divData.status+ "</td><td>" + divData.reason+ "</td><td><button onclick=\"javascript:startImport('"+divData.fileName+"');\">Start Import</button></td></tr>";
			      $('#myTable').append(html);
			   });
			}).fail(function(jqXHR, textStatus) {
				alert('File upload failed ...');
			});
		});
		
	});
	
function startImport(fileName){
	
	var airlineId= $("#airlineId").val();
	var configurationId= $("#configurationId").val();
	
	var json = { "airlineId": airlineId,"configurationId" : configurationId,"fileName" : fileName};
	
	console.log("json = ",json);
	$.ajax({
		url : "http://localhost:8080/ECMS_6/apis/metadata/importcontenttype/",
		type : "POST",
		contentType : "application/json",
		dataType : "json",
		processData : false,
		data : JSON.stringify(json),
		success : function (result) {
			console.log("Added success", result);
			showIngestionReport(result);
		},
		error : function (error) {
			console.log("error occured", error);
		}
	})
	
}


function showIngestionReport(result){
	var str = JSON.stringify(result);
	output(syntaxHighlight(str));
}

function output(inp) {
    document.body.appendChild(document.createElement('pre')).innerHTML = inp;
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}