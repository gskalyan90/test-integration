package com.thales.ecms.conversions;

import java.text.ParseException;

/**
 * This interface define acceptable datatype and return type of class:
 * com.thales.ecms.conversions.ObjectDataTypeConverter
 * 
 * @author Manish.Gour
 * 
 */

@FunctionalInterface
public interface DataTypeConverter {

	Object convert(String s, String airlineId) throws ParseException, NumberFormatException,IllegalArgumentException;
}
