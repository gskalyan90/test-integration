package com.thales.ecms.conversions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.psddev.dari.db.Query;
//import com.thales.ecms.model.Genre;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.Rating;

/**
 * Generic object DataType converter.
 * <p>
 * <h3>Use examples</h3>
 * 
 * <pre>
 * 
 * Object longObject = ObjectDataTypeConverter.converterForClass.get(Integer.TYPE).convert(String value);
 * 
 * Object longObject = ObjectDataTypeConverter.converterForClass.get(Long.TYPE).convert(String value);
 * 
 * </pre>
 * 
 *
 * This class maintains a map with the logic of each datatype conversion.
 * 
 * @author Manish.Gour
 * 
 */
public final class ObjectDataTypeConverter {

	public static Map<Class, DataTypeConverter> converterForClass = new HashMap<>();
	static {
		converterForClass.put(Integer.TYPE, (s, airlineId) -> Integer.parseInt(s));
		converterForClass.put(Double.TYPE, (s, airlineId) -> Double.parseDouble(s));
		converterForClass.put(String.class, (s, airlineId) -> s);
		converterForClass.put(Long.TYPE, (s, airlineId) -> Long.parseLong(s));
		converterForClass.put(Date.class, (s, airlineId) -> {

			Date date = null;

			String expectedPattern = "EEE, MMM d, yyyy hh:mm aaa";
			SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
			date = formatter.parse(s);
			System.out.println(date);

			return date;
		});

//		converterForClass.put(com.thales.ecms.model.Genre.class, (s, airlineId) -> Genre.valueOf(s));
		converterForClass.put(com.thales.ecms.model.Rating.class, (s, airlineId) -> Rating.valueOf(s));

		converterForClass.put(com.thales.ecms.model.PaxguiLanguage.class, (s, airlineId) -> {

			PaxguiLanguage paxguiLanguage = null;

			paxguiLanguage = Query.from(PaxguiLanguage.class).where("paxguiLanguageName = ?", s)
					.and("cms.site.owner = ?", airlineId).first();

			return paxguiLanguage;
		});

		converterForClass.put(com.thales.ecms.model.Image.class, (s, airlineId) -> null);
		converterForClass.put(com.thales.ecms.model.PlayList.class, (s, airlineId) -> null);
		converterForClass.put(java.util.List.class, (s, airlineId) -> null);
	};

	public void myName() {

	}

}
