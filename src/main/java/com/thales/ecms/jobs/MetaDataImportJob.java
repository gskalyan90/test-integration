package com.thales.ecms.jobs;

import java.util.concurrent.Callable;

import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.IngestionDetail;

/**
* <h1>Import MetaData CSV/XLS/XLSX file!</h1>
* This Interface has common method to import metadata into ECMS portal
*
* @author  Manish gour
* @version 1.0
*/

/**
 * @author Manish gour, Interface for importing metadata- CSV/XLS/XLSX
 */

public interface MetaDataImportJob extends Callable<IngestionDetail> {

	/**
	 * Setting required objects, before processing.
	 * 
	 * @return Nothing.
	 */
	void setRequriedObjects(String airlineId, String configurationId, String ingestionDetailId, String fileName)
			throws ObjectNotFoundException;

	/**
	 * This method is opening file before processing.
	 * 
	 * @return Nothing.
	 */
	void openfile();

	/**
	 * This method will validate the file before starting process.
	 * 
	 * @return Nothing.
	 */
	boolean validateFile();

	/**
	 * This method have actual process to import the data into database.
	 * 
	 * @return Nothing.
	 */
	void process();

	/**
	 * This method is used to close the open file instance.
	 * 
	 * @return Nothing.
	 */
	void closeFile();

}