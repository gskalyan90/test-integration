package com.thales.ecms.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;
import com.thales.ecms.model.util.DisplayGroup;

/**
 * @author Mihir
 * An Order is a collection of content (titles) from the airline catalog to be packaged and delivered, usually for a certain period in time.
 * An Order may be valid for one or multiple Configurations.
 */
public class Order extends Content {
	
	@Indexed
	private String name;
	
	@Indexed
	private String description;
	
	@Indexed
	private Date startDate;
	
	@Indexed
	private Date endDate;
	
	@Indexed
	private Date lastModifiedDate;
	
	@Indexed
	private Date shipmentDeadLine;
	
	@Indexed
	private Date contentDeadLine;
	
	@Indexed
	private Date assetDeadLine;
	
	@Indexed
	private List<DisplayGroup> displayGroups;
	
	@Indexed
	private Status orderStatus;
	
	@Indexed
	@DisplayName("Configurations")
	private Set<Configuration> configurationSet;
	
	@Indexed
	@DisplayName("Titles")
	private List<OrderTitle> titleList;
	
	//Class to represent Titles belonging to an Order
	@Content.Embedded
	@Content.LabelFields("content")
	@DisplayName("Title")
	public static class OrderTitle extends Content {
	
		@Indexed
		private ContentType content;
		
		private Date startDate;
		
		private Date endDate;

		public ContentType getContent() {
			return content;
		}

		public void setContent(ContentType content) {
			this.content = content;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	public Date getShipmentDeadLine() {
		return shipmentDeadLine;
	}

	public void setShipmentDeadLine(Date shipmentDeadLine) {
		this.shipmentDeadLine = shipmentDeadLine;
	}

	public Date getContentDeadLine() {
		return contentDeadLine;
	}

	public void setContentDeadLine(Date contentDeadLine) {
		this.contentDeadLine = contentDeadLine;
	}

	public Date getAssetDeadLine() {
		return assetDeadLine;
	}

	public void setAssetDeadLine(Date assetDeadLine) {
		this.assetDeadLine = assetDeadLine;
	}

	public List<DisplayGroup> getDisplayGroups() {
		return displayGroups;
	}

	public void setDisplayGroups(List<DisplayGroup> displayGroups) {
		this.displayGroups = displayGroups;
	}

	public Status getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Status orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public Set<Configuration> getConfigurationSet() {
		return configurationSet;
	}

	public void setConfigurationSet(Set<Configuration> configurationSet) {
		this.configurationSet = configurationSet;
	}
	
	public List<OrderTitle> getTitleList() {
		return titleList;
	}

	public void setTitleList(List<OrderTitle> titleList) {
		this.titleList = titleList;
	}

	public enum Status {
		COMPLETE, INCOMPLETE, PACKAGED, SHIPPED
	}
}
