package com.thales.ecms.model;

import com.psddev.cms.db.Content;

/**
 * A storage location, which can be public (accessible from the eCMS public cloud) or private (accessible only in a customer’s private environment)
 */
public abstract class Location extends Content {

	@Indexed
	// @Required
	// private String storageLocationPath;

	@Required
	private LocationType locationType;

	public LocationType getLocationType() {
		return locationType;
	}

	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}
}