package com.thales.ecms.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.psddev.cms.db.Content;
import com.thales.ecms.model.util.ReferentialTextClass;
import com.thales.ecms.model.util.TextClass;

public class FeatureVideo extends ContentType {

	// just added this to make viewing and searching easier in CMS. Might not
	// use this later
	@Indexed
	private String englishTitle;

	@Indexed
	@Embedded
	private List<TextClass> title;

	@Indexed
	@Embedded
	private List<TextClass> tagLine;

//	@Indexed
//	private Genre genre;

	@Indexed
	@Embedded
	private List<TextClass> actor;

	@Indexed
	@Embedded
	private List<TextClass> director;

	@Indexed
	private Date releaseDate;

	@Indexed
	private String releaseYear;

	@Indexed
	private Rating rating;

	@Indexed
	private long duration;

	@Indexed
	@Embedded
	private List<TextClass> distributor;

	@Indexed
	private PaxguiLanguage audioLanguage;

	@Indexed
	private PaxguiLanguage closedCaptionsLanguage;

	@Embedded
	private List<ReferentialTextClass> synopsis;

	// private List<Image> images;

	private Image originalImage;

	private Image standardImage;

	private Image smallImage;

	// preview video asset

	@Indexed
	private int contentPosition;

	private PlayList playlist;

	private List<Asset> assets;

	@Embedded
	public static class Asset extends Content {

		private String name;

		private Set<Rendition> renditions;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Set<Rendition> getRenditions() {
			return renditions;
		}

		public void setRenditions(Set<Rendition> renditions) {
			this.renditions = renditions;
		}

		@Embedded
		public static class Rendition extends Content {

			private String filePath;

			private Quality quality;

			private Codec codec;

			private AspectRatio aspectRatio;

			public enum Quality {
				_144p("144p"), _240p("240p"), _360p("360p"), _480p("480p"), _720p("720p"), _1080p("1080p");

				String value;

				Quality(String value) {
					this.value = value;
				}

				public String getValue() {
					return value;
				}
			}

			public enum Codec {
				MPEG1, MPEG2, MPEG4
			}

			public enum AspectRatio {
				_16X9("16x9"), _4X3("4x3");

				String value;

				AspectRatio(String value) {
					this.value = value;
				}

				public String getValue() {
					return value;
				}
			}

			public String getFilePath() {
				return filePath;
			}

			public void setFilePath(String filePath) {
				this.filePath = filePath;
			}

			public Quality getQuality() {
				return quality;
			}

			public void setQuality(Quality quality) {
				this.quality = quality;
			}

			public Codec getCodec() {
				return codec;
			}

			public void setCodec(Codec codec) {
				this.codec = codec;
			}

			public AspectRatio getAspectRatio() {
				return aspectRatio;
			}

			public void setAspectRatio(AspectRatio aspectRatio) {
				this.aspectRatio = aspectRatio;
			}
		}
	}

	public String getEnglishTitle() {
		return englishTitle;
	}

	public void setEnglishTitle(String englishTitle) {
		this.englishTitle = englishTitle;
	}

	public List<TextClass> getTitle() {
		return title;
	}

	public void setTitle(List<TextClass> title) {
		this.title = title;
	}

	public List<TextClass> getTagLine() {
		return tagLine;
	}

	public void setTagLine(List<TextClass> tagLine) {
		this.tagLine = tagLine;
	}

//	public Genre getGenre() {
//		return genre;
//	}
//
//	public void setGenre(Genre genre) {
//		this.genre = genre;
//	}

	public List<TextClass> getActor() {
		return actor;
	}

	public void setActor(List<TextClass> actor) {
		this.actor = actor;
	}

	public List<TextClass> getDirector() {
		return director;
	}

	public void setDirector(List<TextClass> director) {
		this.director = director;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public List<TextClass> getDistributor() {
		return distributor;
	}

	public void setDistributor(List<TextClass> distributor) {
		this.distributor = distributor;
	}

	public PaxguiLanguage getAudioLanguage() {
		return audioLanguage;
	}

	public void setAudioLanguage(PaxguiLanguage audioLanguage) {
		this.audioLanguage = audioLanguage;
	}

	public PaxguiLanguage getClosedCaptionsLanguage() {
		return closedCaptionsLanguage;
	}

	public void setClosedCaptionsLanguage(PaxguiLanguage closedCaptionsLanguage) {
		this.closedCaptionsLanguage = closedCaptionsLanguage;
	}

	public List<ReferentialTextClass> getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(List<ReferentialTextClass> synopsis) {
		this.synopsis = synopsis;
	}

	/*
	 * public List<Image> getImages() { return images; }
	 * 
	 * public void setImages(List<Image> images) { this.images = images; }
	 */

	public List<Asset> getAssets() {
		return assets;
	}

	public void setAssets(List<Asset> assets) {
		this.assets = assets;
	}

	public int getContentPosition() {
		return contentPosition;
	}

	public void setContentPosition(int contentPosition) {
		this.contentPosition = contentPosition;
	}

	public void setPlaylist(PlayList playlist) {
		this.playlist = playlist;
	}

	public PlayList getPlaylist() {
		return playlist;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "FeatureVideo: " + this.getEnglishTitle() + " Titles:" + this.getTitle(); //+ " Genre:" + this.getGenre();
	}

	public Image getOriginalImage() {
		return originalImage;
	}

	public void setOriginalImage(Image originalImage) {
		this.originalImage = originalImage;
	}

	public Image getStandardImage() {
		return standardImage;
	}

	public void setStandardImage(Image standardImage) {
		this.standardImage = standardImage;
	}

	public Image getSmallImage() {
		return smallImage;
	}

	public void setSmallImage(Image smallImage) {
		this.smallImage = smallImage;
	}

}
