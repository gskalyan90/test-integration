package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

/**
 * @author Mihir
 * Available types of LRUs, for example: DSU, TPMU, SVDU…
 */
@DisplayName("LRU Type")
public class LruType extends Content {

	@Indexed
	@Maximum(45)
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
