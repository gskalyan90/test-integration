package com.thales.ecms.model;

/**
 * 
 * Model represents a Local Location
 */
public class LocalLocation extends Location {
	
	@Indexed
	@Required
	private String name;
	
	@Required
	private String path;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}