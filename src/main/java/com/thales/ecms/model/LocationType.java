package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.Indexed;
import com.psddev.dari.db.Recordable.Required;

public class LocationType extends Content {

	@Indexed(unique = true)
	@Required
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
