package com.thales.ecms.model;

import java.util.Date;
import java.util.List;

import com.psddev.cms.db.Content;

public class IngestionDetail extends Content {

	@Indexed
	private String fileName;

	@Indexed
	private Date ingestedDate;

	@Indexed
	private Mode mode;

	@Values({ "Pass", "Fail", "In Progress" })
	private String status;

	private String configuration;

	// TODO set configurationId id
	private String configurationId;

	private long titleCount;

	// @ToolUi.ReadOnly
	@Embedded
	private List<IngestionTitle> titles;

	public enum Mode {
		Uploaded, Imported
	}

	@Embedded
	private ImportStatus importStatus;

	@Content.Embedded
	public static class ImportStatus extends Content {
		private String exception;
		private String userFriendlyMessage;

		public String getException() {
			return exception;
		}

		public void setException(String exception) {
			this.exception = exception;
		}

		public String getUserFriendlyMessage() {
			return userFriendlyMessage;
		}

		public void setUserFriendlyMessage(String userFriendlyMessage) {
			this.userFriendlyMessage = userFriendlyMessage;
		}

	}

	@Content.Embedded
	public static class IngestionTitle extends Content {

		private String titleName;

		private String contentType;

		@Values({ "Pass", "Fail", "In Progress" })
		private String status;

		private Date ingestedDate;

		@Embedded
		private ImportStatusTitle importStatusTitle;

		@Content.Embedded
		public static class ImportStatusTitle extends Content {
			private String exception;
			private String userFriendlyMessage;

			public String getException() {
				return exception;
			}

			public void setException(String exception) {
				this.exception = exception;
			}

			public String getUserFriendlyMessage() {
				return userFriendlyMessage;
			}

			public void setUserFriendlyMessage(String userFriendlyMessage) {
				this.userFriendlyMessage = userFriendlyMessage;
			}

		}

		public String getTitleName() {
			return titleName;
		}

		public void setTitleName(String titleName) {
			this.titleName = titleName;
		}

		public String getContentType() {
			return contentType;
		}

		public void setContentType(String contentType) {
			this.contentType = contentType;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Date getIngestedDate() {
			return ingestedDate;
		}

		public void setIngestedDate(Date ingestedDate) {
			this.ingestedDate = ingestedDate;
		}

		public ImportStatusTitle getImportStatusTitle() {
			return importStatusTitle;
		}

		public void setImportStatusTitle(ImportStatusTitle importStatusTitle) {
			this.importStatusTitle = importStatusTitle;
		}

		@Override
		public boolean equals(Object object) {
			boolean sameSame = false;
			if (object != null && object instanceof IngestionTitle) {
				sameSame = this.titleName.equalsIgnoreCase(((IngestionTitle) object).titleName);
			}
			return sameSame;
		}

	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getIngestedDate() {
		return ingestedDate;
	}

	public void setIngestedDate(Date ingestedDate) {
		this.ingestedDate = ingestedDate;
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public long getTitleCount() {
		return titleCount;
	}

	public void setTitleCount(long titleCount) {
		this.titleCount = titleCount;
	}

	public List<IngestionTitle> getTitles() {
		return titles;
	}

	public void setTitles(List<IngestionTitle> titles) {
		this.titles = titles;
	}

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public ImportStatus getImportStatus() {
		return importStatus;
	}

	public void setImportStatus(ImportStatus importStatus) {
		this.importStatus = importStatus;
	}

}
