package com.thales.ecms.model.util;

import com.psddev.cms.db.Content;
import com.psddev.cms.db.ToolUi;
import com.psddev.dari.db.Recordable.DisplayName;
import com.thales.ecms.model.PaxguiLanguage;

@Content.Embedded
@DisplayName("Item")
@Content.LabelFields("value")
public class TextClass extends Content {

	private PaxguiLanguage language;

	private String value;

	@DisplayName("Maximum Character Length")
	@ToolUi.ReadOnly
	private String maxChar;

	public PaxguiLanguage getLanguage() {
		return language;
	}

	public void setLanguage(PaxguiLanguage language) {
		this.language = language;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMaxChar() {
		return maxChar;
	}

	public void setMaxChar(String maxChar) {
		this.maxChar = maxChar;
	}

	@Override
	public String toString() {
		return this.getValue();
	}
}
