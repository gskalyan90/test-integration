package com.thales.ecms.model.util;

import com.psddev.cms.db.Content;
import com.psddev.cms.db.ToolUi;
import com.psddev.dari.db.Recordable.DisplayName;
import com.psddev.dari.db.ReferentialText;
import com.thales.ecms.model.PaxguiLanguage;

@Content.Embedded
@DisplayName("Item")
@Content.LabelFields("language")
public class ReferentialTextClass extends Content {
	
	private PaxguiLanguage language;
	
	private ReferentialText value;
	
	@DisplayName("Maximum Character Length")
	@ToolUi.ReadOnly
	private String maxChar;

	public PaxguiLanguage getLanguage() {
		return language;
	}

	public void setLanguage(PaxguiLanguage language) {
		this.language = language;
	}

	public ReferentialText getValue() {
		return value;
	}

	public void setValue(ReferentialText value) {
		this.value = value;
	}

	public String getMaxChar() {
		return maxChar;
	}

	public void setMaxChar(String maxChar) {
		this.maxChar = maxChar;
	}
}
