package com.thales.ecms.model.util;

import java.util.Date;

import com.psddev.cms.db.Content;

@Content.Embedded
public class DisplayGroup extends Content {
	
	private String month;
	
	private Date startDate;
	
	private Date endDate;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
