package com.thales.ecms.model;

import com.psddev.cms.db.Content;

/**
 * @author Mihir
 * An origin-destination airport pair. Routes are generic to all airlines.
 */
public class Route extends Content {

	@Indexed
	private String name;
	
	@Indexed
	@Required
	private Airport origin;
	
	@Indexed
	@Required
	private Airport destination;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Airport getOrigin() {
		return origin;
	}

	public void setOrigin(Airport origin) {
		this.origin = origin;
	}

	public Airport getDestination() {
		return destination;
	}

	public void setDestination(Airport destination) {
		this.destination = destination;
	}
	
	@Override
	public String toString() {
		return "Route instance:- Name: "+this.getName()+" Origin: "+this.getOrigin()+" Destination: "+this.getDestination();
	}
}