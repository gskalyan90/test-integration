package com.thales.ecms.model.datatype;


import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Integer Values")
public class IntegerType extends BaseType {

	@Indexed
	private Set<Integer> values;

	public Set<Integer> getValues() {
		return values;
	}

	public void setValues(Set<Integer> values) {
		this.values = values;
	}
}
