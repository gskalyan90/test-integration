package com.thales.ecms.model.datatype;

import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Image Values")
public class ImageType extends BaseType {

	@Indexed
	private List<ImageTypeLanguage> values;

	public List<ImageTypeLanguage> getValues() {
		return values;
	}

	public void setValues(List<ImageTypeLanguage> values) {
		this.values = values;
	}

}
