package com.thales.ecms.model.datatype;

import java.util.Date;

import com.psddev.cms.db.Content;

@Content.LabelFields("date")
public class DateTypeUtil extends Content {

	private Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
