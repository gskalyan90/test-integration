package com.thales.ecms.model.datatype;

import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Text Values")
public class TextType extends BaseType {

//	@Indexed
//	private List<String> values;
//
//	public List<String> getValues() {
//		return values;
//	}
//
//	public void setValues(List<String> values) {
//		this.values = values;
//	}
	
	@Indexed
	private List<TextTypeLanguage> values;

	public List<TextTypeLanguage> getValues() {
		return values;
	}

	public void setValues(List<TextTypeLanguage> values) {
		this.values = values;
	}
	
}
