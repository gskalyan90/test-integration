package com.thales.ecms.model.datatype;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Genre Values")
public class GenreType extends BaseType {

	@Indexed
	private Set<GenreTypeLanguage> values;

	public Set<GenreTypeLanguage> getValues() {
		return values;
	}

	public void setValues(Set<GenreTypeLanguage> values) {
		this.values = values;
	}
	
}
