package com.thales.ecms.model.datatype;

import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;
import com.psddev.dari.db.ReferentialText;
import com.thales.ecms.model.PaxguiLanguage;

@Content.Embedded
@DisplayName("Language-Specific Long Text Values")
public class ReferentialTextTypeLanguage extends Content {

	@Indexed
	private PaxguiLanguage language;
	
	@Indexed
	private List<ReferentialText> values;

	public PaxguiLanguage getLanguage() {
		return language;
	}

	public void setLanguage(PaxguiLanguage language) {
		this.language = language;
	}

	public List<ReferentialText> getValues() {
		return values;
	}

	public void setValues(List<ReferentialText> values) {
		this.values = values;
	}
}
