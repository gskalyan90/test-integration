package com.thales.ecms.model.datatype;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Long Values")
public class LongType extends BaseType {

	@Indexed
	private Set<Long> values;

	public Set<Long> getValues() {
		return values;
	}

	public void setValues(Set<Long> values) {
		this.values = values;
	}
}
