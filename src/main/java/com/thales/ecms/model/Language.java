package com.thales.ecms.model;

public enum Language {

	Arabic, Chinese, English, French, German, Hindi, Japanese, Korean, Persian, Russian, Spanish, Turkish;

}
