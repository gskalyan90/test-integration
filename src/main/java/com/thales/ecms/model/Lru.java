package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.cms.db.ToolUi;
import com.psddev.dari.db.Recordable.DisplayName;

/**
 * @author alok
 * Class representing an LRU - Line Replaceable Unit
 * LRU - IFE system installed on a seat.
 */

@DisplayName("LRU")
public class Lru extends Content {
	
	@Required
	@Indexed(unique=true)
	@ToolUi.Note("LRU name")
	@DisplayName("LRU Name")
	private String lruName;
	
	@Required
	@Indexed
	@ToolUi.Note("LRU Type")
	@DisplayName("LRU Type")
	private LruType lruType;

	public String getLruName() {
		return lruName;
	}

	public void setLruName(String lruName) {
		this.lruName = lruName;
	}

	public LruType getLruType() {
		return lruType;
	}

	public void setLruType(LruType lruType) {
		this.lruType = lruType;
	}

}
