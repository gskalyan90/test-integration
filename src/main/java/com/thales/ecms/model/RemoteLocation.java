package com.thales.ecms.model;

import com.psddev.cms.db.ToolUi;
/**
 * 
 * Model represents a Remote Location
 *
 */
public class RemoteLocation extends Location {
	
	@Indexed
	@Required
	private String name;
	
	@Required
	private String path;
	
	@ToolUi.Note("UserName/AccessId")
	private String userName;
	
	@ToolUi.Note("Password/SecrectKey")
	private String password;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}