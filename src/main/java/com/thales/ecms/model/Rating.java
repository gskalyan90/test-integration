package com.thales.ecms.model;

public enum Rating {

	NR, G, PG, PG_13, R, NC_17, TV_Y, TV_Y7, TV_Y7_FV, TV_G, TV_14, TV_MA, TV_PG
	;

	@Override
	public String toString() {
		String s = super.toString();
		return s.replaceAll("_", "-");
	}
}
