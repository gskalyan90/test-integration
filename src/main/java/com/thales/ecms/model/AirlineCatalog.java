package com.thales.ecms.model;

import java.util.Date;
import java.util.Set;

import com.psddev.cms.db.Content;

/**
 * @author Mihir
 * Airline Catalog - A catalog of all Titles for an Airline
 * There will be just one instance of this object per Airline
 */
public class AirlineCatalog extends Content {

	@Indexed
	private String name = "Airline Catalog";
	
	@Indexed
	private Set<AirlineCatalogTitle> airlineCatalogTitles;

	@Content.Embedded
	@Content.LabelFields("configuration")
	@DisplayName("Airline Catalog Title")
	public static class AirlineCatalogTitle extends Content {
		
		//The Configuration
		@Indexed
		private Configuration configuration;
		
		//The Set of Titles in the above Configuration
		@Indexed
		private Set<ConfigTitle> titles;

		public Configuration getConfiguration() {
			return configuration;
		}

		public void setConfiguration(Configuration configuration) {
			this.configuration = configuration;
		}
		
		public Set<ConfigTitle> getTitles() {
			return titles;
		}

		public void setTitles(Set<ConfigTitle> titles) {
			this.titles = titles;
		}

		@Content.Embedded
		@Content.LabelFields("title")
		@DisplayName("Title")
		public static class ConfigTitle extends Content {
			
			private ContentType title;
			
			private Date createdDate;
			
			private Status status;
			
			public ContentType getTitle() {
				return title;
			}

			public void setTitle(ContentType title) {
				this.title = title;
			}

			public Status getStatus() {
				return status;
			}

			public void setStatus(Status status) {
				this.status = status;
			}

			public Date getCreatedDate() {
				return createdDate;
			}

			public void setCreatedDate(Date createdDate) {
				this.createdDate = createdDate;
			}



			public enum Status {
				Incomplete, Validated, Completed
			}
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Set<AirlineCatalogTitle> getAirlineCatalogTitles() {
		return airlineCatalogTitles;
	}

	public void setAirlineCatalogtitles(Set<AirlineCatalogTitle> airlineCatalogTitles) {
		this.airlineCatalogTitles = airlineCatalogTitles;
	}

}
