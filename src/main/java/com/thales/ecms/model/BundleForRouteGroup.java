package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;
import com.psddev.dari.db.Recordable.Indexed;

@Content.LabelFields("bundle")
public class BundleForRouteGroup extends Content {

	@Indexed
	private Bundle bundle;
	
	@Indexed
	private RouteGroup routeGroup;
	
	@Indexed
	private SeatingClass seatingClass;
	
	@Indexed
	@DisplayName("Pay Per Access Fee")
	private double ppaFee;
	
	@Indexed
	@DisplayName("Pay Per Vie Fee")
	private double ppvFee;

	public Bundle getBundle() {
		return bundle;
	}

	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}

	public RouteGroup getRouteGroup() {
		return routeGroup;
	}

	public void setRouteGroup(RouteGroup routeGroup) {
		this.routeGroup = routeGroup;
	}

	public SeatingClass getSeatingClass() {
		return seatingClass;
	}

	public void setSeatingClass(SeatingClass seatingClass) {
		this.seatingClass = seatingClass;
	}

	public double getPpaFee() {
		return ppaFee;
	}

	public void setPpaFee(double ppaFee) {
		this.ppaFee = ppaFee;
	}

	public double getPpvFee() {
		return ppvFee;
	}

	public void setPpvFee(double ppvFee) {
		this.ppvFee = ppvFee;
	}
	
	
}
