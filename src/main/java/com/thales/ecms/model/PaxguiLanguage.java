package com.thales.ecms.model;
//default package

//Generated Dec 18, 2016 11:15:54 PM by Hibernate Tools 3.6.0.Final

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.Indexed;

/**
 * 
 * Model for PaxuiLanguage
 * The languages available for use on the passenger GUI for a specific airline.
 */
public class PaxguiLanguage extends Content {

	@Required
	@Indexed
	private String paxguiLanguageName;
	
	@Required
	@Indexed
	private String paxguiLanguageCode;

	public PaxguiLanguage() {
	}

	public PaxguiLanguage(String paxguiLanguageName, String paxguiLanguageCode) {
		this.paxguiLanguageName = paxguiLanguageName;
		this.paxguiLanguageCode = paxguiLanguageCode;
	}

	public String getPaxguiLanguageName() {
		return this.paxguiLanguageName;
	}

	public void setPaxguiLanguageName(String paxguiLanguageName) {
		this.paxguiLanguageName = paxguiLanguageName;
	}

	public String getPaxguiLanguageCode() {
		return this.paxguiLanguageCode;
	}

	public void setPaxguiLanguageCode(String paxguiLanguageCode) {
		this.paxguiLanguageCode = paxguiLanguageCode;
	}

	@Override
	public String toString() {
		return getPaxguiLanguageName();
	}

}
