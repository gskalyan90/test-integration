package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.ReferentialText;
//import com.snagfilms.model.Image;

public class Ebook extends Content{

	@Indexed
	private String title;
	
	@Indexed
	private String shortTitle;
	
	@Indexed
	private String longTitle;
	
	@Indexed
	private String categoryText;
	
	private ReferentialText synopsis;
	
	private ReferentialText shortSynopsis;
	
	private ReferentialText longSynopsis;
	
//	private Image coverImage;
//	
//	private Image standardImage;
//	
//	private Image smallImage;
	
	@Indexed
	private Rating censorRating;
	
	@Indexed
	private String standardTag;
	
	@Indexed
	private String resourceKey;
	
	@Indexed
	private String extendedDataBlob;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public String getLongTitle() {
		return longTitle;
	}

	public void setLongTitle(String longTitle) {
		this.longTitle = longTitle;
	}

	public String getCategoryText() {
		return categoryText;
	}

	public void setCategoryText(String categoryText) {
		this.categoryText = categoryText;
	}

	public ReferentialText getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(ReferentialText synopsis) {
		this.synopsis = synopsis;
	}

	public ReferentialText getShortSynopsis() {
		return shortSynopsis;
	}

	public void setShortSynopsis(ReferentialText shortSynopsis) {
		this.shortSynopsis = shortSynopsis;
	}

	public ReferentialText getLongSynopsis() {
		return longSynopsis;
	}

	public void setLongSynopsis(ReferentialText longSynopsis) {
		this.longSynopsis = longSynopsis;
	}

//	public Image getCoverImage() {
//		return coverImage;
//	}
//
//	public void setCoverImage(Image coverImage) {
//		this.coverImage = coverImage;
//	}
//
//	public Image getStandardImage() {
//		return standardImage;
//	}
//
//	public void setStandardImage(Image standardImage) {
//		this.standardImage = standardImage;
//	}
//
//	public Image getSmallImage() {
//		return smallImage;
//	}
//
//	public void setSmallImage(Image smallImage) {
//		this.smallImage = smallImage;
//	}

	public Rating getCensorRating() {
		return censorRating;
	}

	public void setCensorRating(Rating censorRating) {
		this.censorRating = censorRating;
	}

	public String getStandardTag() {
		return standardTag;
	}

	public void setStandardTag(String standardTag) {
		this.standardTag = standardTag;
	}

	public String getResourceKey() {
		return resourceKey;
	}

	public void setResourceKey(String resourceKey) {
		this.resourceKey = resourceKey;
	}

	public String getExtendedDataBlob() {
		return extendedDataBlob;
	}

	public void setExtendedDataBlob(String extendedDataBlob) {
		this.extendedDataBlob = extendedDataBlob;
	}
	
	
}
