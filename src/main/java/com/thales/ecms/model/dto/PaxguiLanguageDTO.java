package com.thales.ecms.model.dto;

public class PaxguiLanguageDTO {

	private String paxguiLanguageId;
	private String paxguiLanguageName;
	private String paxguiLanguageCode;

	public String getPaxguiLanguageId() {
		return paxguiLanguageId;
	}

	public void setPaxguiLanguageId(String paxguiLanguageId) {
		this.paxguiLanguageId = paxguiLanguageId;
	}

	public String getPaxguiLanguageName() {
		return this.paxguiLanguageName;
	}

	public void setPaxguiLanguageName(String paxguiLanguageName) {
		this.paxguiLanguageName = paxguiLanguageName;
	}

	public String getPaxguiLanguageCode() {
		return this.paxguiLanguageCode;
	}

	public void setPaxguiLanguageCode(String paxguiLanguageCode) {
		this.paxguiLanguageCode = paxguiLanguageCode;
	}

}
