package com.thales.ecms.model.dto;

import java.util.Set;

/**
 * 
 * @author kumargupta.v
 *
 */
public class ServerGroupDTO {

	private String serverGroupId;
	
	private String name;
	
	private String lopaId;
	
	private Set<LRUTypeDTO> lruTypeDTOs;
	
	public static class LRUTypeDTO {
		
		private String lruTypeId;
		
		private double maxSpaceAllowance;

		public String getLruTypeId() {
			return lruTypeId;
		}

		public void setLruTypeId(String lruTypeId) {
			this.lruTypeId = lruTypeId;
		}

		public double getMaxSpaceAllowance() {
			return maxSpaceAllowance;
		}

		public void setMaxSpaceAllowance(double maxSpaceAllowance) {
			this.maxSpaceAllowance = maxSpaceAllowance;
		}			
	}

	public String getServerGroupId() {
		return serverGroupId;
	}

	public void setServerGroupId(String serverGroupId) {
		this.serverGroupId = serverGroupId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLopaId() {
		return lopaId;
	}

	public void setLopaId(String lopaId) {
		this.lopaId = lopaId;
	}

	public Set<LRUTypeDTO> getLruTypeDTOs() {
		return lruTypeDTOs;
	}

	public void setLruTypeDTOs(Set<LRUTypeDTO> lruTypeDTOs) {
		this.lruTypeDTOs = lruTypeDTOs;
	}	
}
