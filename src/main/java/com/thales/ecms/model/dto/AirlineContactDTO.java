package com.thales.ecms.model.dto;

import io.swagger.annotations.ApiModelProperty;

public class AirlineContactDTO {

	private String airlineContactInternalId;

	private String firstName;
	
	private String lastName;
	
	private String phone;
	
	private String email;
	
	private String airlineContactType;
	
	public String getAirlineContactInternalId() {
		return airlineContactInternalId;
	}

	public void setAirlineContactInternalId(String airlineContactInternalId) {
		this.airlineContactInternalId = airlineContactInternalId;
	}

	public String getFirstName() {
		return firstName;
	}

	@ApiModelProperty(required = true)
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}		

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	@ApiModelProperty(required = true)
	public void setEmail(String email) {
		this.email = email;
	}

	public String getAirlineContactType() {
		return airlineContactType;
	}

	@ApiModelProperty(required = true, allowableValues="Maintenance,Hotline")
	public void setAirlineContactType(String airlineContactType) {
		this.airlineContactType = airlineContactType;
	}
	
}
