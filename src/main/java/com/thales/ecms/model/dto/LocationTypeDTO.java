package com.thales.ecms.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

public class LocationTypeDTO {

	private String locationTypeInternalId;
	
	private String locationType;


	public String getLocationTypeInternalId() {
		return locationTypeInternalId;
	}

	public void setLocationTypeInternalId(String locationTypeInternalId) {
		this.locationTypeInternalId = locationTypeInternalId;
	}
	
	@ApiModelProperty(example="Loading Dock",required=true)
	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}	
}