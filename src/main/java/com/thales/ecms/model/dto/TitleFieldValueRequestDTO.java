package com.thales.ecms.model.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Mihir
 * Request DTO for the value of a Field of a Title - used when creating a Title
 */
public class TitleFieldValueRequestDTO {

	@ApiModelProperty(value = "Name of the language, not required for number, boolean and date fields", required = false)
	private String language;
	
	private List<Object> values;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public List<Object> getValues() {
		return values;
	}

	public void setValues(List<Object> values) {
		this.values = values;
	}
}
