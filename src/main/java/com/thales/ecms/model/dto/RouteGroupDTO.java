package com.thales.ecms.model.dto;

import java.util.Set;

import io.swagger.annotations.ApiModelProperty;

/**
 * DTO Class for RoutGroup
 * 
 * @author arjun.p
 *
 */
public class RouteGroupDTO {

	
	/**
	 * RouteGroup ID
	 */
	private String routeGroupId;
	
	/**
	 * RouteGroup Name	
	 */
	private String routeGroupName;
	
	/**
	 * Set of RouteDTO
	 */
	private Set<RouteDTO> routes;

	public String getRouteGroupId() {
		return routeGroupId;
	}

	public void setRouteGroupId(String routeGroupId) {
		this.routeGroupId = routeGroupId;
	}

	public String getRouteGroupName() {
		return routeGroupName;
	}

	@ApiModelProperty(example = "LONG_HAUL", required = true)
	public void setRouteGroupName(String routeGroupName) {
		this.routeGroupName = routeGroupName;
	}

	public Set<RouteDTO> getRoutes() {
		return routes;
	}

	@ApiModelProperty(required = true)
	public void setRoutes(Set<RouteDTO> routes) {
		this.routes = routes;
	}
				
	
}
