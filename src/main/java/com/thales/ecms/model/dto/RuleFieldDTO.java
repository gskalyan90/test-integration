package com.thales.ecms.model.dto;

public class RuleFieldDTO {
	
	private String fieldName;

	private ValidatorAttributeDTO validatorDTO;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public ValidatorAttributeDTO getValidatorDTO() {
		return validatorDTO;
	}

	public void setValidatorDTO(ValidatorAttributeDTO validatorDTO) {
		this.validatorDTO = validatorDTO;
	}

	
}
