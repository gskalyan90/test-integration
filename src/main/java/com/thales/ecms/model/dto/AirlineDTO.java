package com.thales.ecms.model.dto;

import java.util.Date;

public class AirlineDTO {
	
	private String airlineInternalId;
	
	private String airlineName;
	
	private String website;
	
	private String companyName;
	
	private Boolean activate;
	
	private String icaoCode;
	
	private String iataCode;
	
	private String codeShare;
	
	private Date shipmentStartdate;
	
	private Date shipmentEndDate;
	
	private Date contentStartDate;
	
	private Date contentEndDate;
	
	private Date assetStartDate;
	
	private Date assetEndDate;

	public String getAirlineInternalId() {
		return airlineInternalId;
	}

	public void setAirlineInternalId(String airlineInternalId) {
		this.airlineInternalId = airlineInternalId;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Boolean getActivate() {
		return activate;
	}

	public void setActivate(Boolean activate) {
		this.activate = activate;
	}

	public String getIcaoCode() {
		return icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}
	
	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getCodeShare() {
		return codeShare;
	}

	public void setCodeShare(String codeShare) {
		this.codeShare = codeShare;
	}

	public Date getShipmentStartdate() {
		return shipmentStartdate;
	}

	public void setShipmentStartdate(Date shipmentStartdate) {
		this.shipmentStartdate = shipmentStartdate;
	}

	public Date getShipmentEndDate() {
		return shipmentEndDate;
	}

	public void setShipmentEndDate(Date shipmentEndDate) {
		this.shipmentEndDate = shipmentEndDate;
	}

	public Date getContentStartDate() {
		return contentStartDate;
	}

	public void setContentStartDate(Date contentStartDate) {
		this.contentStartDate = contentStartDate;
	}

	public Date getContentEndDate() {
		return contentEndDate;
	}

	public void setContentEndDate(Date contentEndDate) {
		this.contentEndDate = contentEndDate;
	}

	public Date getAssetStartDate() {
		return assetStartDate;
	}

	public void setAssetStartDate(Date assetStartDate) {
		this.assetStartDate = assetStartDate;
	}

	public Date getAssetEndDate() {
		return assetEndDate;
	}

	public void setAssetEndDate(Date assetEndDate) {
		this.assetEndDate = assetEndDate;
	}

}
