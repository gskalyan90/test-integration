package com.thales.ecms.model.dto;

public class AircraftDTO {

	private String aircraftTypeInternalId;
	
	private String name;

	public String getAircraftTypeInternalId() {
		return aircraftTypeInternalId;
	}

	public void setAircraftTypeInternalId(String aircraftTypeInternalId) {
		this.aircraftTypeInternalId = aircraftTypeInternalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
