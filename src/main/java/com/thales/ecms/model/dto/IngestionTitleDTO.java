package com.thales.ecms.model.dto;

import java.util.Date;

public class IngestionTitleDTO {

	private String titleName;
	
	private String contentType;

	private String status;
	
	private Date ingestedDate;
	
	private ImportStatusTitleDTO importStatusTitleDTO;

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getIngestedDate() {
		return ingestedDate;
	}

	public void setIngestedDate(Date ingestedDate) {
		this.ingestedDate = ingestedDate;
	}

	public ImportStatusTitleDTO getImportStatusTitleDTO() {
		return importStatusTitleDTO;
	}

	public void setImportStatusTitleDTO(ImportStatusTitleDTO importStatusTitleDTO) {
		this.importStatusTitleDTO = importStatusTitleDTO;
	}
	
	

}
