package com.thales.ecms.model.dto;

public class LruTypeDTO {

	private String lruTypeInternalId;
	
	private String lruTypeName;
	
//	private double maxSpaceAllowance;

	public String getLruTypeInternalId() {
		return lruTypeInternalId;
	}

	public void setLruTypeInternalId(String lruTypeInternalId) {
		this.lruTypeInternalId = lruTypeInternalId;
	}

	public String getLruTypeName() {
		return lruTypeName;
	}

	public void setLruTypeName(String lruTypeName) {
		this.lruTypeName = lruTypeName;
	}

//	public double getMaxSpaceAllowance() {
//		return maxSpaceAllowance;
//	}
//
//	public void setMaxSpaceAllowance(double maxSpaceAllowance) {
//		this.maxSpaceAllowance = maxSpaceAllowance;
//	}
	
}
