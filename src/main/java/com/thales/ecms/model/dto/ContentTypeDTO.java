package com.thales.ecms.model.dto;

import java.util.List;

/**
 * @author Mihir
 * DTO class for a ContentType
 */
public class ContentTypeDTO {

	private String contentTypeInternalId;
	
	private String contentTypeName;
	
//	private String parentContentTypeName;

//	private List<ContentFieldDTO> contentFieldDTOList;

	public String getContentTypeInternalId() {
		return contentTypeInternalId;
	}

	public void setContentTypeInternalId(String contentTypeInternalId) {
		this.contentTypeInternalId = contentTypeInternalId;
	}

	public String getContentTypeName() {
		return contentTypeName;
	}

	public void setContentTypeName(String contentTypeName) {
		this.contentTypeName = contentTypeName;
	}

//	public String getParentContentTypeName() {
//		return parentContentTypeName;
//	}
//
//	public void setParentContentTypeName(String parentContentTypeName) {
//		this.parentContentTypeName = parentContentTypeName;
//	}
//
//	public List<ContentFieldDTO> getContentFieldDTOList() {
//		return contentFieldDTOList;
//	}
//
//	public void setContentFieldDTOList(List<ContentFieldDTO> contentFieldDTOList) {
//		this.contentFieldDTOList = contentFieldDTOList;
//	}
	
}
