package com.thales.ecms.model.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class GenericContentTypeRequestDTO {

	@ApiModelProperty(required = true)
	private String contentTypeName;
	
	private List<FieldRequestDTO> fields;

	public String getContentTypeName() {
		return contentTypeName;
	}

	public void setContentTypeName(String contentTypeName) {
		this.contentTypeName = contentTypeName;
	}

	public List<FieldRequestDTO> getFields() {
		return fields;
	}

	public void setFields(List<FieldRequestDTO> fields) {
		this.fields = fields;
	}
}