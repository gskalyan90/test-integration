package com.thales.ecms.model.dto;

import java.util.Date;
import java.util.List;

public class OrderRequestDTO {

	private String orderName;

	private String orderDescription;

	private Long orderStartDate;

	private Long orderEndDate;
	
	private Date lastModifiedDate;
	
	private String orderStatus;

	private List<String> configurationIdList;

	private List<DisplayGroupRequestDTO> displayGroupList;

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public Long getOrderStartDate() {
		return orderStartDate;
	}

	public void setOrderStartDate(Long orderStartDate) {
		this.orderStartDate = orderStartDate;
	}

	public Long getOrderEndDate() {
		return orderEndDate;
	}


	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public void setOrderEndDate(Long orderEndDate) {
		this.orderEndDate = orderEndDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public List<String> getConfigurationIdList() {
		return configurationIdList;
	}

	public void setConfigurationIdList(List<String> configurationIdList) {
		this.configurationIdList = configurationIdList;
	}


	public List<DisplayGroupRequestDTO> getDisplayGroupList() {
		return displayGroupList;
	}

	public void setDisplayGroupList(List<DisplayGroupRequestDTO> displayGroupList) {
		this.displayGroupList = displayGroupList;
	}




	public static class DisplayGroupRequestDTO {

		private String month;

		private Date startDate;

		private Date endDate;

		public String getMonth() {
			return month;
		}

		public void setMonth(String month) {
			this.month = month;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
	}

}
