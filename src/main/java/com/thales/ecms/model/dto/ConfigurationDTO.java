package com.thales.ecms.model.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Priority;

public class ConfigurationDTO {

	private String configurationId;
	
	private String configurationName;
	
	private String configurationStatus;

	private String packagingType;

	private List<TitleDTO> titleList = new ArrayList<>();


	public String getConfigurationStatus() {
		return configurationStatus;
	}

	public void setConfigurationStatus(String configurationStatus) {
		this.configurationStatus = configurationStatus;
	}

	public String getPackagingType() {
		return packagingType;
	}

	public void setPackagingType(String packagingType) {
		this.packagingType = packagingType;
	}

	public List<TitleDTO> getTitleList() {
		return titleList;
	}

	public void setTitleList(List<TitleDTO> titleList) {
		this.titleList = titleList;
	}

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public String getConfigurationName() {
		return configurationName;
	}

	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
	}
	
	public static class TitleDTO {

		private String contentName;
		
		private String contentType;
		
		private List<String> destinationList;

		private String contentInternalId;

		private String contentStatus;

		private Date contentStartDate;

		private Date contentendDate;

		private Priority priority;

		public String getContentName() {
			return contentName;
		}

		public void setContentName(String contentName) {
			this.contentName = contentName;
		}

		public String getContentType() {
			return contentType;
		}

		public void setContentType(String contentType) {
			this.contentType = contentType;
		}

		public List<String> getDestinationList() {
			return destinationList;
		}

		public void setDestinationList(List<String> destinationList) {
			this.destinationList = destinationList;
		}

		public String getContentInternalId() {
			return contentInternalId;
		}

		public void setContentInternalId(String contentInternalId) {
			this.contentInternalId = contentInternalId;
		}

		public String getContentStatus() {
			return contentStatus;
		}

		public void setContentStatus(String contentStatus) {
			this.contentStatus = contentStatus;
		}

		public Date getContentStartDate() {
			return contentStartDate;
		}

		public void setContentStartDate(Date contentStartDate) {
			this.contentStartDate = contentStartDate;
		}

		public Date getContentendDate() {
			return contentendDate;
		}

		public void setContentendDate(Date contentendDate) {
			this.contentendDate = contentendDate;
		}

		public Priority getPriority() {
			return priority;
		}

		public void setPriority(Priority priority) {
			this.priority = priority;
		}

	}
	
}
