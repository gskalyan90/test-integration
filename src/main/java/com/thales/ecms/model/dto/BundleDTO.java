package com.thales.ecms.model.dto;

import java.util.Set;

/**
 * 
 * @author arjun.p
 *
 */
public class BundleDTO {

	private String bundleId;

	private String bundleName;
	
	private Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings;
	
	private Long creationDate;
	
	private Long lastModifiedDate;
	
	public static class SeatingClassPriceMappingDTO {
		
		private String seatingClassId;
		
		private String seatingClassName;
		
		private double ppa;
		
		private double ppv;

		public String getSeatingClassId() {
			return seatingClassId;
		}

		public void setSeatingClassId(String seatingClassId) {
			this.seatingClassId = seatingClassId;
		}

		public String getSeatingClassName() {
			return seatingClassName;
		}

		public void setSeatingClassName(String seatingClassName) {
			this.seatingClassName = seatingClassName;
		}

		public double getPpa() {
			return ppa;
		}

		public void setPpa(double ppa) {
			this.ppa = ppa;
		}

		public double getPpv() {
			return ppv;
		}

		public void setPpv(double ppv) {
			this.ppv = ppv;
		}
		
		@Override
		public int hashCode() {
			int factor=1;
			if(seatingClassId!=null)
				factor=seatingClassId.hashCode();
			final int seed = 37;
		    int hash = 1;
		    hash = seed * hash + factor;		    
		    return hash;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj==null)
				return false;
			if(!(obj instanceof SeatingClassPriceMappingDTO))
				return false;
			SeatingClassPriceMappingDTO other = (SeatingClassPriceMappingDTO)obj;
			if(this.getSeatingClassId()!=null && other.getSeatingClassId()!=null)
				return this.getSeatingClassId().equals(other.getSeatingClassId());
			return false;
		}
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	public Set<SeatingClassPriceMappingDTO> getSeatingClassPriceMappings() {
		return seatingClassPriceMappings;
	}

	public void setSeatingClassPriceMappings(Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings) {
		this.seatingClassPriceMappings = seatingClassPriceMappings;
	}

	public Long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Long creationDate) {
		this.creationDate = creationDate;
	}

	public Long getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Long lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
