package com.thales.ecms.model.dto;

import java.util.List;
import java.util.Set;

import com.psddev.cms.db.Content;
//import com.thales.ecms.model.Configuration.ServerGroup;

public class ConfigurationRequestDTO {

	private String configurationId;

	private String configurationName;

	private String version;

	private String platform;

	private String configurationStatus;

	private String packagingType;

	private List<String> lopaIdList;

	private Set<String> interfaceLanguagesId;

	private Set<String> lrusId;

	private Set<String> lruTypesId;

	private List<ServerGroupDTO> serverGroupList;

	private Set<String> contentTypeIdList;

	private Set<String> rulesId;

	private List<ConfigurationTitleDTO> titleList;	
	
	private Long creationDate;
	
	private Long lastModifiedDate;

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public String getConfigurationName() {
		return configurationName;
	}

	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getConfigurationStatus() {
		return configurationStatus;
	}

	public void setConfigurationStatus(String configurationStatus) {
		this.configurationStatus = configurationStatus;
	}

	public String getPackagingType() {
		return packagingType;
	}

	public void setPackagingType(String packagingType) {
		this.packagingType = packagingType;
	}

	public List<String> getLopaIdList() {
		return lopaIdList;
	}

	public void setLopaIdList(List<String> lopaIdList) {
		this.lopaIdList = lopaIdList;
	}

	public Set<String> getInterfaceLanguagesId() {
		return interfaceLanguagesId;
	}

	public void setInterfaceLanguagesId(Set<String> interfaceLanguagesId) {
		this.interfaceLanguagesId = interfaceLanguagesId;
	}

	public Set<String> getLrusId() {
		return lrusId;
	}

	public void setLrusId(Set<String> lrusId) {
		this.lrusId = lrusId;
	}

	public Set<String> getLruTypesId() {
		return lruTypesId;
	}

	public void setLruTypesId(Set<String> lruTypesId) {
		this.lruTypesId = lruTypesId;
	}

	public List<ServerGroupDTO> getServerGroupList() {
		return serverGroupList;
	}

	public void setServerGroupList(List<ServerGroupDTO> serverGroupList) {
		this.serverGroupList = serverGroupList;
	}

	public Set<String> getContentTypeIdList() {
		return contentTypeIdList;
	}

	public void setContentTypeIdList(Set<String> contentTypeIdList) {
		this.contentTypeIdList = contentTypeIdList;
	}

	public Set<String> getRulesId() {
		return rulesId;
	}

	public void setRulesId(Set<String> rulesId) {
		this.rulesId = rulesId;
	}

	public List<ConfigurationTitleDTO> getTitleList() {
		return titleList;
	}

	public void setTitleList(List<ConfigurationTitleDTO> titleList) {
		this.titleList = titleList;
	}
	
	public Long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Long creationDate) {
		this.creationDate = creationDate;
	}

	public Long getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Long lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public static class ServerGroupDTO extends Content {

		private String name;

		private Set<String> lrus;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Set<String> getLrus() {
			return lrus;
		}

		public void setLrus(Set<String> lrus) {
			this.lrus = lrus;
		}
	}

	public static class ConfigurationTitleDTO {

		private String contentId;

		private String contentStatus;

		private Long startDate;

		private Long endDate;

		private String priority;

		private List<String> destinationsId;

		public String getContentId() {
			return contentId;
		}

		public void setContentId(String contentId) {
			this.contentId = contentId;
		}

		public String getContentStatus() {
			return contentStatus;
		}

		public void setContentStatus(String contentStatus) {
			this.contentStatus = contentStatus;
		}

		public Long getStartDate() {
			return startDate;
		}

		public void setStartDate(Long startDate) {
			this.startDate = startDate;
		}

		public Long getEndDate() {
			return endDate;
		}

		public void setEndDate(Long endDate) {
			this.endDate = endDate;
		}

		public String getPriority() {
			return priority;
		}

		public void setPriority(String priority) {
			this.priority = priority;
		}

		public List<String> getDestinations() {
			return destinationsId;
		}

		public void setDestinations(List<String> destinationsId) {
			this.destinationsId = destinationsId;
		}
	}

}
