package com.thales.ecms.model.dto;

/**
 * 
 * @author kumargupta.v
 *
 */
public class BundleForRouteGroupDTO {
	
	private String bundleForRouteGroupId;
	
	private String bundleId;

    private String routeGroupId;

    private String seatingClassId;

    private double ppaFee;

    private double ppvFee;        

    public String getBundleForRouteGroupId() {
		return bundleForRouteGroupId;
	}

	public void setBundleForRouteGroupId(String bundleForRouteGroupId) {
		this.bundleForRouteGroupId = bundleForRouteGroupId;
	}

	public String getBundleId() {
           return bundleId;
    }

    public void setBundleId(String bundleId) {
           this.bundleId = bundleId;
    }

    public String getRouteGroupId() {
           return routeGroupId;
    }

    public void setRouteGroupId(String routeGroupId) {
           this.routeGroupId = routeGroupId;
    }

    public String getSeatingClassId() {
           return seatingClassId;
    }

    public void setSeatingClassId(String seatingClassId) {
           this.seatingClassId = seatingClassId;
    }

    public double getPpaFee() {
           return ppaFee;
    }

    public void setPpaFee(double ppaFee) {
           this.ppaFee = ppaFee;
    }

    public double getPpvFee() {
           return ppvFee;
    }

    public void setPpvFee(double ppvFee) {
           this.ppvFee = ppvFee;
    }

}
