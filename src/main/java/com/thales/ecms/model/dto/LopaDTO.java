package com.thales.ecms.model.dto;

import java.util.List;

public class LopaDTO {

	private String lopaInternalId;
	
	private String lopaName;
	
	private String aircraftTypeId;
	
	private String aircraftType;
	
	private List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList;

	public String getLopaInternalId() {
		return lopaInternalId;
	}

	public void setLopaInternalId(String lopaInternalId) {
		this.lopaInternalId = lopaInternalId;
	}

	public String getLopaName() {
		return lopaName;
	}

	public void setLopaName(String lopaName) {
		this.lopaName = lopaName;
	}

	public String getAircraftTypeId() {
		return aircraftTypeId;
	}

	public void setAircraftTypeId(String aircraftTypeId) {
		this.aircraftTypeId = aircraftTypeId;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
	
	public List<SeatingClassLruTypeMappingDTO> getSeatingClassLruTypeMappingDTOList() {
		return seatingClassLruTypeMappingDTOList;
	}

	public void setSeatingClassLruTypeMappingDTOList(
			List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList) {
		this.seatingClassLruTypeMappingDTOList = seatingClassLruTypeMappingDTOList;
	}
}
