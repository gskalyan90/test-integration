package com.thales.ecms.model.dto;

import java.util.List;

/**
 * @author Mihir
 * Request DTO for a field of a Title - used when creating a Title
 */
public class TitleFieldRequestDTO {

	private String fieldId;
	
	//This list will just contain just one TitleFieldValueRequestDTO for fields whose fieldTypes are long, integer, double, boolean and date
	//This is because these fieldTypes are non-translatable 
	private List<TitleFieldValueRequestDTO> fieldValues;

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public List<TitleFieldValueRequestDTO> getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(List<TitleFieldValueRequestDTO> fieldValues) {
		this.fieldValues = fieldValues;
	}
	
}
