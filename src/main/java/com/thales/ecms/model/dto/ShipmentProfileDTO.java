package com.thales.ecms.model.dto;

import java.util.Set;

/**
 * @author kumargupta.v
 *
 */
public class ShipmentProfileDTO {

	private String shipmentProfileId;

	private String name;
	
	private int hardDrives;
	
	private String shippingType;
		
	private Set<String> locationIds;
	
	public Set<String> getLocationIds() {
		return locationIds;
	}

	public void setLocationIds(Set<String> locationIds) {
		this.locationIds = locationIds;
	}

	public String getShipmentProfileId() {
		return shipmentProfileId;
	}

	public void setShipmentProfileId(String shipmentProfileId) {
		this.shipmentProfileId = shipmentProfileId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHardDrives() {
		return hardDrives;
	}

	public void setHardDrives(int hardDrives) {
		this.hardDrives = hardDrives;
	}

	public String getShippingType() {
		return shippingType;
	}

	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}

}
