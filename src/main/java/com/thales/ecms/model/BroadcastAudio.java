package com.thales.ecms.model;

import java.util.List;

import com.psddev.dari.db.Recordable.Indexed;
import com.thales.ecms.model.util.TextClass;

public class BroadcastAudio extends ContentType{

	//just added this to make viewing and searching easier in CMS. Might not use this later 
	@Indexed
	private String englishTitle;

	@Indexed
	private List<TextClass> channelTitle;

	@Indexed
	private int contentPosition;

	public String getEnglishTitle() {
		return englishTitle;
	}

	public void setEnglishTitle(String englishTitle) {
		this.englishTitle = englishTitle;
	}

	public List<TextClass> getChannelTitle() {
		return channelTitle;
	}

	public void setChannelTitle(List<TextClass> channelTitle) {
		this.channelTitle = channelTitle;
	}

	public int getContentPosition() {
		return contentPosition;
	}

	public void setContentPosition(int contentPosition) {
		this.contentPosition = contentPosition;
	}

}
