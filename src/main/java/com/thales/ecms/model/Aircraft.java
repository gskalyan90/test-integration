package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.Indexed;

/**
 * @author Mihir
 * A type of aircraft such as Boeing 777-200, etc.
 */
public class Aircraft extends Content {

	@Indexed (unique = true)
	@Required
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}