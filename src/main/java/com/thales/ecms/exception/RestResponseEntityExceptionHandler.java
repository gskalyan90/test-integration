package com.thales.ecms.exception;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.gson.Gson;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;

/**
 * @author Mihir
 * Global Exception Handler 
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{

	private static Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);
	
	@ExceptionHandler(EmptyResponseException.class)
	protected ResponseEntity<ErrorResponse> handleEmptyResponseException(EmptyResponseException ex, WebRequest req){
		logger.info("Empty Response Exception thrown");
		logger.info("Exception message: "+ex.getErrorResponse().getErrorMessage());
		
		ErrorResponse errorResponse = ex.getErrorResponse();
		return new ResponseEntity<ErrorResponse>(errorResponse, errorResponse.getErrorStatusCode());
		
	}
	
	@ExceptionHandler(ObjectExistsException.class)
	protected ResponseEntity<ErrorResponse> handleObjectExistsException(ObjectExistsException ex, WebRequest req){
		logger.info("Object Exists Exception thrown");
		logger.info("Exception message: " +ex.getErrorResponse().getErrorMessage());
		
		ErrorResponse errorResponse = ex.getErrorResponse();
		return new ResponseEntity<ErrorResponse>(errorResponse, errorResponse.getErrorStatusCode());
	}
	
	@ExceptionHandler(ObjectNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleObjectNotFoundException(ObjectNotFoundException ex, WebRequest req){
		logger.info("Object Not Found Exception thrown");
		logger.info("Exception message: " +ex.getErrorResponse().getErrorMessage());
		
		ErrorResponse errorResponse = ex.getErrorResponse();
		return new ResponseEntity<ErrorResponse>(errorResponse, errorResponse.getErrorStatusCode());
	}
	
	@ExceptionHandler(BadRequestException.class)
	protected ResponseEntity<ErrorResponse> handleBadRequestException(BadRequestException ex, WebRequest req){
		logger.info("Bad Request Exception thrown");
		logger.info("Exception message: " +ex.getErrorResponse().getErrorMessage());
		
		ErrorResponse errorResponse = ex.getErrorResponse();
		return new ResponseEntity<ErrorResponse>(errorResponse, errorResponse.getErrorStatusCode());
	}
	
	@ExceptionHandler(DuplicateEntryException.class)
	protected ResponseEntity<ErrorResponse> handleDuplicateURLException(DuplicateEntryException ex, WebRequest req){
		logger.info("Duplicate URL Exception thrown");
		logger.info("Exception message: " +ex.getErrorResponse().getErrorMessage());
		
		ErrorResponse errorResponse = ex.getErrorResponse();
		return new ResponseEntity<ErrorResponse>(errorResponse, errorResponse.getErrorStatusCode());
	}
	
	@ExceptionHandler(UnsupportedEncodingException.class)
	protected ResponseEntity<ErrorResponse> handleUnsupportedEncodingException(UnsupportedEncodingException ex, WebRequest req){
		logger.info("Unsupported Encoding Exception thrown");
		logger.info("Exception message: "+ex.getMessage());
		
		ErrorResponse errorResponse = new ErrorResponse("Cannot decode parameter - "+ex.getMessage(), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<ErrorResponse>(errorResponse, errorResponse.getErrorStatusCode());
	}
	
	@ExceptionHandler(ObjectAssociationExistsException.class)
	protected ResponseEntity<ErrorResponse> handleObjectNotFoundException(ObjectAssociationExistsException ex, WebRequest req){
		logger.info("Object Association Exists Exception thrown");
		logger.info("Exception message: " +ex.getErrorResponse().getErrorMessage());
		
		ErrorResponse errorResponse = ex.getErrorResponse();
		return new ResponseEntity<ErrorResponse>(errorResponse, errorResponse.getErrorStatusCode());
	}
//	@ExceptionHandler(EmptyResponseException.class)
//	@ResponseBody
//	protected String handleEmptyResponseException(EmptyResponseException ex, WebRequest req){
//		logger.info("Empty Response Exception thrown");
//		logger.info("Exception message: "+ex.getErrorResponse().getErrorMessage());
//		
//		ErrorResponse errorResponse = ex.getErrorResponse();
//		String responseString = new Gson().toJson(errorResponse);
//		return responseString;
//	}
}
