package com.thales.ecms.exception;

public class ObjectNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MESSAGE = "The object does not exist";
	
	private ErrorResponse errorResponse;
	
	public ObjectNotFoundException() {
		super(DEFAULT_MESSAGE);
	}
	
	public ObjectNotFoundException(String message){
		super(message);
	}
	
	public ObjectNotFoundException(ErrorResponse errorResponse){
		super(errorResponse.getErrorMessage());
		this.errorResponse = errorResponse;
	}

	public ErrorResponse getErrorResponse() {
		return this.errorResponse;
	}
	
}
