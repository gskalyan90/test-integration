package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.cms.tool.CmsTool;
import com.psddev.dari.db.Database;
import com.psddev.dari.db.DatabaseEnvironment;
import com.psddev.dari.db.ObjectField;
import com.psddev.dari.db.ObjectIndex;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.psddev.dari.db.Record;
import com.psddev.dari.db.State;
import com.psddev.dari.util.UuidUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Field;
import com.thales.ecms.model.dto.FieldDTO;
import com.thales.ecms.model.dto.FieldRequestDTO;
import com.thales.ecms.model.dto.FieldResponseDTO;
import com.thales.ecms.model.dto.GenericContentTypeRequestDTO;
import com.thales.ecms.model.dto.GenericContentTypeResponseDTO;
import com.thales.ecms.model.dto.SpecificContentTypeDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Service Class to perform operations on a Generic Content Type
 */
@Service
public class GenericContentTypeService {

	public static Logger logger = LoggerFactory.getLogger(GenericContentTypeService.class);

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private FieldService fieldService;

	/**
	 * Service Method to create a Generic Content Type
	 * @param genericContentTypeRequestDTO
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 */
	public GenericContentTypeResponseDTO addGenericContentType(GenericContentTypeRequestDTO genericContentTypeRequestDTO) throws NoSuchMessageException, ObjectExistsException, BadRequestException{

		String fieldId = null;
		String fieldDisplayName = null;
		String fieldType = null;
		boolean indexed;
		Field field = null;
		
		ObjectType objectType = null;
		ObjectField objectField = null;
		List<ObjectField> objectFields = null;

		ObjectIndex objectIndex = null;
		List<ObjectIndex> indexes = new ArrayList<>();

		List<String> labelFields = new ArrayList<>();
		
		GenericContentTypeResponseDTO genericContentTypeResponseDTO = null;

		String displayName = genericContentTypeRequestDTO.getContentTypeName();
		if(StringUtils.isNotBlank(displayName)){

			String internalName = displayName.replaceAll(" ", "");
			internalName = internalName.substring(0, 1).toUpperCase() + internalName.substring(1);
			internalName = "com.thales.ecms.model."+internalName;
			
			//check if the generic content type already exists
			objectType = Query.from(ObjectType.class).where("internalName = ?",internalName).first();
			if(objectType != null){
				logger.error("An ObjectType with this name already exists");
				throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE, displayName}, Locale.US), HttpStatus.FORBIDDEN));
			}

			objectType = new ObjectType();
			objectType.setDisplayName(displayName);
			objectType.setInternalName(internalName);

			ObjectType contentType = Query.from(ObjectType.class).where("internalName = ?","com.thales.ecms.model.ContentType").first();
			
			objectFields = new ArrayList<>();
			objectFields.addAll(contentType.getFields()); //Adding the fields of com.thales.ecms.model.ContentType
			
			//adding the list of fields
			List<FieldRequestDTO> fields = genericContentTypeRequestDTO.getFields();
			if(!CollectionUtils.isEmpty(fields)){
				
				for(FieldRequestDTO fieldRequestDTO : fields){
					fieldId = fieldRequestDTO.getFieldId();
					field = Query.from(Field.class).where("fieldId = ?",fieldId).first();
					if(field == null){
						//This field does not exist. Create it
						field = createGenericField(fieldRequestDTO);
					}
					indexed = fieldRequestDTO.isIndexed();
					
					objectField = new ObjectField(objectType, null);
					fieldId = field.getFieldId();
					objectField.setInternalName(fieldId);
					objectField.setJavaFieldName(fieldId);
					fieldDisplayName = fieldRequestDTO.getDisplayName();
					if(StringUtils.isNotBlank(fieldDisplayName)){
						objectField.setDisplayName(fieldDisplayName);
					}
					else{
						logger.error("Display Name of the Field is empty");
						throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
					}
					
					//Check if the field should be indexed
					if(indexed){
						objectIndex = new ObjectIndex(objectType, null);
						objectIndex.setField(fieldId);
						objectIndex.setJavaDeclaringClassName(internalName);
					}
					
					//setting the fieldType
					fieldType = field.getFieldType();
					String fieldDataType = null;
					switch(fieldType){
						case "text": fieldDataType = "com.thales.ecms.model.datatype.TextType";
								break;
						case "long text": fieldDataType = "com.thales.ecms.model.datatype.ReferentialTextType";
								break;
						case "integer": fieldDataType = "com.thales.ecms.model.datatype.IntegerType";
								break;
						case "long": fieldDataType = "com.thales.ecms.model.datatype.LongType";
								break;
						case "double": fieldDataType = "com.thales.ecms.model.datatype.DoubleType";
								break;
						case "boolean": fieldDataType = "com.thales.ecms.model.datatype.BooleanType";
								break;
						case "date": fieldDataType = "com.thales.ecms.model.datatype.DateType";
								break;
						case "image": fieldDataType = "com.thales.ecms.model.datatype.ImageType";
								break;
						case "selection":
							String selectionListName = field.getSelectionListName();
							fieldDataType = "com.thales.ecms.model.datatype."+selectionListName+"Type";
							logger.info(fieldDataType);
							break;
					}
					ObjectType dataType = Query.from(ObjectType.class).where("internalName = ?", fieldDataType).first();
					Set<ObjectType> types = new HashSet<>();
					types.add(dataType);
					objectField.setTypes(types);
					objectField.setInternalType("list/record");
					
					if(indexed){
						objectIndex.setType("record");
						indexes.add(objectIndex);
					}
					
					objectField.setJavaDeclaringClassName(internalName);
					objectFields.add(objectField);
				}

				objectType.setFields(objectFields);

				objectType.setIndexes(indexes);

				//setting the label fields
				labelFields.add("englishTitle");
				objectType.setLabelFields(labelFields);

			}

			//setting the objectClass name
			objectType.setObjectClassName(internalName);
			
			//setting the modification Classes
			Set<String> modificationClasses = new HashSet<>();
			modificationClasses.add(internalName);
			objectType.setModificationClasses(modificationClasses);

			//setting the super classes
			List<String> superClassNames = new ArrayList<>();
			superClassNames.add(internalName);
			superClassNames.addAll(contentType.getSuperClassNames());
			objectType.setSuperClassNames(superClassNames);

			//setting the groups
			Set<String> groups = new HashSet<>();
			groups.add(internalName);
			groups.addAll(contentType.getGroups());
			objectType.setGroups(groups);
			
			//setting the assignable classes
			Set<String> assignableClassNames = new HashSet<>();
			assignableClassNames.add(internalName);
			assignableClassNames.addAll(contentType.getAssignableClassNames());
			objectType.setAssignableClassNames(assignableClassNames);
			
			//Instances of a Generic Content Type should never be created, hence set it as abstract
			objectType.setAbstract(true);

			objectType.as(Site.ObjectModification.class).setGlobal(true);
			objectType.save();
			
			DatabaseEnvironment env = Database.Static.getDefault().getEnvironment();
			env.refreshGlobals();
			env.refreshTypes();
			env.refreshGlobals();
			env.refreshTypes();
			
//			CmsTool cmsTool = Query.from(CmsTool.class).where("name = ?", "CMS").first();
//			cmsTool.getCommonContentSettings().getCreateNewTypes().add(objectType);
//			cmsTool.save();
			genericContentTypeResponseDTO = createResponseDTO(objectType);
		}
		else{
			logger.error("Display Name of the Content Type is empty");
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
		return genericContentTypeResponseDTO;
	}

	/**
	 * Service Method to return a list of Generic Content Types
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public List<GenericContentTypeResponseDTO> getGenericContentTypes() throws NoSuchMessageException, ObjectNotFoundException{

		List<GenericContentTypeResponseDTO> genericContentTypeResponseDTOList = null;
		GenericContentTypeResponseDTO genericContentTypeResponseDTO = null;

		List<ObjectType> objectTypeList = Query.from(ObjectType.class).where("cms.site.isGlobal = ?",true).selectAll();

		if(!CollectionUtils.isEmpty(objectTypeList)){
			genericContentTypeResponseDTOList = new ArrayList<>();
			for(ObjectType objectType : objectTypeList){
				
				if(!objectType.getInternalName().equalsIgnoreCase("com.thales.ecms.model.ContentType") && objectType.getSuperClassNames().contains("com.thales.ecms.model.ContentType")){	
					logger.info("Found a Generic Content Type");
					genericContentTypeResponseDTO = new GenericContentTypeResponseDTO();
					genericContentTypeResponseDTO.setContentTypeInternalId(objectType.getId().toString());
					genericContentTypeResponseDTO.setContentTypeName(objectType.getDisplayName());
					genericContentTypeResponseDTOList.add(genericContentTypeResponseDTO);
				}
			}
		}
		else{
			logger.error("No Generic Content Types exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPES}, Locale.US), HttpStatus.NOT_FOUND));
		}
		

//		ObjectType objectType2 = Query.from(ObjectType.class).where("name = ?","Sample Content Type").first();
//		Map<String, Object> stateMap = objectType2.getState().getValues();
//		Object isGlobalValue = stateMap.get("cms.site.isGlobal");
//		boolean isGlobal = (boolean) isGlobalValue;
//		logger.info("Value:"+isGlobal);
		
		return genericContentTypeResponseDTOList;
	}

	/**
	 * Service Method to return a Generic Content Type
	 * @param contentTypeId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public GenericContentTypeResponseDTO getGenericContentTypeById(String contentTypeId) throws NoSuchMessageException, ObjectNotFoundException{

		GenericContentTypeResponseDTO genericContentTypeResponseDTO = null;

		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",contentTypeId).first();
		if(objectType != null){

			genericContentTypeResponseDTO = createResponseDTO(objectType);
		}
		else{
			logger.error("Generic Content Type does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return genericContentTypeResponseDTO;
	}

	/**
	 * Service Method to delete a Generic Content Type
	 * @param contentTypeId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public boolean deleteGenericContentType(String contentTypeId) throws NoSuchMessageException, ObjectNotFoundException{

		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",contentTypeId).first();
		if(objectType != null){
			objectType.delete();
			return true;
		}
		else{
			logger.error("Generic Content Type Not Found");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
	
	/**
	 * Helper method to create a Field
	 * @param fieldRequestDTO
	 * @return
	 * @throws BadRequestException 
	 * @throws NoSuchMessageException 
	 */
	public Field createGenericField(FieldRequestDTO fieldRequestDTO) throws NoSuchMessageException, BadRequestException{
		
		Field field = null;
		String fieldId = fieldRequestDTO.getFieldId();
		String fieldType = fieldRequestDTO.getFieldType();
		
		if(StringUtils.isNotBlank(fieldId) && StringUtils.isNotBlank(fieldType)){
			field = new Field();
			field.setFieldId(fieldId);
//			field.setDisplayName(displayName);
			field.setFieldType(fieldType);
			if(fieldType.equalsIgnoreCase("selection")){
				String selectionListName = fieldRequestDTO.getSelectionListName();
				if(StringUtils.isNotBlank(selectionListName)){
					field.setSelectionListName(selectionListName);
					logger.info("Field Type is Selection");
					logger.info("Creating a Data Type corresponding to this List");
					//fieldService.createSelectionDataType(selectionListName);
				}
				else{
					logger.error("Name of the selection list is empty");
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}
			}
			field.as(Site.ObjectModification.class).setGlobal(true);
			field.save();
			return field;
		}
		else{
			logger.error("Either fieldId or fieldType is empty");
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
	}
	
	/**
	 * Helper Method to create a Generic Content Type Response DTO
	 * @param objectType
	 * @return
	 */
	public GenericContentTypeResponseDTO createResponseDTO(ObjectType objectType){
		
		List<FieldResponseDTO> fieldResponseDTOList = new ArrayList<>();
		FieldResponseDTO fieldResponseDTO = null;
		Field field = null;
		
		GenericContentTypeResponseDTO genericContentTypeResponseDTO = new GenericContentTypeResponseDTO();
		genericContentTypeResponseDTO.setContentTypeInternalId(objectType.getId().toString());
		genericContentTypeResponseDTO.setContentTypeName(objectType.getDisplayName());

		List<ObjectField> objectFields = objectType.getFields();
		for(ObjectField objectField : objectFields){
			fieldResponseDTO = new FieldResponseDTO();
			String fieldId = objectField.getInternalName();
			if(!fieldId.equalsIgnoreCase("status") && !fieldId.equalsIgnoreCase("englishTitle")){
				field = Query.from(Field.class).where("fieldId = ?",fieldId).first();
				fieldResponseDTO.setFieldInternalId(field.getId().toString());
				fieldResponseDTO.setFieldId(field.getFieldId());
				fieldResponseDTO.setDisplayName(objectField.getDisplayName());
				fieldResponseDTO.setFieldType(field.getFieldType());
				if(field.getFieldType().equalsIgnoreCase("selection") && StringUtils.isNotBlank(field.getSelectionListName())){
					fieldResponseDTO.setSelectionListName(field.getSelectionListName());
				}
				fieldResponseDTOList.add(fieldResponseDTO);
			}
		}
		genericContentTypeResponseDTO.setFields(fieldResponseDTOList);
		return genericContentTypeResponseDTO;
	}

	public String addSpecificContentTypes(String airlineId, SpecificContentTypeDTO specificContentTypeDTO) throws NoSuchMessageException, ObjectExistsException, ObjectNotFoundException, BadRequestException{

		ObjectType objectType = null;
		String internalName = null;

		List<ObjectField> objectFields = null;
		ObjectField objectField = null;

		List<ObjectIndex> indexes = new ArrayList<>();
		ObjectIndex objectIndex = null;

		String fieldName = null;
		String fieldType = null;

		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();

		String displayName = specificContentTypeDTO.getDisplayName();
		if(StringUtils.isNotBlank(displayName)){

			//check if the content type already exists
			objectType = Query.from(ObjectType.class).where("name = ?",displayName).first();
			if(objectType != null){
				logger.error("An ObjectType with this name already exists");
				throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
			}

			objectType = new ObjectType();
			objectType.setDisplayName(displayName);
			internalName = displayName.replaceAll(" ", "");
			internalName = internalName.substring(0, 1).toUpperCase() + internalName.substring(1);
			objectType.setInternalName("com.thales.ecms.model."+internalName);
		}

		String genericContentTypeName = specificContentTypeDTO.getGenericContentTypeName();
		String genericContentTypeInternalName = genericContentTypeName.replaceAll(" ", "");
		genericContentTypeInternalName = genericContentTypeInternalName.substring(0, 1) + genericContentTypeInternalName.substring(1);
		genericContentTypeInternalName = "com.thales.ecms.model."+genericContentTypeInternalName;
		ObjectType genericContentType = Query.from(ObjectType.class).where("internalName = ?",genericContentTypeInternalName).first();

		//setting the fields
		if(genericContentType != null){
			objectFields = new ArrayList<>();
			objectFields.addAll(genericContentType.getFields()); //This adds the fields of the generic content type

			//adding the specific fields
			List<FieldDTO> fields = specificContentTypeDTO.getFields();
			if(!CollectionUtils.isEmpty(fields)){

				for(FieldDTO fieldDTO : fields){
					fieldName = fieldDTO.getFieldName();
					boolean indexed = fieldDTO.isIndexed();

					if(StringUtils.isNotBlank(fieldName)){
						objectField = new ObjectField(objectType, null);
						objectField.setInternalName(fieldName);
						objectField.setJavaFieldName(fieldName);

						if(indexed){
							objectIndex = new ObjectIndex(objectType, null);
							objectIndex.setField(fieldName);
						}
					}
					else{
						logger.error("Empty field name");
						throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
					}

					fieldType = fieldDTO.getFieldType();
					if(StringUtils.isNotBlank(fieldType)){

						if(fieldType.equalsIgnoreCase("image")){
							ObjectType imageType = Query.from(ObjectType.class).where("internalName = ?","com.thales.ecms.model.Image").first();
							Set<ObjectType> types = new HashSet<>();
							types.add(imageType);
							objectField.setTypes(types);
							objectField.setInternalType("record");
						}
						else{
							objectField.setInternalType(fieldType);
							if(indexed){
								objectIndex.setType(fieldType);
							}
						}
					}
					else{
						logger.error("Empty field type");
						throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
					}

					objectField.setJavaDeclaringClassName("com.thales.ecms.model."+internalName);
					objectFields.add(objectField);

					if(indexed){
						objectIndex.setJavaDeclaringClassName("com.thales.ecms.model."+internalName);
						indexes.add(objectIndex);
					}
				}
				objectType.setFields(objectFields);

				//setting the indexes
				objectType.setIndexes(indexes);

				//setting the label fields
				objectType.setLabelFields(genericContentType.getLabelFields());
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse("Generic Content Type does not exist", HttpStatus.OK));
		}
		//setting the objectClass name
		objectType.setObjectClassName("com.thales.ecms.model."+internalName);

		//setting the super classes
		List<String> superClassNames = new ArrayList<>();
		superClassNames.add("com.thales.ecms.model."+internalName);
		superClassNames.add(genericContentTypeInternalName);
		superClassNames.addAll(genericContentType.getSuperClassNames());
		objectType.setSuperClassNames(superClassNames);

		//setting the groups
		Set<String> groups = new HashSet<>();
		groups.add("com.thales.ecms.model."+internalName);
		groups.add(genericContentTypeInternalName);
		groups.addAll(genericContentType.getGroups());
		objectType.setGroups(groups);

		//objectType.as(Site.ObjectModification.class).setGlobal(true);
		objectType.as(Site.ObjectModification.class).setOwner(airline);
		objectType.save();

		return "Specific Content Type created successfully";
	}

	public String createContentTypeInstances(String contentTypeId){

		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",contentTypeId).first();
		List<ObjectField> objectFields = objectType.getFields();

		Map<String, Object> fieldMap = new HashMap<>();

		Object newObject = objectType.createObject(UuidUtils.createSequentialUuid());
		logger.info(newObject.getClass().getName()); //this gives com.psddev.dari.db.Record
		if(newObject instanceof Record){
			Record newRecord = (Record) newObject; //casting it to Record
			newRecord.save();
			logger.info(newRecord.getId().toString()); //retrieving the id

			State state = State.getInstance(newRecord);

			for(ObjectField objectField : objectFields){
				logger.info(objectField.getDisplayName());
				fieldMap.put(objectField.getInternalName(), "Test Title for dynamic content type");
				logger.info("Added to the fieldMap");
			}
			state.setValues(fieldMap);
			newRecord.setState(state);
			newRecord.save();
			logger.info(newRecord.getId().toString()); //retrieving the id
		}
		return "Instance created successfully";
	}
}
