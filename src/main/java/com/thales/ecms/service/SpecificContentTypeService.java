package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.cms.tool.CmsTool;
import com.psddev.dari.db.Database;
import com.psddev.dari.db.DatabaseEnvironment;
import com.psddev.dari.db.ObjectField;
import com.psddev.dari.db.ObjectIndex;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Field;
import com.thales.ecms.model.dto.FieldRequestDTO;
import com.thales.ecms.model.dto.FieldResponseDTO;
import com.thales.ecms.model.dto.SpecificContentTypeRequestDTO;
import com.thales.ecms.model.dto.SpecificContentTypeResponseDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Service Class to perform operations on Specific Content Types
 */
@Service
public class SpecificContentTypeService {

	public static Logger logger = LoggerFactory.getLogger(SpecificContentTypeService.class);
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Service Method to create a Specific Content Type
	 * @param airlineId
	 * @param specificContentTypeRequestDTO
	 * @return
	 * @throws ObjectNotFoundException 
	 * @throws NoSuchMessageException 
	 * @throws BadRequestException 
	 * @throws ObjectExistsException 
	 */
	public SpecificContentTypeResponseDTO addSpecificContentType(String airlineId, SpecificContentTypeRequestDTO specificContentTypeRequestDTO) throws NoSuchMessageException, ObjectNotFoundException, BadRequestException, ObjectExistsException{
		
		String fieldId = null;
		String fieldDisplayName = null;
		String fieldType = null;
		boolean indexed;
		Field field = null;
		
		ObjectType objectType = null;
		ObjectField objectField = null;
		List<ObjectField> objectFields = null;

		ObjectIndex objectIndex = null;
		List<ObjectIndex> indexes = new ArrayList<>();
		
		List<String> labelFields = new ArrayList<>();
		
		SpecificContentTypeResponseDTO specificContentTypeResponseDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		ObjectType parentContentType = null;
		
		if(airline != null){
			
			String displayName = specificContentTypeRequestDTO.getContentTypeName();
			if(StringUtils.isNotBlank(displayName)){
				
				String internalName = displayName.replaceAll(" ", "");
				internalName = internalName.substring(0, 1).toUpperCase() + internalName.substring(1);
				internalName = "com.thales.ecms.model."+internalName;
				
				//check if the specific content type already exists
				objectType = Query.from(ObjectType.class).where("internalName = ?",internalName).first();
				if(objectType != null){
					logger.error("An ObjectType with this name already exists");
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPE, displayName}, Locale.US), HttpStatus.FORBIDDEN));
				}
				
				objectType = new ObjectType();
				objectType.setDisplayName(displayName);
				objectType.setInternalName(internalName);
				
				String parentContentTypeName = specificContentTypeRequestDTO.getParentContentType();
				if(StringUtils.isNotBlank(parentContentTypeName)){
					String parentContentTypeInternalName = parentContentTypeName.replaceAll(" ", "");
					parentContentTypeInternalName = parentContentTypeInternalName.substring(0, 1).toUpperCase() + parentContentTypeInternalName.substring(1);
					parentContentTypeInternalName = "com.thales.ecms.model."+parentContentTypeInternalName;
					parentContentType = Query.from(ObjectType.class).where("internalName = ?", parentContentTypeInternalName).first();
					if(parentContentType != null){
						objectFields = new ArrayList<>();
						objectFields.addAll(parentContentType.getFields());
						indexes.addAll(parentContentType.getIndexes());
					}
					else{
						logger.error("Parent Content Type does not exist");
						throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US), HttpStatus.NOT_FOUND));
					}
				}
				else{
					logger.error("Parent Content Type Name is Empty");
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.NOT_FOUND));
				}
				
				//adding the list of fields
				List<FieldRequestDTO> fields = specificContentTypeRequestDTO.getFields();
				if(!CollectionUtils.isEmpty(fields)){
					
					for(FieldRequestDTO fieldRequestDTO : fields){
						fieldId = fieldRequestDTO.getFieldId();
						field = Query.from(Field.class).where("fieldId = ?",fieldId).and("cms.site.owner = ?",airline).first();
						if(field == null){
							//This field does not exist. Create it
							field = createSpecificField(fieldRequestDTO, airline);
						}
						indexed = fieldRequestDTO.isIndexed();
						
						objectField = new ObjectField(objectType, null);
						fieldId = field.getFieldId();
						objectField.setInternalName(fieldId);
						objectField.setJavaFieldName(fieldId);
						fieldDisplayName = fieldRequestDTO.getDisplayName();
						if(StringUtils.isNotBlank(fieldDisplayName)){
							objectField.setDisplayName(fieldDisplayName);
						}
						else{
							logger.error("Display Name of the Field is empty");
							throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
						}
						
						//Check if the field should be indexed
						if(indexed){
							objectIndex = new ObjectIndex(objectType, null);
							objectIndex.setField(fieldId);
							objectIndex.setJavaDeclaringClassName(internalName);
						}
						
						//setting the fieldType
						fieldType = field.getFieldType();
						String fieldDataType = null;
						switch(fieldType){
							case "text": fieldDataType = "com.thales.ecms.model.datatype.TextType";
									break;
							case "long text": fieldDataType = "com.thales.ecms.model.datatype.ReferentialTextType";
									break;
							case "integer": fieldDataType = "com.thales.ecms.model.datatype.IntegerType";
									break;
							case "long": fieldDataType = "com.thales.ecms.model.datatype.LongType";
									break;
							case "double": fieldDataType = "com.thales.ecms.model.datatype.DoubleType";
									break;
							case "boolean": fieldDataType = "com.thales.ecms.model.datatype.BooleanType";
									break;
							case "date": fieldDataType = "com.thales.ecms.model.datatype.DateType";
									break;
							case "image": fieldDataType = "com.thales.ecms.model.datatype.ImageType";
									break;
							case "selection":
								String selectionListName = field.getSelectionListName();
								fieldDataType = "com.thales.ecms.model.datatype."+selectionListName+"Type";
								logger.info(fieldDataType);
								break;
						}
						ObjectType dataType = Query.from(ObjectType.class).where("internalName = ?", fieldDataType).first();
						Set<ObjectType> types = new HashSet<>();
						types.add(dataType);
						objectField.setTypes(types);
						objectField.setInternalType("list/record");
						
						if(indexed){
							objectIndex.setType("record");
							indexes.add(objectIndex);
						}
						
						objectField.setJavaDeclaringClassName(internalName);
						objectFields.add(objectField);
					}
				}
				
				objectType.setFields(objectFields);

				objectType.setIndexes(indexes);

				//setting the label fields
				labelFields.addAll(parentContentType.getLabelFields());
				objectType.setLabelFields(labelFields);
				
				//setting the objectClass name
				objectType.setObjectClassName(internalName);
				
				//setting the modification Classes
				Set<String> modificationClasses = new HashSet<>();
				modificationClasses.add(internalName);
				objectType.setModificationClasses(modificationClasses);
				
				//setting the super classes
				List<String> superClassNames = new ArrayList<>();
				superClassNames.add(internalName);
				superClassNames.addAll(parentContentType.getSuperClassNames());
				objectType.setSuperClassNames(superClassNames);
				
				//setting the groups
				Set<String> groups = new HashSet<>();
				groups.add(internalName);
				groups.addAll(parentContentType.getGroups());
				objectType.setGroups(groups);
				
				//setting the assignable classes
				Set<String> assignableClassNames = new HashSet<>();
				assignableClassNames.add(internalName);
				assignableClassNames.addAll(parentContentType.getAssignableClassNames());
				objectType.setAssignableClassNames(assignableClassNames);
				
				objectType.as(Site.ObjectModification.class).setOwner(airline);
				objectType.save();
				
				DatabaseEnvironment env = Database.Static.getDefault().getEnvironment();
				env.refreshGlobals();
				env.refreshTypes();
				env.refreshGlobals();
				env.refreshTypes();
				
				CmsTool cmsTool = Query.from(CmsTool.class).where("name = ?", "CMS").first();
				cmsTool.getCommonContentSettings().getCreateNewTypes().add(objectType);
				cmsTool.save();
				
				specificContentTypeResponseDTO = createResponseDTO(objectType);
			}
			else{
				logger.error("Display Name of the Content Type is empty");
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRLINE}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return specificContentTypeResponseDTO;
	}
	
	/**
	 * Service Method to return a list of Specific Content Types of an Airline
	 * @param airlineId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public List<SpecificContentTypeResponseDTO> getSpecificContentTypes(String airlineId) throws NoSuchMessageException, ObjectNotFoundException{
		
		List<SpecificContentTypeResponseDTO> specificContentTypeResponseDTOList = null;
		SpecificContentTypeResponseDTO specificContentTypeResponseDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			
			List<ObjectType> objectTypeList = Query.from(ObjectType.class).where("cms.site.owner = ?",airline).selectAll();
			if(!CollectionUtils.isEmpty(objectTypeList)){
				specificContentTypeResponseDTOList = new ArrayList<>();
				for(ObjectType objectType : objectTypeList){
					if(objectType.getSuperClassNames().contains("com.thales.ecms.model.ContentType")){
						
						specificContentTypeResponseDTO = new SpecificContentTypeResponseDTO();
						specificContentTypeResponseDTO.setContentTypeInternalId(objectType.getId().toString());
						specificContentTypeResponseDTO.setContentTypeName(objectType.getDisplayName());
						
						List<String> superClassNames = objectType.getSuperClassNames();
						for(String superClassName : superClassNames){
							if(!superClassName.equalsIgnoreCase("com.thales.ecms.model.ContentType") && !superClassName.equalsIgnoreCase(objectType.getInternalName()) && superClassName.startsWith("com.thales.ecms.model.")){
								ObjectType superClass = Query.from(ObjectType.class).where("internalName = ?", superClassName).first();
								specificContentTypeResponseDTO.setParentContentType(superClass.getDisplayName());
								break;
							}
						}
						specificContentTypeResponseDTOList.add(specificContentTypeResponseDTO);
					}
				}
			}
			else{
				logger.error("No Specific Content Types exist");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPES}, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRLINE}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return specificContentTypeResponseDTOList;
	}
	
	/**
	 * Service Method to return a Specific Content Type
	 * @param specificContentTypeId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public SpecificContentTypeResponseDTO getSpecificContentTypeById(String specificContentTypeId) throws NoSuchMessageException, ObjectNotFoundException{
		
		SpecificContentTypeResponseDTO specificContentTypeResponseDTO = null;
		
		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",specificContentTypeId).first();
		if(objectType != null){
			specificContentTypeResponseDTO = createResponseDTO(objectType);
		}
		else{
			logger.error("Specific Content Type does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPE}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return specificContentTypeResponseDTO;
	}
	
	/**
	 * Service Method to delete a Specific Content Type
	 * @param specificContentTypeId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public boolean deleteSpecificContentType(String specificContentTypeId) throws NoSuchMessageException, ObjectNotFoundException{

		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",specificContentTypeId).first();
		if(objectType != null){
			objectType.delete();
			return true;
		}
		else{
			logger.error("Specific Content Type Not Found");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPE}, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
	
	/**
	 * Helper method to create a Specific Field
	 * @param fieldRequestDTO
	 * @param airline
	 * @return
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 */
	public Field createSpecificField(FieldRequestDTO fieldRequestDTO, Site airline) throws NoSuchMessageException, BadRequestException{
		
		Field field = null;
		String fieldId = fieldRequestDTO.getFieldId();
		String fieldType = fieldRequestDTO.getFieldType();
		
		if(StringUtils.isNotBlank(fieldId) && StringUtils.isNotBlank(fieldType)){
			field = new Field();
			field.setFieldId(fieldId);
//			field.setDisplayName(displayName);
			field.setFieldType(fieldType);
			if(fieldType.equalsIgnoreCase("selection")){
				String selectionListName = fieldRequestDTO.getSelectionListName();
				if(StringUtils.isNotBlank(selectionListName)){
					field.setSelectionListName(selectionListName);
				}
				else{
					logger.error("Name of the selection list is empty");
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}
			}
			field.as(Site.ObjectModification.class).setOwner(airline);
			field.save();
			return field;
		}
		else{
			logger.error("Either fieldId or fieldType is empty");
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
	}
	
	public SpecificContentTypeResponseDTO createResponseDTO(ObjectType objectType){
		
		List<FieldResponseDTO> fieldResponseDTOList = new ArrayList<>();
		FieldResponseDTO fieldResponseDTO = null;
		Field field = null;
		
		SpecificContentTypeResponseDTO specificContentTypeResponseDTO = new SpecificContentTypeResponseDTO();
		specificContentTypeResponseDTO.setContentTypeInternalId(objectType.getId().toString());
		specificContentTypeResponseDTO.setContentTypeName(objectType.getDisplayName());
		List<String> superClassNames = objectType.getSuperClassNames();
		for(String superClassName : superClassNames){
			if(!superClassName.equalsIgnoreCase("com.thales.ecms.model.ContentType") && !superClassName.equalsIgnoreCase(objectType.getInternalName()) && superClassName.startsWith("com.thales.ecms.model.")){
				ObjectType superClass = Query.from(ObjectType.class).where("internalName = ?", superClassName).first();
				specificContentTypeResponseDTO.setParentContentType(superClass.getDisplayName());
				break;
			}
		}

		List<ObjectField> objectFields = objectType.getFields();
		for(ObjectField objectField : objectFields){
			fieldResponseDTO = new FieldResponseDTO();
			String fieldId = objectField.getInternalName();
			if(!fieldId.equalsIgnoreCase("status") && !fieldId.equalsIgnoreCase("englishTitle")){
				field = Query.from(Field.class).where("fieldId = ?",fieldId).first();
				fieldResponseDTO.setFieldInternalId(field.getId().toString());
				fieldResponseDTO.setFieldId(field.getFieldId());
				fieldResponseDTO.setDisplayName(objectField.getDisplayName());
				fieldResponseDTO.setFieldType(field.getFieldType());
				if(field.getFieldType().equalsIgnoreCase("selection") && StringUtils.isNotBlank(field.getSelectionListName())){
					fieldResponseDTO.setSelectionListName(field.getSelectionListName());
				}
				fieldResponseDTOList.add(fieldResponseDTO);
			}
		}
		specificContentTypeResponseDTO.setFields(fieldResponseDTOList);
		return specificContentTypeResponseDTO;
	}
}
