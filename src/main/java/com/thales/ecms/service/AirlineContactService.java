package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.airline.AirlineContact;
import com.thales.ecms.model.airline.AirlineContact.AirlineContactType;
import com.thales.ecms.model.airline.AirlinePortal;
import com.thales.ecms.model.dto.AirlineContactDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Service Class to perform CRUD operations on Airline Contacts
 */
@Service
public class AirlineContactService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(AirlineContactService.class);
	
	public AirlineContactDTO addAirlineContact(String airlineId, AirlineContactDTO airlineContactDetails) throws ObjectNotFoundException, BadRequestException, ObjectExistsException{
		
		AirlineContactDTO airlineContactDTO = null;
		AirlineContact airlineContact = new AirlineContact();
		List<AirlineContact> airlineContactList = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		
		if(airline != null){
			
			String firstName = airlineContactDetails.getFirstName();
			String lastName = airlineContactDetails.getLastName();
			if(StringUtils.isNotBlank(firstName) && StringUtils.isNotBlank(lastName)){
				if(Query.from(AirlineContact.class).where("firstName = ?",firstName).and("lastName = ?",lastName).first() != null){
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
							new Object[] { ECMSConstants.AIRLINE_CONTACT, ECMSConstants.NAME }, Locale.US), HttpStatus.FORBIDDEN));
				} else {
					airlineContact.setFirstName(firstName);
					airlineContact.setLastName(lastName);
				}
			} else {
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
			if(StringUtils.isNotBlank(airlineContactDetails.getPhone())){				
				if(Query.from(AirlineContact.class).where("phone = ?",airlineContactDetails.getPhone()).first() != null){
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
							new Object[] { ECMSConstants.AIRLINE_CONTACT, ECMSConstants.FIELD_PHONE }, Locale.US), HttpStatus.FORBIDDEN));
				}else{
					airlineContact.setPhone(airlineContactDetails.getPhone());
				}				
			}else{
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}			
			String email = airlineContactDetails.getEmail();
			if(email != null && StringUtils.isNotBlank(email)){
				if(Query.from(AirlineContact.class).where("email = ?",email).first() != null){
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
							new Object[] { ECMSConstants.AIRLINE_CONTACT, ECMSConstants.FIELD_EMAIL }, Locale.US), HttpStatus.FORBIDDEN));
				}else
					airlineContact.setEmail(email);
			}
			else{
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
			
			String airlineContactType = airlineContactDetails.getAirlineContactType();
			if(airlineContactType != null && StringUtils.isNotBlank(airlineContactType)){
				airlineContactType = airlineContactType.toUpperCase();
				try{
					airlineContact.setAirlineContactType(AirlineContactType.valueOf(airlineContactType));
				}catch(IllegalArgumentException e){
					logger.info("Enum Constant "+airlineContactType + " does not exist");
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}
			}
			else{
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
			
			airlineContact.as(Site.ObjectModification.class).setOwner(airline);
			airlineContact.save();
			
			airlineContactList = airline.as(AirlinePortal.class).getAirlineContactList();
			if(airlineContactList == null || airlineContactList.isEmpty()){
				airlineContactList = new ArrayList<>();
			}
			airlineContactList.add(airlineContact);
			
			airline.as(AirlinePortal.class).setAirlineContactList(airlineContactList);
			airline.save();
			
			//airlineContact = Query.from(AirlineContact.class).where("name = ?",name).first();
			airlineContactDTO = new AirlineContactDTO();
			airlineContactDTO.setAirlineContactInternalId(airlineContact.getId().toString());
			airlineContactDTO.setFirstName(airlineContact.getFirstName());
			airlineContactDTO.setLastName(airlineContact.getLastName());
			airlineContactDTO.setPhone(airlineContact.getPhone());
			airlineContactDTO.setEmail(airlineContact.getEmail());
			airlineContactDTO.setAirlineContactType(airlineContact.getAirlineContactType().toString());
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		
		return airlineContactDTO;
	}
	
	/**
	 * Service Method to return a list of all Airline Contacts of an Airline
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException 
	 */
	public List<AirlineContactDTO> getAirlineContacts(String airlineId) throws ObjectNotFoundException{
		
		List<AirlineContact> airlineContactList = null;
		List<AirlineContactDTO> airlineContactDTOList = null;
		
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		
		if(airline != null){	
			airlineContactList = airline.as(AirlinePortal.class).getAirlineContactList();
			if(airlineContactList != null && !airlineContactList.isEmpty()){
				
				airlineContactDTOList = new ArrayList<>();
				for(AirlineContact airlineContact : airlineContactList){
					AirlineContactDTO airlineContactDTO = new AirlineContactDTO();
					airlineContactDTO.setAirlineContactInternalId(airlineContact.getId().toString());
					airlineContactDTO.setFirstName(airlineContact.getFirstName());
					airlineContactDTO.setLastName(airlineContact.getLastName());
					airlineContactDTO.setPhone(airlineContact.getPhone());
					airlineContactDTO.setEmail(airlineContact.getEmail());
					airlineContactDTO.setAirlineContactType(airlineContact.getAirlineContactType().toString());
					airlineContactDTOList.add(airlineContactDTO);
				}
			}
			else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return airlineContactDTOList;
	}
	
	/**
	 * Service Method to return a list of all Airline Contacts of all Airline
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException 
	 */
	public List<AirlineContactDTO> getAllAirlineContacts() {
		List<AirlineContactDTO> airlineContactDTOList = new ArrayList<>();
		for (AirlineContact airlineContact : Query.from(AirlineContact.class).selectAll()) {
			AirlineContactDTO airlineContactDTO = new AirlineContactDTO();
			airlineContactDTO.setAirlineContactInternalId(airlineContact.getId().toString());
			airlineContactDTO.setFirstName(airlineContact.getFirstName());
			airlineContactDTO.setLastName(airlineContact.getLastName());
			airlineContactDTO.setPhone(airlineContact.getPhone());
			airlineContactDTO.setEmail(airlineContact.getEmail());
			airlineContactDTO.setAirlineContactType(airlineContact.getAirlineContactType().toString());
			airlineContactDTOList.add(airlineContactDTO);
		}
		return airlineContactDTOList;
	}
	
	/**
	 * Service Method to return an Airline Contact for an Airline
	 * @param airlineId
	 * @param contactId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public AirlineContactDTO getAirlineContactById(String airlineId, String contactId) throws ObjectNotFoundException{
		
		AirlineContactDTO airlineContactDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			AirlineContact airlineContact = Query.from(AirlineContact.class).where("id = ?",contactId).and("cms.site.owner = ?",airline).first();
			if(airlineContact != null){
				airlineContactDTO = new AirlineContactDTO();
				airlineContactDTO.setAirlineContactInternalId(airlineContact.getId().toString());
				airlineContactDTO.setFirstName(airlineContact.getFirstName());
				airlineContactDTO.setLastName(airlineContact.getLastName());
				airlineContactDTO.setPhone(airlineContact.getPhone());
				airlineContactDTO.setEmail(airlineContact.getEmail());
				airlineContactDTO.setAirlineContactType(airlineContact.getAirlineContactType().toString());
			}
			else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return airlineContactDTO;
	}
	
	/**
	 * Service Method to return an Airline Contact for an Airline by phone
	 * @param airlineId
	 * @param phone
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public AirlineContactDTO getAirlineContactByPhone(String airlineId, String phone) throws ObjectNotFoundException{
		
		AirlineContactDTO airlineContactDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			AirlineContact airlineContact = Query.from(AirlineContact.class).where("phone = ?",phone).and("cms.site.owner = ?",airline).first();
			if(airlineContact != null){
				airlineContactDTO = new AirlineContactDTO();
				airlineContactDTO.setAirlineContactInternalId(airlineContact.getId().toString());
				airlineContactDTO.setFirstName(airlineContact.getFirstName());
				airlineContactDTO.setLastName(airlineContact.getLastName());
				airlineContactDTO.setPhone(airlineContact.getPhone());
				airlineContactDTO.setEmail(airlineContact.getEmail());
				airlineContactDTO.setAirlineContactType(airlineContact.getAirlineContactType().toString());
			}
			else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return airlineContactDTO;
	}
	
	/**
	 * Get AirlineContact by email ID 
	 * 
	 * @author arjun.p
	 * 
	 * @param airlineId
	 * @param email
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public AirlineContactDTO getAirlineContactByEmail(String airlineId, String email) throws ObjectNotFoundException{
		
		AirlineContactDTO airlineContactDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			AirlineContact airlineContact = Query.from(AirlineContact.class).where("email = ?",email).and("cms.site.owner = ?",airline).first();
			if(airlineContact != null){
				airlineContactDTO = new AirlineContactDTO();
				airlineContactDTO.setAirlineContactInternalId(airlineContact.getId().toString());
				airlineContactDTO.setFirstName(airlineContact.getFirstName());
				airlineContactDTO.setLastName(airlineContact.getLastName());
				airlineContactDTO.setPhone(airlineContact.getPhone());
				airlineContactDTO.setEmail(airlineContact.getEmail());
				airlineContactDTO.setAirlineContactType(airlineContact.getAirlineContactType().toString());
			}
			else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return airlineContactDTO;
	}
	
	/**
	 * Service Method to update an Airline Contact for an Airline
	 * @param airlineId
	 * @param contactId
	 * @return
	 * @throws ObjectNotFoundException 
	 * @throws BadRequestException 
	 */
	public AirlineContactDTO updateAirlineContact(String contactId, AirlineContactDTO airlineContactDetails) throws ObjectNotFoundException, BadRequestException,ObjectExistsException{
		
		AirlineContactDTO airlineContactDTO = null;
		
		AirlineContact tempAirlineContact;
		
		
			AirlineContact airlineContact = Query.from(AirlineContact.class).where("id = ?",contactId).first();
			if(airlineContact != null){
				
				tempAirlineContact = airlineContact;				
				
				String firstName = airlineContactDetails.getFirstName();
				String lastName = airlineContactDetails.getLastName();
				if(StringUtils.isNotBlank(firstName) && StringUtils.isNotBlank(lastName)){
					if(!firstName.equals(tempAirlineContact.getFirstName())){
						airlineContact.setFirstName(firstName);
					}
					if(!lastName.equals(tempAirlineContact.getLastName())){
						airlineContact.setLastName(lastName);
					}
					AirlineContact temp = Query.from(AirlineContact.class).where("firstName = ?",firstName).and("lastName = ?",lastName).first();					
					if(temp != null && !temp.getId().toString().equals(airlineContact.getId().toString())){
						throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRLINE_CONTACT, ECMSConstants.NAME }, Locale.US), HttpStatus.FORBIDDEN));
					}
				} else {
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}
				
				if(StringUtils.isNotBlank(airlineContactDetails.getPhone())){
					if(!airlineContactDetails.getPhone().equals(tempAirlineContact.getPhone())){
						airlineContact.setPhone(airlineContactDetails.getPhone());
					}
					AirlineContact temp = Query.from(AirlineContact.class).where("phone = ?",airlineContactDetails.getPhone()).first();
					if(temp != null && !temp.getId().toString().equals(airlineContact.getId().toString())){
						throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRLINE_CONTACT, ECMSConstants.FIELD_PHONE }, Locale.US), HttpStatus.FORBIDDEN));
					}				
				}else{
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}	
				
				String email = airlineContactDetails.getEmail();
				if(email != null && StringUtils.isNotBlank(email)){
					if(!email.equals(tempAirlineContact.getEmail())){
						airlineContact.setEmail(email);
					}
					AirlineContact temp = Query.from(AirlineContact.class).where("email = ?",email).first();
					if(temp != null && !temp.getId().toString().equals(airlineContact.getId().toString())){
						throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRLINE_CONTACT, ECMSConstants.FIELD_EMAIL }, Locale.US), HttpStatus.FORBIDDEN));
					}
				}
				else{
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}
				
				String airlineContactType = airlineContactDetails.getAirlineContactType();
				if(airlineContactType != null && StringUtils.isNotBlank(airlineContactType)){
					airlineContactType = airlineContactType.toUpperCase();
					try{
						airlineContact.setAirlineContactType(AirlineContactType.valueOf(airlineContactType));
					}catch(IllegalArgumentException e){
						logger.info("Enum Constant "+airlineContactType + " does not exist");
						throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
					}
				}
				

				airlineContact.save();

				
				airlineContact = Query.from(AirlineContact.class).where("id = ?",contactId).first();
				airlineContactDTO = new AirlineContactDTO();
				airlineContactDTO.setAirlineContactInternalId(airlineContact.getId().toString());
				airlineContactDTO.setFirstName(airlineContact.getFirstName());
				airlineContactDTO.setLastName(airlineContact.getLastName());
				airlineContactDTO.setPhone(airlineContact.getPhone());
				airlineContactDTO.setEmail(airlineContact.getEmail());
				airlineContactDTO.setAirlineContactType(airlineContact.getAirlineContactType().toString());
			}
			else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.NOT_FOUND));
			}

		return airlineContactDTO;
	}
	
	public Boolean deleteAirlineContact(String airlineId, String contactId) throws ObjectNotFoundException{
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			AirlineContact airlineContact = Query.from(AirlineContact.class).where("id = ?",contactId).and("cms.site.owner = ?",airline).first();
			if(airlineContact != null){
				airlineContact.delete();
				return true;
			}
			else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
}
