package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Aircraft;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.dto.AircraftDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service class for CRUD operations on Aircraft
 */
@Service
public class AircraftService {

	@Autowired
	private MessageSource messageSource;

	public static Site globalOwner = Query.from(Site.class).where("name = ?", "Global").first();
	public static Logger logger = LoggerFactory.getLogger(AircraftService.class);

	/**
	 * Service Method to add an Aircraft
	 * 
	 * @param aircraftTypeName
	 * @return
	 */
	public AircraftDTO addAircraft(AircraftDTO aircraftTypeDetails) throws ObjectExistsException {

		AircraftDTO aircraftDTO = null;
		String aircraftTypeName = aircraftTypeDetails.getName();
		if (aircraftTypeName != null && StringUtils.isNotBlank(aircraftTypeName)) {
			if (Query.from(Aircraft.class).where("name = ?", aircraftTypeName).first() != null) {
				logger.error("Aircraft " + aircraftTypeName + " Already Present");
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRCRAFT, ECMSConstants.NAME }, Locale.US),
						HttpStatus.CONFLICT));
			}

			Aircraft aircraft = new Aircraft();
			aircraft.setName(aircraftTypeName);
			aircraft.as(Site.ObjectModification.class).setGlobal(true);
			logger.debug("Aircraft saved successfully");
			aircraft.save();

			aircraftDTO = new AircraftDTO();
			aircraftDTO.setAircraftTypeInternalId(aircraft.getId().toString());
			aircraftDTO.setName(aircraft.getName());
		}
		return aircraftDTO;
	}

	/**
	 * Service Method to return a list of all AircraftTypes
	 * 
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<AircraftDTO> getAircraftTypes() throws ObjectNotFoundException {

		List<AircraftDTO> aircraftTypeDTOList = null;
		List<Aircraft> aircraftTypeList = Query.from(Aircraft.class).selectAll();

		if (!CollectionUtils.isEmpty(aircraftTypeList)) {
			aircraftTypeDTOList = new ArrayList<>();
			for (Aircraft aircraft : aircraftTypeList) {
				AircraftDTO aircraftTypeDTO = new AircraftDTO();
				aircraftTypeDTO.setAircraftTypeInternalId(aircraft.getId().toString());
				aircraftTypeDTO.setName(aircraft.getName());
				aircraftTypeDTOList.add(aircraftTypeDTO);
			}
		}

		return aircraftTypeDTOList;
	}

	/**
	 * Service Method to update an Aircraft
	 * 
	 * @param aircraftTypeName
	 * @param aircraftTypeDetails
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public AircraftDTO updateAircraftType(String aircraftTypeId, AircraftDTO aircraftTypeDetails)
			throws ObjectNotFoundException {

		AircraftDTO aircraftTypeDTO = null;
		Aircraft aircraft = Query.from(Aircraft.class).where("id = ?", aircraftTypeId).first();
		if (aircraft != null) {
			String newAircraftTypeName = aircraftTypeDetails.getName();
			if (newAircraftTypeName != null && StringUtils.isNotBlank(newAircraftTypeName)) {
				aircraft.setName(newAircraftTypeName);
			}
			logger.debug("Aircraft saved successfully");
			aircraft.save();
			aircraftTypeDTO = new AircraftDTO();
			aircraftTypeDTO.setAircraftTypeInternalId(aircraft.getId().toString());
			aircraftTypeDTO.setName(aircraft.getName());
		} else {
			logger.error("Aircraft Type with id " + aircraftTypeId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRCRAFT }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return aircraftTypeDTO;
	}

	/**
	 * Service Method to delete an Aircraft Type
	 * 
	 * @param aircraftTypeName
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public Boolean deleteAircraftType(String aircraftTypeId) throws ObjectNotFoundException, ObjectAssociationExistsException {

		Aircraft aircraft = Query.from(Aircraft.class).where("id = ?", aircraftTypeId).first();
		if (aircraft != null) {
			List<Lopa> lopas = Query.from(Lopa.class).where("aircraft = ?", aircraft).selectAll();
			if (CollectionUtils.isEmpty(lopas)) {
				aircraft.delete();
				return true;
			} else {
				logger.error("Aircraft Type with id " + aircraftTypeId + " is associated with many Lopa objects.");
				throw new ObjectAssociationExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_ASSOCIATED,
						new Object[] { ECMSConstants.AIRCRAFT, ECMSConstants.LOPA }, Locale.US), HttpStatus.FORBIDDEN));
			}
		} else {
			logger.error("Aircraft Type with id " + aircraftTypeId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRCRAFT }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
}