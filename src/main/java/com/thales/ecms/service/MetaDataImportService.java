package com.thales.ecms.service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.psddev.dari.util.MultipartRequest;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.jobs.MetaDataImportJob;
import com.thales.ecms.jobs.MetaDataImportJobFactory;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.IngestionDetail;
import com.thales.ecms.model.IngestionDetail.Mode;
import com.thales.ecms.model.dto.FileMetaDTO;
import com.thales.ecms.model.dto.IngestionDetailDTO;
import com.thales.ecms.model.dto.MetaDataImportDTO;
import com.thales.ecms.utils.ECMSConstants;
import com.thales.ecms.utils.ECMSFileHandlingUtils;

/**
 * @author Manish gour, Service MetaData export
 */

/**
 * <h1>Import MetaData XLS file!</h1>
 * 
 * <p>
 * This Service class is used to import metadata into ECMS portal MetaData
 * </p>
 *
 * @author Manish gour
 * @version 1.0
 */

@Service
public class MetaDataImportService {

	@Autowired
	ThreadPoolTaskExecutor taskExecutor;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	ConfigurationService configurationService;
	
	@Autowired
	IngestionDetailService ingestionDetailService;

	ExecutorService executor = null;

	@Autowired
	MetaDataImportJobFactory metaDataImportJobFactory = null;

	public static Logger logger = LoggerFactory.getLogger(MetaDataImportService.class);

	private static final Set<String> SUPPORTED_FORMATS = new HashSet<String>(
			Arrays.asList(new String[] { "xls", "xlsx", "XLS", "XLSX" }));

	/**
	 * Service method for uploading file(s)
	 * 
	 * @param request
	 * @param airlineId
	 * @return fileUploaded
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 */
	public List<IngestionDetailDTO> uploadfiles(MultipartRequest request, String airlineId, String configurationId)
			throws NoSuchMessageException, EmptyResponseException, BadRequestException, UnsupportedEncodingException, ObjectExistsException, ObjectNotFoundException {
		IngestionDetailDTO ingestionDetailDTO = null;

		if (!StringUtils.isNotBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse("Incorrect Request - empty airlineId", HttpStatus.BAD_REQUEST));

		if (Objects.isNull(request)) {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		
		configurationId = URLDecoder.decode(configurationId, ECMSConstants.UTF);
		Configuration configuration = configurationService.getConfigurationById(airlineId, configurationId);
		
		List<IngestionDetailDTO> filesUploaded = new ArrayList<IngestionDetailDTO>();

		String uploadFilePath = null;

		FileItem[] fileItems = request.getFileItems("file");
		
		ingestionDetailDTO = new IngestionDetailDTO();
		if (fileItems.length > 0) {

			// create our destination folder, if it not exists
			uploadFilePath = messageSource.getMessage("metadata.upload.dir.path", null, Locale.US) + File.separator
					+ File.separator + messageSource.getMessage("metadata.upload.dir.name", null, Locale.US)
					+ File.separator;

			// create our destination folder, if it not exists
			File uploadDirectoryPath = new File(uploadFilePath);
			if (!uploadDirectoryPath.exists()) {
				uploadDirectoryPath.mkdirs();
			}

			for (FileItem fileItem : fileItems) {
				FileMetaDTO fileMeta = new FileMetaDTO();
				String uploadedFileLocation = uploadFilePath + fileItem.getName();

				// Validating file type

				String fileExtension = FilenameUtils.getExtension(uploadedFileLocation);
				if (SUPPORTED_FORMATS.contains(fileExtension) == false) {
					fileMeta.setFileName(fileItem.getName());
					fileMeta.setFileSize(fileItem.getSize());
					fileMeta.setFileType(fileItem.getContentType());
					fileMeta.setStatus(false);
					fileMeta.setReason(
							messageSource.getMessage(ECMSConstants.FILE_FORMAT_NOT_SUPPORTED, null, Locale.US));
				} else {
					// file format is okay, then saving to server
					try {
						ECMSFileHandlingUtils.saveToFile(fileItem.getInputStream(), uploadedFileLocation);

						File result = new File(uploadedFileLocation);

						/*
						 * File uploaded successfully. now renaming FILENAME, so
						 * file with same name file will not case any issue.
						 */
						if (result.exists()) {
							SimpleDateFormat simpleDate = new SimpleDateFormat("d-MMM-YYYY HH-mm-ss");
							String date = simpleDate.format(new Date());
							String appendTime = date + "." + FilenameUtils.getExtension(uploadedFileLocation);
							File renamedFile = new File(result.getParent() + File.separator + FilenameUtils.removeExtension(result.getName()) + "-" + appendTime);

							result.renameTo(renamedFile);

							// TODO 1. Ingetion object while uploding file -
							// uploaded
							// mode. save and return DTO no filemetadata.
							// return Ingetion object with configuration id.
							fileMeta.setFileName(renamedFile.getName());
							fileMeta.setFileSize(fileItem.getSize());
							fileMeta.setFileType(fileItem.getContentType());
							fileMeta.setStatus(true);
							ingestionDetailDTO.setFileName(renamedFile.getName());
							ingestionDetailDTO.setMode("Uploaded");
							ingestionDetailDTO.setIngestedDate(new Date());
							ingestionDetailDTO.setConfigurationId(configurationId);
							ingestionDetailDTO.setConfiguration(configuration.getName());
							//Now updating DTO with value from API.
							ingestionDetailDTO = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineId, ingestionDetailDTO);
						}
					} catch (IOException e) {
						fileMeta.setFileName(fileItem.getName());
						fileMeta.setFileSize(fileItem.getSize());
						fileMeta.setFileType(fileItem.getContentType());
						fileMeta.setStatus(false);
						fileMeta.setReason(
								messageSource.getMessage(ECMSConstants.FILE_FORMAT_SAVE_ERROR, null, Locale.US));
						logger.info(messageSource.getMessage(ECMSConstants.FILE_FORMAT_SAVE_ERROR, null, Locale.US)
								+ " " + e.getMessage());
					}
				}

				filesUploaded.add(ingestionDetailDTO);
				System.out.println(uploadedFileLocation);
			}
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}

		return filesUploaded;
	}

	/**
	 * Service method for invoking import meta data background process
	 * 
	 * @param metaDataImportDTO
	 * @param request
	 * @return ingestionDetail
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws UnsupportedEncodingException
	 */
	public IngestionDetailDTO importMetaDataProcess(MetaDataImportDTO metaDataImportDTO, HttpServletRequest request)
			throws BadRequestException, NoSuchMessageException, EmptyResponseException, UnsupportedEncodingException {

		IngestionDetailDTO ingestionDetailDTO = null;

		String airlineId = metaDataImportDTO.getAirlineId();
		String configurationId = metaDataImportDTO.getConfigurationId();
		String fileName = metaDataImportDTO.getFileName();
		String ingestionDetailId = metaDataImportDTO.getIngestionDetailId();

		if (!StringUtils.isNotBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse("Incorrect Request - empty airlineId", HttpStatus.BAD_REQUEST));

		if (!StringUtils.isNotBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse("Incorrect Request - empty configurationId", HttpStatus.BAD_REQUEST));

		if (!StringUtils.isNotBlank(fileName))
			throw new BadRequestException(
					new ErrorResponse("Incorrect Request - empty fileName", HttpStatus.BAD_REQUEST));
		
		if (!StringUtils.isNotBlank(ingestionDetailId))
			throw new BadRequestException(
					new ErrorResponse("Incorrect Request - empty ingestionDetailId", HttpStatus.BAD_REQUEST));

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		configurationId = URLDecoder.decode(configurationId, ECMSConstants.UTF);
		ingestionDetailId = URLDecoder.decode(ingestionDetailId, ECMSConstants.UTF);

		String uplaodFileWithDirPath = messageSource.getMessage("metadata.upload.dir.path", null, Locale.US)
				+ File.separator + File.separator
				+ messageSource.getMessage("metadata.upload.dir.name", null, Locale.US) + File.separator + fileName;

		File result = new File(uplaodFileWithDirPath);

		if (result.exists()) {
			MetaDataImportJob metaDataImportJob = null;
			try {
				// Passing airline and configurationId, to set all required
				// objects
				metaDataImportJob = metaDataImportJobFactory
						.getMetaDataImportJobInstance(FilenameUtils.getExtension(uplaodFileWithDirPath));

				System.out.println("Hash Code for metaData PRocess: " + metaDataImportJob);

				metaDataImportJob.setRequriedObjects(airlineId, configurationId, ingestionDetailId, uplaodFileWithDirPath);

			} catch (ObjectNotFoundException e) {
				throw new EmptyResponseException(new ErrorResponse("Configuration not found", HttpStatus.NOT_FOUND));
			}
			
			ingestionDetailDTO = new IngestionDetailDTO();
			if (Objects.nonNull(metaDataImportJob)) {
				IngestionDetail ingestionDetailFromDB = ingestionDetailService.getIngestionDetailById(ingestionDetailId);
				if (ingestionDetailFromDB != null) {
					ingestionDetailFromDB.setMode(Mode.Imported);
					ingestionDetailFromDB.setStatus("In Progress");
					ingestionDetailFromDB.save();
					ingestionDetailDTO.setIngestionDetailInternalId(ingestionDetailId);
					ingestionDetailDTO.setFileName(ingestionDetailFromDB.getFileName());
					ingestionDetailDTO.setConfiguration(ingestionDetailFromDB.getConfiguration());
					ingestionDetailDTO.setConfigurationId(ingestionDetailFromDB.getConfigurationId());
					ingestionDetailDTO.setIngestedDate(ingestionDetailFromDB.getIngestedDate());
					ingestionDetailDTO.setStatus(ingestionDetailFromDB.getStatus());
					ingestionDetailDTO.setMode(ingestionDetailFromDB.getMode().toString());
					//While starting the import, change the mode -> "Imported" and status -> "In Progress" but titlelist would be null.
					// MetaDataImportJob would be updating the titles list in the IngestionDetail object in background.
					ingestionDetailDTO.setIngestionTitleDTOList(null);
					ingestionDetailDTO.setTitleCount(0);
					// if we have correct configuration then we are starting
					// process.
					logger.info("#######  Sending request for metaDataImportJob ===>  ");
					taskExecutor.submit(metaDataImportJob);
					logger.info("#######  Sent request for metaDataImportJob ===>  ");
				}else{
					throw new EmptyResponseException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.INGESTION_DETAIL, null, Locale.US),
									HttpStatus.NOT_FOUND));
				
				}

			}
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FILE_NOT_FOUND_SERVER, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
		return ingestionDetailDTO;
	}

}
