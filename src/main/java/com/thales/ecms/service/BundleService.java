package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Bundle;
import com.thales.ecms.model.Bundle.SeatingClassPriceMapping;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.dto.BundleDTO;
import com.thales.ecms.model.dto.BundleDTO.SeatingClassPriceMappingDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author vaibhav Service Class for CRUD operations on Bundle
 */

@Service
public class BundleService {

	public Logger logger = LoggerFactory.getLogger(BundleService.class);

	@Autowired
	MessageSource messageSource;

	/**
	 * Service method for create new Bundle
	 * 
	 * @param airlineId
	 * @param bundleDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException 
	 */

	public BundleDTO addBundle(String airlineId, BundleDTO bundleDetails)
			throws NoSuchMessageException, ObjectExistsException, ObjectNotFoundException, BadRequestException {		
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			Set<Site> consumers = null;
			consumers = Sets.newHashSet();
			consumers.add(airline);
			String bundleName = bundleDetails.getBundleName();
			if (bundleName != null && StringUtils.isNotBlank(bundleName)) {
				if (Query.from(Bundle.class).where("bundleName = ? ", bundleName).and("cms.site.owner = ?", airline).first() != null) {

					logger.error("Bundle " + bundleName + " Bundle Present");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.BUNDLE, ECMSConstants.BUNDLE_NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				}

				Bundle bundle = new Bundle();
				bundle.setBundleName(bundleName);

				Set<SeatingClassPriceMapping> seatingClassPriceMappings = new HashSet<>();

				for (SeatingClassPriceMappingDTO seatingClassPriceMappingDTO : bundleDetails.getSeatingClassPriceMappings()) {
					SeatingClass seatingClass = Query.from(SeatingClass.class).where("id = ?", seatingClassPriceMappingDTO.getSeatingClassId())
							.and("cms.site.owner = ?", airline).first();					
					if (seatingClass == null) {
						throw new ObjectNotFoundException(new ErrorResponse(
								messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.SEATING_CLASS}, Locale.US),
								HttpStatus.NOT_FOUND));
					}
					SeatingClassPriceMapping seatingClassPriceMapping = new SeatingClassPriceMapping();
					seatingClassPriceMapping.setSeatingClass(seatingClass);
					if (seatingClassPriceMappingDTO.getPpa() >= 0.0) {
						seatingClassPriceMapping.setPpa(seatingClassPriceMappingDTO.getPpa());
					}
					if (seatingClassPriceMappingDTO.getPpv() >= 0.0) {
						seatingClassPriceMapping.setPpv(seatingClassPriceMappingDTO.getPpv());
					}
					seatingClassPriceMappings.add(seatingClassPriceMapping);
				}

				logger.debug("Bundle saved successfully");				
				bundle.setSeatingClassPriceMappings(seatingClassPriceMappings);
				Date date = new Date();
				bundle.setCreationDate(date);
				bundle.setLastModifiedDate(date);
				bundle.as(Site.ObjectModification.class).setOwner(airline);
				bundle.as(Site.ObjectModification.class).setConsumers(consumers);
				bundle.save();
				bundleDetails.setBundleId(bundle.getId().toString());
				bundleDetails.setCreationDate(bundle.getCreationDate().getTime());
				bundleDetails.setLastModifiedDate(bundle.getLastModifiedDate().getTime());
			}else{
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
						new Object[] { ECMSConstants.BUNDLE_NAME }, Locale.US), HttpStatus.NOT_FOUND));
			}

		} else {			
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return bundleDetails;

	}

	/**
	 * Service method for get Bundles
	 * 
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws NoSuchMessageException
	 */

	public List<BundleDTO> getBundleList(String airlineId) throws NoSuchMessageException, ObjectNotFoundException {
		List<BundleDTO> bundleDTOList = new ArrayList<>();	
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			List<Bundle> bundles = Query.from(Bundle.class).where("cms.site.owner = ?", airline).selectAll();
			if (Objects.nonNull(bundles)) {				
				for (Bundle bundle : bundles) {
					bundleDTOList.add(populateBundleDTO(bundle));
				}
			} 
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}		

		return bundleDTOList;
	}
	
	public BundleDTO getBundleById(String airlineId,String bundleId) throws NoSuchMessageException, ObjectNotFoundException{		
		BundleDTO bundleDetails = null;
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			Bundle bundle = Query.from(Bundle.class).where(" id = ?", bundleId).and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(bundle)) {
				bundleDetails = populateBundleDTO(bundle); 
			} else {
				logger.error("Bundle List is empty");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.BUNDLE }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}		
		return bundleDetails;
	}
	
	public BundleDTO updateBundle(String airlineId, String bundleId,
			BundleDTO bundleDetails)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException {

		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			Set<Site> consumers = null;
			consumers = Sets.newHashSet();
			consumers.add(airline);
			Bundle existingBundle = Query.from(Bundle.class).where("id = ? ", bundleId).and("cms.site.owner = ?", airline).first();
			if (existingBundle == null) {				
				throw new ObjectNotFoundException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.BUNDLE, ECMSConstants.BUNDLE_NAME }, Locale.US),
						HttpStatus.FORBIDDEN));
			}
			String bundleName = bundleDetails.getBundleName();
			if (bundleName != null && StringUtils.isNotBlank(bundleName)) {
				Bundle temp = Query.from(Bundle.class).where("bundleName = ? ", bundleName).and("cms.site.owner = ?", airline).first();
				if (temp != null && !temp.getId().toString().equals(bundleId)) {

					logger.error("Bundle " + bundleName + " Bundle Present");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.BUNDLE, ECMSConstants.BUNDLE_NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				}

				Bundle bundle = existingBundle;
				bundle.setBundleName(bundleName);

				Set<SeatingClassPriceMapping> seatingClassPriceMappings = new HashSet<>();

				for (SeatingClassPriceMappingDTO seatingClassPriceMappingDTO : bundleDetails.getSeatingClassPriceMappings()) {
					SeatingClass seatingClass = Query.from(SeatingClass.class).where("id = ?", seatingClassPriceMappingDTO.getSeatingClassId())
							.and("cms.site.owner = ?", airline).first();					
					if (seatingClass == null) {
						throw new ObjectNotFoundException(new ErrorResponse(
								messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.SEATING_CLASS}, Locale.US),
								HttpStatus.NOT_FOUND));
					}
					SeatingClassPriceMapping seatingClassPriceMapping = new SeatingClassPriceMapping();
					seatingClassPriceMapping.setSeatingClass(seatingClass);
					if (seatingClassPriceMappingDTO.getPpa() >= 0.0) {
						seatingClassPriceMapping.setPpa(seatingClassPriceMappingDTO.getPpa());
					}
					if (seatingClassPriceMappingDTO.getPpv() >= 0.0) {
						seatingClassPriceMapping.setPpv(seatingClassPriceMappingDTO.getPpv());
					}
					seatingClassPriceMappings.add(seatingClassPriceMapping);
				}

				logger.debug("Bundle Updated successfully");				
				bundle.setSeatingClassPriceMappings(seatingClassPriceMappings);
				Date date = new Date();
				if(bundle.getCreationDate()==null)
					bundle.setCreationDate(date);
				bundle.setLastModifiedDate(date);
				bundle.as(Site.ObjectModification.class).setOwner(airline);
				bundle.as(Site.ObjectModification.class).setConsumers(consumers);
				bundle.save();
				bundleDetails.setBundleId(bundle.getId().toString());
				bundleDetails.setCreationDate(bundle.getCreationDate().getTime());
				bundleDetails.setLastModifiedDate(bundle.getLastModifiedDate().getTime());
			}else{
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
						new Object[] { ECMSConstants.BUNDLE_NAME }, Locale.US), HttpStatus.NOT_FOUND));
			}

		} else {			
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return bundleDetails;
	}
	
	public Boolean deleteBundle(String airlineId,String bundleId) throws NoSuchMessageException, ObjectNotFoundException{
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			Bundle bundle = Query.from(Bundle.class).where(" id = ?", bundleId).and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(bundle)) {
				bundle.delete();
				return Boolean.TRUE;
			} else {
				logger.error("Bundle List is empty");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.BUNDLE }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}				
	}
	
	private BundleDTO populateBundleDTO(Bundle bundle){
		BundleDTO bundleDTO = new BundleDTO();
		bundleDTO.setBundleId(bundle.getId().toString());
		bundleDTO.setBundleName(bundle.getBundleName());
		Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings = new HashSet<>();
		if(bundle.getSeatingClassPriceMappings()!=null){
			for(SeatingClassPriceMapping seatingClassPriceMapping : bundle.getSeatingClassPriceMappings()){
				SeatingClassPriceMappingDTO seatingClassPriceMappingDTO = new SeatingClassPriceMappingDTO();
				if(seatingClassPriceMapping.getSeatingClass()!=null){
					seatingClassPriceMappingDTO.setSeatingClassId(seatingClassPriceMapping.getSeatingClass().getId().toString());
					seatingClassPriceMappingDTO.setSeatingClassName(seatingClassPriceMapping.getSeatingClass().getName());
				}
				seatingClassPriceMappingDTO.setPpa(seatingClassPriceMapping.getPpa());
				seatingClassPriceMappingDTO.setPpv(seatingClassPriceMapping.getPpv());
				seatingClassPriceMappings.add(seatingClassPriceMappingDTO);
			}
		}		
		bundleDTO.setSeatingClassPriceMappings(seatingClassPriceMappings);
		if(bundle.getCreationDate()!=null)
			bundleDTO.setCreationDate(bundle.getCreationDate().getTime());
		if(bundle.getLastModifiedDate()!=null)
			bundleDTO.setLastModifiedDate(bundle.getLastModifiedDate().getTime());
		return bundleDTO;
	}

}
