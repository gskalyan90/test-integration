package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.ObjectField;
import com.psddev.dari.db.ObjectIndex;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.psddev.dari.db.State;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Field;
import com.thales.ecms.model.GlobalList;
import com.thales.ecms.model.dto.FieldRequestDTO;
import com.thales.ecms.model.dto.FieldResponseDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Service class for CRUD operations on Fields
 */
@Service
public class FieldService {

	@Autowired
	private MessageSource messageSource;
	public static Logger logger = LoggerFactory.getLogger(FieldService.class);

	/**
	 * Service Method to create a Field
	 * @param fieldRequestDTO
	 * @return
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 */
	public FieldResponseDTO addGenericField(FieldRequestDTO fieldRequestDTO) throws NoSuchMessageException, BadRequestException{

		String fieldId = fieldRequestDTO.getFieldId();
		//		String displayName = fieldRequestDTO.getDisplayName();
		String fieldType = fieldRequestDTO.getFieldType();

		Field field = null;
		FieldResponseDTO fieldResponseDTO = null;

		if(StringUtils.isNotBlank(fieldId) && StringUtils.isNotBlank(fieldType)){
			field = new Field();
			field.setFieldId(fieldId);
			//			field.setDisplayName(displayName);
			field.setFieldType(fieldType);
			if(fieldType.equalsIgnoreCase("selection")){
				String selectionListName = fieldRequestDTO.getSelectionListName();
				if(StringUtils.isNotBlank(selectionListName)){
					field.setSelectionListName(selectionListName);
					//createSelectionDataType(selectionListName);
				}
				else{
					logger.error("Name of the selection list is empty");
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}
			}
			field.as(Site.ObjectModification.class).setGlobal(true);
			field.save();

			fieldResponseDTO = createResponseDTO(field);
		}
		else{
			logger.error("Either fieldId or fieldType is empty");
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
		return fieldResponseDTO;
	}

	/**
	 * Service Method to return a list of all Generic Fields
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public List<FieldResponseDTO> getGenericFields() throws NoSuchMessageException, ObjectNotFoundException{

		List<FieldResponseDTO> fieldResponseDTOList = null;
		FieldResponseDTO fieldResponseDTO = null;

		List<Field> fieldList = Query.from(Field.class).where("cms.site.isGlobal = ?",true).selectAll();
		if(!CollectionUtils.isEmpty(fieldList)){
			fieldResponseDTOList = new ArrayList<>();

			for(Field field : fieldList){
				fieldResponseDTO = createResponseDTO(field);
				fieldResponseDTOList.add(fieldResponseDTO);
			}
		}
		else{
			logger.error("No generic fields exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.FIELDS}, Locale.US), HttpStatus.OK));
		}
		
		return fieldResponseDTOList;
	}

	/**
	 * Service Method to return a Field (Generic/Specific) by id
	 * @param fieldInternalId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public FieldResponseDTO getFieldById(String fieldInternalId) throws NoSuchMessageException, ObjectNotFoundException{

		FieldResponseDTO fieldResponseDTO = null;

		Field field = Query.from(Field.class).where("id = ?",fieldInternalId).first();
		if(field != null){
			fieldResponseDTO = createResponseDTO(field);
		}
		else{
			logger.error("Field does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.FIELD}, Locale.US), HttpStatus.OK));
		}
		return fieldResponseDTO;
	}

	/**
	 * Service Method to delete a Field
	 * @param fieldInternalId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public boolean deleteField(String fieldInternalId) throws NoSuchMessageException, ObjectNotFoundException{

		Field field = Query.from(Field.class).where("id = ?",fieldInternalId).first();
		if(field != null){
			field.delete();
			return true;
		}
		else{
			logger.error("Cannot delete field - it does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.FIELD}, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Service Method to add a Specific Field
	 * @param airlineId
	 * @param fieldRequestDTO
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	public FieldResponseDTO addSpecificField(String airlineId, FieldRequestDTO fieldRequestDTO) throws NoSuchMessageException, ObjectNotFoundException, BadRequestException{

		Field field = null;
		FieldResponseDTO fieldResponseDTO = null;

		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();

		if(airline != null){
			String fieldId = fieldRequestDTO.getFieldId();
			//			String displayName = fieldRequestDTO.getDisplayName();
			String fieldType = fieldRequestDTO.getFieldType();

			if(StringUtils.isNotBlank(fieldId) && StringUtils.isNotBlank(fieldType)){
				field = new Field();
				field.setFieldId(fieldId);
				//				field.setDisplayName(displayName);
				field.setFieldType(fieldType);
				if(fieldType.equalsIgnoreCase("selection")){
					String selectionListName = fieldRequestDTO.getSelectionListName();
					if(StringUtils.isNotBlank(selectionListName)){
						field.setSelectionListName(selectionListName);
						//createSelectionDataType(selectionListName);
					}
					else{
						logger.error("Name of the selection list is empty");
						throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
					}
				}
				field.as(Site.ObjectModification.class).setOwner(airline);
				field.save();

				fieldResponseDTO = createResponseDTO(field);
			}
			else{
				logger.error("Either fieldId or fieldType is empty");
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRLINE}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return fieldResponseDTO;
	}
	
	/**
	 * Service Method to return a list of Specific Fields
	 * @param airlineId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 */
	public List<FieldResponseDTO> getSpecificFields(String airlineId) throws NoSuchMessageException, ObjectNotFoundException{
		
		List<FieldResponseDTO> fieldResponseDTOList = null;
		FieldResponseDTO fieldResponseDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();

		if(airline != null){
			List<Field> fieldList = Query.from(Field.class).where("cms.site.owner = ?",airline).selectAll();
			if(!CollectionUtils.isEmpty(fieldList)){
				fieldResponseDTOList = new ArrayList<>();

				for(Field field : fieldList){
					fieldResponseDTO = createResponseDTO(field);
					fieldResponseDTOList.add(fieldResponseDTO);
				}
			}
			else{
				logger.error("No specific fields exist");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.FIELDS}, Locale.US), HttpStatus.OK));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRLINE}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return fieldResponseDTOList;
	}

	/**
	 * Helper method to create a Field Response DTO
	 * @param field
	 * @return
	 */
	public FieldResponseDTO createResponseDTO(Field field){

		FieldResponseDTO fieldResponseDTO = new FieldResponseDTO();
		fieldResponseDTO.setFieldInternalId(field.getId().toString());
		fieldResponseDTO.setFieldId(field.getFieldId());
		//			fieldResponseDTO.setDisplayName(field.getDisplayName());
		fieldResponseDTO.setFieldType(field.getFieldType());
		if(field.getFieldType().equalsIgnoreCase("selection") && StringUtils.isNotBlank(field.getSelectionListName())){
			fieldResponseDTO.setSelectionListName(field.getSelectionListName());
		}
		return fieldResponseDTO;
	}
	
	/**
	 * Helper Method to create a Selection Data Type based on the list name
	 * For e.g., if the list name is Genre a GenreType will be created, RatingType for Rating and so on.
	 * @param selectionListName
	 */
	public void createSelectionDataType(String selectionListName){
		
		ObjectField.Value objectFieldValue = null;
	
		selectionListName = selectionListName.substring(0, 1).toUpperCase() + selectionListName.substring(1);
		String displayName = selectionListName.concat("Type");
		String internalName = "com.thales.ecms.model.datatype."+displayName;
		
		if(Query.from(ObjectType.class).where("internalName = ?", internalName).first() == null){
			
			logger.info("Creating a Field Type for the Selection List");
			GlobalList globalList = Query.from(GlobalList.class).where("name = ?", selectionListName).first();
			
			ObjectType selectionDataType = new ObjectType();
			
			selectionDataType.setDisplayName(displayName);
			selectionDataType.setInternalName(internalName);
			
			ObjectType baseType = Query.from(ObjectType.class).where("internalName = ?", "com.thales.ecms.model.datatype.BaseType").first();
			
			List<ObjectField> objectFields = new ArrayList<>();
			objectFields.addAll(baseType.getFields());
			
			ObjectField valuesField = new ObjectField(selectionDataType, null);
			valuesField.setInternalName("values");
			valuesField.setJavaFieldName("values");
			valuesField.setDisplayName("Values");
			valuesField.setInternalType("list/text");
			
			//The allowable values for the 'values' field will be the entries in the list
			Set<ObjectField.Value> allowableValues = new HashSet<>();
			Set<String> values = globalList.getValues();
			for(String value : values){
				objectFieldValue = new ObjectField.Value();
				objectFieldValue.setValue(value);
				allowableValues.add(objectFieldValue);
			}
			valuesField.setValues(allowableValues);
			
			valuesField.setJavaDeclaringClassName(internalName);
			
			objectFields.add(valuesField);
			selectionDataType.setFields(objectFields);
			
			//setting the indexes
			List<ObjectIndex> objectIndexes = new ArrayList<>();
			objectIndexes.addAll(baseType.getIndexes());
			ObjectIndex objectIndex = new ObjectIndex(selectionDataType, null);
			objectIndex.setField("values");
			objectIndex.setJavaDeclaringClassName(internalName);
			objectIndexes.add(objectIndex);
			selectionDataType.setIndexes(objectIndexes);
			
			//setting this data type as an embedded data type
			selectionDataType.setEmbedded(true);
			
			//setting the label fields
			selectionDataType.setLabelFields(baseType.getLabelFields());
			
			//setting the objectClassName
			selectionDataType.setObjectClassName(internalName);
			
			//setting the modification Classes
			Set<String> modificationClasses = new HashSet<>();
			modificationClasses.add(internalName);
			selectionDataType.setModificationClasses(modificationClasses);

			//setting the super classes
			List<String> superClassNames = new ArrayList<>();
			superClassNames.add(internalName);
			superClassNames.addAll(baseType.getSuperClassNames());
			selectionDataType.setSuperClassNames(superClassNames);

			//setting the groups
			Set<String> groups = new HashSet<>();
			groups.add(internalName);
			groups.addAll(baseType.getGroups());
			selectionDataType.setGroups(groups);
			
			//setting the assignable classes
			Set<String> assignableClassNames = new HashSet<>();
			assignableClassNames.add(internalName);
			assignableClassNames.addAll(baseType.getAssignableClassNames());
			selectionDataType.setAssignableClassNames(assignableClassNames);

			selectionDataType.save();
			logger.info(selectionDataType.getId().toString());
			logger.info(selectionDataType.getInternalName());
			logger.info(selectionDataType.getDisplayName());
			
//			State state = selectionDataType.getState();
//			Map<String, Object> fieldMap = state.getValues();
//			fieldMap.put("cms.ui.publishable", true);
//			state.setValues(fieldMap);
//			selectionDataType.setState(state);
//			selectionDataType.save();
//			logger.info("After publishable:"+ selectionDataType.getId().toString());
		}
		else{
			logger.info("A Selection Data Type for this List already exists");
		}
	}
}
