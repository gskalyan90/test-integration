package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.dto.SeatingClassDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Service Class for performing CRUD operations on Class 
 */
@Service
public class SeatingClassService {
	

	@Autowired
	private MessageSource messageSource;
	
	public static Logger logger = LoggerFactory.getLogger(SeatingClassService.class);

	/**
	 * Service Method to add a Seating Class
	 * @param airlineId
	 * @param classDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 */
	public SeatingClassDTO addClass(String airlineId, SeatingClassDTO classDetails) throws ObjectNotFoundException, ObjectExistsException, BadRequestException{
		
		SeatingClassDTO seatingClassDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			
			String className = classDetails.getName();
			if(className != null && StringUtils.isNotBlank(className)){
				if(Query.from(SeatingClass.class).where("name = ?",className).and("cms.site.owner = ?",airline).first() == null){
					SeatingClass seatingClass = new SeatingClass();
					seatingClass.setName(className);
					seatingClass.as(Site.ObjectModification.class).setOwner(airline);
					logger.debug("Seating class instance saved successfully");
					seatingClass.save();
					
					seatingClassDTO = new SeatingClassDTO();
					seatingClassDTO.setSeatingClassInternalId(seatingClass.getId().toString());
					seatingClassDTO.setName(seatingClass.getName());
				}
				else{
					logger.error("Cannot create Seating Class - Seating Class already exists");
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[] {ECMSConstants.SEATING_CLASS,ECMSConstants.NAME},Locale.US), HttpStatus.FORBIDDEN));
				}
			}
			else{
				logger.error("Bad Request - empty Seating Class Name");
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST,null,Locale.US), HttpStatus.BAD_REQUEST));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,new Object[] {ECMSConstants.AIRLINE},Locale.US), HttpStatus.NOT_FOUND));
		}
		
		return seatingClassDTO;
	}
	
	/**
	 * Service Method to return a list of all Classes
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<SeatingClassDTO> getClasses(String airlineId) throws ObjectNotFoundException{
		
		List<SeatingClassDTO> seatingClassDTOList = null;
		SeatingClassDTO seatingClassDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		
		if(airline != null){
			List<SeatingClass> seatingClassList = Query.from(SeatingClass.class).where("cms.site.owner = ?",airline).selectAll();
			if(!CollectionUtils.isEmpty(seatingClassList)){
				seatingClassDTOList = new ArrayList<>();
				for(SeatingClass seatingClass : seatingClassList){
					seatingClassDTO = new SeatingClassDTO();
					seatingClassDTO.setSeatingClassInternalId(seatingClass.getId().toString());
					seatingClassDTO.setName(seatingClass.getName());
					seatingClassDTOList.add(seatingClassDTO);
				}
			}
			else{
				logger.error("No Seating Classes present");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,new Object[] {ECMSConstants.SEATING_CLASS},Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,new Object[] {ECMSConstants.AIRLINE},Locale.US), HttpStatus.NOT_FOUND));
		}
		return seatingClassDTOList;
	}
	
	/**
	 * Service Method to return a Class
	 * @param airlineId
	 * @param classId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public SeatingClassDTO getClassById(String airlineId, String seatingClassId) throws ObjectNotFoundException{
		
		SeatingClassDTO seatingClassDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		
		if(airline != null){
			SeatingClass seatingClass = Query.from(SeatingClass.class).where("id = ?",seatingClassId).and("cms.site.owner = ?",airline).first();
			if(seatingClass != null){
				seatingClassDTO = new SeatingClassDTO();
				seatingClassDTO.setSeatingClassInternalId(seatingClass.getId().toString());
				seatingClassDTO.setName(seatingClass.getName());
			}
			else{
				logger.error("Seating Class does not exist");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,new Object[] {ECMSConstants.SEATING_CLASS},Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,new Object[] {ECMSConstants.AIRLINE},Locale.US), HttpStatus.NOT_FOUND));
		}
		return seatingClassDTO;
	}
	
	/**
     * Service Method to update a Class
     * 
      * @param airlineId
     * @param classId
     * @param classDetails
     * @return
     * @throws ObjectNotFoundException
     * @throws BadRequestException
     * @throws NoSuchMessageException
     * @throws ObjectExistsException
     */
     public SeatingClassDTO updateClass(String seatingClassId, SeatingClassDTO classDetails)
			throws ObjectNotFoundException, NoSuchMessageException, BadRequestException, ObjectExistsException {

            SeatingClassDTO seatingClassDTO = null;
            

            
                   SeatingClass seatingClass = Query.from(SeatingClass.class).where("id = ?", seatingClassId).first();
                   if (seatingClass != null) {

                         String className = classDetails.getName();
                         if (className != null && StringUtils.isNotBlank(className)) {
                                if (Query.from(SeatingClass.class).where("name = ?", className).first() == null) {
                                       seatingClass.setName(className);

                                       logger.debug("Seating class instance saved successfully");
                                       seatingClass.save();

                                       seatingClassDTO = new SeatingClassDTO();
                                       seatingClassDTO.setSeatingClassInternalId(seatingClass.getId().toString());
                                       seatingClassDTO.setName(seatingClass.getName());
                                } else {
                                       logger.error("Cannot create Seating Class - Seating Class already exists");
                                       throw new ObjectExistsException(new ErrorResponse(
                                                     messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
                                                                   new Object[] { ECMSConstants.SEATING_CLASS, ECMSConstants.NAME }, Locale.US),
                                                     HttpStatus.FORBIDDEN));
                                }

                         } else {
                                logger.error("Bad Request - empty Seating Class Name");
                                throw new BadRequestException(
                                              new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
                                                            HttpStatus.BAD_REQUEST));
                         }

                   } else {
                         logger.error("Seating Class does not exist");
                         throw new ObjectNotFoundException(
                                       new ErrorResponse(
                                                     messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
                                                                   new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US),
                                                     HttpStatus.NOT_FOUND));
                   }
            return seatingClassDTO;
     }

     /**
     * Service Method to delete a Class
     * 
     * @param seatingClassId
     * @return
     * @throws ObjectNotFoundException
     */

     public Boolean deleteClass(String seatingClassId)
                   throws ObjectNotFoundException, ObjectExistsException, ObjectAssociationExistsException {

                   SeatingClass seatingClass = Query.from(SeatingClass.class).where("id = ?", seatingClassId).first();
                   if (seatingClass != null) {
                         Lopa lopa = Query.from(Lopa.class)
                                       .where("seatingClassLruTypeMappingList/seatingClass = ?", seatingClassId).first();
                         if (lopa == null) {
                                seatingClass.delete();
                                return true;
                         } else {
                                logger.error("Cannot delete seatingClass- 1 or more LOPA exist for this seatingClass");
                                throw new ObjectAssociationExistsException(new ErrorResponse(
                                              messageSource.getMessage(ECMSConstants.OBJECT_ASSOCIATED,
                                                            new Object[] { ECMSConstants.SEATING_CLASS, ECMSConstants.LOPA }, Locale.US),
                                              HttpStatus.FORBIDDEN));
                         }
                   } else {
                         logger.error("Seating Class does not exist");
                         throw new ObjectNotFoundException(
                                       new ErrorResponse(
                                                     messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
                                                                   new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US),
                                                     HttpStatus.NOT_FOUND));
                   }
           
     }

}
