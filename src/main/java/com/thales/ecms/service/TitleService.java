package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Content;
import com.psddev.cms.db.EmbeddedDataOperation;
import com.psddev.cms.db.MatchAlwaysRule;
import com.psddev.cms.db.Operation;
import com.psddev.cms.db.Site;
import com.psddev.cms.db.Variation;
import com.psddev.dari.db.ObjectField;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.psddev.dari.db.Record;
import com.psddev.dari.db.ReferentialText;
import com.psddev.dari.db.State;
import com.psddev.dari.util.UuidUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.AirlineCatalog;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.Field;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.datatype.BooleanType;
import com.thales.ecms.model.datatype.DateType;
import com.thales.ecms.model.datatype.DateTypeUtil;
import com.thales.ecms.model.datatype.DoubleType;
import com.thales.ecms.model.datatype.IntegerType;
import com.thales.ecms.model.datatype.LongType;
import com.thales.ecms.model.datatype.ReferentialTextType;
import com.thales.ecms.model.datatype.ReferentialTextTypeLanguage;
import com.thales.ecms.model.datatype.TextType;
import com.thales.ecms.model.datatype.TextTypeLanguage;
import com.thales.ecms.model.dto.TitleDTO;
import com.thales.ecms.model.dto.TitleFieldDTO;
import com.thales.ecms.model.dto.TitleFieldRequestDTO;
import com.thales.ecms.model.dto.TitleFieldValueRequestDTO;
import com.thales.ecms.model.dto.TitleRequestDTO;
import com.thales.ecms.utils.ECMSConstants;

@Service
public class TitleService {

	public static Logger logger = LoggerFactory.getLogger(TitleService.class);

	@Autowired
	private MessageSource messageSource;

	/**
	 * Service Method to create a Content Type
	 * @param contentTypeId
	 * @param titleDTO
	 * @return
	 * @throws NoSuchMessageException 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	/*	public String createTitle(String contentTypeId, TitleDTO titleDTO) throws ObjectNotFoundException, BadRequestException{

		Map<String, Object> fieldMap = new HashMap<>();
		String fieldName = null;
		Object fieldValue = null;

		Field field = null;
		String fieldType = null;

		String responseString = null;

		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",contentTypeId).first();
		if(objectType != null){

			Object newObject = objectType.createObject(UuidUtils.createSequentialUuid());
			logger.info(newObject.getClass().getName()); //this gives com.psddev.dari.db.Record
			if(newObject instanceof Record){
				Record newTitle = (Record) newObject;
				newTitle.save();
				logger.info("Before Adding fields:"+newTitle.getId().toString());

				State state = State.getInstance(newTitle);

				List<TitleFieldDTO> titleFields = titleDTO.getTitleFields();
				if(!CollectionUtils.isEmpty(titleFields)){

					for(TitleFieldDTO titleFieldDTO : titleFields){
						fieldName = titleFieldDTO.getFieldName();
						logger.info("Field Name:"+fieldName);
						fieldValue = titleFieldDTO.getFieldValue();
						field = Query.from(Field.class).where("fieldId = ?", fieldName).first();
						fieldType = field.getFieldType();
						List<TextType> fieldValues = new ArrayList<>();
						if(fieldType.equalsIgnoreCase("text")){
							String fieldValueString = (String) fieldValue;
							List<String> values = new ArrayList<>();
							values.add(fieldValueString);
							TextType textType = new TextType();
							textType.setValues(values);
							fieldValues.add(textType);
						}
						fieldMap.put(fieldName, fieldValues);
					}
					state.setValues(fieldMap);
					newTitle.setState(state);
					newTitle.save();

					Variation variation = new Variation();
					variation.setName("Configuration 1");

					variation.save();
					logger.info("Saved Variation: "+variation.getId().toString());
					state = State.getInstance(newTitle);
					logger.info("Queried the state");
					Object original = Query.from(Object.class).where("_id = ?",state.getId()).first();
					logger.info("Queried Object");
					State.getInstance(original).putByPath("variations/"+variation.getId(), state.getValues());
					logger.info("Set the Variation");
					state = State.getInstance(original);
					state.save();

					logger.info("After adding Fields:"+newTitle.getId().toString());
					responseString = "Title created successfully";
				}
				else{
					throw new BadRequestException(new ErrorResponse("Fields are empty - cannot create a Title", HttpStatus.BAD_REQUEST));
				}
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse("Content Type does not exist", HttpStatus.NOT_FOUND));
		}
		return responseString;
	}
	 */

	public String createTitle(String airlineId, String contentTypeId, TitleRequestDTO titleRequestDTO) throws NoSuchMessageException, ObjectNotFoundException{

		ObjectType objectType = null;
		Object newObject = null;
		Record newTitle = null;

		Configuration configuration = null;

		String fieldId = null;
		String fieldType = null;
		Field field = null;

		List<TitleFieldRequestDTO> titleFieldRequestDTOs = titleRequestDTO.getFields();
		List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs = null;

		Map<String, Object> fieldMap = new HashMap<>(); //Map to store fieldName : fieldValue

		//Getting the Airline
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();

		if(airline != null){
			//Getting the Specific Content Type
			objectType = Query.from(ObjectType.class).where("id = ?",contentTypeId).first();
			if(objectType != null){

				newObject = objectType.createObject(UuidUtils.createSequentialUuid()); //This creates a new instance of the ObjectType, but its data type is Object
				//logger.info(newObject.getClass().getName()); //this gives com.psddev.dari.db.Record
				if(newObject instanceof Record){
					newTitle = (Record) newObject;
					newTitle.save();
					logger.info("First save of the Title: "+newTitle.getId().toString());
					
					//Getting the englishTitle
					String englishTitle = titleRequestDTO.getEnglishTitle();
					if(StringUtils.isNotBlank(englishTitle)){
						fieldMap.put("englishTitle", englishTitle);
					}
					
					//Getting the name of the Configuration
					String configurationId = titleRequestDTO.getConfigurationId();
					configuration = Query.from(Configuration.class).where("id = ?",configurationId).first();

					//For each field in the Request DTO
					for(TitleFieldRequestDTO titleFieldRequestDTO : titleFieldRequestDTOs){

						fieldId = titleFieldRequestDTO.getFieldId(); //getting the fieldId
						field = Query.from(Field.class).where("fieldId = ?",fieldId).first(); //Querying the Field using the fieldId
						fieldType = field.getFieldType(); //Getting the fieldType
						logger.info("Field Name:"+field.getFieldId());
						logger.info("Field Type:"+fieldType);

						titleFieldValueRequestDTOs = titleFieldRequestDTO.getFieldValues();
						if(fieldType.equalsIgnoreCase("text")){

							List<TextType> textTypeValues = new ArrayList<>();

							//Create the TextType object
							TextType textTypeValue = createTextTypeValue(titleFieldValueRequestDTOs);
							textTypeValue.setConfiguration(configuration); //Set its configuration

							textTypeValues.add(textTypeValue);

							//The object for the Field is now fully constructed
							//Add this object as a value for the field in the fieldMap
							fieldMap.put(fieldId, textTypeValues);
						}
						else if(fieldType.equalsIgnoreCase("long text")){

							List<ReferentialTextType> referentialTextTypeValues = new ArrayList<>();

							//Create the ReferentialTextType object
							ReferentialTextType referentialTextTypeValue = createReferentialTextTypeValue(titleFieldValueRequestDTOs);
							referentialTextTypeValue.setConfiguration(configuration);

							referentialTextTypeValues.add(referentialTextTypeValue);

							//The object for the Field is now fully constructed
							//Add this object as a value for the field in the fieldMap
							fieldMap.put(fieldId, referentialTextTypeValues);
						}
						else if(fieldType.equalsIgnoreCase("integer")){

							List<IntegerType> integerTypeValues = new ArrayList<>();

							//Create the IntegerType object
							IntegerType integerTypeValue = createIntegerTypeValue(titleFieldValueRequestDTOs);
							integerTypeValue.setConfiguration(configuration);

							integerTypeValues.add(integerTypeValue);

							//The object for the Field is now fully constructed
							//Add this object as a value for the field in the fieldMap
							fieldMap.put(fieldId, integerTypeValues);
						}
						else if(fieldType.equalsIgnoreCase("long")){

							List<LongType> longTypeValues = new ArrayList<>();

							//Create the LongType object
							LongType longTypeValue = createLongTypeValue(titleFieldValueRequestDTOs);
							longTypeValue.setConfiguration(configuration);

							longTypeValues.add(longTypeValue);

							//The object for the Field is now fully constructed
							//Add this object as a value for the field in the fieldMap
							fieldMap.put(fieldId, longTypeValues);
						}
						else if(fieldType.equalsIgnoreCase("double")){

							List<DoubleType> doubleTypeValues = new ArrayList<>();

							//Create the DoubleType object
							DoubleType doubleTypeValue = createDoubleTypeValue(titleFieldValueRequestDTOs);
							doubleTypeValue.setConfiguration(configuration);

							doubleTypeValues.add(doubleTypeValue);

							//The object for the Field is now fully constructed
							//Add this object as a value for the field in the fieldMap
							fieldMap.put(fieldId, doubleTypeValues);
						}
						else if(fieldType.equalsIgnoreCase("boolean")){

							List<BooleanType> booleanTypeValues = new ArrayList<>();

							//Create the BooleanType object
							BooleanType booleanTypeValue = createBooleanTypeValue(titleFieldValueRequestDTOs);
							booleanTypeValue.setConfiguration(configuration);

							booleanTypeValues.add(booleanTypeValue);

							//The object for the Field is now fully constructed
							//Add this object as a value for the field in the fieldMap
							fieldMap.put(fieldId, booleanTypeValues);
						}
						else if(fieldType.equalsIgnoreCase("date")){

							List<DateType> dateTypeValues = new ArrayList<>();

							//Create the DateType object
							DateType dateTypeValue = createDateTypeValue(titleFieldValueRequestDTOs);
							dateTypeValue.setConfiguration(configuration);

							dateTypeValues.add(dateTypeValue);

							//The object for the Field is now fully constructed
							//Add this object as a value for the field in the fieldMap
							fieldMap.put(fieldId, dateTypeValues);
						}
					}
					//Once processing of all fields has been completed, fieldMap will contain all fields and their values
					State state = State.getInstance(newTitle);
					state.setValues(fieldMap);
					newTitle.setState(state);
					newTitle.as(Site.ObjectModification.class).setOwner(airline);
					newTitle.save();
					logger.info("After setting Fields: "+newTitle.getId());
//					Content.Static.publish(newTitle, airline, null); 
//					logger.info("After publish:"+newTitle.getId().toString());
					
					ContentType title = newTitle.as(ContentType.class);
					logger.info(title.getEnglishTitle());
					
//					Variation variation = new Variation();
//					variation.setName(configuration.getName());
//					Set<ObjectType> variationTypes = new HashSet<>();
//					variationTypes.add(objectType);
//					variation.setContentTypes(variationTypes);
//					variation.setOperation(new EmbeddedDataOperation());
//					variation.setRule(new MatchAlwaysRule());
//					variation.save();
//					logger.info(variation.getName());
//					logger.info(variation.getId().toString());
					
					//Assign Title to Airline Catalog
//					AirlineCatalog airlineCatalog = Query.from(AirlineCatalog.class).where("name = ?", "Airline Catalog").first();
//					List<ContentType> airlineCatalogTitleList = new ArrayList<>();
//					airlineCatalogTitleList.add(title);
//					airlineCatalog.setTitles(airlineCatalogTitleList);
//					airlineCatalog.save();
//					logger.info(""+airlineCatalog.getTitles().size());
					
					return "Title created successfully";
				}
			}
			else{
				logger.error("Specific Content Type does not exist");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPE}, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRLINE}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return "Error creating Title";
	}

	public List<TitleDTO> getTitles(String contentTypeId) throws ObjectNotFoundException{

		List<String> fieldList = new ArrayList<>();
		Map<String, Object> fieldMap;

		Record title = null;
		State state = null;

		List<TitleDTO> titleDTOList = null;
		TitleDTO titleDTO = null;
		List<TitleFieldDTO> titleFieldDTOList = null;
		TitleFieldDTO titleFieldDTO = null;

		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",contentTypeId).first();
		if(objectType != null){

			List<ObjectField> objectFields = objectType.getFields();
			for(ObjectField objectField : objectFields){
				logger.info("Object Field Name: "+objectField.getDisplayName());
				fieldList.add(objectField.getInternalName());
			}

			List<Object> titles = Query.fromType(objectType).selectAll();
			if(!CollectionUtils.isEmpty(titles)){
				titleDTOList = new ArrayList<>();

				for(Object object : titles){
					title = (Record) object;
					titleDTO = new TitleDTO();
					titleDTO.setTitleInternalId(title.getId().toString());

					titleFieldDTOList = new ArrayList<>();
					state = title.getState();
					fieldMap = state.getValues();
					for(Map.Entry<String, Object> entry : fieldMap.entrySet()){
						if(fieldList.contains(entry.getKey())){
							titleFieldDTO = new TitleFieldDTO();
							titleFieldDTO.setFieldName(entry.getKey());
							titleFieldDTO.setFieldValue(entry.getValue());
							titleFieldDTOList.add(titleFieldDTO);
						}
					}
					titleDTO.setTitleFields(titleFieldDTOList);
					titleDTOList.add(titleDTO);
				}
			}
			else{
				throw new ObjectNotFoundException(new ErrorResponse("No titles exist for this content type", HttpStatus.NOT_FOUND));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse("Content Type does not exist", HttpStatus.NOT_FOUND));
		}
		return titleDTOList;
	}

	public boolean deleteTitle(String titleId){

		Object titleObject = Query.from(Object.class).where("id = ?",titleId).first();
		if(titleObject != null){
			Record title = (Record) titleObject;
			title.delete();
			return true;
		}
		return false;

	}

	/**
	 * Helper method to create an instance of the TextType class
	 * @param titleFieldValueRequestDTOs
	 * @return
	 */
	public TextType createTextTypeValue(List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs){

		TextType textTypeValue = new TextType();
		List<TextTypeLanguage> textTypeLanguages = new ArrayList<>();

		//For each language of this field
		for(TitleFieldValueRequestDTO titleFieldValueRequestDTO : titleFieldValueRequestDTOs){

			//Create a new textTypeLanguage object. TextTypeLanguage contains 2 properties - 1. Language 2. List<String> values
			TextTypeLanguage textTypeLanguage = new TextTypeLanguage();
			PaxguiLanguage paxguiLanguage = Query.from(PaxguiLanguage.class).where("paxguiLanguageName = ?", titleFieldValueRequestDTO.getLanguage()).first();
			textTypeLanguage.setLanguage(paxguiLanguage);

			List<Object> objectValues = titleFieldValueRequestDTO.getValues();
			List<String> stringValues = new ArrayList<>();
			//Cast List<Object> to List<String> since the fieldType is text
			for(Object objectValue : objectValues){
				String value = (String) objectValue;
				stringValues.add(value);
			}
			textTypeLanguage.setValues(stringValues);
			textTypeLanguages.add(textTypeLanguage);
		}
		//For this field, textTypeLanguages now contains the values for all the languages
		logger.info("All languages completed for this field");
		textTypeValue.setValues(textTypeLanguages);
		return textTypeValue;
	}

	/**
	 * Helper method to create an instance of the ReferentialTextType class
	 * @param titleFieldValueRequestDTOs
	 * @return
	 */
	public ReferentialTextType createReferentialTextTypeValue(List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs) {

		ReferentialTextType referentialTextTypeValue = new ReferentialTextType();
		List<ReferentialTextTypeLanguage> referentialTextTypeLanguages = new ArrayList<>();

		//For each language of this field
		for(TitleFieldValueRequestDTO titleFieldValueRequestDTO : titleFieldValueRequestDTOs){

			//Create a new ReferentialTextTypeLanguage object. ReferentialTextTypeLanguage contains 2 properties - 1. Language 2. List<ReferentialText> values
			ReferentialTextTypeLanguage referentialTextTypeLanguage = new ReferentialTextTypeLanguage();
			PaxguiLanguage paxguiLanguage = Query.from(PaxguiLanguage.class).where("paxguiLanguageName = ?", titleFieldValueRequestDTO.getLanguage()).first();
			referentialTextTypeLanguage.setLanguage(paxguiLanguage);

			List<Object> objectValues = titleFieldValueRequestDTO.getValues();
			List<ReferentialText> referentialTextValues = new ArrayList<>();
			//The value received will be of type String, so we cannot directly cast it to ReferentialText 
			for(Object objectValue : objectValues){
				ReferentialText value = new ReferentialText();
				String stringValue = (String) objectValue;
				value.add(stringValue);
				referentialTextValues.add(value);
			}
			referentialTextTypeLanguage.setValues(referentialTextValues);
			referentialTextTypeLanguages.add(referentialTextTypeLanguage);
		}
		//For this field, textTypeLanguages now contains the values for all the languages
		logger.info("All languages completed for this field");
		referentialTextTypeValue.setValues(referentialTextTypeLanguages);
		return referentialTextTypeValue;
	}

	/**
	 * Helper method to create an instance of the IntegerType class
	 * @param titleFieldValueRequestDTOs
	 * @return
	 */
	public IntegerType createIntegerTypeValue(List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs){

		IntegerType integerTypeValue = new IntegerType();

		//For each language of this field
		for(TitleFieldValueRequestDTO titleFieldValueRequestDTO : titleFieldValueRequestDTOs){

			List<Object> objectValues = titleFieldValueRequestDTO.getValues();
			Set<Integer> integerValues = new HashSet<>();
			//Cast List<Object> to Set<Integer> since the fieldType is integer
			for(Object objectValue : objectValues){
				int value = (int) objectValue;
				integerValues.add(value);
			}
			integerTypeValue.setValues(integerValues);
		}
		return integerTypeValue;
	}

	/**
	 * Helper method to create an instance of the LongType class
	 * @param titleFieldValueRequestDTOs
	 * @return
	 */
	public LongType createLongTypeValue(List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs){

		LongType longTypeValue = new LongType();

		for(TitleFieldValueRequestDTO titleFieldValueRequestDTO : titleFieldValueRequestDTOs){

			//There will be no languages involved here, so there will just one object in titleFieldValueRequestDTOs
			List<Object> objectValues = titleFieldValueRequestDTO.getValues();
			Set<Long> longValues = new HashSet<>();
			//Cast List<Object> to Set<Long> since the fieldType is long
			for(Object objectValue : objectValues){
				long value;
				if(objectValue.getClass().getName().equalsIgnoreCase("java.lang.Integer")){
					int intValue = (int) objectValue;
					value = (long) intValue; 
				}
				else{
					value = (long) objectValue;
				}
				longValues.add(value);
			}
			longTypeValue.setValues(longValues);
		}
		return longTypeValue;
	}

	/**
	 * Helper method to create an instance of the DoubleType class
	 * @param titleFieldValueRequestDTOs
	 * @return
	 */
	public DoubleType createDoubleTypeValue(List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs){

		DoubleType doubleTypeValue = new DoubleType();

		for(TitleFieldValueRequestDTO titleFieldValueRequestDTO : titleFieldValueRequestDTOs){

			//There will be no languages involved here, so there will just one object in titleFieldValueRequestDTOs
			List<Object> objectValues = titleFieldValueRequestDTO.getValues();
			Set<Double> doubleValues = new HashSet<>();
			//Cast List<Object> to Set<Double> since the fieldType is double
			for(Object objectValue : objectValues){
				double value = (double) objectValue;
				doubleValues.add(value);
			}
			doubleTypeValue.setValues(doubleValues);
		}
		
		return doubleTypeValue;
	}

	/**
	 * Helper method to create an instance of the BooleanType class
	 * @param titleFieldValueRequestDTOs
	 * @return
	 */
	public BooleanType createBooleanTypeValue(List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs){

		BooleanType booleanTypeValue = new BooleanType();

		for(TitleFieldValueRequestDTO titleFieldValueRequestDTO : titleFieldValueRequestDTOs){
			
			//There will be no languages involved here, so there will just one object in titleFieldValueRequestDTOs
			List<Object> objectValues = titleFieldValueRequestDTO.getValues();
			Set<Boolean> booleanValues = new HashSet<>();
			//Cast List<Object> to Set<Boolean> since the fieldType is boolean
			for(Object objectValue : objectValues){
				boolean value = (boolean) objectValue;
				booleanValues.add(value);
			}
			booleanTypeValue.setValues(booleanValues);
		}
		
		return booleanTypeValue;
	}

	/**
	 * Helper method to create an instance of the DateType class
	 * @param titleFieldValueRequestDTOs
	 * @return
	 */
	public DateType createDateTypeValue(List<TitleFieldValueRequestDTO> titleFieldValueRequestDTOs){

		DateType dateTypeValue = new DateType();

		for(TitleFieldValueRequestDTO titleFieldValueRequestDTO : titleFieldValueRequestDTOs){

			//There will be no languages involved here, so there will just one object in titleFieldValueRequestDTOs
			List<Object> objectValues = titleFieldValueRequestDTO.getValues();
			Set<DateTypeUtil> dateValues = new HashSet<>();
			//Cast List<Object> to Set<Date> since the fieldType is date
			for(Object objectValue : objectValues){
				long longVal = (long) objectValue;
				logger.info("Long Value:"+longVal);
				Date value = new Date(longVal);
				logger.info("Value Date:"+value.toString());
				DateTypeUtil dateTypeUtil = new DateTypeUtil();
				dateTypeUtil.setDate(value);
				dateTypeUtil.save();
				dateValues.add(dateTypeUtil);
			}
			dateTypeValue.setValues(dateValues);
		}
		return dateTypeValue;
	}

}