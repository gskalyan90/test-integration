package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.conversions.ContentTypeFactory;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Status;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.Order;
import com.thales.ecms.model.Package;
import com.thales.ecms.model.Package.PackageTitle;
import com.thales.ecms.model.dto.OrderResponseDTO.DisplayGroupDTO;
import com.thales.ecms.model.dto.PackageDTO;
import com.thales.ecms.model.dto.PackageDTO.PackageTitleDTO;
import com.thales.ecms.model.util.DisplayGroup;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service Method to perform operations on a Package
 */
@Service
public class PackageService {

	public static Logger logger = LoggerFactory.getLogger(PackageService.class);

	@Autowired
	MessageSource messageSource;

	/**
	 * Service Method to create a Package
	 * 
	 * @param airlineId
	 * @param orderId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public String createPackage(String airlineId, String orderId) throws ObjectNotFoundException {

		int packageCount = 0;
		List<PackageTitle> packageTitles = null;
		PackageTitle packageTitle = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {

			Order order = Query.from(Order.class).where("id = ?", orderId).and("cms.site.owner = ?", airline).first();
			if (order != null) {
				logger.info("Order: " + order.getName());

				// Get the Set of Configurations in this Order
				Set<Configuration> configurationSet = order.getConfigurationSet();
				if (!configurationSet.isEmpty()) {
					Iterator<Configuration> configurationIterator = configurationSet.iterator();

					while (configurationIterator.hasNext()) {

						Configuration configuration = configurationIterator.next();
						logger.info("Current Configuration: " + configuration.getName());

						// create a new package for this configuration
						Package packageObject = new Package();
						packageObject.setName(order.getName().concat("-").concat(configuration.getName()));
						packageObject.setOrder(order);
						packageObject.setConfiguration(configuration);
						packageObject.setPackagingDate(new Date());
						packageObject.setDisplayGroups(order.getDisplayGroups());

						// Get the list of Titles in the Configuration
						List<ConfigurationTitle> configurationTitles = configuration.getTitleList();
						if (configurationTitles != null && !configurationTitles.isEmpty()) {
							packageTitles = new ArrayList<>();

							// For each Validated Title in the Configuration,
							// add it to the Package
							for (ConfigurationTitle configurationTitle : configurationTitles) {
								if (configurationTitle.getContentStatus() == Status.Validated) {
									logger.info("Status of " + configurationTitle.getContent().getLabel() + " is "
											+ configurationTitle.getContentStatus().toString());
									configurationTitle.setContentStatus(Status.Completed);

									packageTitle = new PackageTitle();
									packageTitle.setContent(configurationTitle.getContent());
									packageTitle.setContentStatus(Status.Completed);
									packageTitle.setPriority(configurationTitle.getPriority());
									packageTitle.setDestinations(configurationTitle.getDestinations());
									packageTitles.add(packageTitle);
								} else {
									logger.info("Discarding " + configurationTitle.getContent().getLabel()
											+ ". Its Status is " + configurationTitle.getContentStatus().toString());
								}
							}
						} else {
							throw new ObjectNotFoundException(
									new ErrorResponse("No Titles exist in the Configuration " + configuration.getName(),
											HttpStatus.NOT_FOUND));
						}
						packageObject.setPackageTitles(packageTitles);
						packageObject.as(Site.ObjectModification.class).setOwner(airline);
						packageObject.save();
						packageCount++;
					}
				} else {
					logger.error("No Configurations exist in the Order " + order.getName());
				}
			} else {
				throw new ObjectNotFoundException(
						new ErrorResponse("Order " + orderId + " does not exist", HttpStatus.NOT_FOUND));
			}
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse("Airline " + airlineId + " does not exist", HttpStatus.NOT_FOUND));
		}
		return packageCount + " Package(s) created successfully";
	}

	public List<PackageDTO> getPackageList(String airlineId, String orderId)
			throws NoSuchMessageException, ObjectNotFoundException {
		List<PackageDTO> packageDTOList = null;
		List<DisplayGroupDTO> displayGroupDTOList = null;
		Order order = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			order = Query.from(Order.class).where("id = ?", orderId).and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(order)) {
				List<Package> packagesList = Query.from(Package.class).where("order = ?", order).selectAll();
				if (Objects.nonNull(packagesList) && !packagesList.isEmpty()) {
					packageDTOList = new ArrayList<>();
					for (Package packageObj : packagesList) {
						PackageDTO packageDTO = new PackageDTO();

						packageDTO.setPackageId(packageObj.getId().toString());
						packageDTO.setName(packageObj.getName());
						packageDTO.setPackagingDate(packageObj.getPackagingDate());

						Order orderObj = packageObj.getOrder();
						packageDTO.setOrderName(orderObj.getName());
						packageDTO.setOrderId(order.getId().toString());

						displayGroupDTOList = getDisplayGroup(packageObj.getDisplayGroups());
						packageDTO.setDisplayGroupDTO(displayGroupDTOList);

						packageDTO.setFullSize(packageObj.getFullSize());

						Configuration configuration = packageObj.getConfiguration();
						if (Objects.nonNull(configuration)) {
							packageDTO.setConfigurationName(configuration.getName());
							packageDTO.setConfigurationId(configuration.getId().toString());
						}
						List<PackageTitleDTO> packageTitleDTOList = getPackageTitle(packageObj.getPackageTitles());
						if (Objects.nonNull(packageTitleDTOList) && !packageTitleDTOList.isEmpty()) {
							packageDTO.setPackageTitles(packageTitleDTOList);
						}

						packageDTOList.add(packageDTO);

					}
				} else {
					throw new ObjectNotFoundException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.PACKAGE }, Locale.US), HttpStatus.NOT_FOUND));
				}
			} else {
				logger.error("order does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return packageDTOList;
	}

	public PackageDTO getPackageById(String airlineId, String orderId, String packageId)
			throws NoSuchMessageException, ObjectNotFoundException {
		Order order = null;
		PackageDTO packageDTO = null;
		List<DisplayGroupDTO> displayGroupDTOList = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			order = Query.from(Order.class).where("id = ?", orderId).and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(order)) {
				Package packageObj = Query.from(Package.class).where("order = ?", order).first();

				if (Objects.nonNull(packageObj)) {
						if (packageObj.getId().toString().equals(packageId)) {
							packageDTO = new PackageDTO();

							packageDTO.setPackageId(packageObj.getId().toString());
							packageDTO.setName(packageObj.getName());
							packageDTO.setPackagingDate(packageObj.getPackagingDate());

							Order orderObj = packageObj.getOrder();
							packageDTO.setOrderName(orderObj.getName());
							packageDTO.setOrderId(order.getId().toString());

							displayGroupDTOList = getDisplayGroup(packageObj.getDisplayGroups());
							packageDTO.setDisplayGroupDTO(displayGroupDTOList);

							packageDTO.setFullSize(packageObj.getFullSize());

							Configuration configuration = packageObj.getConfiguration();
							if (Objects.nonNull(configuration)) {
								packageDTO.setConfigurationName(configuration.getName());
								packageDTO.setConfigurationId(configuration.getId().toString());
							}

							List<PackageTitleDTO> packageTitleDTOList = getPackageTitle(packageObj.getPackageTitles());
							packageDTO.setPackageTitles(packageTitleDTOList);

						}
					
				} else {
					throw new ObjectNotFoundException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.PACKAGE }, Locale.US), HttpStatus.NOT_FOUND));
				}

			} else {
				logger.error("order does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return packageDTO;
	}

	private List<PackageTitleDTO> getPackageTitle(List<PackageTitle> packageTitles) {
		List<PackageTitleDTO> packageTitleDTOList = null;
		PackageTitleDTO packageTitleDTO = null;
		List<DisplayGroupDTO> displayGroupDTOList = null;

		if (Objects.nonNull(packageTitles) && !packageTitles.isEmpty()) {
			packageTitleDTOList = new ArrayList<>();
			for (PackageTitle packageTitle : packageTitles) {
				packageTitleDTO = new PackageTitleDTO();

				packageTitleDTO.setContentName(packageTitle.getContent().getLabel());
				packageTitleDTO.setContentType(ContentTypeFactory
						.convertClassNameWithUnderScore(packageTitle.getContent().getClass().toString()));
				packageTitleDTO.setContentStatus(packageTitle.getContentStatus().name());
				packageTitleDTO.setPriority(packageTitle.getPriority());

				displayGroupDTOList = getDisplayGroup(packageTitle.getDisplayGroups());
				packageTitleDTO.setDisplayGroupDTO(displayGroupDTOList);

				List<LruType> lruTypeList = packageTitle.getDestinations();
				List<String> LruType = new ArrayList<>();
				for (LruType lruType : lruTypeList) {
					LruType.add(lruType.getName());
				}
				packageTitleDTO.setDestinations(LruType);

				packageTitleDTOList.add(packageTitleDTO);
			}
		} else {
			logger.info("No display groups are availble for this order");
		}
		return packageTitleDTOList;
	}

	private List<DisplayGroupDTO> getDisplayGroup(List<DisplayGroup> displayGroups) {
		List<DisplayGroupDTO> displayGroupDTOList = null;
		DisplayGroupDTO displayGroupDTO = null;

		if (Objects.nonNull(displayGroups) && !displayGroups.isEmpty()) {
			displayGroupDTOList = new ArrayList<>();
			for (DisplayGroup displayGroup : displayGroups) {
				displayGroupDTO = new DisplayGroupDTO();
				displayGroupDTO.setMonth(displayGroup.getMonth());
				displayGroupDTO.setStartDate(displayGroup.getStartDate());
				displayGroupDTO.setEndDate(displayGroup.getEndDate());
				displayGroupDTOList.add(displayGroupDTO);
			}
		} else {
			logger.info("No display groups are availble for this order");
		}
		return displayGroupDTOList;
	}

}
