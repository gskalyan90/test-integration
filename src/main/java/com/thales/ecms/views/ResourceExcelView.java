package com.thales.ecms.views;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.thales.ecms.utils.ExcelCellStyleUtil;
import com.thales.ecms.utils.ExcelCellStyleUtil.SECTION;
import com.thales.ecms.utils.ResourceExportManager;

/**
 * Custom Excel View for Resources exported 
 * 
 * @author arjun.p
 *
 */
public class ResourceExcelView extends AbstractXlsxView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {		
		response.setHeader("Content-Disposition", "attachment; filename=\"ecms-resources.xlsx\"");				
		Map<SECTION, CellStyle> cellStyles = ExcelCellStyleUtil.getCellStyles(workbook);
		ResourceExportManager resourceExportManager = ResourceExportManager.newInstance(workbook,cellStyles);		
		resourceExportManager.getResourceTemplateBuilder().buildTemplate();
		if(workbook.getNumberOfSheets()==0)
			workbook.createSheet();
	}
}
