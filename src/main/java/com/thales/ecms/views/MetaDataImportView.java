package com.thales.ecms.views;

import java.util.ArrayList;
import java.util.List;

import com.psddev.cms.tool.Plugin;
import com.psddev.cms.tool.Tool;

public class MetaDataImportView extends Tool {

	@Override
	public List<Plugin> getPlugins() {
		List<Plugin> plugins = new ArrayList();

		plugins.add(createArea2("MetaData Import", "metaDataImport", "dashboard/metaDataImport",
				"/cms/content/metadaimport/import.jsp"));

		return plugins;
	}
}
