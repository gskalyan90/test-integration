package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.FieldTypes;
import com.thales.ecms.model.dto.FieldRequestDTO;
import com.thales.ecms.model.dto.FieldResponseDTO;
import com.thales.ecms.service.FieldService;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Controller Class to perform CRUD operations on Fields
 */
@Controller
public class FieldController {
	
	public static Logger logger = LoggerFactory.getLogger(FieldController.class);
	
	@Autowired
	private FieldService fieldService;
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Controller Method to create a Generic Field
	 * @param request
	 * @param response
	 * @param fieldRequestDTO
	 * @return
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws EmptyResponseException 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/fields", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> addGenericField(HttpServletRequest request, HttpServletResponse response, @RequestBody FieldRequestDTO fieldRequestDTO) throws NoSuchMessageException, BadRequestException, EmptyResponseException{
		
		FieldResponseDTO fieldResponseDTO = fieldService.addGenericField(fieldRequestDTO);
		if(fieldResponseDTO != null){
			return new ResponseEntity<>(fieldResponseDTO, HttpStatus.OK);
		}
		else{
			logger.error("Response was empty - cannot create Generic Field");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE, new Object[]{ECMSConstants.FIELD}, Locale.US), HttpStatus.FORBIDDEN));
		}		
	}
	
	/**
	 * Controller Method to return a List of Generic Fields
	 * @param request
	 * @param response
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/fields", produces = "application/json")
	public ResponseEntity<?> getGenericFields(HttpServletRequest request, HttpServletResponse response) throws NoSuchMessageException, ObjectNotFoundException, EmptyResponseException{
		
		List<FieldResponseDTO> fieldResponseDTOList = fieldService.getGenericFields();
		if(!CollectionUtils.isEmpty(fieldResponseDTOList)){
			return new ResponseEntity<>(fieldResponseDTOList, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to return a Field (Generic/Specific) by id
	 * @param request
	 * @param response
	 * @param fieldId
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws EmptyResponseException 
	 * @throws NoSuchMessageException 
	 * @throws ObjectNotFoundException 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/fields/{fieldInternalId}", produces = "application/json")
	public ResponseEntity<?> getField(HttpServletRequest request, HttpServletResponse response, @PathVariable String fieldInternalId) throws UnsupportedEncodingException, NoSuchMessageException, EmptyResponseException, ObjectNotFoundException{
		
		fieldInternalId = URLDecoder.decode(fieldInternalId, "UTF-8");
		FieldResponseDTO fieldResponseDTO = fieldService.getFieldById(fieldInternalId);
		if(fieldResponseDTO != null){
			return new ResponseEntity<>(fieldResponseDTO, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to delete a Field (Generic/Specific)
	 * @param fieldInternalId
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws ObjectNotFoundException 
	 * @throws NoSuchMessageException 
	 * @throws EmptyResponseException 
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/fields/{fieldInternalId}", produces = "application/json")
	public ResponseEntity<?> deleteField(HttpServletRequest request, HttpServletResponse response, @PathVariable String fieldInternalId) throws UnsupportedEncodingException, NoSuchMessageException, ObjectNotFoundException, EmptyResponseException{
		
		fieldInternalId = URLDecoder.decode(fieldInternalId, "UTF-8");
		boolean result = fieldService.deleteField(fieldInternalId);
		if(result){
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY, new Object[]{ECMSConstants.FIELD}, Locale.US)), HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty - cannot delete Field");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE, new Object[]{ECMSConstants.FIELD}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to create a Specific Field
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param fieldRequestDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws BadRequestException 
	 * @throws ObjectNotFoundException 
	 * @throws NoSuchMessageException 
	 * @throws EmptyResponseException 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/fields", consumes = "application/json", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> addSpecificField(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId, @RequestBody FieldRequestDTO fieldRequestDTO) throws UnsupportedEncodingException, NoSuchMessageException, ObjectNotFoundException, BadRequestException, EmptyResponseException{
		
		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		FieldResponseDTO fieldResponseDTO = fieldService.addSpecificField(airlineId, fieldRequestDTO);
		if(fieldResponseDTO != null){
			return new ResponseEntity<>(fieldResponseDTO, HttpStatus.OK);
		}
		else{
			logger.error("Response was empty - cannot create Specific Field");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE, new Object[]{ECMSConstants.FIELD}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to get a list of Specific Fields
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException 
	 * @throws NoSuchMessageException 
	 * @throws EmptyResponseException 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/fields", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getSpecificFields(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId) throws UnsupportedEncodingException, NoSuchMessageException, ObjectNotFoundException, EmptyResponseException{
		
		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		List<FieldResponseDTO> fieldResponseDTOList = fieldService.getSpecificFields(airlineId);
		if(!CollectionUtils.isEmpty(fieldResponseDTOList)){
			return new ResponseEntity<>(fieldResponseDTOList, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/get/fieldTypes", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getAllFieldType() {		
		return new ResponseEntity<>(FieldTypes.class.getEnumConstants(), HttpStatus.OK);		
	}
}
