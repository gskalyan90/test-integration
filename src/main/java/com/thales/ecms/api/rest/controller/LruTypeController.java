package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.LruTypeDTO;
import com.thales.ecms.service.LruTypeService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir
 * Controller Class for CRUD operations on LRU Type
 */
@Controller
public class LruTypeController {

	@Autowired
	LruTypeService lruTypeService;
	
	@Autowired
	private MessageSource messageSource;

	
	/**
	 * Controller Method to add an LRU Type
	 * @param request
	 * @param response
	 * @param lruTypeDetails
	 * @return
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Add lruType as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/lruTypes", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addLRUType(HttpServletRequest request, HttpServletResponse response,  
			@ApiParam(value = "DTO Object for lruType", required = true) @RequestBody LruTypeDTO lruTypeDetails) 
					throws EmptyResponseException, ObjectExistsException, BadRequestException, UnsupportedEncodingException, ObjectNotFoundException{

		LruTypeDTO lruTypeDTO = lruTypeService.addLRUType(lruTypeDetails);
		if(lruTypeDTO != null){
			return new ResponseEntity<>(lruTypeDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
					new Object[] { ECMSConstants.LRU_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to return a list of LRU Types
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Retrieve all lruTypes")
	@RequestMapping(method = RequestMethod.GET, value = "/lruTypes", produces =  ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getLRUTypes(HttpServletRequest request, HttpServletResponse response) 
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException{

		List<LruTypeDTO> lruTypeDTOList = lruTypeService.getLRUTypes();
		if(lruTypeDTOList != null){
			return new ResponseEntity<>(lruTypeDTOList, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,null, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	
	/**
	 * Controller Method to return an LRU Type
	 * @param request
	 * @param response
	 * @param lruTypeId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException 
	 * @throws EmptyResponseException 
	 */
	
	@ApiOperation(value = "Retrieve lruType as per given lruTypeID")
	@RequestMapping(method = RequestMethod.GET, value = "/lruTypes/{lruTypeId}", produces =  ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getLRUTypeById(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable("lruTypeId") String lruTypeId) throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException{

		lruTypeId = URLDecoder.decode(lruTypeId, ECMSConstants.UTF);
		LruTypeDTO lruTypeDTO = lruTypeService.getLRUTypeById(lruTypeId);
		if(lruTypeDTO != null){
			return new ResponseEntity<>(lruTypeDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,null, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to update an LRU Type
	 * @param request
	 * @param response
	 * @param lruTypeId
	 * @param lruTypeDetails
	 * @return
	 * @throws EmptyResponseException 
	 * @throws UnsupportedEncodingException 
	 * @throws ObjectNotFoundException 
	 * @throws BadRequestException 
	 * @throws ObjectExistsException 
	 */
	
	@ApiOperation(value = "Update lruType as per given lruTypeID")
	@RequestMapping(method = RequestMethod.PUT, value = "/lruTypes/{lruTypeId}", consumes =  ECMSConstants.CONSUME_FORMAT, produces =  ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateLRUType(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable("lruTypeId") String lruTypeId,
			@ApiParam(value = "DTO Object for lru", required = true) @RequestBody LruTypeDTO lruTypeDetails) 
					throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException, BadRequestException, ObjectExistsException{
		
		lruTypeId = URLDecoder.decode(lruTypeId, ECMSConstants.UTF);
		LruTypeDTO lruTypeDTO = lruTypeService.updateLRUType(lruTypeId, lruTypeDetails);
		if(lruTypeDTO != null){
			return new ResponseEntity<>(lruTypeDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
					new Object[] { ECMSConstants.LRU_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to delete an LRU Type
	 * @param request
	 * @param response
	 * @param lruTypeId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException 
	 * @throws ObjectNotFoundException 
	 * @throws ObjectExistsException 
	 * @throws BadRequestException 
	 * @throws NoSuchMessageException 
	 */
	
	@ApiOperation(value = "Delete lruType as per given lruTypeID")
	@RequestMapping(method = RequestMethod.DELETE, value = "lruTypes/{lruTypeId}", produces =  ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteLRUType(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable("lruTypeId") String lruTypeId) 
					throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, ObjectExistsException, NoSuchMessageException, BadRequestException{
		
		lruTypeId = URLDecoder.decode(lruTypeId, ECMSConstants.UTF);
		Boolean responseString = lruTypeService.deleteLRUType(lruTypeId);
		if(responseString){
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.LRU_TYPE}, Locale.US )), HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
					new Object[] { ECMSConstants.LRU_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
}
