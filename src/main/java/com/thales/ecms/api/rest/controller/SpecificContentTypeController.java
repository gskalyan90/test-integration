package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.SpecificContentTypeRequestDTO;
import com.thales.ecms.model.dto.SpecificContentTypeResponseDTO;
import com.thales.ecms.service.SpecificContentTypeService;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Controller Class to perform operations on Specific Content Types
 */
@Controller
public class SpecificContentTypeController {

	public static Logger logger = LoggerFactory.getLogger(SpecificContentTypeController.class);
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private SpecificContentTypeService specificContentTypeService;
	
	/**
	 * Controller Method to add a Specific Content Type
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param specificContentTypeRequestDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException 
	 * @throws ObjectExistsException 
	 * @throws BadRequestException 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/specificContentTypes", consumes = "application/json", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> addSpecificContentType(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId, 
			@RequestBody SpecificContentTypeRequestDTO specificContentTypeRequestDTO) throws UnsupportedEncodingException, NoSuchMessageException, EmptyResponseException, ObjectNotFoundException, BadRequestException, ObjectExistsException{
		
		airlineId = URLDecoder.decode(airlineId,"UTF-8");
		SpecificContentTypeResponseDTO specificContentTypeResponseDTO = specificContentTypeService.addSpecificContentType(airlineId, specificContentTypeRequestDTO);
		if(specificContentTypeResponseDTO != null){
			return new ResponseEntity<>(specificContentTypeResponseDTO, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty - cannot create Specific Content Type");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to return a list of Specific Content Types of an Airline
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/specificContentTypes", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getSpecificContentTypes(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId) throws UnsupportedEncodingException, NoSuchMessageException, EmptyResponseException, ObjectNotFoundException{
		
		airlineId = URLDecoder.decode(airlineId,"UTF-8");
		List<SpecificContentTypeResponseDTO> specificContentTypeDTOList = specificContentTypeService.getSpecificContentTypes(airlineId);
		if(!CollectionUtils.isEmpty(specificContentTypeDTOList)){
			Gson gson = new Gson();
			String responseString = gson.toJson(specificContentTypeDTOList);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to return a Specific Content Type of an Airline
	 * @param request
	 * @param response
	 * @param specificContentTypeId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException 
	 * @throws NoSuchMessageException 
	 * @throws ObjectNotFoundException 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/specificContentTypes/{specificContentTypeId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getSpecificContentType(HttpServletRequest request, HttpServletResponse response, @PathVariable("specificContentTypeId") String specificContentTypeId) throws UnsupportedEncodingException, NoSuchMessageException, EmptyResponseException, ObjectNotFoundException{
		
		specificContentTypeId = URLDecoder.decode(specificContentTypeId,"UTF-8");
		SpecificContentTypeResponseDTO specificContentTypeResponseDTO = specificContentTypeService.getSpecificContentTypeById(specificContentTypeId);
		if(specificContentTypeResponseDTO != null){
			return new ResponseEntity<>(specificContentTypeResponseDTO, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to delete a Specific Content Type
	 * @param request
	 * @param response
	 * @param specificContentTypeId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/specificContentTypes/{specificContentTypeId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> deleteSpecificContentType(HttpServletRequest request, HttpServletResponse response, @PathVariable String specificContentTypeId) throws UnsupportedEncodingException, NoSuchMessageException, ObjectNotFoundException, EmptyResponseException{
		
		specificContentTypeId = URLDecoder.decode(specificContentTypeId,"UTF-8");
		if(specificContentTypeService.deleteSpecificContentType(specificContentTypeId)){
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPE}, Locale.US)),
					HttpStatus.OK);
		}
		else{
			logger.error("Cannot delete Generic Content Type");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE, new Object[]{ECMSConstants.SPECIFIC_CONTENT_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
}
