package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.AircraftDTO;
import com.thales.ecms.service.AircraftService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir
 * Controller Class for CRUD operations on AircraftType
 */
@Controller
public class AircraftController {

	@Autowired
	AircraftService aircraftService;

	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Controller Method to add an Aircraft
	 * @param request
	 * @param response
	 * @param aircraftTypeDetails
	 * @return
	 * @throws ObjectExistsException
	 * @throws EmptyResponseException
	 */
	
	@ApiOperation(value = "Add Aircrafts as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/aircraftTypes", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addAircraft(HttpServletRequest request, HttpServletResponse response, 
			@ApiParam(value = "DTO Objects for Aircraft", required = true)
			@RequestBody AircraftDTO aircraftTypeDetails) throws ObjectExistsException, EmptyResponseException{

		AircraftDTO aircraftTypeDTO = aircraftService.addAircraft(aircraftTypeDetails);
		if(aircraftTypeDTO != null){
			return new ResponseEntity<>(aircraftTypeDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
					new Object[] { ECMSConstants.AIRCRAFT }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to return all AircraftTypes
	 * @param request
	 * @param response
	 * @return
	 * @throws ObjectNotFoundException 
	 * @throws EmptyResponseException
	 */
	
	@ApiOperation(value = "Retrieve all Aircrafts")
	@RequestMapping(method = RequestMethod.GET, value = "/aircraftTypes", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getAircraftTypes(HttpServletRequest request, HttpServletResponse response) 
			throws EmptyResponseException, ObjectNotFoundException {

		List<AircraftDTO> aircraftTypeList = aircraftService.getAircraftTypes();
		if(aircraftTypeList != null ){
			return new ResponseEntity<>(aircraftTypeList, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,null, Locale.US), HttpStatus.NOT_FOUND));
		}
	}


	/**
	 * Controller Method to update an AircraftType
	 * @param request
	 * @param response
	 * @param aircraftTypeName
	 * @param aircraftTypeDetails
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws EmptyResponseException 
	 * @throws ObjectNotFoundException  
	 */
	
	@ApiOperation(value = "Update Aircraft as per given AircraftID")
	@RequestMapping(method = RequestMethod.PUT, value = "/aircraftTypes/{aircraftTypeId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateAircraftType(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String aircraftTypeId, 
			@ApiParam(value = "DTO Objects for Aircraft", required = true) @RequestBody AircraftDTO aircraftTypeDetails) 
					throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		aircraftTypeId = URLDecoder.decode(aircraftTypeId,ECMSConstants.UTF);
		AircraftDTO aircraftTypeDTO = aircraftService.updateAircraftType(aircraftTypeId, aircraftTypeDetails);
		if(aircraftTypeDTO != null){
			return new ResponseEntity<>(aircraftTypeDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
					new Object[] { ECMSConstants.AIRCRAFT }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to delete an AircraftType
	 * @param request
	 * @param response
	 * @param aircraftTypeName
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws ObjectNotFoundException 
	 * @throws EmptyResponseException 
	 * @throws O
	 */
	
	@ApiOperation(value = "Delete aircraft as per given AircraftID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/aircraftTypes/{aircraftTypeId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteAircraftType(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String aircraftTypeId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException, ObjectAssociationExistsException{

		aircraftTypeId = URLDecoder.decode(aircraftTypeId,ECMSConstants.UTF);
		Boolean responseString = aircraftService.deleteAircraftType(aircraftTypeId);
		if(responseString != null){
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.AIRCRAFT }, Locale.US )), HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
					new Object[] { ECMSConstants.AIRCRAFT }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
}
