package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.ShipmentProfileDTO;
import com.thales.ecms.service.ShipmentProfileService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author kumargupta.v
 *
 */
@Controller
@RequestMapping("shipmentProfile")
public class ShipmentProfileController {
	
	@Autowired
	ShipmentProfileService shipmentProfileService;
	
	@Autowired
	private MessageSource messageSource;
	
	public static Logger logger = LoggerFactory.getLogger(ShipmentProfileController.class);
	
	
	/**
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param shipmentProfileDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Add Shipment Profile for an Airline") 
	@RequestMapping(method = RequestMethod.POST, value = "/add/{airlineId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addShipmentProfile(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,
			@ApiParam(value = "DTO Objects for Shipment Profile", required = true) @RequestBody ShipmentProfileDTO shipmentProfileDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ShipmentProfileDTO shipmentDTO = shipmentProfileService.addShipmentProfile(airlineId, shipmentProfileDTO);
		if (Objects.nonNull(shipmentDTO)) {
			return new ResponseEntity<>(shipmentDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] {ECMSConstants.SHIPMENTPROFILE }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param shipmentProfileId
	 * @param shipmentProfileDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Update Shipment Profile for an Airline")
	@RequestMapping(method = RequestMethod.PUT, value = "/update/{airlineId}/{shipmentProfileId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateShipmentProfile(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,@PathVariable String shipmentProfileId,
			@ApiParam(value = "DTO Objects for Shipment Profile", required = true) @RequestBody ShipmentProfileDTO shipmentProfileDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		shipmentProfileId = URLDecoder.decode(shipmentProfileId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(shipmentProfileId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] {}, Locale.US), HttpStatus.BAD_REQUEST));
		ShipmentProfileDTO shipmentDTO = shipmentProfileService.updateShipmentProfile(airlineId, shipmentProfileId, shipmentProfileDTO);
		if (Objects.nonNull(shipmentDTO)) {
			return new ResponseEntity<>(shipmentDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] {ECMSConstants.SHIPMENTPROFILE }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	
	
	/**
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param shipmentProfileId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Delete Shipment Profile for an Airline")
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete/{airlineId}/{shipmentProfileId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteShipmentProfile(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,@PathVariable String shipmentProfileId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		shipmentProfileId = URLDecoder.decode(shipmentProfileId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(shipmentProfileId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] {  }, Locale.US), HttpStatus.BAD_REQUEST));
		Boolean result = shipmentProfileService.deleteShipmentProfile(airlineId, shipmentProfileId);
		if (result) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] {ECMSConstants.SHIPMENTPROFILE}, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Get all Shipment Profiles for an Airline")
	@RequestMapping(method = RequestMethod.GET, value = "/shipmentProfileList/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getShipmentProfileList(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		List<ShipmentProfileDTO> shipmentProfileDTOList = shipmentProfileService.getShipmentProfileList(airlineId);
		if (Objects.nonNull(shipmentProfileDTOList)) {
			if(shipmentProfileDTOList.isEmpty()){
				throw new EmptyResponseException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
								new Object[] { }, Locale.US), HttpStatus.FORBIDDEN));
			}
			return new ResponseEntity<>(shipmentProfileDTOList, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] {ECMSConstants.SHIPMENTPROFILE }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param shipmentProfileId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Get Shipment Profile by shipment profile Id")
	@RequestMapping(method = RequestMethod.GET, value = "/shipmentProfileListById/{airlineId}/{shipmentProfileId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getShipmentProfileById(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId,@PathVariable String shipmentProfileId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ShipmentProfileDTO shipmentProfileDTO = shipmentProfileService.getShipmentProfileById(airlineId, shipmentProfileId);
		if (Objects.nonNull(shipmentProfileDTO)) {
			return new ResponseEntity<>(shipmentProfileDTO, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.SHIPMENTPROFILE }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
}
