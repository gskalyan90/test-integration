package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.LopaDTO;
import com.thales.ecms.model.dto.SeatingClassLruTypeMappingDTO;
import com.thales.ecms.service.LopaService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir Controller Class to perform CRUD operations on LOPA
 */
@Controller
public class LopaController {

	@Autowired
	LopaService lopaService;

	@Autowired
	private MessageSource messageSource;

	/**
	 * Controller Method to add a LOPA, along with its Seating Class and LRU
	 * Type Mappings
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lopaDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@ApiOperation("Add a LOPA, together with its Seating Class and LRU Type Mappings")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/lopas", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> addLopa(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId,
			@ApiParam(value = "DTO Objects for Lopa", required = true) @RequestBody LopaDTO lopaDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException,
			ObjectExistsException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		LopaDTO lopaDTO = lopaService.addLopa(airlineId, lopaDetails);
		if (lopaDTO != null) {
			Gson gson = new Gson();
			String responseString = gson.toJson(lopaDTO);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to return a list of LOPAs
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation("Return a List of LOPAs")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/lopas", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getLopas(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<LopaDTO> lopaDTOList = lopaService.getLopas(airlineId);
		if (lopaDTOList != null && !lopaDTOList.isEmpty()) {
			Gson gson = new Gson();
			String responseString = gson.toJson(lopaDTOList);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to return a LOPA
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lopaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation("Return a LOPA as per given id")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/lopas/{lopaId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getLopa(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("airlineId") String airlineId, @PathVariable("lopaId") String lopaId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		lopaId = URLDecoder.decode(lopaId, ECMSConstants.UTF);
		LopaDTO lopaDTO = lopaService.getLopaById(airlineId, lopaId);
		if (lopaDTO != null) {
			return new ResponseEntity<>(lopaDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to update a LOPA - can only update the name of the LOPA
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lopaId
	 * @param lopaDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectExistsException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation("Update a LOPA as per given id- you can only update the name of the LOPA here")
	@RequestMapping(method = RequestMethod.PUT, value = "/lopas/{lopaId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)

	public ResponseEntity<?> updateLopa(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("lopaId") String lopaId,
			@ApiParam(value = "DTO Objects for Lopa", required = true) @RequestBody LopaDTO lopaDetails)
			throws UnsupportedEncodingException, ObjectNotFoundException, ObjectExistsException,
			EmptyResponseException {

		lopaId = URLDecoder.decode(lopaId, ECMSConstants.UTF);
		LopaDTO lopaDTO = lopaService.updateLopa(lopaId, lopaDetails);
		if (lopaDTO != null) {
			return new ResponseEntity<>(lopaDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to delete a LOPA
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lopaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation("Delete a LOPA as per given id")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}/lopas/{lopaId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> deleteLopa(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("airlineId") String airlineId, @PathVariable("lopaId") String lopaId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		lopaId = URLDecoder.decode(lopaId, ECMSConstants.UTF);
		Boolean responseString = lopaService.deleteLopa(airlineId, lopaId);
		if (responseString) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.LOPA }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to add a Seating Class LRU Type Mapping to a LOPA You
	 * can either add both or just add the Seating Class
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lopaId
	 * @param seatingClassLruTypeMappingDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */

	@ApiOperation("Add/Update a 'Seating Class & LRU Type Mapping' to a given LOPA. You need to provide both the Seating Class and the LRU Type. Assumes that the Seating Class and the LRU Type exist")
	@RequestMapping(method = RequestMethod.PUT, value = "/airlines/{airlineId}/lopas/{lopaId}/addSeatingClassLRUType", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)

	public ResponseEntity<?> addSeatingClassLruType(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("airlineId") String airlineId, @PathVariable("lopaId") String lopaId,
			@ApiParam(value = "DTO Object for SeatingClassLruTypeMapping", required = true) @RequestBody(required = true) SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException, BadRequestException,
			ObjectExistsException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		lopaId = URLDecoder.decode(lopaId, ECMSConstants.UTF);
		LopaDTO lopaDTO = lopaService.addSeatingClassLruType(airlineId, lopaId, seatingClassLruTypeMappingDTO);
		if (lopaDTO != null) {
			return new ResponseEntity<>(lopaDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.LOPA_ADD,
					new Object[] { ECMSConstants.SEATING_CLASS, ECMSConstants.LRU_TYPE, ECMSConstants.LOPA },
					Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to delete a Seating Class LRU Type Mapping from a LOPA
	 * You only need to pass in the Seating Class Id
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lopaId
	 * @param seatingClassId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws EmptyResponseException
	 */
	@ApiOperation("Delete a Seating Class & LRU Type Mapping from a given LOPA. You only need to pass in the Seating Class. Assumes that the Seating Class exists")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}/lopas/{lopaId}/seatingClass/{seatingClassId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> deleteSeatingClassLruType(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("airlineId") String airlineId, @PathVariable("lopaId") String lopaId,
			@PathVariable("seatingClassId") String seatingClassId)
			throws UnsupportedEncodingException, ObjectNotFoundException, BadRequestException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		lopaId = URLDecoder.decode(lopaId, ECMSConstants.UTF);
		Boolean responseString = lopaService.deleteSeatingClassLruType(airlineId, lopaId, seatingClassId);
		if (responseString) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.LOPA_REMOVE,
					new Object[] { ECMSConstants.SEATING_CLASS, ECMSConstants.LRU_TYPE, ECMSConstants.LOPA },
					Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param aircraftInternalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation("Return a List of LOPAs by AircraftId")
	@RequestMapping(method = RequestMethod.GET, value = "/aircraft/{aircraftInternalId}/lopas", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getLopasByAircraftId(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String aircraftInternalId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException {

		aircraftInternalId = URLDecoder.decode(aircraftInternalId, ECMSConstants.UTF);
		List<LopaDTO> lopaDTOList = lopaService.getLopasByAircraftId(aircraftInternalId);
		if (lopaDTOList != null && !lopaDTOList.isEmpty()) {
			Gson gson = new Gson();
			String responseString = gson.toJson(lopaDTOList);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

}