package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.ConfigurationDTO;
import com.thales.ecms.model.dto.OrderRequestDTO;
import com.thales.ecms.model.dto.OrderResponseDTO;
import com.thales.ecms.service.OrderService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	OrderService orderService;
	@Autowired
	private MessageSource messageSource;

	/**
	 * Controller method for adding Order
	 * 
	 * @param request
	 * @param response
	 * @param Order
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/orderList", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getOrerList(
			@ApiParam(value = "Airline ID ", required = true) @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<OrderResponseDTO> orderDTOList = orderService.getOrerList(airlineId);
		if (Objects.nonNull(orderDTOList)) {
			return new ResponseEntity<>(orderDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to return an Order by ID
	 * 
	 * @param request
	 * @param response
	 * @param orderId
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */

	@ApiOperation(value = "Retrieve Order as per given Order ID")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/orderById/{orderId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getOrderById(
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Order ID", required = true) @PathVariable String orderId)
			throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		orderId = URLDecoder.decode(orderId, ECMSConstants.UTF);
		OrderResponseDTO orderResponseDTO = orderService.getOrderById(airlineId, orderId);
		if (orderResponseDTO != null) {
			return new ResponseEntity<>(orderResponseDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to create an order with airline id
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws NoSuchMessageException
	 */
	@ApiOperation(value = "creating order with airline id")
	@RequestMapping(method = RequestMethod.POST, value = "/createOrder/airlines/{airlineId}", consumes = "application/json", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> createOrder(HttpServletRequest request, HttpServletResponse response,
			@RequestBody OrderRequestDTO orderRequestDTO,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException,
			NoSuchMessageException, ObjectExistsException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		OrderResponseDTO orderResponseDTO = orderService.createOrder(airlineId, orderRequestDTO);
		if (!Objects.isNull(orderResponseDTO)) {
			return new ResponseEntity<>(orderResponseDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to Search an order by name, description, status, Start
	 * date and end date.
	 * 
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation(value = "Retrieve Order as per given search attribute")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/searchOrder", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> searchOrder(@RequestBody OrderRequestDTO orderRequestDTO,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId)
			throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<OrderResponseDTO> orderResponseDTOList = orderService.searchOrder(airlineId, orderRequestDTO);
		if (orderResponseDTOList != null) {
			return new ResponseEntity<>(orderResponseDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to modify a order.
	 * 
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException 
	 * @throws NoSuchMessageException 
	 */
	@ApiOperation(value = "Update order by order ID")
	@RequestMapping(method = RequestMethod.PUT, value = "/airlines/{airlineId}/updateOrderById/{orderId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> updateOrder(@RequestBody OrderRequestDTO orderRequestDTO,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Order ID", required = true) @PathVariable String orderId)
			throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException, NoSuchMessageException, ObjectExistsException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		orderId = URLDecoder.decode(orderId, ECMSConstants.UTF);
		OrderResponseDTO orderResponseDTO = orderService.updateOrder(airlineId, orderId, orderRequestDTO);
		if (orderResponseDTO != null) {
			return new ResponseEntity<>(orderResponseDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE, null, Locale.US),
							HttpStatus.FORBIDDEN));
		}
	}


	/**
	 * Controller Method to modify a order.
	 * 
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation(value = "Delete order by order ID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}/deleteOrderById/{orderId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> deleteOrder(
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Order ID", required = true) @PathVariable String orderId)
			throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		orderId = URLDecoder.decode(orderId, ECMSConstants.UTF);
		String deleteResponse = orderService.deleteOrder(airlineId, orderId);
		if (deleteResponse != null) {
			deleteResponse = ObjectUtils.toJson(deleteResponse);
			return new ResponseEntity<>(deleteResponse, HttpStatus.OK);
		} else
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.FORBIDDEN));
	}

	// @ApiOperation(value = "Retrieve Order as per given Order ID")
	@RequestMapping(method = RequestMethod.GET, value = "/configurationByAirline/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getCongigurationByAirline(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId)
			throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<ConfigurationDTO> configurationDTO = orderService.getCongifurationByAirline(airlineId);
		if (configurationDTO != null) {
			return new ResponseEntity<>(configurationDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}
}
