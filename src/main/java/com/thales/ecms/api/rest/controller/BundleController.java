package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.BundleDTO;
import com.thales.ecms.service.BundleService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author vaibhav Controller Class for CRUD operations on Bundle
 */

@RestController
@RequestMapping("bundle")
public class BundleController {

	@Autowired
	BundleService bundleService;

	@Autowired
	private MessageSource messageSource;

	/**
	 * Controller Method to add a Bundle
	 * 
	 * @param request
	 * @param response
	 * @param bundleTypeDetails
	 * @return
	 * @throws ObjectExistsException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 */

	@ApiOperation(value = "Add Bundle as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/add/{airlineId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> addBundle(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "DTO Objects for Bundle", required = true) @RequestBody BundleDTO bundleDetails)
			throws ObjectExistsException, EmptyResponseException, NoSuchMessageException, ObjectNotFoundException,
			BadRequestException, UnsupportedEncodingException {
		airlineId = URLDecoder.decode(airlineId, "UTF-8").trim();
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		BundleDTO bundleDTO = bundleService.addBundle(airlineId, bundleDetails);
		if (bundleDTO != null) {
			return new ResponseEntity<>(bundleDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.BUNDLE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller method for getting Bundles
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ObjectExistsException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 */

	@ApiOperation(value = "Bundle List as per given airlineId")
	@RequestMapping(method = RequestMethod.GET, value = "/getBundles/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getBundleList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId)
			throws NoSuchMessageException, ObjectNotFoundException, EmptyResponseException, BadRequestException,
			UnsupportedEncodingException {
		airlineId = URLDecoder.decode(airlineId, "UTF-8").trim();
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		List<BundleDTO> bundleDTOList = bundleService.getBundleList(airlineId);
		if (Objects.nonNull(bundleDTOList)) {
			return new ResponseEntity<>(bundleDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param bundleId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value = "Bundle as per given airlineId and bundleId")
	@RequestMapping(method = RequestMethod.GET, value = "/getBundle/{airlineId}/{bundleId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getBundleById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Bundle ID", required = true) @PathVariable String bundleId)
			throws NoSuchMessageException, ObjectNotFoundException, EmptyResponseException, BadRequestException,
			UnsupportedEncodingException {
		airlineId = URLDecoder.decode(airlineId, "UTF-8").trim();
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		bundleId = URLDecoder.decode(bundleId, "UTF-8").trim();
		if (StringUtils.isBlank(bundleId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.BUNDLE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		BundleDTO bundle = bundleService.getBundleById(airlineId, bundleId);
		if (Objects.nonNull(bundle)) {
			return new ResponseEntity<>(bundle, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param bundleId
	 * @param bundleDetails
	 * @return
	 * @throws ObjectExistsException
	 * @throws EmptyResponseException
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value = "Update Bundle as per given DTO")
	@RequestMapping(method = RequestMethod.PUT, value = "/update/{airlineId}/{bundleId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> updateBundle(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Bundle ID", required = true) @PathVariable String bundleId,
			@ApiParam(value = "DTO Objects for Bundle", required = true) @RequestBody BundleDTO bundleDetails)
			throws ObjectExistsException, EmptyResponseException, NoSuchMessageException, ObjectNotFoundException,
			BadRequestException, UnsupportedEncodingException {
		airlineId = URLDecoder.decode(airlineId, "UTF-8").trim();
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		bundleId = URLDecoder.decode(bundleId, "UTF-8").trim();
		if (StringUtils.isBlank(bundleId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.BUNDLE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		BundleDTO bundleDTO = bundleService.updateBundle(airlineId, bundleId, bundleDetails);
		if (bundleDTO != null) {
			return new ResponseEntity<>(bundleDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.BUNDLE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param bundleId
	 * @return
	 * @throws NoSuchMessageException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value = "Delete Bundle as per given airlineId and bundleId")
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete/{airlineId}/{bundleId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> deleteBundle(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Bundle ID", required = true) @PathVariable String bundleId)
			throws NoSuchMessageException, ObjectNotFoundException, EmptyResponseException, BadRequestException,
			UnsupportedEncodingException {
		airlineId = URLDecoder.decode(airlineId, "UTF-8").trim();
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		bundleId = URLDecoder.decode(bundleId, "UTF-8").trim();
		if (StringUtils.isBlank(bundleId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.BUNDLE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		Boolean result = bundleService.deleteBundle(airlineId, bundleId);
		if (result) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.BUNDLE }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.BUNDLE }, Locale.US), HttpStatus.FORBIDDEN));
		}

	}

}
