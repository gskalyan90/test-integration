package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.ServerGroupDTO;
import com.thales.ecms.model.dto.ShipmentProfileDTO;
import com.thales.ecms.service.ServerGroupService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author kumargupta.v
 *
 */
@Controller
@RequestMapping("serverGroup")
public class ServerGroupController {
	
	@Autowired
	ServerGroupService serverGroupService;
	
	@Autowired
	private MessageSource messageSource;
	
	public static Logger logger = LoggerFactory.getLogger(ServerGroupController.class);
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param serverGroupDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Add Server Group") 
	@RequestMapping(method = RequestMethod.POST, value = "/add/{airlineId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addServerGroup(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,
			@ApiParam(value = "DTO Objects for Server Group", required = true) @RequestBody ServerGroupDTO serverGroupDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ServerGroupDTO serverGroupDto = serverGroupService.addServerGroup(airlineId, serverGroupDTO);
		if (Objects.nonNull(serverGroupDto)) {
			return new ResponseEntity<>(serverGroupDto, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] {ECMSConstants.SERVERGROUP}, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param serverGroupId
	 * @param serverGroupDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Update Server Group")
	@RequestMapping(method = RequestMethod.PUT, value = "/update/{airlineId}/{serverGroupId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateServerGroup(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,@PathVariable String serverGroupId,
			@ApiParam(value = "DTO Objects for Server Group", required = true) @RequestBody ServerGroupDTO serverGroupDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		serverGroupId = URLDecoder.decode(serverGroupId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(serverGroupId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] {}, Locale.US), HttpStatus.BAD_REQUEST));
		ServerGroupDTO serverGroupDto = serverGroupService.updateServerGroup(airlineId, serverGroupId, serverGroupDTO);
		if (Objects.nonNull(serverGroupDto)) {
			return new ResponseEntity<>(serverGroupDto, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] {ECMSConstants.SERVERGROUP}, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param serverGroupId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Delete Server Group")
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete/{airlineId}/{serverGroupId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteServerGroup(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,@PathVariable String serverGroupId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		serverGroupId = URLDecoder.decode(serverGroupId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(serverGroupId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.SERVERGROUP_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		Boolean result = serverGroupService.deleteServerGroup(airlineId, serverGroupId);
		if (result) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.SERVERGROUP}, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] {ECMSConstants.SERVERGROUP}, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Get all Server Group")
	@RequestMapping(method = RequestMethod.GET, value = "/serverGroupList/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getServerGroupList(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		List<ServerGroupDTO> serverGroupList= serverGroupService.getServerGroupList(airlineId);
		if (Objects.nonNull(serverGroupList)) {
			if(serverGroupList.isEmpty()){
				throw new EmptyResponseException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
								new Object[] { }, Locale.US), HttpStatus.FORBIDDEN));
			}
			return new ResponseEntity<>(serverGroupList, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] {ECMSConstants.SERVERGROUP }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param serverGroupId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Get ServerGroup by server group Id")
	@RequestMapping(method = RequestMethod.GET, value = "/serverGroupById/{airlineId}/{serverGroupId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getServerGroupById(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId,@PathVariable String serverGroupId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ServerGroupDTO serverGroupDTO = serverGroupService.getServerGroupById(airlineId, serverGroupId);
		if (Objects.nonNull(serverGroupDTO)) {
			return new ResponseEntity<>(serverGroupDTO, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] {ECMSConstants.SERVERGROUP }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
}
