package com.thales.ecms.api.rest.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.model.dto.ContentTypeDTO;
import com.thales.ecms.service.ContentTypeService;

/**
 * @author Mihir
 * Controller Class to get a List of Content Types
 */
@Controller
public class ContentTypeController {

	@Autowired
	ContentTypeService contentTypeService;
	
	/**
	 * Controller method to return a list of Content Types 
	 * @param request
	 * @param response
	 * @return
	 * @throws EmptyResponseException 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/contentTypes", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getContentTypes(HttpServletRequest request, HttpServletResponse response) throws EmptyResponseException{
		
		List<ContentTypeDTO> contentTypeDTOList = contentTypeService.getContentTypes();
		if(contentTypeDTOList != null && !contentTypeDTOList.isEmpty()){
			return new ResponseEntity<>(contentTypeDTOList, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse("The response returned was empty - No Content Types present", HttpStatus.OK));
		}
	}
	
}
