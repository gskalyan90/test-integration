package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.RuleContentFieldDTO;
import com.thales.ecms.model.dto.RuleDTO;
import com.thales.ecms.service.RuleService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * Controller for location type.
 *
 */
@Controller
//@RequestMapping(value = "/rule")
public class RuleController {

	@Autowired
	RuleService ruleService;
	
	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(RuleController.class);

	/**
	 * Controller method for getting list of all contentTypeList
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/rule/contentTypeList", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> contentTypeList(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		List<RuleContentFieldDTO> ruleContentFieldDTO = ruleService.contentTypeList();
		if (Objects.nonNull(ruleContentFieldDTO)) {
			return new ResponseEntity<>(ruleContentFieldDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
							new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/rule/getFieldByContentTypeId/{contentTypeId}", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> getFieldByContentTypeId(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String contentTypeId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {

		contentTypeId = URLDecoder.decode(contentTypeId, "UTF-8").trim();
		if(StringUtils.isBlank(contentTypeId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.CONTENT_TYPE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		List<RuleContentFieldDTO> ruleContentFieldDTO = ruleService.getFieldByContentTypeId(contentTypeId);
		if (Objects.nonNull(ruleContentFieldDTO)) {
			return new ResponseEntity<>(ruleContentFieldDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
							new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/rule/getRules", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> getRules(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException ,BadRequestException{

		airlineId = URLDecoder.decode(airlineId, "UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		List<RuleDTO> ruleDTO = ruleService.getRules(airlineId);
		if (Objects.nonNull(ruleDTO)) {
			return new ResponseEntity<>(ruleDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
							new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller method for adding Rule
	 * 
	 * @param request
	 * @param response
	 * @param Rule
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Adds Rule as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/rule/addRule", consumes = "application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> addRule(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId,
			@ApiParam(value = "DTO object for Rule", required = true) @RequestBody RuleDTO ruleDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException,
			ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		String responseString = ruleService.addRule(airlineId, ruleDetails);
		if (responseString != null) {
			responseString = ObjectUtils.toJson(responseString);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to delete a Rule
	 * 
	 * @param request
	 * @param response
	 * @param ruleId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/rule/deleteRule/{ruleId}/{airlineId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> deleteRule(HttpServletRequest request, HttpServletResponse response,@PathVariable String airlineId,
			@PathVariable("ruleId") String ruleId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ruleId = URLDecoder.decode(ruleId, "UTF-8").trim();
		if(StringUtils.isBlank(ruleId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.RULE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		String responseString = ruleService.deleteRule(airlineId,ruleId);
		if (responseString != null) {
			responseString = ObjectUtils.toJson(responseString);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/rule/getRule/{ruleId}/{airlineId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getRuleById(HttpServletRequest request, HttpServletResponse response,			
			@PathVariable String airlineId,@PathVariable("ruleId") String ruleId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ruleId = URLDecoder.decode(ruleId, "UTF-8").trim();
		if(StringUtils.isBlank(ruleId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.RULE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		RuleDTO ruleDTO = ruleService.getRuleById(airlineId,ruleId);
		if (ruleDTO != null) {
			// ruleDTO = ObjectUtils.toJson(ruleDTO);
			return new ResponseEntity<>(ruleDTO, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller method for updating Rule
	 * 
	 * @param request
	 * @param response
	 * @param Rule
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Update Rule as per given DTO and Rule ID")
	@RequestMapping(method = RequestMethod.PUT, value = "updateRule/{airlineId}/{ruleId}", consumes = "application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> updateRule(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId, @PathVariable String ruleId,
			@ApiParam(value = "DTO object for Rule", required = true) @RequestBody RuleDTO ruleDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException,
			ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ruleId = URLDecoder.decode(ruleId,"UTF-8").trim();
		if(StringUtils.isBlank(ruleId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.RULE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		String responseString = ruleService.updateRule(airlineId, ruleId ,ruleDetails);
		if (responseString != null) {
			responseString = ObjectUtils.toJson(responseString);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot create Rule", HttpStatus.OK));
		}
	}

}
