package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.PackageDTO;
import com.thales.ecms.model.dto.RouteGroupDTO;
import com.thales.ecms.service.PackageService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir Controller Class for operations on a Package
 */
@Controller
public class PackageController {

	@Autowired
	PackageService packageService;
	
	@Autowired
	MessageSource messageSource;

	/**
	 * Controller Method to create a Package
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param orderId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/createPackage/{orderId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> createPackage(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("airlineId") String airlineId, @PathVariable("orderId") String orderId)
			throws ObjectNotFoundException, UnsupportedEncodingException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		orderId = URLDecoder.decode(orderId, "UTF-8");
		String result = packageService.createPackage(airlineId, orderId);
		if (result != null && StringUtils.isNotBlank(result)) {
			result = ObjectUtils.toJson(result);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - cannot create Package(s)", HttpStatus.OK));
		}
	}

	/**
	 * Controller Method to get List of package
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param orderId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws BadRequestException 
	 * @throws NoSuchMessageException 
	 */
	@ApiOperation(value = "Controller Method to get list of package")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/order/{orderId}/getPackageList", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getPackageList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId, @ApiParam(value = "Order ID", required = true) @PathVariable String orderId)
			throws ObjectNotFoundException, UnsupportedEncodingException, EmptyResponseException, NoSuchMessageException, BadRequestException {

		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		if (StringUtils.isBlank(orderId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		orderId = URLDecoder.decode(orderId, ECMSConstants.UTF);
		
		List<PackageDTO> packageDTOList = packageService.getPackageList(airlineId, orderId);
		if (packageDTOList != null) {
			return new ResponseEntity<>(packageDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - no package available", HttpStatus.OK));
		}
	}

	/**
	 * Controller Method to get package by packageId
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param orderId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 */
	@ApiOperation(value = "Controller Method to get package by airlineId, orderId and packageId ")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/order/{orderId}/getPackageById/{packageId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getPackageById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Order ID", required = true) @PathVariable String orderId,
			@ApiParam(value = "Package ID", required = true) @PathVariable String packageId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException {
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		if (StringUtils.isBlank(orderId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		orderId = URLDecoder.decode(orderId, ECMSConstants.UTF);
		if (StringUtils.isBlank(packageId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		packageId = URLDecoder.decode(packageId, ECMSConstants.UTF);
		
		PackageDTO packageDTO = packageService.getPackageById(airlineId, orderId, packageId);
		if (packageDTO != null) {
			return new ResponseEntity<>(packageDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}
}
