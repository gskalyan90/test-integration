package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.psddev.dari.util.MultipartRequest;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.FileMetaDTO;
import com.thales.ecms.model.dto.IngestionDetailDTO;
import com.thales.ecms.model.dto.MetaDataImportDTO;
import com.thales.ecms.service.MetaDataImportService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Manish gour, Controller MetaData import/export process
 */

@RestController
@RequestMapping("/metadata")
public class MetaDataController {

	@Autowired
	MetaDataImportService metaDataImportService;

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(MetaDataController.class);

	/**
	 * Controller method for uploading file(s)
	 * 
	 * @param request
	 * @param airlineId
	 * @return fileUploaded
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value = "MetaData import process")
	@RequestMapping(value = "/uploadfiles/{airlineId}/{configurationId}", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseBody
	public ResponseEntity<?> uploadfiles(MultipartRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId, @ApiParam(value = "Configuration ID", required = true) @PathVariable String configurationId)
			throws NoSuchMessageException, EmptyResponseException, BadRequestException, UnsupportedEncodingException, ObjectExistsException, ObjectNotFoundException {

		List<IngestionDetailDTO> fileUploaded = metaDataImportService.uploadfiles(request, airlineId, configurationId);

		if (fileUploaded.isEmpty()) {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		} else {
			return new ResponseEntity<>(fileUploaded, HttpStatus.OK);
		}
	}

	/**
	 * Controller Method for invoking import meta data background process
	 * 
	 * @param request
	 * @param response
	 * @throws EmptyResponseException
	 * @throws NoSuchMessageException
	 * @throws UnsupportedEncodingException
	 */

	@ApiOperation(value = "MetaData import process")
	@RequestMapping(method = RequestMethod.POST, value = "/importcontenttype", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> importContentType(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "DTO Objects for ImportMetaDeta", required = true) @RequestBody MetaDataImportDTO metaDataImportDTO)
			throws BadRequestException, NoSuchMessageException, EmptyResponseException, UnsupportedEncodingException {

		IngestionDetailDTO ingestionDetail = metaDataImportService.importMetaDataProcess(metaDataImportDTO, request);

		if (Objects.nonNull(ingestionDetail)) {
			return new ResponseEntity<>(ingestionDetail, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}
}
