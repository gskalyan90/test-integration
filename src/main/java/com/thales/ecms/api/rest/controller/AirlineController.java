package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.airline.AirlinePortal.AirlineStatus;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.service.AirlineService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir Controller class for CRUD operations on Airlines
 */
@Controller
public class AirlineController {

	@Autowired
	AirlineService airlineService;

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(AirlineController.class);

	/**
	 * Controller Method for adding an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineDetails
	 * @return
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws DuplicateEntryException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/airlines", consumes = "application/json", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> addAirline(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirlineDTO airlineDetails)
			throws EmptyResponseException, BadRequestException, ObjectExistsException, DuplicateEntryException {

		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		if (airlineDTO != null) {
			return new ResponseEntity<>(airlineDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to return a list of all Airlines
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws EmptyResponseException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/airlines", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getAirlines(HttpServletRequest request, HttpServletResponse response)
			throws EmptyResponseException {

		List<AirlineDTO> airlineDTOList = airlineService.getAirlines();
		if (airlineDTOList != null && !airlineDTOList.isEmpty()) {
			return new ResponseEntity<>(airlineDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
							new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to return an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws EmptyResponseException
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getAirline(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId)
			throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		AirlineDTO airlineDTO = airlineService.getAirlineById(airlineId);
		if (airlineDTO != null) {
			return new ResponseEntity<>(airlineDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
							new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to update an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param airlineDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 * @throws DuplicateEntryException
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/airlines/{airlineId}", consumes = "application/json", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> updateAirline(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId, @RequestBody AirlineDTO airlineDetails)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException, ObjectExistsException,
			BadRequestException, DuplicateEntryException {

		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		AirlineDTO airlineDTO = airlineService.updateAirline(airlineId, airlineDetails);
		if (airlineDTO != null) {
			return new ResponseEntity<>(airlineDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller method to delete an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> deleteAirline(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId) throws UnsupportedEncodingException, EmptyResponseException,
			ObjectNotFoundException, NoSuchMessageException, BadRequestException, ObjectExistsException {

		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		String responseString = airlineService.deleteAirline(airlineId);
		if (responseString != null && StringUtils.isNotBlank(responseString)) {
			responseString = ObjectUtils.toJson(responseString);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value = "Activate and Deactivate airline by airlineId")
	@RequestMapping(method = RequestMethod.PUT, value = "/airline/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> changeAirlineStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId,
			@ApiParam(value = "Airline Status", allowableValues = "ACTIVE, INACTIVE", required = true) @RequestParam AirlineStatus airlineStatus)
			throws EmptyResponseException, ObjectNotFoundException, UnsupportedEncodingException, BadRequestException {
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		if(airlineStatus!=null){
		Boolean status = airlineService.changeAirlineStatus(airlineId, airlineStatus);
		if (status) {
			if (airlineStatus.equals(AirlineStatus.ACTIVE)) {
				return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.STATUS_ACTIVE,
						new Object[] { ECMSConstants.AIRLINE }, Locale.US)), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.STATUS_INACTIVE,
						new Object[] { ECMSConstants.AIRLINE }, Locale.US)), HttpStatus.OK);
			}
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		}else{
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
	}

}
