
package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.RouteDTO;
import com.thales.ecms.service.RouteService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
public class RouteController {

	@Autowired
	RouteService routeService;

	/**
	 * Controller Method for adding an Route
	 * @param request
	 * @param response
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws DuplicateEntryException
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Add Routes as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airports/route", consumes = "application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody 
	public ResponseEntity<?> addRoute(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "DTO Objects for Route", required = true) @RequestBody RouteDTO routeDTO)
					throws EmptyResponseException, BadRequestException, ObjectExistsException,
			DuplicateEntryException, UnsupportedEncodingException, ObjectNotFoundException {

		if(routeDTO!=null){
			RouteDTO routeDTOs = routeService.addRoute(routeDTO);
	
			if (null!= routeDTOs) {
				return new ResponseEntity<>(routeDTOs, HttpStatus.OK);
			} else {
				throw new EmptyResponseException(
						new ErrorResponse("Response returned was empty - cannot create route", HttpStatus.FORBIDDEN));
			}
		}else{
			throw new BadRequestException(new ErrorResponse("Request parameters are empty - cannot create route", HttpStatus.BAD_REQUEST));
		}
	}

	/**
	 * Controller method to get list of routes
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Retrieve all Routes")
	@RequestMapping(method = RequestMethod.GET, value = "/airports/route", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> getAllRoutes(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {
		List<RouteDTO> routeList = routeService.getRoutes();

		if (null!= routeList) {
			return new ResponseEntity<>(routeList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - no routes are present", HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller method to delete a Route
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Delete route as per given RouteID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airports/route/{id}", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> deleteRoute(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Route ID", required = true) @PathVariable("id") String id)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		id = URLDecoder.decode(id, "UTF-8");
		String responseString = routeService.deleteRoute(id);
		if (null!= responseString) {
			responseString = ObjectUtils.toJson(responseString);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - cannot delete route", HttpStatus.FORBIDDEN));
	}
			
	/**
	 * Controller Method to update a Route
	 * @param request
	 * @param response
	 * @param routeId
	 * @param routeDetails
	 * @throws UnsupportedEncodingException 
	 * @throws ObjectNotFoundException 
	 * @throws EmptyResponseException 
	 * @throws ObjectExistsException 
	 * @throws BadRequestException 
	 */
			
	@ApiOperation(value = "Update route as per given RouteID")
	@RequestMapping(method = RequestMethod.PUT, value = "/airports/route/{routeId}", consumes = "application/json", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> updateRoute(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String routeId, @RequestBody RouteDTO routeDetails) throws UnsupportedEncodingException,
			EmptyResponseException, ObjectNotFoundException, BadRequestException, ObjectExistsException {

		routeId = URLDecoder.decode(routeId, "UTF-8");
		RouteDTO routeDTO = routeService.updateRoute(routeId, routeDetails);
		if (null!= routeDTO) {
			return new ResponseEntity<>(routeDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot update route", HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to get route by ID
	 * @param request
	 * @param response
	 * @param routeId
	 * @throws EmptyResponseException
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Retrieve route as per given Airport ID")
	@RequestMapping(method = RequestMethod.GET, value = "/route/{routeId}", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> getAirport(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String routeId) throws EmptyResponseException, UnsupportedEncodingException, ObjectNotFoundException {

		routeId = URLDecoder.decode(routeId, "UTF-8");
		RouteDTO routeDTO = routeService.getRouteById(routeId);
		if (routeDTO != null) {
			return new ResponseEntity<>(routeDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Route does not exist", HttpStatus.NOT_FOUND));
		}
	}
}