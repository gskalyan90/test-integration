package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.TitleDTO;
import com.thales.ecms.model.dto.TitleRequestDTO;
import com.thales.ecms.service.TitleService;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Reference Controller class for performing operations on Titles
 */
@Controller
public class TitleController {

	@Autowired
	TitleService titleService;
	
	public static Logger logger = LoggerFactory.getLogger(TitleController.class);

//	@RequestMapping(method = RequestMethod.POST, value = "/titles/{contentTypeId}", consumes = "application/json")
//	public ResponseEntity<?> createTitle(HttpServletRequest request, HttpServletResponse response, @PathVariable String contentTypeId, 
//			@RequestBody TitleDTO titleDTO) throws UnsupportedEncodingException, ObjectNotFoundException, BadRequestException{
//
//		contentTypeId = URLDecoder.decode(contentTypeId, "UTF-8");
//		String responseString = titleService.createTitle(contentTypeId, titleDTO);
//		return new ResponseEntity<>(responseString, HttpStatus.OK);
//	}

	
	@RequestMapping(method = RequestMethod.POST, value = "airlines/{airlineId}/titles/{contentTypeId}", consumes = "application/json;charset=UTF-8")
	public ResponseEntity<?> createTitle(HttpServletRequest request, HttpServletResponse response, @PathVariable("airlineId") String airlineId, 
			@PathVariable("contentTypeId") String contentTypeId, @RequestBody TitleRequestDTO titleRequestDTO) throws UnsupportedEncodingException, NoSuchMessageException, ObjectNotFoundException{
		
		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		contentTypeId = URLDecoder.decode(contentTypeId, "UTF-8");
		String responseString = titleService.createTitle(airlineId, contentTypeId, titleRequestDTO);
		return new ResponseEntity<>(ObjectUtils.toJson(responseString), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/titles/{contentTypeId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getTitles(HttpServletRequest request, HttpServletResponse response, @PathVariable String contentTypeId) throws UnsupportedEncodingException, ObjectNotFoundException{

		contentTypeId = URLDecoder.decode(contentTypeId, "UTF-8");
		List<TitleDTO> titleDTOList = titleService.getTitles(contentTypeId);
		return new ResponseEntity<>(titleDTOList, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/titles/{titleId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> deleteTitle(HttpServletRequest request, HttpServletResponse response, @PathVariable String titleId) throws UnsupportedEncodingException, EmptyResponseException{

		titleId = URLDecoder.decode(titleId, "UTF-8");
		if(titleService.deleteTitle(titleId)){
			return new ResponseEntity<>(ObjectUtils.toJson("Title deleted successfully"),HttpStatus.OK);
		}
		else{
			logger.error("Cannot delete Title");
			throw new EmptyResponseException(new ErrorResponse("Cannot delete Title", HttpStatus.FORBIDDEN));
		}
	}
}
