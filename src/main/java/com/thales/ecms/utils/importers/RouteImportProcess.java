package com.thales.ecms.utils.importers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.jobs.MetaDataImportJob;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.Route;
import com.thales.ecms.model.RouteGroup;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.model.dto.RouteDTO;
import com.thales.ecms.model.dto.RouteGroupDTO;
import com.thales.ecms.service.AirlineService;
import com.thales.ecms.service.AirportService;
import com.thales.ecms.service.RouteGroupService;
import com.thales.ecms.service.RouteService;
import com.thales.ecms.utils.ECMSConstants;
import com.thales.ecms.utils.ImportStatus;

/**
 * Class for importing metadata
 */
@Component
public class RouteImportProcess {

	public static Logger logger = LoggerFactory.getLogger(MetaDataImportJob.class);

	@Autowired
	MessageSource messageSource;

	@Autowired
	AirportService airportService;

	@Autowired
	RouteService routeService;

	@Autowired
	AirlineService airlineService;

	@Autowired
	RouteGroupService routeGroupService;

	
	public enum FileType {
		EXCEL, TEXT, XML;
	}

	
	private List<ImportStatus> importStatusReport = null;
	//the map will atore RouteGroupDTOs
	private Map<String, Map<String, RouteGroupDTO>> mapOfRouteGroupDTOs = null;

	//The map will store airline name as key and AirlineDTO
	private Map<String, AirlineDTO> mapAirlineDTOs = null;

	
	
	/**
	 * Function for getting the Sheet object of excel file.
	 * 
	 * @param fileName
	 * @param sheetIndex
	 * @return
	 */
	
	
	private XSSFSheet getSheet(String fileName, int sheetIndex) throws BadRequestException{

		XSSFWorkbook wb = null;
		XSSFSheet sheet = null;

		try (FileInputStream excellInputStream = new FileInputStream(new File(fileName))) {
			mapOfRouteGroupDTOs = new HashMap<>();
			mapAirlineDTOs = new HashMap<>();
			wb= new XSSFWorkbook(excellInputStream);
			sheet = wb.getSheetAt(sheetIndex);
		} catch (IOException e) {
			logger.info("exception in Reading XlS file :" + e.getMessage());
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.ROUTEGROUP_IMPORT_READ_FILE_ERROR, null, Locale.US), HttpStatus.BAD_REQUEST));
			
			
		}
		return sheet;
     }

	
	public String importRouteInfo(String file, String fileType)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		FileType type = null;
		if (fileType.equals("xlsx") || fileType.equals("xls")) {
			type = FileType.EXCEL;
		}

		if (type == FileType.EXCEL) {
			// currently we are parsing first sheet of excel
			XSSFSheet xssFSheet = getSheet(file, 0);
			if (Objects.nonNull(xssFSheet)) {

				Iterator<Row> rowIterator = xssFSheet.iterator();
				logger.info("total Number of rows count: " + xssFSheet.getLastRowNum());
				// Loop for each Row in one sheet
				List<ImportStatus> importStatusList = new ArrayList<>();

			   while (rowIterator.hasNext()) {

					Row row = rowIterator.next();
					if (Objects.nonNull(row)) {
						importStatusList.add(saveRecord(row));
					}

				}
				logger.info("End of Reading  rows of one sheet.");
				this.setImportStatusReport(importStatusList);
			}
			return "successfull";
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EXCEL_IMPORTFILE_REQUIRED,
							new Object[] { ECMSConstants.EXCEL }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	
	
	/**
	 * This method will contain actual data population. and setting DTOs
	 * 
	 * @param row
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	
	
	private ImportStatus saveRecord(Row row)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		
		ImportStatus importStatus = new ImportStatus();

		AirportDTO existingAiportOrigin = null;
		AirportDTO existingAiportDestination = null;
		AirportDTO savedAirportOrigin = null;
		AirportDTO savedAirportDest = null;
		RouteDTO existingRoute = null;
		logger.info("reading route origin");
		
		
		// Route Origin (ICAO|IATA)
		if (Objects.nonNull(row) && row.getCell(3) != null) {
			String[] aiportsOriginArray = row.getCell(3).toString().split("\\|");
			AirportDTO aiportOrigin = getAirportDetailsFromExcel(aiportsOriginArray);

			if (Objects.nonNull(
					Query.from(Airport.class).where("airportName = ?", aiportOrigin.getAirportName()).first())) 
			
			{
				// fetch already existing airport object
				List<AirportDTO> allAllports = airportService.getAirport();
				existingAiportOrigin = allAllports.stream()
						.filter(p -> p.getAirportIATA().equals(aiportOrigin.getAirportIATA())).findFirst().orElse(null);
				
				// Airport already exists
				logger.info("Airport origin save fail exception ");
				throw new ObjectExistsException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.ROUTEGROUP_AIRPORT_IMPORT_ERROR,
								new Object[] { ECMSConstants.ROUTEGROUP_IMPORT_ORIGIN_AIRPORT_EXIST },
								Locale.US), HttpStatus.CONFLICT));
				
                }
			else 
			{
				logger.info("saving route origin");
				// Saving Origin airport
              savedAirportOrigin = airportService.addAirport(aiportOrigin);
				logger.info("Airport origin saved");
			}
			logger.info("reading route destination");
			
			
			
			// Route Destination (ICAO|IATA)
			String[] aiportsDestinationArray = row.getCell(4).toString().split("\\|");
			AirportDTO aiportDestination = getAirportDetailsFromExcel(aiportsDestinationArray);
			if (Objects.nonNull(
					Query.from(Airport.class).where("airportName = ?", aiportDestination.getAirportName()).first())) {
			
				
				// fetch already existing airport object
				List<AirportDTO> allAllports = airportService.getAirport();
				existingAiportDestination = allAllports.stream()
						.filter(p -> p.getAirportIATA().equals(aiportDestination.getAirportIATA())).findFirst()
						.orElse(null);
				// Airport already exists
				logger.info("Airport Destination save fail exception ");
				throw new ObjectExistsException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.ROUTEGROUP_AIRPORT_IMPORT_ERROR,
								new Object[] { ECMSConstants.ROUTEGROUP_IMPORT_DESTINATION_AIRPORT_EXIST },
								Locale.US), HttpStatus.CONFLICT));
			} else {
				logger.info("saving route destination");
				// Saving Destination airport
				savedAirportDest = airportService.addAirport(aiportDestination);
				logger.info("Airport Destination saved");
			}
			
			
			
			// RouteDTO
			RouteDTO route = new RouteDTO();
			route.setName(row.getCell(2).toString().trim());
			if (Objects.nonNull(savedAirportOrigin))
				route.setOrigin(savedAirportOrigin);
			else
				route.setOrigin(existingAiportOrigin);

			if (Objects.nonNull(savedAirportDest))
				route.setDestination(savedAirportDest);
			else
				route.setDestination(existingAiportDestination);

			// logger.debug("Route Object " + route.toString());

			RouteDTO savedRouteDTO = null;
			if (Objects.nonNull(Query.from(Route.class).where("name = ?", route.getName()).first())) {
					// fetch already existing route object
				List<RouteDTO> allRoutes = routeService.getRoutes();
				existingRoute = allRoutes.stream().filter(p -> p.getName().equalsIgnoreCase(route.getName()))
						.findFirst().orElse(null);
				
				// route already exists
				// e.printStackTrace();
				logger.info("Route saved fail exception ");
				throw new ObjectExistsException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.ROUTEGROUP_IMPORT_ROUTE_IMPORT_ERROR,
								new Object[] { ECMSConstants.ROUTEGROUP_IMPORT_ROUTE_EXIST },
								Locale.US), HttpStatus.CONFLICT));
			} else {
				savedRouteDTO = routeService.addRoute(route);
				logger.info("Route saved Name : " + savedRouteDTO.getName());
			}

			// code for saving route group details

			try {
				String airlineName = row.getCell(0).toString().trim();
				AirlineDTO airlineDTO = null;
				if (!mapAirlineDTOs.containsKey(airlineName)) {
					List<AirlineDTO> airlineDTOList = airlineService.getAirlines();
					logger.info("Number of airlines " + airlineDTOList.size());
					airlineDTO = airlineDTOList.stream().filter(p -> p.getAirlineName().equalsIgnoreCase(airlineName))
							.findFirst().orElse(null);
				} else
				{
					airlineDTO = mapAirlineDTOs.get(airlineName);
				}
				mapAirlineDTOs.put(airlineName, airlineDTO);
				String routeGroupName = row.getCell(1).toString().trim();
				if (mapOfRouteGroupDTOs.containsKey(airlineName)) {
					Map<String, RouteGroupDTO> map = mapOfRouteGroupDTOs.get(airlineName);
					if (map.containsKey(routeGroupName)) {
						RouteGroupDTO dto = map.get(routeGroupName);
						Set<RouteDTO> routeDtos = dto.getRoutes();
						if (Objects.isNull(existingRoute))
							routeDtos.add(savedRouteDTO);
						else
							routeDtos.add(existingRoute);
						dto.setRoutes(routeDtos);
						map.put(routeGroupName, dto);
					} else {
						RouteGroupDTO routeGroupDto = new RouteGroupDTO();
						routeGroupDto.setRouteGroupName(row.getCell(1).toString().trim());
						Set<RouteDTO> routeDtos = new HashSet<RouteDTO>();
						if (Objects.isNull(existingRoute))
							routeDtos.add(savedRouteDTO);
						else
							routeDtos.add(existingRoute);
						routeGroupDto.setRoutes(routeDtos);
						map.put(routeGroupName, routeGroupDto);
					}
					mapOfRouteGroupDTOs.put(airlineName, map);
				} else {
					RouteGroupDTO routeGroupDto = new RouteGroupDTO();
					routeGroupDto.setRouteGroupName(row.getCell(1).toString().trim());
					Set<RouteDTO> routeDtos = new HashSet<RouteDTO>();
					if (Objects.isNull(existingRoute))
						routeDtos.add(savedRouteDTO);
					else
						routeDtos.add(existingRoute);
					routeGroupDto.setRoutes(routeDtos);
					Map<String, RouteGroupDTO> map = new HashMap<>();
					map.put(routeGroupName, routeGroupDto);
					mapOfRouteGroupDTOs.put(airlineName, map);

				}
             } catch (Exception e) {
				// route group already exists
				logger.info("RouteGroup save/update fail Exception : " + e.getMessage());
				throw new ObjectExistsException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.ROUTEGROUP_IMPORT_ERROR,
								new Object[] { ECMSConstants.ROUTEGROUP_IMPORT_ROUTEGROUP_EXIST },
								Locale.US), HttpStatus.CONFLICT));
			}
		}
		return importStatus;
	}

	
	public RouteGroupDTO addRouteGroup() throws  ObjectExistsException {
		logger.info("Adding routegroup");
		RouteGroupDTO rgd = null;
		for (String airlineName : mapOfRouteGroupDTOs.keySet()) {
			Map<String, RouteGroupDTO> map = mapOfRouteGroupDTOs.get(airlineName);
			for (RouteGroupDTO routeGroupDTO : map.values()) {
				try {
					RouteGroup routeGroup = Query.from(RouteGroup.class).where("name = ?", routeGroupDTO.getRouteGroupName()).first();
					if(Objects.isNull(routeGroup)){
					rgd = routeGroupService.addRouteGroup(mapAirlineDTOs.get(airlineName).getAirlineInternalId(),
							routeGroupDTO);
					logger.info(rgd.toString() + "is added for airline id"
							+ mapAirlineDTOs.get(airlineName).getAirlineInternalId());
					}
					else{
						rgd = routeGroupService.updateRouteGroupFromXlsx(mapAirlineDTOs.get(airlineName).getAirlineInternalId(),routeGroup.getId().toString(),
								routeGroupDTO);
						logger.info(rgd.toString() + " is updated for airline id"
								+ mapAirlineDTOs.get(airlineName).getAirlineInternalId());
					}
				} catch (ObjectExistsException|BadRequestException|ObjectNotFoundException ex) {
					logger.info("RouteGroup save fail : " + ex.getMessage());
					throw new ObjectExistsException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.ROUTEGROUP_IMPORT_ERROR,
									new Object[] { ECMSConstants.ROUTEGROUP_IMPORT_SAVE_FAIL + ex.getMessage()},
									Locale.US), HttpStatus.NOT_FOUND));
				}
			}
		}
		return rgd;
	}

	/**
	 * get airport details as array and return AirportDTO
	 * 
	 * @param aiportsDetailsArray
	 * @return AirportDTO
	 */
	private AirportDTO getAirportDetailsFromExcel(String[] aiportsDetailsArray) {
		AirportDTO aiportDetails = new AirportDTO();
		if (null != aiportsDetailsArray && aiportsDetailsArray.length > 2) {
			aiportDetails.setAirportName(aiportsDetailsArray[0].trim());
			aiportDetails.setAirportICAO(aiportsDetailsArray[1].trim());
			aiportDetails.setAirportIATA(aiportsDetailsArray[2].trim());
		}
		return aiportDetails;
	}


	public List<ImportStatus> getImportStatusReport() {
		return importStatusReport;
	}

	public void setImportStatusReport(List<ImportStatus> importStatusReport) {
		this.importStatusReport = importStatusReport;
	}

}
