package com.thales.ecms.utils;

import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.psddev.dari.db.ObjectType;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.ValidatorAttribute;
import com.thales.ecms.utils.ExcelCellStyleUtil.SECTION;

/**
 * A Manager class to be used while exporting:
 * 	1.	A blank Excel file containing all the Content Types having a Configuration
 * 	2.	A blank Excel file containing specific Content Type having a Configuration
 * 	3.	An excel file containing all the data of ContentType for all the elements of title list in a Configuration,
 * 		assuming the prescribed structure by TemplateBuilder
 * 
 * @author arjun.p
 *
 */
public class ExcelTemplateManager {
	
	private static ExcelTemplateManager excelTemplateManager;
	
	public static final int DATA_ROW_COUNTER_INITIAL_INDEX = 3, DATA_COLUMN_COUNTER_INITIAL_INDEX=2;
	
	private Configuration configuration;
	private Workbook workbook;
	private Map<String, Map<String,ValidatorAttribute>> rules;	
	private Map<SECTION,CellStyle> cellStyles;
			
	private ExcelTemplateManager(Configuration configuration, Workbook workbook) {		
		this.configuration = configuration;
		this.workbook = workbook;
	}		

	/**
	 * Create a new instance every time on request
	 * 
	 * @param configuration
	 * @param workbook
	 * @return
	 */
	public static ExcelTemplateManager newInstance(Configuration configuration,Workbook workbook){		
		excelTemplateManager=new ExcelTemplateManager(configuration,workbook);
		return excelTemplateManager;
	}
	
	public static ExcelTemplateManager newInstance(Configuration configuration,Workbook workbook,Map<String, Map<String, ValidatorAttribute>> rules,Map<SECTION, CellStyle> cellStyles){		
		excelTemplateManager=newInstance(configuration,workbook);
		excelTemplateManager.setRules(rules);
		excelTemplateManager.setCellStyles(cellStyles);		
		return excelTemplateManager;
	}
	
	public TemplateBuilder getTemplateBuilder(){
		TemplateBuilder templateBuilder = null;
		templateBuilder = new TemplateBuilderImpl(configuration, workbook);
		templateBuilder.rules(rules);
		templateBuilder.cellStyles(cellStyles);
		return templateBuilder;
	}
	
	public TemplateDataIntegrater getTemplateDataIntegrater(){
		TemplateDataIntegrater templateDataIntegrater = null;
		templateDataIntegrater = new TemplateDataIntegraterImpl(configuration, workbook);
		templateDataIntegrater.rules(rules);
		templateDataIntegrater.cellStyles(cellStyles);
		return templateDataIntegrater;
	}
	

	public Map<String, Map<String, ValidatorAttribute>> getRules() {
		return rules;
	}

	public void setRules(Map<String, Map<String, ValidatorAttribute>> rules) {
		this.rules = rules;
	}

	public Map<SECTION, CellStyle> getCellStyles() {
		return cellStyles;
	}

	public void setCellStyles(Map<SECTION, CellStyle> cellStyles) {
		this.cellStyles = cellStyles;
	}
	
	/**
	 * This interface must be implemented by a class serving as a template builder
	 * 
	 * @author arjun.p
	 * 
	 */
	public static interface TemplateBuilder {
		TemplateBuilder buildTemplate();
		TemplateBuilder buildSheet(ObjectType objectType);
		TemplateBuilder rules(Map<String, Map<String,ValidatorAttribute>> rules);
		TemplateBuilder cellStyles(Map<SECTION,CellStyle> cellStyles);				
	}
	
	/**
	 * This interface must be implemented by a class serving the data integration to a blank excel template
	 * 
	 * @author arjun.p
	 *
	 */
	public static interface TemplateDataIntegrater {
		TemplateDataIntegrater fillWorkbook() throws IllegalArgumentException, IllegalAccessException;
		TemplateDataIntegrater fillSheet(ObjectType objectType, Sheet sheet) throws IllegalArgumentException, IllegalAccessException;
		TemplateDataIntegrater rules(Map<String, Map<String,ValidatorAttribute>> rules);
		TemplateDataIntegrater cellStyles(Map<SECTION,CellStyle> cellStyles);
	}

}
