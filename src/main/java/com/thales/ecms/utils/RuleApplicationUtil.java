package com.thales.ecms.utils;

import java.lang.reflect.Field;
import java.util.List;

import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Status;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.Rule;
import com.thales.ecms.model.Rule.ContentFieldValidation;
import com.thales.ecms.model.ValidatorAttribute;

/**
 * 
 * Rule Application class
 * 
 * @author arjun.p
 *
 */
public class RuleApplicationUtil {
	
	public static void applyRuleOnConfigurationTitle(Rule rule, ConfigurationTitle configurationTitle) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
		if(configurationTitle!=null){
			ContentType contentType = configurationTitle.getContent();
			if(contentType!=null && rule!=null){
//				Validation Logic
				List<ContentFieldValidation> listOfFieldValidations = rule.getContentFieldValidations();
				Class<? extends ContentType> contentTypeclass = contentType.getClass();
				boolean isValidTitle=true;
				for(ContentFieldValidation contentFieldValidation : listOfFieldValidations){
					if(contentFieldValidation.getField()!=null && !contentFieldValidation.getField().isEmpty()){
						if(contentFieldValidation.getValidatorAttribute()!=null){							
							Field field = contentTypeclass.getDeclaredField(contentFieldValidation.getField());							
							ValidatorAttribute validatorAttribute = contentFieldValidation.getValidatorAttribute();
							field.setAccessible(true);
							Object object = field.get(contentType);
							if(validatorAttribute.isRequired()){
								if(object==null){
									configurationTitle.setContentStatus(Status.Incomplete);
									isValidTitle=false;
									break;
								}								
							}
							if(validatorAttribute.getMaxChar()!=null){
//								MaxChar validation only applies on String type field
//								if Field is not of String.class type
//								Something wrong with Rule setter code 
								if(object instanceof String){
									if(validatorAttribute.getMaxChar().matches("\\d+")){
										int maxChar = Integer.parseInt(validatorAttribute.getMaxChar());
										String data = (String)object;
										if(data==null || data.length() > maxChar){
											configurationTitle.setContentStatus(Status.Incomplete);
											isValidTitle=false;
											break;
										}										
									}									
								}
							}
//							How to get information that this field is translatable
//							and validate as per that info
						}												
					}															
				}
				if(isValidTitle)
					configurationTitle.setContentStatus(Status.Validated);
			}
		}		
	}

}
