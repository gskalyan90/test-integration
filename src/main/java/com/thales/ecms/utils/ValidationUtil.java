package com.thales.ecms.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.AbstractList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.thales.ecms.conversions.ContentTypeFactory;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.Rule;
import com.thales.ecms.model.Rule.ContentFieldValidation;
import com.thales.ecms.model.ValidatorAttribute;
import com.thales.ecms.model.util.TextClass;

public class ValidationUtil {

	public static Logger logger = LoggerFactory.getLogger(ValidationUtil.class);

	/**
	 * This method validates content type against rules which is associated into
	 * configuration.
	 * 
	 * @param configurationId
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws ObjectNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 */
	public static String validateOrder(String configurationId) throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, ObjectNotFoundException, NoSuchMethodException, SecurityException {

		boolean isRuleExist = false;
		StringBuilder result = null;
		Rule foundRule = null;

		Configuration configuration = Query.from(Configuration.class).where("id = ?", configurationId).first();
		logger.info(configuration.getName());

		Set<ObjectType> contentTypeSet = configuration.getContentTypeList();

		Set<Rule> rules = configuration.getRules();

		List<ConfigurationTitle> titleList = configuration.getTitleList();
		for (ConfigurationTitle configurationTitle : titleList) {
			ContentType content = configurationTitle.getContent();
			logger.info("Title Name: " + content.getLabel());
			logger.info("Content Type: " + content.getClass().getName());

			ObjectType objectType = Query.from(ObjectType.class).where("internalName = ?", content.getClass().getName())
					.first();
			if (!Objects.isNull(objectType)) {
				boolean isObjectTypeExistInSet = contentTypeSet.contains(objectType);

				if (isObjectTypeExistInSet) {
					for (Rule rule : rules) {
						if (!Objects.isNull(rule) && !Objects.isNull(rule.getObjectType())) {
							if (rule.getObjectType() == objectType) {
								isRuleExist = true;
								foundRule = rule;
								break;
							}

						}

					}
				}
			}

			if (isRuleExist) {
				boolean isValid = validateContentType(content, foundRule, objectType);
				result = new StringBuilder();
				if (isValid) {
					logger.info("Title validated successfully");
					result = result.append("Title validated successfully");

				} else {
					logger.info("Title does not satsify the validation rules");
					result = result.append("Title does not satsify the validation rules");
				}
			} else {
				logger.info("We did not find a Rule for this Configuration and Content Type");
				result = new StringBuilder();
				result = result.append("We did not find a Rule for this Configuration and Content Type");
			}

		}

		return "Order validation complete, " + result.toString();
	}

	/**
	 * This method validates content type is valid or not against given rule.
	 * 
	 * @param contentParam
	 * @param rule
	 * @param objectType
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("unchecked")
	public static boolean validateContentType(ContentType contentParam, Rule rule, ObjectType objectType)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
			SecurityException {

		Boolean isValid = false;
		// method is getting called to type cast the object dynamically
		ContentType content = ContentTypeFactory.getContentType(contentParam);

		Class<? extends ContentType> clazz = content.getClass();
		Field[] fieldArray = clazz.getDeclaredFields();

		if (!Objects.isNull(rule) && !Objects.isNull(rule.getContentFieldValidations())
				&& !rule.getContentFieldValidations().isEmpty()) {

			for (ContentFieldValidation contentFieldValidation : rule.getContentFieldValidations()) {

				String fieldName = contentFieldValidation.getField();
				logger.info("fieldName contentFieldValidation : " + fieldName);

				// here we only able to check length of String against
				// rule but we need to check against every data like
				// image, ReferentialText,TextClass etc.
				for (Field field : fieldArray) {
					if (field.getName().equals(fieldName)) {
						logger.info(field.getName());
						ValidatorAttribute validatorAttribute = contentFieldValidation.getValidatorAttribute();
						int maxChar = Integer.parseInt(validatorAttribute.getMaxChar());
						StringBuilder methodName = new StringBuilder();
						methodName.append("get");
						methodName.append(WordUtils.capitalize(field.getName()));
						Method methodTemp = clazz.getDeclaredMethod(methodName.toString());
						if (methodTemp.getReturnType() != null
								&& methodTemp.getReturnType().getName().equals("java.lang.String")) {
							String result = (String) MethodUtils.invokeExactMethod(content, methodName.toString());
							logger.info("result: " + result);
							if (maxChar >= result.length()) {
								isValid = true;
							} else if (maxChar < result.length()) {
								isValid = false;
							}
						} else if (methodTemp.getReturnType() != null
								&& methodTemp.getReturnType().getName().equals("java.util.List")) {

							AbstractList<TextClass> textClassList = (AbstractList<TextClass>) MethodUtils
									.invokeExactMethod(content, methodName.toString());
							logger.info("object: " + textClassList);
							for (TextClass textClass : textClassList) {
								logger.info("textClass.getMaxChar(): " + textClass.getMaxChar());
								logger.info("textClass.getValue(): " + textClass.getValue());
								// here need to TextClass attribute validate
								// against rule.
								PaxguiLanguage language = textClass.getLanguage();
								logger.info("language.getPaxguiLanguageName(): " + language.getPaxguiLanguageName());
								logger.info("language.getPaxguiLanguageCode(): " + language.getPaxguiLanguageCode());
							}

						} // end else if

					} // end inner if fieldName comparison

				} // end inner for loop
			} // end outer for loop

		} // end outermost if
		return isValid;
	}

}
