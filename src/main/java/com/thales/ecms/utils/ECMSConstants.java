package com.thales.ecms.utils;

public class ECMSConstants {

	public static final String UTF = "UTF-8";
	public static final String PRODUCE_FORMAT = "application/json;charset=UTF-8";
	public static final String CONSUME_FORMAT = "application/json";

	public static final String OBJECT_EXISTS = "object.exists";
	public static final String OBJECT_ASSOCIATED = "object.associated";
	public static final String EMPTY_RESPONSE_CREATE = "empty.response.create";
	public static final String EMPTY_RESPONSE_READ = "empty.response.read";
	public static final String EMPTY_RESPONSE_UPDATE = "empty.response.update";
	public static final String EMPTY_RESPONSE_DELETE = "empty.response.delete";
	public static final String DELETED_SUCCESSFULLY = "deleted.successfully";
	public static final String BAD_REQUEST = "bad.request";
	public static final String OBJECT_NOT_FOUND = "object.not.found";
	public static final String LOPA_ADD = "lopa.empty.response.add";
	public static final String LOPA_REMOVE = "lopa.empty.response.remove";
	public static final String OBJECT_NOT_CONTAINS_OBJECT = "object.not.found.in.object";
	public static final String MAPPING_NOT_EXISTS = "mapping.not.exists";
	public static final String REEUIRED_FIELD = "required field";
	public static final String ORDER_FIELD_REQUIRED = "orderfield.required";
	public static final String FILE_NOT_FOUND_SERVER = "file.not.found.server";
	public static final String FILE_FORMAT_NOT_SUPPORTED = "file.format.not.supported";
	public static final String FILE_FORMAT_SAVE_ERROR = "file.save.error";
	public static final String FILE_DIRECTORY_CREATION_ERROR = "file.directory.creation.error";
	public static final String WEBSITE = "incorrect.url";
	public static final String DUPLICATE_URL = "duplicate.url";
	public static final String LRU_TYPE_NAME = "lru.type.name";
	public static final String EXCEL_FILE_REQUIRED = "excelFile.required";
	public static final String NOT_CREATED = "not.created";
	public static final String STATUS_ACTIVE = "status.active";
	public static final String STATUS_INACTIVE = "status.inactive";

	public static final String ROUTE = "Route";
	public static final String ROUTE_GROUP = "Route Group";
	public static final String AIRPORT = "airport";
	public static final String AIRCRAFT = "aircraft";
	public static final String AIRLINE = "airline";
	public static final String AIRLINE_CONTACT = "airline contact";
	public static final String LOPA = "lopa";
	public static final String PAXGUILANGUAGE = "PaxguiLanguage";
	public static final String LRU = "lru";
	public static final String SHIPMENTDATE = "shipment date";
	public static final String SHIPMENTPROFILE = "Shipment Profile";
	public static final String SHIPMENT_TYPE="Shipment type";
	public static final String SHIPMENT_HARD_DRIVE="Shipment hard drive value";
	public static final String SHIPMENT_PROFILE_NAME="Shipment profile name";
	public static final String LOCATION="Location";
	public static final String SERVERGROUP_NAME="Server group name";
	public static final String SERVERGROUP="Server group";
	public static final String SERVERGROUP_ID="server group id";
	public static final String LOPA_ID = "lopa id";
	public static final String CONTENTDATE = "content date";
	public static final String  ASSETDATE= "asset date";
	public static final String DEADLINE = "deadline";

	public static final String NAME = "name";
	public static final String TITLE = "title";
	public static final String SEATING_CLASS = "seating class";
	public static final String LRU_TYPE = "lru type";
	public static final String IATA = "IATA code";
	public static final String ICAO = "ICAO code";
	public static final String ORDER = "order";
	public static final String CONFIGURATION = "configuration";
	public static final String DATE = "Date";
	public static final String PACKAGE = "package";
	public static final String EXCEL = "excel";
	public static final String ROUTEGROUP = "Route Group ";

	public static final String FIELD = "Field";
	public static final String FIELDS = "Fields";
	public static final String GENERIC_CONTENT_TYPE = "Generic Content Type";
	public static final String GENERIC_CONTENT_TYPES = "Generic Content Types";
	public static final String SPECIFIC_CONTENT_TYPE = "Specific Content Type";
	public static final String SPECIFIC_CONTENT_TYPES = "Specific Content Types";

	public static final String INVALID_PARAMETER = "invalid.parameter";
	public static final String LANGUAGE_NAME = "Language Name";
	public static final String LANGUAGE_CODE = "Language Code";
	public static final String INVALID_MAPPING = "invalid.mapping";

	public static final String INGESTION_DETAIL = "Ingestion Detail";
	public static final String INGESTION_DETAILS = "Ingestion Details";

	/* MetadataImport */

	public static final int METADATA_IMPORT_STARTING_ROW = 0;
	public static final int METADATA_IMPORT_STARTING_ROW_DATA = 3;

	public static final int METADATA_IMPORT_STARTING_COLUMN = 2;
	public static final String METADATA_IMPORT_SET = "set";
	public static final String METADATA_IMPORT_JAVA_UTIL_LIST = "java.util.List";
	public static final String METADATA_IMPORT_ENGLISH_TITLE = "englishTitle";
	public static final String METADATA_IMPORT_ENGLISH_TITLE_LABEL = "M3 Title Name ** ";
	public static final String METADATA_IMPORT_TOP_LEVEL = "Top Level";
	public static final String METADATA_IMPORT_ENGLISH_TITLE_METHOD_NAME = "getEnglishTitle";
	public static final String METADATA_IMPORT_SET_STATUS_METHOD_NAME = "setStatus";
	public static final String METADATA_IMPORT_CONTENT_TYPE_LABEL = "content type";
	public static final String METADATA_IMPORT_OPEN_FILE = "open.xlsx.file";
	public static final String METADATA_IMPORT_OPEN_FILE_ERROR = "open.file.error";
	public static final String METADATA_IMPORT_READ_FILE_ERROR = "read.file.error";
	public static final String METADATA_IMPORT_CONTENTTYPE_IMPORT_ERROR = "contentType.import.error";
	public static final String METADATA_IMPORT_SETTING_CONTENTTYPE_ERROR = "set.contentType.error";
	public static final String METADATA_IMPORT_SETTING_ENGLISHTITLE_ERROR = "set.englishTitle.error";
	public static final String METADATA_IMPORT_SETTING_STATUS_ERROR = "set.status.error";
	public static final String METADATA_IMPORT_ONE_ROW_COMPLETED = "one.row.status";
	public static final String METADATA_IMPORT_ONE_SHEET_COMPLETED = "one.sheet.status";

	public static final String METADATA_IMPORT_SUCCESS_STATUS = "success.status";
	public static final String METADATA_IMPORT_DUPLICATE_DATA_ERROR = "duplicate.data.error";
	public static final String METADATA_IMPORT_CLOSE_FILE_ERROR = "close.file.error";
	public static final String IMPORT = "Imported";
	
	public static final int METADATA_IMPORT_STARTING_DECLARED_FIELD_ROW = 1;
	public static final int METADATA_IMPORT_BLANK_ROW = 2;
	public static final int METADATA_IMPORT_HEADER_STARTING_COLUMN = 0;
	public static final int METADATA_IMPORT_LANGUAGE_COLUMN = 1;
	public static final int METADATA_IMPORT_STARTING_DECLARED_FIELD_COLUMN = 3;
	public static final String METADATA_IMPORT_TOP_LEVEL_TITLE = "Top Level";
	public static final String METADATA_IMPORT_TRACK_TITLE = "Track";
	public static final String METADATA_IMPORT_LANGUAGE_LABEL = "Languages";
	public static final String METADATA_IMPORT_ENGLISH_ONLY_LABEL = "(English Only)";
	public static final int METADATA_IMPORT_THREAD_CHUNK_SIZE = 3;
	

	/* MetadataImport */
	
	/* Route Group Import  */
	public static final String ROUTEGROUP_IMPORT_READ_FILE_ERROR = "read.file.excel.error";
	public static final String EXCEL_IMPORTFILE_REQUIRED ="excelFile.required";
	public static final String ROUTEGROUP_AIRPORT_IMPORT_ERROR = "airport.import.error";
	public static final String ROUTEGROUP_IMPORT_ORIGIN_AIRPORT_EXIST = "Origin airport exist";
	public static final String ROUTEGROUP_IMPORT_DESTINATION_AIRPORT_EXIST ="Destination airport exist";
	public static final String ROUTEGROUP_IMPORT_ROUTE_IMPORT_ERROR = "route.import.error";
	public static final String ROUTEGROUP_IMPORT_ROUTE_EXIST="route exist";
	public static final String ROUTEGROUP_IMPORT_ERROR="routegroup.import.error";
	public static final String ROUTEGROUP_IMPORT_ROUTEGROUP_EXIST="Routegroup exist";
	public static final String ROUTEGROUP_IMPORT_SAVE_FAIL="RouteGroup save fail :";
	/* Route Group Import*/

	public static final String FIELD_EMAIL_INVALID = "field.email.invalid";
	public static final String FIELD_EMAIL = "Email";
	public static final String FIELD_PHONE_INVALID = "field.phone.invalid";
	public static final String FIELD_PHONE = "Phone";

	public static final String CONTENT_TYPE = "ContentType";
	public static final String AIRLINE_ID = "airlineId";
	public static final String CONFIGURATION_ID = "configurationId";
	public static final String CONTENT_TYPE_ID = "contentTypeId";
	public static final String ORDER_ID = "orderId";
	public static final String CONFIGURATION_NAME = "configurationName";
	public static final String FIELD_REQUIRED = "field.required";
	public static final String CONFIGURATION_STATUS = "Configuration Status";
	public static final String PACKAGING_TYPE = "Packaging Type";
	public static final String RULE = "Rule";
	public static final String CONFIGURATION_TITLE_STATUS = "Configuration Title Status";
	public static final String RULE_ID = "ruleId";
	public static final String RULE_NAME = "ruleName";
	public static final String BUNDLE_FOR_ROUTE_GROUP = "Bundle For RouteGroup";
	public static final String BUNDLE_FOR_ROUTE_GROUP_ID = "bundleForRouteGroupId";
	public static final String BUNDLE = "Bundle";
	public static final String BUNDLE_NAME = "bundleName";
	public static final String BUNDLE_ID = "bundleId";
	public static final String OBJECT_NOT_ACCESSIBLE = "object.not.accessible";
	public static final String CONFIGURATION_INACTIVE = "Configuration Status is INACTIVE";
}