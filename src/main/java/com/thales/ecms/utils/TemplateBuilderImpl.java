package com.thales.ecms.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.psddev.dari.db.ObjectField;
import com.psddev.dari.db.ObjectType;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Status;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.Language;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.ValidatorAttribute;
import com.thales.ecms.utils.ExcelCellStyleUtil.SECTION;
import com.thales.ecms.utils.ExcelTemplateManager.TemplateBuilder;

/**
 * It is a general purpose class that must be used to prepare a workbook containing a list of ContentType
 * represented as different sheets.
 * One sheet represents one ContentType object.
 * 
 * @author arjun.p
 *
 */
public class TemplateBuilderImpl implements ExcelTemplateManager.TemplateBuilder{
	
	private Logger logger = Logger.getLogger(TemplateBuilderImpl.class.getSimpleName());
	
	private Configuration configuration;
	
	private Workbook workbook;
	
	private Map<SECTION,CellStyle> cellStyles;
	
	private int rowCounter;
	
	private List<DropdownCellRange> cellRanges;
	
	private Map<String, Map<String,ValidatorAttribute>> rules;
	
	public TemplateBuilderImpl(Configuration configuration, Workbook workbook) {				
		this.configuration = configuration;
		this.workbook=workbook;
		rowCounter=0;
		cellRanges = new ArrayList<>();		
	}
	
	@Override
	public TemplateBuilder buildTemplate() {
		for(ObjectType objectType : configuration.getContentTypeList()){
//			reset rowConter for new sheet
			rowCounter=0;
			cellRanges = new ArrayList<>();
			buildSheet(objectType);			
		}		
//		createHiddenSheet("Lookup");	
		return this;
	}
	
	@Override
	public TemplateBuilder buildSheet(ObjectType objectType) {
		Sheet sheet = workbook.createSheet(objectType.getDisplayName());
//		column counter for first two rows of sheet
		int column=0;
		Row rowHeaderJavaFieldName = createRow(sheet);
		Row rowHeaderDisplayName = createRow(sheet);
		Row rowFieldValidationType = createRow(sheet);
		createCell(rowHeaderJavaFieldName, column, "Content Type",SECTION.HEADER);
		sheet.autoSizeColumn(column++);
		createCell(rowHeaderJavaFieldName, column, "Languages",SECTION.HEADER);
		sheet.autoSizeColumn(column++);
		createCell(rowHeaderDisplayName, 0, "",SECTION.HEADER);
		createCell(rowHeaderDisplayName, 1, "",SECTION.HEADER);
		createCell(rowFieldValidationType, 0, "",SECTION.HEADER);
		createCell(rowFieldValidationType, 1, "",SECTION.HEADER);
		for(ObjectField objectField : objectType.getFields()){
			if(objectField.getJavaEnumClassName()!=null && objectField.getJavaEnumClassName().equals(Status.class.getName()))
				continue;
			ValidatorAttribute validatorAttribute = null;
			Map<String,ValidatorAttribute> map = rules.get(objectType.getInternalName());
			if(map!=null)
				validatorAttribute = map.get(objectField.getJavaFieldName());				
//			applying rules
			/*if(validatorAttribute!=null){
				if(validatorAttribute.isRequired())
					continue;
			}*/			
			String javaFieldName=objectField.getJavaFieldName();
			String displayFieldName=objectField.getDisplayName();
//			This is for demo purpose. Modified javaFieldName and displayFieldName for FeatureVideo object. It must be modified later
			if(objectField.getJavaFieldName().equals("englishTitle")){
				javaFieldName = "M3 Title Name ** ";
				displayFieldName = "(English Only)";
			}				
			createCell(rowHeaderJavaFieldName, column, javaFieldName,SECTION.HEADER);
			createCell(rowHeaderDisplayName, column, displayFieldName,SECTION.HEADER);
			if(validatorAttribute!=null && validatorAttribute.isTranslatable())
				createCell(rowFieldValidationType, column, "Requires Translation",SECTION.HEADER);
			else
				createCell(rowFieldValidationType, column, "",SECTION.HEADER);				
//			Decision for drop down list
			if(objectField.getJavaEnumClassName()!=null){
				try {
					if(!objectField.getJavaEnumClassName().equals(Language.class.getName())){
						Class<?> c = Class.forName(objectField.getJavaEnumClassName());
						DropdownCellRange cellRange = new DropdownCellRange(new DropdownCellValue(c.getEnumConstants()), rowCounter, SpreadsheetVersion.valueOf("EXCEL2007").getLastRowIndex(), column, column);
						cellRanges.add(cellRange);
					}else{
//						Setting Language drop down for Language.class field same as Configuration interfaceLanguages
						Object[] languages=new Object[configuration.getInterfaceLanguages().size()];
						int c = 0;
						for(PaxguiLanguage paxguiLanguage : configuration.getInterfaceLanguages()){
							languages[c++]=paxguiLanguage.getPaxguiLanguageName();
						}
						DropdownCellRange cellRange = new DropdownCellRange(new DropdownCellValue(languages), rowCounter, SpreadsheetVersion.valueOf("EXCEL2007").getLastRowIndex(), column, column);
						cellRanges.add(cellRange);
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
//					What can I do here?
				}												
			} else {
//				For Dynamic Content Type
//				checking the 'Set<ObjectType> types' object's value of ObjectField
//				we are trying to get what data type has been set while creating this Content Type
//				Refer : GenericCOntentTypeService->addGenericContentType(..) method for more info
				/*if(objectField.getTypes()!=null && !objectField.getTypes().isEmpty()){
					System.out.println("Type : "+objectField.getTypes().iterator().next().getInternalName().contains("com.thales.ecms.model.datatype."));
				}*/
			}
			
			sheet.autoSizeColumn(column);
			column++;
		}
		
		Row rowTopLevel = createRow(sheet);
		createCell(rowTopLevel, 0, "Top Level",SECTION.TOP_LEVEL);	
		insertLanguageRows(sheet, rowTopLevel, SECTION.TOP_LEVEL);
		
		Row rowTrack = createRow(sheet);				
		createCell(rowTrack, 0, "Track",SECTION.TRACK);
		insertLanguageRows(sheet, rowTrack, SECTION.TRACK);
		
		for(DropdownCellRange dropdownCellRange : cellRanges){
			createDropDownList((XSSFSheet)sheet, dropdownCellRange.getDropdownCellValue().getObjects(), dropdownCellRange.getCellRangeAddressList());
		}
		
		/*createDropDownList((XSSFSheet)sheet, new String[]{"Arabic","Chinese","English","French","German",
				"Italian","Japanese","Korean","Portuguese","Russian"}, 2, 2, 15, 15);*/
		return this;
	}

	@Override
	public TemplateBuilder rules(Map<String, Map<String, ValidatorAttribute>> rules) {
		this.rules=rules;
		return this;
	}
	
	@Override
	public TemplateBuilder cellStyles(Map<SECTION, CellStyle> cellStyles) {
		this.cellStyles=cellStyles;
		return this;
	}
	
	private void insertLanguageRows(Sheet sheet,Row startingRow, SECTION section){
//		###################################################################
//		PaxguiLanguage insertion on column index 1, from row 3
		Row languageRow=startingRow;// For first Iteration, first language has to on same row
		if(configuration.getInterfaceLanguages()==null || configuration.getInterfaceLanguages().size()==0){
			createCell(languageRow, 1, "",section);
			languageRow=sheet.createRow(languageRow.getRowNum()+1);
		}else{
			for(PaxguiLanguage paxguiLanguage : configuration.getInterfaceLanguages()){
				if(languageRow.getRowNum() != startingRow.getRowNum())
					createCell(languageRow, 0, "",section);
				createCell(languageRow, 1, paxguiLanguage.getPaxguiLanguageName(),section);
//				every time create a new row
				languageRow=createRow(sheet);
			}
//			remove the last row as created by above loop
			sheet.removeRow(languageRow);
			rowCounter--;// Also remember to decrement rowCounter as one row has been removed
		}		
//		PaxguiLanguage insertion done
//		###################################################################
	}	
	
	public Row createRow(Sheet sheet){
		return sheet.createRow(rowCounter++);
	}
	
	public void createCell(Row row,int columnNumber,String value,SECTION section){
		Cell cell = row.createCell(columnNumber);
		cell.setCellValue(value);
		CellStyle style;		
		switch (section) {
		case HEADER:
			style=cellStyles.get(SECTION.HEADER);
			break;
		case TOP_LEVEL:
			style=cellStyles.get(SECTION.TOP_LEVEL);
			break;
		case TRACK:
			style=cellStyles.get(SECTION.TRACK);
			break;
		default:
			style=cellStyles.get(SECTION.TRACK);
			break;
		}
		cell.setCellStyle(style);
	}		
	
	public void createHiddenSheet(String name){
		Sheet sheet = workbook.createSheet(name);		
		workbook.setSheetHidden(workbook.getSheetIndex(sheet), true);
	}
	
	public void createDropDownList(XSSFSheet sheet,String[] listOfValues,int firstRow,int lastRow,int firstCol,int lastCol){
		XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
		XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(listOfValues);
		CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);
		XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
		validation.setSuppressDropDownArrow(true);
		validation.setErrorStyle(DataValidation.ErrorStyle.STOP);
		validation.createErrorBox("ECMS", "The value does not match any element of specified list");
		validation.setShowErrorBox(true);
		sheet.addValidationData(validation);		
	}
	
	public void createDropDownList(XSSFSheet sheet,Object[] listOfValues,CellRangeAddressList addressList){
		XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
		String[] vals = new String[listOfValues.length];
		for(int i=0;i<listOfValues.length;i++)
			vals[i]=listOfValues[i].toString();
		XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(vals);		
		XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
		validation.setSuppressDropDownArrow(true);
		validation.setErrorStyle(DataValidation.ErrorStyle.STOP);
		validation.createErrorBox("ECMS", "The value does not match any element of specified list");
		validation.setShowErrorBox(true);
		sheet.addValidationData(validation);		
	}
	
	private class DropdownCellRange{
		
		private CellRangeAddressList cellRangeAddressList;
		
		private DropdownCellValue dropdownCellValue;
		
		private int firstRow,lastRow,firstCol,lastCol;

		public DropdownCellRange(DropdownCellValue dropdownCellValue, int firstRow, int lastRow, int firstCol,int lastCol) {			
			this.dropdownCellValue = dropdownCellValue;
			this.firstRow = firstRow;
			this.lastRow = lastRow;
			this.firstCol = firstCol;
			this.lastCol = lastCol;
		}

		public DropdownCellValue getDropdownCellValue() {
			return dropdownCellValue;
		}
		
		public CellRangeAddressList getCellRangeAddressList() {
			if(firstRow==-1 || lastRow==-1 || firstCol==-1 || lastCol==-1)
				throw new IllegalStateException("Unable to create CellRangeAddressList, any parameter can not be negative");
			cellRangeAddressList = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);
			return cellRangeAddressList;
		}

		@Override
		public int hashCode() {
			final int seed = 37;
		    int hash = 1;
		    hash = seed * hash + firstRow;
		    hash = seed * hash + lastRow;
		    hash = seed * hash + firstCol;
		    hash = seed * hash + lastCol;
		    return hash;
		}		
		
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof DropdownCellRange))
				return false;
			DropdownCellRange other=(DropdownCellRange)obj;
			if(firstRow!=other.firstRow)
				return false;
			if(lastRow!=other.lastRow)
				return false;
			if(firstCol!=other.firstCol)
				return false;
			if(lastRow!=other.lastRow)
				return false;
			return true;
		}
				
	}
	
	private class DropdownCellValue{
		
		private Object[] objects;

		public DropdownCellValue(Object[] objects) {
			this.objects = objects;
		}

		public Object[] getObjects() {
			return objects;
		}			
	}
	
}
