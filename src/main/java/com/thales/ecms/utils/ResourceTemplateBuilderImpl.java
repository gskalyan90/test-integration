package com.thales.ecms.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.psddev.dari.db.ObjectField;
import com.psddev.dari.db.ObjectType;
import com.thales.ecms.model.Aircraft;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.Bundle;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Language;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.Lru;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.Route;
import com.thales.ecms.model.RouteGroup;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.airline.AirlineContact;
import com.thales.ecms.utils.ExcelCellStyleUtil.SECTION;
import com.thales.ecms.utils.ResourceExportManager.TemplateBuilder;

/**
 * 
 * An Implementor of ResourceExportManager.TemplateBuilder
 * 
 * @author arjun.p
 *
 */
public class ResourceTemplateBuilderImpl implements ResourceExportManager.TemplateBuilder {

	private Logger logger = Logger.getLogger(ResourceTemplateBuilderImpl.class.getSimpleName());	
	
	private Workbook workbook;
	
	private Map<SECTION,CellStyle> cellStyles;
	
	private int rowCounter;
	
	private List<DropdownCellRange> cellRanges;
	
	private List<Class> resources;
	
	public ResourceTemplateBuilderImpl(Workbook workbook, List<Class> resources) {						
		this.workbook = workbook;
		this.resources = resources;		
		rowCounter=0;
		cellRanges = new ArrayList<>();		
	}
	
	@Override
	public TemplateBuilder buildTemplate() {
//		List<ObjectType> objectTypes = new ArrayList<>();
//		if(resources!=null){
//			for(Class c : resources)
//				objectTypes.add(ObjectType.getInstance(c));
//		}
		ObjectType[] objectTypes = new ObjectType[]{
				ObjectType.getInstance(AirlineContact.class),
				ObjectType.getInstance(Airport.class),
				ObjectType.getInstance(LruType.class),
				ObjectType.getInstance(Lru.class),
				ObjectType.getInstance(Aircraft.class),
				ObjectType.getInstance(SeatingClass.class),
				ObjectType.getInstance(Lopa.class),
				ObjectType.getInstance(Route.class),
				ObjectType.getInstance(RouteGroup.class),
				ObjectType.getInstance(Configuration.class),
				ObjectType.getInstance(Bundle.class)
		};		
		for(ObjectType objectType : objectTypes){
//			reset rowConter for new sheet
			rowCounter=0;
			cellRanges = new ArrayList<>();
			buildSheet(objectType);			
		}		
//		createHiddenSheet("Lookup");	
		return this;
	}
	
	@Override
	public TemplateBuilder buildSheet(ObjectType objectType) {
		Sheet sheet = workbook.createSheet(objectType.getDisplayName());
//		column counter for first two rows of sheet
		int column=0;
		Row rowHeaderDisplayName = createRow(sheet);		
		Row rowHeaderJavaFieldName = createRow(sheet);
		for(ObjectField objectField : objectType.getFields()){	
			String displayFieldName=objectField.getDisplayName();
			String javaFieldName=objectField.getJavaFieldName();
			createCell(rowHeaderDisplayName, column, displayFieldName,SECTION.TRACK);							
			createCell(rowHeaderJavaFieldName, column, javaFieldName,SECTION.TRACK);
//			Decision for drop down list
			if(objectField.getJavaEnumClassName()!=null){
				try {
					if(!objectField.getJavaEnumClassName().equals(Language.class.getName())){
						Class<?> c = Class.forName(objectField.getJavaEnumClassName());
						DropdownCellRange cellRange = new DropdownCellRange(new DropdownCellValue(c.getEnumConstants()), rowCounter, SpreadsheetVersion.valueOf("EXCEL2007").getLastRowIndex(), column, column);
						cellRanges.add(cellRange);
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
//					What can I do here?
				}												
			}			
			sheet.autoSizeColumn(column);
			column++;
		}				
		
		for(DropdownCellRange dropdownCellRange : cellRanges){
			createDropDownList((XSSFSheet)sheet, dropdownCellRange.getDropdownCellValue().getObjects(), dropdownCellRange.getCellRangeAddressList());
		}		
		return this;
	}
	
	@Override
	public TemplateBuilder cellStyles(Map<SECTION, CellStyle> cellStyles) {
		this.cellStyles=cellStyles;
		return this;
	}		
	
	public Row createRow(Sheet sheet){
		return sheet.createRow(rowCounter++);
	}
	
	public void createCell(Row row,int columnNumber,String value,SECTION section){
		Cell cell = row.createCell(columnNumber);
		cell.setCellValue(value);
		CellStyle style;		
		switch (section) {
		case HEADER:
			style=cellStyles.get(SECTION.HEADER);
			break;
		case TOP_LEVEL:
			style=cellStyles.get(SECTION.TOP_LEVEL);
			break;
		case TRACK:
			style=cellStyles.get(SECTION.TRACK);
			break;
		default:
			style=cellStyles.get(SECTION.TRACK);
			break;
		}
		cell.setCellStyle(style);
	}		
	
	public void createHiddenSheet(String name){
		Sheet sheet = workbook.createSheet(name);		
		workbook.setSheetHidden(workbook.getSheetIndex(sheet), true);
	}
	
	public void createDropDownList(XSSFSheet sheet,String[] listOfValues,int firstRow,int lastRow,int firstCol,int lastCol){
		XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
		XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(listOfValues);
		CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);
		XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
		validation.setSuppressDropDownArrow(true);
		validation.setErrorStyle(DataValidation.ErrorStyle.STOP);
		validation.createErrorBox("ECMS", "The value does not match any element of specified list");
		validation.setShowErrorBox(true);
		sheet.addValidationData(validation);		
	}
	
	public void createDropDownList(XSSFSheet sheet,Object[] listOfValues,CellRangeAddressList addressList){
		XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
		String[] vals = new String[listOfValues.length];
		for(int i=0;i<listOfValues.length;i++)
			vals[i]=listOfValues[i].toString();
		XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(vals);		
		XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
		validation.setSuppressDropDownArrow(true);
		validation.setErrorStyle(DataValidation.ErrorStyle.STOP);
		validation.createErrorBox("ECMS", "The value does not match any element of specified list");
		validation.setShowErrorBox(true);
		sheet.addValidationData(validation);		
	}
	
	private class DropdownCellRange{
		
		private CellRangeAddressList cellRangeAddressList;
		
		private DropdownCellValue dropdownCellValue;
		
		private int firstRow,lastRow,firstCol,lastCol;

		public DropdownCellRange(DropdownCellValue dropdownCellValue, int firstRow, int lastRow, int firstCol,int lastCol) {			
			this.dropdownCellValue = dropdownCellValue;
			this.firstRow = firstRow;
			this.lastRow = lastRow;
			this.firstCol = firstCol;
			this.lastCol = lastCol;
		}

		public DropdownCellValue getDropdownCellValue() {
			return dropdownCellValue;
		}
		
		public CellRangeAddressList getCellRangeAddressList() {
			if(firstRow==-1 || lastRow==-1 || firstCol==-1 || lastCol==-1)
				throw new IllegalStateException("Unable to create CellRangeAddressList, any parameter can not be negative");
			cellRangeAddressList = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);
			return cellRangeAddressList;
		}

		@Override
		public int hashCode() {
			final int seed = 37;
		    int hash = 1;
		    hash = seed * hash + firstRow;
		    hash = seed * hash + lastRow;
		    hash = seed * hash + firstCol;
		    hash = seed * hash + lastCol;
		    return hash;
		}		
		
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof DropdownCellRange))
				return false;
			DropdownCellRange other=(DropdownCellRange)obj;
			if(firstRow!=other.firstRow)
				return false;
			if(lastRow!=other.lastRow)
				return false;
			if(firstCol!=other.firstCol)
				return false;
			if(lastRow!=other.lastRow)
				return false;
			return true;
		}
				
	}
	
	private class DropdownCellValue{
		
		private Object[] objects;

		public DropdownCellValue(Object[] objects) {
			this.objects = objects;
		}

		public Object[] getObjects() {
			return objects;
		}			
	}
	
}
