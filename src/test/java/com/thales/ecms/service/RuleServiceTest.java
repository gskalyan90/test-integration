package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Rule;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.ContentTypeDTO;
import com.thales.ecms.model.dto.RuleContentFieldDTO;
import com.thales.ecms.model.dto.RuleContentTypeMappingDTO;
import com.thales.ecms.model.dto.RuleDTO;
import com.thales.ecms.model.dto.RuleFieldDTO;
import com.thales.ecms.model.dto.ValidatorAttributeDTO;
import com.thales.ecms.service.RuleService;

/**
 * @author sushma
 * Service class for JUNIT test cases on RuleService
 */


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class RuleServiceTest {
	@Autowired
	private RuleService ruleService;
	@Autowired
	private ContentTypeService ContentTypeService;
	@Autowired
	private ContentTypeService contentTypeService;
	@Autowired
	private AirlineService airlineService;

	@Before
	public void setup() {
		
	}
	
	/**
	 * Get contentTypeList 
	 * @return list of RuleContentFieldDTO
     */
	
@Test
public void contentTypeListTest(){
	List<RuleContentFieldDTO> ruleContentFieldDTOList = ruleService.contentTypeList();
    Assert.assertTrue(ruleContentFieldDTOList.size()>0);
}

/**
 * Get RuleContentFieldDTO list by Content id
 * @param Content id
 * @return list of RuleContentFieldDTO
 */

@Test
public void getFieldByContentTypeIdTest() throws ObjectNotFoundException{
	List<ContentTypeDTO> contentTypeDTOList=contentTypeService.getContentTypes();
	ContentTypeDTO contenttype=contentTypeDTOList.get(0);
    List<RuleContentFieldDTO> ruleContentFieldDTOList = ruleService.getFieldByContentTypeId(contenttype.getContentTypeInternalId());
    Assert.assertTrue(ruleContentFieldDTOList.size()>0);
 }

/**
 * Get RuleContentFieldDTO list by Content id for throws Exception
 * @param Content id
 * @return ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void getFieldByContentTypeIdExceptionTest() throws ObjectNotFoundException{
    List<RuleContentFieldDTO> ruleContentFieldDTOList = ruleService.getFieldByContentTypeId("ContentTypeInternalId");
    Assert.assertTrue(ruleContentFieldDTOList.size()>0);
 }

/**
 * Add Rule
 * @param airline id
 * @param RuleDTO 
 * @return String
 */

@Test
public void addRuleTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sicejetjg1yy");
	airlineDetails.setWebsite("https://www.gogjyntu.com");
	airlineDetails.setCompanyName("Sicejejtg1ui");
	airlineDetails.setCodeShare("9036jg89");
	airlineDetails.setIataCode("BBjg");
	airlineDetails.setIcaoCode("AABujgi");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    RuleContentTypeMappingDTO ruleContentTypeMappingDTO=new RuleContentTypeMappingDTO();
	ValidatorAttributeDTO validatorDTO=new ValidatorAttributeDTO();
	validatorDTO.setMaxChar("345");
	validatorDTO.setTranslatable(true);
	validatorDTO.setRequired(true);
	RuleDTO ruleDetails=new RuleDTO();
	RuleFieldDTO ruleFieldDTO=new RuleFieldDTO();
	ruleFieldDTO.setFieldName("Rull_10");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	List<RuleFieldDTO> contentTypeField=new ArrayList<>();
	contentTypeField.add(ruleFieldDTO);
	ruleFieldDTO.setFieldName("Rule-32");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	ruleDetails.setRuleName("ruleName");
	ruleDetails.setRuleContentTypeMappingDTO(ruleContentTypeMappingDTO);
	List<ContentTypeDTO> contentTypeDTOList=contentTypeService.getContentTypes();
	ContentTypeDTO contenttype=contentTypeDTOList.get(0);
    ruleContentTypeMappingDTO.setContentTypeName(contenttype.getContentTypeName());
	ruleContentTypeMappingDTO.setContentTypeId(contenttype.getContentTypeInternalId());
	ruleContentTypeMappingDTO.setContentTypeField(contentTypeField);
	String result=ruleService.addRule(airlineDTO.getAirlineInternalId(), ruleDetails);
    Assert.assertEquals(result,"Rule saved successfully");
    Site airline1 = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline1.delete();
	

}

/**
 * Add Rule for throws exception
 * @param airline id
 * @param RuleDTO 
 * @return ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void addRuleExceptionTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	
    RuleContentTypeMappingDTO ruleContentTypeMappingDTO=new RuleContentTypeMappingDTO();
	ValidatorAttributeDTO validatorDTO=new ValidatorAttributeDTO();
	validatorDTO.setMaxChar("345");
	validatorDTO.setTranslatable(true);
	validatorDTO.setRequired(true);
	RuleDTO ruleDetails=new RuleDTO();
	RuleFieldDTO ruleFieldDTO=new RuleFieldDTO();
	ruleFieldDTO.setFieldName("Rull_10");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	List<RuleFieldDTO> contentTypeField=new ArrayList<>();
	contentTypeField.add(ruleFieldDTO);
	ruleFieldDTO.setFieldName("Rule-32");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	ruleDetails.setRuleName("ruleName");
	ruleDetails.setRuleContentTypeMappingDTO(ruleContentTypeMappingDTO);
	List<ContentTypeDTO> contentTypeDTOList=contentTypeService.getContentTypes();
	ContentTypeDTO contenttype=contentTypeDTOList.get(0);
    ruleContentTypeMappingDTO.setContentTypeName(contenttype.getContentTypeName());
	ruleContentTypeMappingDTO.setContentTypeId(contenttype.getContentTypeInternalId());
	ruleContentTypeMappingDTO.setContentTypeField(contentTypeField);
	String result=ruleService.addRule("airlineId", ruleDetails);
    Assert.assertEquals(result,"Rule saved successfully");
}

/**
 * Get Rules 
 * @param airline id
 * @param RuleDTO 
 * @return List of RuleDTO
 */

@Test
public void getRulesTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sicejegtj1yy");
	airlineDetails.setWebsite("https://www.gojgyntu.com");
	airlineDetails.setCompanyName("Sicejejtg1ui");
	airlineDetails.setCodeShare("9036jg89");
	airlineDetails.setIataCode("BBjjg");
	airlineDetails.setIcaoCode("AABugji");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    RuleContentTypeMappingDTO ruleContentTypeMappingDTO=new RuleContentTypeMappingDTO();
	ValidatorAttributeDTO validatorDTO=new ValidatorAttributeDTO();
	validatorDTO.setMaxChar("345");
	validatorDTO.setTranslatable(true);
	validatorDTO.setRequired(true);
	RuleDTO ruleDetails=new RuleDTO();
	RuleFieldDTO ruleFieldDTO=new RuleFieldDTO();
	ruleFieldDTO.setFieldName("Rull_10");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	List<RuleFieldDTO> contentTypeField=new ArrayList<>();
	contentTypeField.add(ruleFieldDTO);
	ruleFieldDTO.setFieldName("Rule-32");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	ruleDetails.setRuleName("ruleName");
	ruleDetails.setRuleContentTypeMappingDTO(ruleContentTypeMappingDTO);
	List<ContentTypeDTO> contentTypeDTOList=contentTypeService.getContentTypes();
	ContentTypeDTO contenttype=contentTypeDTOList.get(0);
    ruleContentTypeMappingDTO.setContentTypeName(contenttype.getContentTypeName());
	ruleContentTypeMappingDTO.setContentTypeId(contenttype.getContentTypeInternalId());
	ruleContentTypeMappingDTO.setContentTypeField(contentTypeField);
	ruleService.addRule(airlineDTO.getAirlineInternalId(), ruleDetails);
    List<RuleDTO> ruleDTOList = ruleService.getRules(airlineDTO.getAirlineInternalId());
    Assert.assertTrue(ruleDTOList.size()>0);
    Site airline1 = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline1.delete();
}

/**
 * Get Rules for throws exception
 * @param airline id
 * @param RuleDTO 
 * @return ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void getRulesExceptionTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
    List<RuleDTO> ruleDTOList = ruleService.getRules("airlineId");
    Assert.assertTrue(ruleDTOList.size()>0);
}

/**
 * Delete Rules 
 * @param airline id
 * @param Rule id
 * @return String
 */

@Test
public void deleteRuleTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sicejegtjy1yy");
	airlineDetails.setWebsite("https://www.gojyygntu.com");
	airlineDetails.setCompanyName("Sicejejt1gyui");
	airlineDetails.setCodeShare("9036ygj89");
	airlineDetails.setIataCode("BBjjgy");
	airlineDetails.setIcaoCode("AABguyji");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    RuleContentTypeMappingDTO ruleContentTypeMappingDTO=new RuleContentTypeMappingDTO();
	ValidatorAttributeDTO validatorDTO=new ValidatorAttributeDTO();
	validatorDTO.setMaxChar("345");
	validatorDTO.setTranslatable(true);
	validatorDTO.setRequired(true);
	RuleDTO ruleDetails=new RuleDTO();
	RuleFieldDTO ruleFieldDTO=new RuleFieldDTO();
	ruleFieldDTO.setFieldName("Rull_10");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	List<RuleFieldDTO> contentTypeField=new ArrayList<>();
	contentTypeField.add(ruleFieldDTO);
	ruleFieldDTO.setFieldName("Rule-32");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	ruleDetails.setRuleName("ruleName");
	ruleDetails.setRuleContentTypeMappingDTO(ruleContentTypeMappingDTO);
	List<ContentTypeDTO> contentTypeDTOList=contentTypeService.getContentTypes();
	ContentTypeDTO contenttype=contentTypeDTOList.get(0);
    ruleContentTypeMappingDTO.setContentTypeName(contenttype.getContentTypeName());
	ruleContentTypeMappingDTO.setContentTypeId(contenttype.getContentTypeInternalId());
	ruleContentTypeMappingDTO.setContentTypeField(contentTypeField);
	ruleService.addRule(airlineDTO.getAirlineInternalId(), ruleDetails);
    List<RuleDTO> ruleDTOList = ruleService.getRules(airlineDTO.getAirlineInternalId());
    RuleDTO RuleDTO=ruleDTOList.get(0);
    String result=ruleService.deleteRule(airlineDTO.getAirlineInternalId(), RuleDTO.getRuleId());
    Assert.assertEquals(result,"Rule deleted successfully");
    Site airline1 = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline1.delete();

}

/**
 * Delete Rules for throws exception
 * @param airline id
 * @param Rule id
 * @return ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void deleteRuleExceptionTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
    String result=ruleService.deleteRule("airlineId", "ruleId");
    Assert.assertEquals(result,"Rule deleted successfully");

}

/**
 * Get Rules 
 * @param airline id
 * @return list of RuleDTO
 */


@Test
public void getRuleByIdTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sicejetgj1yy");
	airlineDetails.setWebsite("https://www.ggojyntu.com");
	airlineDetails.setCompanyName("Sicejejtg1ui");
	airlineDetails.setCodeShare("9036gj89");
	airlineDetails.setIataCode("BBjgj");
	airlineDetails.setIcaoCode("AABguji");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    RuleContentTypeMappingDTO ruleContentTypeMappingDTO=new RuleContentTypeMappingDTO();
	ValidatorAttributeDTO validatorDTO=new ValidatorAttributeDTO();
	validatorDTO.setMaxChar("345");
	validatorDTO.setTranslatable(true);
	validatorDTO.setRequired(true);
	RuleDTO ruleDetails=new RuleDTO();
	RuleFieldDTO ruleFieldDTO=new RuleFieldDTO();
	ruleFieldDTO.setFieldName("Rull_10");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	List<RuleFieldDTO> contentTypeField=new ArrayList<>();
	contentTypeField.add(ruleFieldDTO);
	ruleFieldDTO.setFieldName("Rule-32");
	ruleFieldDTO.setValidatorDTO(validatorDTO);
	ruleDetails.setRuleName("ruleName");
	ruleDetails.setRuleContentTypeMappingDTO(ruleContentTypeMappingDTO);
	List<ContentTypeDTO> contentTypeDTOList=contentTypeService.getContentTypes();
	ContentTypeDTO contenttype=contentTypeDTOList.get(0);
    ruleContentTypeMappingDTO.setContentTypeName(contenttype.getContentTypeName());
	ruleContentTypeMappingDTO.setContentTypeId(contenttype.getContentTypeInternalId());
	ruleContentTypeMappingDTO.setContentTypeField(contentTypeField);
	ruleService.addRule(airlineDTO.getAirlineInternalId(), ruleDetails);
    List<RuleDTO> ruleDTOList = ruleService.getRules(airlineDTO.getAirlineInternalId());
    RuleDTO RuleDTO=ruleDTOList.get(0);
    RuleDTO ruleDTO = ruleService.getRuleById(airlineDTO.getAirlineInternalId(), RuleDTO.getRuleId());
	Assert.assertNotNull(ruleDTO);
    Site airline1 = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline1.delete();
}

/**
 * Get Rules for throws exception
 * @param airline id
 * @return  ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void getRuleByIdExceptionTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
    RuleDTO ruleDTO = ruleService.getRuleById("airlineId", "ruleId");
	Assert.assertNotNull(ruleDTO);
}



}
