package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.AirlineContactDTO;
import com.thales.ecms.model.dto.AirlineDTO;

import junit.framework.Assert;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })
public class AirlineContactServiceTest {
	
	
	@Autowired 
	private AirlineService airlineService;
	
	@Autowired 
	private AirlineContactService airlineContactService;

	@Before
	public void setup() {

	}
	
	@Test
	public void addAirlineContactTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException {
		

		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("spice1");
		airlineDetails.setWebsite("https://www.spice1.com");
		airlineDetails.setCompanyName("spice1");
		airlineDetails.setCodeShare("spice11-");
		airlineDetails.setIataCode("spice21-");
		airlineDetails.setIcaoCode("spice31-");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
		AirlineContactDTO airlineContactDetailDTO = new AirlineContactDTO();
		airlineContactDetailDTO.setAirlineContactType("MAINTENANCE");
		airlineContactDetailDTO.setEmail("www.spice1.com");
		airlineContactDetailDTO.setFirstName("spice11");
		airlineContactDetailDTO.setLastName("spice21");
		airlineContactDetailDTO.setPhone("45638323347");
		AirlineContactDTO airlineContactDTO  = airlineContactService.addAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDetailDTO);
		Assert.assertNotNull(airlineContactDTO.getAirlineContactInternalId());
		airlineContactService.deleteAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDTO.getAirlineContactInternalId());
		Query.from(Site.class).where("id = ?",airlineDTO.getAirlineInternalId()).first().delete();
	
	}
	
	
	@Test
	public void getAirlineContactsTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("spice1");
		airlineDetails.setWebsite("https://www.spice1.com");
		airlineDetails.setCompanyName("spice1");
		airlineDetails.setCodeShare("spice11-");
		airlineDetails.setIataCode("spice21-");
		airlineDetails.setIcaoCode("spice31-");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
		AirlineContactDTO airlineContactDetailDTO = new AirlineContactDTO();
		airlineContactDetailDTO.setAirlineContactType("MAINTENANCE");
		airlineContactDetailDTO.setEmail("www.spice1.com");
		airlineContactDetailDTO.setFirstName("spice11");
		airlineContactDetailDTO.setLastName("spice21");
		airlineContactDetailDTO.setPhone("45638323347");
		AirlineContactDTO airlineContactDTO  = airlineContactService.addAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDetailDTO);
		List<AirlineContactDTO> airlineContactDTOs  = airlineContactService.getAirlineContacts(airlineDTO.getAirlineInternalId());
		Assert.assertNotNull(airlineContactDTOs);
		airlineContactService.deleteAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDTO.getAirlineContactInternalId());
		Query.from(Site.class).where("id = ?",airlineDTO.getAirlineInternalId()).first().delete();
		
	}
	
	@Test
	public void getAllAirlineContactsTest(){
		
		List<AirlineContactDTO> airlineContactDTOs = airlineContactService.getAllAirlineContacts();
		Assert.assertNotNull(airlineContactDTOs);
	}
	
	@Test
	public void getAirlineContactByIdTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("spice1");
		airlineDetails.setWebsite("https://www.spice1.com");
		airlineDetails.setCompanyName("spice1");
		airlineDetails.setCodeShare("spice11-");
		airlineDetails.setIataCode("spice21-");
		airlineDetails.setIcaoCode("spice31-");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
		AirlineContactDTO airlineContactDetailDTO = new AirlineContactDTO();
		airlineContactDetailDTO.setAirlineContactType("MAINTENANCE");
		airlineContactDetailDTO.setEmail("www.spice1.com");
		airlineContactDetailDTO.setFirstName("spice11");
		airlineContactDetailDTO.setLastName("spice21");
		airlineContactDetailDTO.setPhone("45638323347");
		AirlineContactDTO airlineContactDTO  = airlineContactService.addAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDetailDTO);
		AirlineContactDTO dto = airlineContactService.getAirlineContactById(airlineDTO.getAirlineInternalId(), 
				airlineContactDTO.getAirlineContactInternalId());
		Assert.assertNotNull(dto);
		airlineContactService.deleteAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDTO.getAirlineContactInternalId());
		Query.from(Site.class).where("id = ?",airlineDTO.getAirlineInternalId()).first().delete();
	}
	
	@Test
	public void getAirlineContactByPhoneTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, ObjectNotFoundException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("spice1");
		airlineDetails.setWebsite("https://www.spice1.com");
		airlineDetails.setCompanyName("spice1");
		airlineDetails.setCodeShare("spice11-");
		airlineDetails.setIataCode("spice21-");
		airlineDetails.setIcaoCode("spice31-");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
		AirlineContactDTO airlineContactDetailDTO = new AirlineContactDTO();
		airlineContactDetailDTO.setAirlineContactType("MAINTENANCE");
		airlineContactDetailDTO.setEmail("www.spice1.com");
		airlineContactDetailDTO.setFirstName("spice11");
		airlineContactDetailDTO.setLastName("spice21");
		airlineContactDetailDTO.setPhone("45638323347");
		AirlineContactDTO airlineContactDTO  = airlineContactService.addAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDetailDTO);
		AirlineContactDTO dto = airlineContactService.getAirlineContactByPhone(airlineDTO.getAirlineInternalId(), airlineContactDTO.getPhone());
		Assert.assertNotNull(dto);
		airlineContactService.deleteAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDTO.getAirlineContactInternalId());
		Query.from(Site.class).where("id = ?",airlineDTO.getAirlineInternalId()).first().delete();
	}
	
	
	@Test
	public void getAirlineContactByEmailTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, ObjectNotFoundException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("spice1");
		airlineDetails.setWebsite("https://www.spice1.com");
		airlineDetails.setCompanyName("spice1");
		airlineDetails.setCodeShare("spice11-");
		airlineDetails.setIataCode("spice21-");
		airlineDetails.setIcaoCode("spice31-");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
		AirlineContactDTO airlineContactDetailDTO = new AirlineContactDTO();
		airlineContactDetailDTO.setAirlineContactType("MAINTENANCE");
		airlineContactDetailDTO.setEmail("www.spice1.com");
		airlineContactDetailDTO.setFirstName("spice11");
		airlineContactDetailDTO.setLastName("spice21");
		airlineContactDetailDTO.setPhone("45638323347");
		AirlineContactDTO airlineContactDTO  = airlineContactService.addAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDetailDTO);
		AirlineContactDTO dto = airlineContactService.getAirlineContactByEmail(airlineDTO.getAirlineInternalId(), airlineContactDTO.getEmail());
        Assert.assertNotNull(dto);
        airlineContactService.deleteAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDTO.getAirlineContactInternalId());
		Query.from(Site.class).where("id = ?",airlineDTO.getAirlineInternalId()).first().delete();
	}
	
	@Test
	public void updateAirlineContactTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, ObjectNotFoundException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("spice1");
		airlineDetails.setWebsite("https://www.spice1.com");
		airlineDetails.setCompanyName("spice1");
		airlineDetails.setCodeShare("spice11-");
		airlineDetails.setIataCode("spice21-");
		airlineDetails.setIcaoCode("spice31-");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
		AirlineContactDTO airlineContactDetailDTO = new AirlineContactDTO();
		airlineContactDetailDTO.setAirlineContactType("MAINTENANCE");
		airlineContactDetailDTO.setEmail("www.spice1.com");
		airlineContactDetailDTO.setFirstName("spice11");
		airlineContactDetailDTO.setLastName("spice21");
		airlineContactDetailDTO.setPhone("45638323347");
		AirlineContactDTO airlineContactDTO  = airlineContactService.addAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDetailDTO);
		AirlineContactDTO updateAirlineContactDetailDTO = new AirlineContactDTO();
		updateAirlineContactDetailDTO.setAirlineContactInternalId(airlineContactDTO.getAirlineContactInternalId());
		updateAirlineContactDetailDTO.setAirlineContactType("MAINTENANCE");
		updateAirlineContactDetailDTO.setEmail("www.sanarsa1wqasyss.com");
		updateAirlineContactDetailDTO.setFirstName("sanara1swqasyss");
		updateAirlineContactDetailDTO.setLastName("sfdaf1d");
		updateAirlineContactDetailDTO.setPhone("4568351312347");
		AirlineContactDTO dto = airlineContactService.updateAirlineContact(airlineDTO.getAirlineInternalId(), 
				airlineContactDTO.getAirlineContactInternalId(), updateAirlineContactDetailDTO);
        Assert.assertNotNull(dto);
        airlineContactService.deleteAirlineContact(airlineDTO.getAirlineInternalId(), airlineContactDTO.getAirlineContactInternalId());
		Query.from(Site.class).where("id = ?",airlineDTO.getAirlineInternalId()).first().delete();
	}
	
	
	
	
	
}
