package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.ConfigurationRequestDTO;
import com.thales.ecms.model.dto.ContentTypeDTO;

import junit.framework.Assert;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })
public class ConfigurationServiceTest {

	@Autowired
	private AirlineService airlineService;
	
	@Autowired
	private  ConfigurationService  configurationService;
	
	@Autowired
	private ContentTypeService contentTypeService;
	
	@Before
	public void setup() {
	  
	}
	
	
	@Test
	public void addConfigurationTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException {
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicejetw");
		airlineDetails.setWebsite("https://www.Sicejetw.com");
		airlineDetails.setCompanyName("Sicejetw");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("Sicejetw1");
		airlineDetails.setIataCode("Sicejetw2");
		airlineDetails.setIcaoCode("Sicejetw3");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Sicejetw");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejetw");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		Assert.assertNotNull(configurationRequestDTO);
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@SuppressWarnings("unused")
	@Test
	public void getconfigurationListTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicejetw");
		airlineDetails.setWebsite("https://www.Sicejetw.com");
		airlineDetails.setCompanyName("Sicejetw");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("Sicejetw1");
		airlineDetails.setIataCode("Sicejetw2");
		airlineDetails.setIcaoCode("Sicejetw3");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Sicejetw");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejetw");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		List<ConfigurationRequestDTO> configurationRequestDTOs = configurationService.getconfigurationList(airlineDTO.getAirlineInternalId());
		Assert.assertNotNull(configurationRequestDTOs);
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void deactivateconfigurationByIdTest() throws NoSuchMessageException, BadRequestException, ObjectExistsException, ObjectNotFoundException, DuplicateEntryException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicejetw");
		airlineDetails.setWebsite("https://www.Sicejetw.com");
		airlineDetails.setCompanyName("Sicejetw");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("Sicejetw1");
		airlineDetails.setIataCode("Sicejetw2");
		airlineDetails.setIcaoCode("Sicejetw3");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Sicejetw");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejetw");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		ConfigurationRequestDTO requestDTO = configurationService.deactivateconfigurationById(airlineDTO.getAirlineInternalId(), 
				configurationRequestDTO.getConfigurationId());
		Assert.assertNotNull(requestDTO);
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@SuppressWarnings("unused")
	@Test
	public void getConfigurationByIdTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicejetw");
		airlineDetails.setWebsite("https://www.Sicejetw.com");
		airlineDetails.setCompanyName("Sicejetw");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("Sicejetw1");
		airlineDetails.setIataCode("Sicejetw2");
		airlineDetails.setIcaoCode("Sicejetw3");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Sicejetw");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejetw");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		Configuration configuration = configurationService.getConfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertNotNull(configuration);
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void updateConfigurationTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicejetw");
		airlineDetails.setWebsite("https://www.Sicejetw.com");
		airlineDetails.setCompanyName("Sicejetw");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("Sicejetw1");
		airlineDetails.setIataCode("Sicejetw2");
		airlineDetails.setIcaoCode("Sicejetw3");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Sicejetw");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejetw");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		configurationRequestDTO.setPackagingType("XCIL");
		ConfigurationRequestDTO configRequestDTO = configurationService.updateConfiguration(airlineDTO.getAirlineInternalId(), 
				configurationRequestDTO.getConfigurationId(), configurationRequestDTO);
		Assert.assertNotNull(configRequestDTO);
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}

}
 