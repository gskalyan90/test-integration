package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.PaxguiLanguageDTO;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })
/**
 * 
 * @author shuklar
 *
 */
public class PaxguiLanguageServiceTest {
	@Autowired
	private PaxguiLanguageService paxguiLanguageService;

	@Autowired
	private AirlineService airlineService;

	@Before
	public void setup() {

	}

	/**
	 * Method to test add paxguilanguage
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws DuplicateEntryException
	 */
	@Test
	public void addPaxguiLanguageTest()
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException {
		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("Sictfe7vfe6t166_");
		airlineDetails.setWebsite("https://www.airline.com");
		airlineDetails.setCodeShare("90");
		airlineDetails.setIataCode("Bsf");
		airlineDetails.setIcaoCode("AAs");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		PaxguiLanguageDTO paxguiDetails = new PaxguiLanguageDTO();
		paxguiDetails.setPaxguiLanguageName("English");
		paxguiDetails.setPaxguiLanguageCode("en");
		PaxguiLanguageDTO paxguiLanguageDTO = paxguiLanguageService.addPaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiDetails);
		Assert.assertNotNull(paxguiLanguageDTO);
		paxguiLanguageService.deletePaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiLanguageDTO.getPaxguiLanguageId());
		airlineService.deleteAirline(airlineDTO.getAirlineInternalId());

	}

	/**
	 * Method to test get paxguilanguage
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws DuplicateEntryException
	 */
	@Test
	public void getPaxguiLanguagesTest()
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException {
		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("Sictf");
		airlineDetails.setWebsite("https://www.Airline.com");
		airlineDetails.setCodeShare("90");
		airlineDetails.setIataCode("Bsf-");
		airlineDetails.setIcaoCode("AAs7P-");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		PaxguiLanguageDTO paxguiDetails = new PaxguiLanguageDTO();
		paxguiDetails.setPaxguiLanguageName("English");
		paxguiDetails.setPaxguiLanguageCode("en");
		PaxguiLanguageDTO paxguiLanguageDTO = paxguiLanguageService.addPaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiDetails);
		List<PaxguiLanguageDTO> paxguiLanguageDTOList = paxguiLanguageService
				.getPaxguiLanguages((airlineDTO.getAirlineInternalId()));
		Assert.assertTrue(paxguiLanguageDTOList.size() > 0);
		paxguiLanguageService.deletePaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiLanguageDTO.getPaxguiLanguageId());
		airlineService.deleteAirline(airlineDTO.getAirlineInternalId());
	}

	/**
	 * Method to test update paxguilanguage
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws DuplicateEntryException
	 */
	@Test
	public void updatePaxguiLanguageTest()
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException {
		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("Sict166_");
		airlineDetails.setWebsite("https://www.airline.com");
		airlineDetails.setCodeShare("9066-");
		airlineDetails.setIataCode("Bsf36-");
		airlineDetails.setIcaoCode("AAB67P-");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		PaxguiLanguageDTO paxguiDetails = new PaxguiLanguageDTO();
		paxguiDetails.setPaxguiLanguageName("English");
		paxguiDetails.setPaxguiLanguageCode("en");
		PaxguiLanguageDTO paxguiLanguageDTO = paxguiLanguageService.addPaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiDetails);
		paxguiDetails.setPaxguiLanguageName("Bulgarian");
		paxguiDetails.setPaxguiLanguageCode("bg");
		PaxguiLanguageDTO paxguiLanguage = paxguiLanguageService.updatePaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiLanguageDTO.getPaxguiLanguageId(), paxguiDetails);
		Assert.assertEquals(paxguiLanguage.getPaxguiLanguageName(), paxguiDetails.getPaxguiLanguageName());
		paxguiLanguageService.deletePaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiLanguageDTO.getPaxguiLanguageId());
		airlineService.deleteAirline(airlineDTO.getAirlineInternalId());
	}

	/**
	 * Method to test delete paxguilanguage
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws DuplicateEntryException
	 */
	@Test
	public void deletePaxguiLanguageTest()
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException {

		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("AIRLINE12");
		airlineDetails.setWebsite("https://www.airline12.com");
		airlineDetails.setCodeShare("CodeAirline1");
		airlineDetails.setIataCode("IATA_AIR1");
		airlineDetails.setIcaoCode("ICAO_AIR1");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		PaxguiLanguageDTO paxguiDetails = new PaxguiLanguageDTO();
		paxguiDetails.setPaxguiLanguageName("German");
		paxguiDetails.setPaxguiLanguageCode("de");
		PaxguiLanguageDTO paxguiLanguageDTO = paxguiLanguageService.addPaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiDetails);
		boolean result = paxguiLanguageService.deletePaxguiLanguage(airlineDTO.getAirlineInternalId(),
				paxguiLanguageDTO.getPaxguiLanguageId());
		Assert.assertEquals(result, true);
		airlineService.deleteAirline(airlineDTO.getAirlineInternalId());

	}

}
