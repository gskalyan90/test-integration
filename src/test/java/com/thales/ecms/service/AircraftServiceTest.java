package com.thales.ecms.service;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Aircraft;
import com.thales.ecms.model.dto.AircraftDTO;

import junit.framework.Assert;

/**
 * @author Sushma Service class for JUIT test cases on Aircraft
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class AircraftServiceTest {

	@Autowired
	private AircraftService aircraftService;

	@Before
	public void setup() {

	}

	/**
	 * Add aircraft
	 * 
	 * @param AircraftDTO
	 * @return AircraftDTO
	 */

	@Test
	public void addAircraftTest() throws ObjectExistsException {
		AircraftDTO aircraftDTO = new AircraftDTO();
		aircraftDTO.setName("Indigo80");
		AircraftDTO aircraft = aircraftService.addAircraft(aircraftDTO);
		Assert.assertEquals(aircraft.getName(), aircraftDTO.getName());
		Assert.assertNotNull(aircraft.getAircraftTypeInternalId());
		Aircraft aircraft1 = Query.from(Aircraft.class).where("id = ?", aircraft.getAircraftTypeInternalId()).first();
		aircraft1.delete();
	}

	/**
	 * get aircraft
	 * 
	 * @return list of AircraftDTO
	 * @throws ObjectExistsException
	 * @throws ObjectAssociationExistsException
	 */

	@Test
	public void aircraftServiceGetTest()
			throws ObjectNotFoundException, ObjectExistsException, ObjectAssociationExistsException {
		AircraftDTO aircraftDTO = new AircraftDTO();
		aircraftDTO.setName("Myaircraft");
		AircraftDTO aircraft = aircraftService.addAircraft(aircraftDTO);
		List<AircraftDTO> aircraftList = aircraftService.getAircraftTypes();
		Assert.assertNotNull(aircraftList);
		aircraftService.deleteAircraftType(aircraft.getAircraftTypeInternalId());
	}

	/**
	 * Update aircraft
	 * 
	 * @param aircraft
	 *            id and AircraftDTO
	 * @return AircraftDTO
	 */

	@Test
	public void updateAircraftTypeTest() throws ObjectNotFoundException, ObjectExistsException {
		AircraftDTO aircraftDTO = new AircraftDTO();
		aircraftDTO.setName("Indigo3");
		AircraftDTO aircraft = aircraftService.addAircraft(aircraftDTO);
		aircraftDTO.setName("Jet76");
		AircraftDTO aircraftTypeDTO = aircraftService.updateAircraftType(aircraft.getAircraftTypeInternalId(),
				aircraftDTO);
		Assert.assertEquals(aircraftTypeDTO.getName(), aircraftDTO.getName());
		Aircraft aircraft1 = Query.from(Aircraft.class).where("id = ?", aircraft.getAircraftTypeInternalId()).first();
		aircraft1.delete();

	}

	/**
	 * Update aircraft
	 * 
	 * @param Exist
	 *            aircraft id
	 * @param AircraftDTO
	 * @return ObjectNotFoundException
	 */

	@Test(expected = ObjectNotFoundException.class)
	public void updateAircraftTypeExceptionTest() throws ObjectNotFoundException, ObjectExistsException {
		AircraftDTO aircraftDTO = new AircraftDTO();
		aircraftDTO.setName("Spicejet");
		aircraftService.updateAircraftType("aircraftTypeId", aircraftDTO);
	}

	/**
	 * Delete aircraft
	 * 
	 * @param aircraft
	 *            id
	 * @return true
	 */
	@Test
	public void deleteAircraftTypeTest()
			throws ObjectNotFoundException, ObjectAssociationExistsException, ObjectExistsException {
		AircraftDTO aircraftDTO = new AircraftDTO();
		aircraftDTO.setName("Indigo75");
		AircraftDTO aircraft = aircraftService.addAircraft(aircraftDTO);
		boolean b = aircraftService.deleteAircraftType(aircraft.getAircraftTypeInternalId());
		Assert.assertTrue(b);
	}

	/**
	 * Delete aircraft for Throw exception
	 * 
	 * @param aircraft
	 *            id
	 * @return ObjectNotFoundException
	 */
	@Test(expected = ObjectNotFoundException.class)
	public void deleteAircraftTypeExceptionTest()
			throws ObjectNotFoundException, ObjectAssociationExistsException, ObjectExistsException {
		boolean b = aircraftService.deleteAircraftType("id");
		Assert.assertTrue(b);
	}

}
