package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Lru;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.LruDTO;
import com.thales.ecms.model.dto.LruTypeDTO;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class LruServiceTest {


@Autowired
private AirlineService airlineService;
@Autowired
private LruService lruService;


@Autowired
private LruTypeService lruTypeService;

	@Before
	public void setup() {
		
	}
	
@Test
public void addLRUTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicedhjtdfrgfhgetwer1yy");
	airlineDetails.setWebsite("https://www.agorhtghdgywerntu.com");
	airlineDetails.setCompanyName("aSicertjgheghfdgdt1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("690rtgh3fgyhg689");
	airlineDetails.setIataCode("hBrtdfghgghBj");
	airlineDetails.setIcaoCode("yArtAdfhghgBui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("asrtdfhfghgad11");
	//lruTypeDetails.setMaxSpaceAllowance(9786.0);

	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	
	LruDTO lruDetails=new LruDTO();
	lruDetails.setLruName("asadrrdgtgfghht511");
	lruDetails.setLruTypeName("asrtdggfhhrtad161");
	lruDetails.setLruTypeInternalId(lruTypeDTO.getLruTypeInternalId());

	

	LruDTO lruDTO=lruService.addLRU(airlineDTO.getAirlineInternalId(), lruDetails);
    Assert.assertEquals(lruDTO.getLruName(), lruDetails.getLruName());
   // Assert.assertEquals(lruDTO.getLruTypeName(), lruDetails.getLruTypeName());
    Assert.assertNotNull(lruDTO.getLruInternalId());
    String lruTypeInternalId = lruDTO.getLruTypeInternalId();
	Lru lru = Query.from(Lru.class).where("id = ?", lruDTO.getLruInternalId()).and("cms.site.owner = ?",airlineDTO.getAirlineInternalId() ).first();
	lru.delete();

	LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeInternalId).first();
	lruType.delete();
	
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
 }


@Test
public void getLRUsTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtrdgetwer1yy");
	airlineDetails.setWebsite("https://www.agdgrtywerntu.com");
	airlineDetails.setCompanyName("aSicertjet1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("690rt3dg689");
	airlineDetails.setIataCode("hBrtdgBj");
	airlineDetails.setIcaoCode("yArtdgABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("asrtdgad11");
	//lruTypeDetails.setMaxSpaceAllowance(9786.0);

	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	
	LruDTO lruDetails=new LruDTO();
	lruDetails.setLruName("asaddgrrtt511");
	lruDetails.setLruTypeName("asrtdgrtad161");
	lruDetails.setLruTypeInternalId(lruTypeDTO.getLruTypeInternalId());
	LruDTO lruDTO=lruService.addLRU(airlineDTO.getAirlineInternalId(), lruDetails);
	
   
	List<LruDTO> lruDTOList=lruService.getLRUs(airlineDTO.getAirlineInternalId());
    Assert.assertTrue(lruDTOList.size()>0);
 
	for(LruDTO dto : lruDTOList){
		String lruTypeInternalId = dto.getLruTypeInternalId();
		Lru lru = Query.from(Lru.class).where("id = ?", dto.getLruInternalId()).and("cms.site.owner = ?",airlineDTO.getAirlineInternalId() ).first();
		lru.delete();

		LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeInternalId).first();
		lruType.delete();
	}
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
   
}
@Test
public void getLRUTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtdgretwer1yy");
	airlineDetails.setWebsite("https://www.agodgrtywerntu.com");
	airlineDetails.setCompanyName("aSicertjedgt1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("690rt36dg89");
	airlineDetails.setIataCode("hBrtBdgj");
	airlineDetails.setIcaoCode("yArtAdgBui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("asrdgtad11");
	//lruTypeDetails.setMaxSpaceAllowance(9786.0);

	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	
	LruDTO lruDetails=new LruDTO();
	lruDetails.setLruName("asadrrtdgt511");
	lruDetails.setLruTypeName("asrdgtrtad161");
	lruDetails.setLruTypeInternalId(lruTypeDTO.getLruTypeInternalId());
	
	LruDTO lruDTO=lruService.addLRU(airlineDTO.getAirlineInternalId(), lruDetails);
	LruDTO lruDTO1=lruService.getLRU(airlineDTO.getAirlineInternalId(), lruDTO.getLruInternalId());
	Assert.assertNotNull(lruDTO1);
	String lruTypeInternalId = lruDTO1.getLruTypeInternalId();
	Lru lru = Query.from(Lru.class).where("id = ?", lruDTO1.getLruInternalId()).and("cms.site.owner = ?",airlineDTO.getAirlineInternalId() ).first();
		lru.delete();

		LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeInternalId).first();
		
		
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		
		lru.delete();
		lruType.delete();
		airline.delete();
		
}

@Test
public void updateLRUTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtfgfretwefhr1yy");
	airlineDetails.setWebsite("https://www.agogrtgywerntu.com");
	airlineDetails.setCompanyName("aSicertjdfgget1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("690rtdffg3689");
	airlineDetails.setIataCode("hBrdffgftBj");
	airlineDetails.setIcaoCode("yArtgdfABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("asrtdffgfgad11");
	//lruTypeDetails.setMaxSpaceAllowance(9786.0);

	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	
	LruDTO lruDetails=new LruDTO();
	lruDetails.setLruName("asadrrttddfgdff511");
	lruDetails.setLruTypeName("asrtrdfdftad161");
	lruDetails.setLruTypeInternalId(lruTypeDTO.getLruTypeInternalId());
	
	LruDTO lruDTO=lruService.addLRU(airlineDTO.getAirlineInternalId(), lruDetails);
	LruDTO lruDTO1=lruService.getLRU(airlineDTO.getAirlineInternalId(),lruDTO.getLruInternalId());
	lruDTO1.setLruName("EDFJHSedfhdwrfewdgDdfdfGFJKDSH");
	LruDTO LruDTO2=lruService.updateLRU(airlineDTO.getAirlineInternalId(), lruDTO1.getLruInternalId(), lruDTO1);
	Assert.assertEquals(lruDTO.getLruName(), lruDetails.getLruName());
  //  Assert.assertEquals(lruDTO.getLruTypeName(), lruDetails.getLruTypeName());
    Assert.assertNotNull(lruDTO.getLruInternalId());
    
    String lruTypeInternalId = LruDTO2.getLruTypeInternalId();
    Lru lru = Query.from(Lru.class).where("id = ?", LruDTO2.getLruInternalId()).and("cms.site.owner = ?",airlineDTO.getAirlineInternalId() ).first();
	lru.delete();

	LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeInternalId).first();
	lruType.delete();
	
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
   
 }

@Test
public void deleteLRUTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtrefrtgdtgtdfdwer1yy");
	airlineDetails.setWebsite("https://www.agfordfgdgtywerntu.com");
	airlineDetails.setCompanyName("aSicerdftdgdgjet1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("yy690rdftddfdf3689");
	airlineDetails.setIataCode("hBrfghdfdftBj");
	airlineDetails.setIcaoCode("yArtdfgdfdffABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("asrftfgfgad11");
	//lruTypeDetails.setMaxSpaceAllowance(9786.0);

	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	
	LruDTO lruDetails=new LruDTO();
	lruDetails.setLruName("asadrrtdfgfgdft511");
	lruDetails.setLruTypeName("asrtrdfgfgftad161");
	lruDetails.setLruTypeInternalId(lruTypeDTO.getLruTypeInternalId());
	
	LruDTO lruDTO=lruService.addLRU(airlineDTO.getAirlineInternalId(), lruDetails);
	String lruTypeInternalId = lruDTO.getLruTypeInternalId();
	Boolean result=lruService.deleteLRU(airlineDTO.getAirlineInternalId(), lruDTO.getLruInternalId());
	Assert.assertTrue(result);
  
   	LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeInternalId).first();
   	lruType.delete();
   	
   	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
   	airline.delete();
      
}


}
