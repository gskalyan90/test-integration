package com.thales.ecms.service;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.dto.AirportDTO;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class AirportServiceTest {


	

@Autowired
private AirportService airportService;

@Before
public void setup() {
		
}

/**
 * Add Airport
 * @param AirportDTO
 * @return AirportDTO
 * @throws ObjectNotFoundException 
 * @throws BadRequestException 
 */	
@Test
public void addAirportTest() throws ObjectExistsException, BadRequestException, DuplicateEntryException{

	AirportDTO airportDetails=new AirportDTO();
	airportDetails.setAirportName("KJHG Airport");
	airportDetails.setAirportICAO("JUY");
	airportDetails.setAirportIATA("KJH");
	AirportDTO airportDTO=	airportService.addAirport(airportDetails);
	Assert.assertEquals(airportDTO.getAirportIATA(), airportDetails.getAirportIATA());
	Assert.assertEquals(airportDTO.getAirportICAO(), airportDetails.getAirportICAO());
	Assert.assertEquals(airportDTO.getAirportName(), airportDetails.getAirportName());
	Assert.assertNotNull(airportDTO.getAirportId());
	Airport airport = Query.from(Airport.class).where("id = ?",airportDTO.getAirportId()).first();
	airport.delete();
	}



/**
 * get Airports  
 * 
 * @return list of Airport
 * @throws BadRequestException 
 * @throws ObjectExistsException 
*/	

@Test
public void getAirportListTest() throws ObjectExistsException, BadRequestException{

	AirportDTO airportDetail=new AirportDTO();
	airportDetail.setAirportName("Saychells Airport");
	airportDetail.setAirportICAO("SAH");
	airportDetail.setAirportIATA("JHG");
	AirportDTO airportDTO=	airportService.addAirport(airportDetail);
	List<AirportDTO> airportDTOList = airportService.getAirport();
	Assert.assertNotNull(airportDTOList);
	Assert.assertTrue(airportDTOList.size()>0);
	Airport airport = Query.from(Airport.class).where("id = ?",airportDTO.getAirportId()).first();
	airport.delete();
	
	
}

/**
 * Update Airport
 * 
 * @param Airport id and AirportDTO
 * @return AirportDTO
*/	

@Test
public void updateAirportTest() throws BadRequestException, ObjectExistsException, ObjectNotFoundException{
	
    AirportDTO airportDetails=new AirportDTO();
	airportDetails.setAirportName("Delh Arport");
	airportDetails.setAirportICAO("DLL");
	airportDetails.setAirportIATA("DLK");
	AirportDTO airportDTO=	airportService.addAirport(airportDetails);
	airportDetails.setAirportName("LKO Airport");
	airportDetails.setAirportICAO("DLH");
	airportDetails.setAirportIATA("DLB");
	
    AirportDTO airportDTO1 =airportService.updateAirport(airportDTO.getAirportId(), airportDetails);
	Assert.assertEquals(airportDTO1.getAirportName(),airportDetails.getAirportName());
	
	Airport airport = Query.from(Airport.class).where("id = ?",airportDTO.getAirportId()).first();
	airport.delete();
	
	

}

/**
 * delete Airport
 * 
 * @throws ObjectNotFoundException
 * @throws BadRequestException
 * @throws ObjectExistsException
 */

@Test
public void deleteAirportTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, ObjectNotFoundException{
	AirportDTO airportDetail=new AirportDTO();
	airportDetail.setAirportName("AHM Airport");
	airportDetail.setAirportICAO("AHM");
	airportDetail.setAirportIATA("ABD");
	AirportDTO airportDTO=	airportService.addAirport(airportDetail);
	boolean result=airportService.deleteAirport(airportDTO.getAirportId());
	Assert.assertTrue(result);
	
	

}


/**
 * method for delete route exception
 * 
 * @throws ObjectNotFoundException
 */
@Test(expected = ObjectNotFoundException.class)
public void deleteAirportExceptionTest() throws ObjectNotFoundException {
	
	boolean result=airportService.deleteAirport("Airport ID");
	Assert.assertTrue(result);
}


/**
 * get AirportById 
 * @return  AirportDTO
*/

@Test
public void getAirportByIdTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException{
	
	AirportDTO airportDetail=new AirportDTO();
	airportDetail.setAirportName("Haryana Airport");
	airportDetail.setAirportICAO("UYT");
	airportDetail.setAirportIATA("DFG");
	AirportDTO airportDTO=	airportService.addAirport(airportDetail);
    AirportDTO airportDTO1 =airportService.getAirportById(airportDTO.getAirportId());
    Assert.assertNotNull(airportDTO1);
	Airport airport = Query.from(Airport.class).where("id = ?",airportDTO.getAirportId()).first();
	airport.delete();
	
	
}


}
