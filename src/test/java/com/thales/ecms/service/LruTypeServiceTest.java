package com.thales.ecms.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Aircraft;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.dto.AircraftDTO;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.LruTypeDTO;
import com.thales.ecms.service.LruTypeService;

/**
 * @author Ashish
 * Service class for JUIT test cases on Aircraft
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class LruTypeServiceTest {
	
	
	
	@Autowired
	private LruTypeService lruTypeService;
	
	@Before
	public void setup() {
		
	}
	
	/**
	 * Add LRUType
	 * @param LruTypeDTO
	 * @return LruTypeDTO
	 * @throws ObjectNotFoundException 
	 * @throws BadRequestException 
     */	
	
@Test
public void addLRUTypeTest() throws ObjectExistsException, BadRequestException, ObjectNotFoundException{

	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("ICMTOIOY");
//	lruTypeDetails.setMaxSpaceAllowance(98.0);
	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
    Assert.assertEquals(lruTypeDTO.getLruTypeName(),lruTypeDetails.getLruTypeName());
//    Assert.assertEquals(lruTypeDTO.getMaxSpaceAllowance(),lruTypeDetails.getMaxSpaceAllowance());
	Assert.assertNotNull(lruTypeDTO.getLruTypeInternalId());
	LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeDTO.getLruTypeInternalId()).first();
	lruType.delete();
}


/**
 * get LRUTypes  
 * 
 * @return list of LruTypeDTO
 * @throws BadRequestException 
 * @throws ObjectExistsException 
*/	
@Test
public void getLRUTypesTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException{
	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("ICMTOIOY");
//	lruTypeDetails.setMaxSpaceAllowance(98.0);
	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	List<LruTypeDTO> lruTypeDTOList=lruTypeService.getLRUTypes();
    Assert.assertTrue(lruTypeDTOList.size()>0);
    LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeDTO.getLruTypeInternalId()).first();
	lruType.delete();
}

/**
 * get LRUTypesById  
 * @return  LruTypeDTO
*/

@Test
public void getLRUTypeByIdTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException{
	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("ICMTOIO");
//	lruTypeDetails.setMaxSpaceAllowance(98.0);
	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	LruTypeDTO lruTypeDTO1 =lruTypeService.getLRUTypeById(lruTypeDTO.getLruTypeInternalId());
	Assert.assertNotNull(lruTypeDTO1);
	LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeDTO.getLruTypeInternalId()).first();
	lruType.delete();


	
	
	
}

/**
	 * Update LRUType 
	 * 
	 * @param LRUType id and LruTypeDTO
	 * @return LruTypeDTO
 */	

@Test
public void updateLRUTypeTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException{
	
	
	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("ICMTOIOpOP");
//	lruTypeDetails.setMaxSpaceAllowance(98.0);
	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	lruTypeDetails.setLruTypeName("MIUTRFVBNIU");
//	lruTypeDetails.setMaxSpaceAllowance(920703.0);
	LruTypeDTO lruTypeDTO1 = lruTypeService.updateLRUType(lruTypeDTO.getLruTypeInternalId(), lruTypeDetails);
    Assert.assertEquals(lruTypeDTO1.getLruTypeName(),lruTypeDetails.getLruTypeName());
//    Assert.assertEquals(lruTypeDTO1.getMaxSpaceAllowance(),lruTypeDetails.getMaxSpaceAllowance());
	LruType lruType = Query.from(LruType.class).where("id = ?",lruTypeDTO1.getLruTypeInternalId()).first();
	lruType.delete();
	
}



/**
 * delete LRUType
 * 
 * @throws ObjectNotFoundException
 * @throws BadRequestException
 * @throws ObjectExistsException
 */

@Test
public void deleteLRUTypeByIdTest() 
		throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
	LruTypeDTO lruTypeDetails=new LruTypeDTO();
	lruTypeDetails.setLruTypeName("ICMTOIOpyuRTER");
//	lruTypeDetails.setMaxSpaceAllowance(98.0);
	LruTypeDTO lruTypeDTO=lruTypeService.addLRUType(lruTypeDetails);
	boolean result=lruTypeService.deleteLRUType(lruTypeDTO.getLruTypeInternalId());
    Assert.assertTrue(result);

}
/**
 * delete LRUType for thrown exception
 * 
 * @throws ObjectNotFoundException
 * @throws ObjectAssociationExistsException
 * @throws ObjectExistsException
 * @throws BadRequestException 
 */
@Test(expected = ObjectNotFoundException.class)
public void deleteLRUTypeExceptionTest()
		throws ObjectNotFoundException, ObjectAssociationExistsException, ObjectExistsException, NoSuchMessageException, BadRequestException {
	boolean result = lruTypeService.deleteLRUType("id");
	Assert.assertTrue(result);
	
}




}
