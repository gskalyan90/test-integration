package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.Bundle;
import com.thales.ecms.model.BundleForRouteGroup;
import com.thales.ecms.model.Route;
import com.thales.ecms.model.RouteGroup;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.model.dto.BundleDTO;
import com.thales.ecms.model.dto.BundleDTO.SeatingClassPriceMappingDTO;
import com.thales.ecms.model.dto.BundleForRouteGroupDTO;
import com.thales.ecms.model.dto.LopaDTO;
import com.thales.ecms.model.dto.RouteDTO;
import com.thales.ecms.model.dto.RouteGroupDTO;
import com.thales.ecms.model.dto.SeatingClassDTO;
import com.thales.ecms.service.RouteGroupService;

/**
 * 
 * @author ashish
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class BundleForRouteGroupServiceTest {

	@Autowired
	private RouteService routeService;

	@Autowired
	private AirportService airportService;

	@Autowired
	private RouteGroupService routeGroupService;

	@Autowired
	private AirlineService airlineService;

	@Autowired
	private BundleForRouteGroupService bundleForRouteGroupService;

	@Autowired
	private BundleService bundleService;

	@Autowired
	private SeatingClassService seatingClassService;

	@Before
	public void setup() {

	}

	/**
	 * Method for add BundleForRouteGrouptest
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test
	public void addBundleForRouteGroupServiceTest()
			throws ObjectExistsException, BadRequestException, ObjectNotFoundException, DuplicateEntryException {

		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("fSicejddfdfdet1yy");
		airlineDetails.setWebsite("https://www.dggofgdffyntu.com");
		airlineDetails.setCompanyName("Sicejefdft1dfui");
		// airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("903df6f8f9");
		airlineDetails.setIataCode("BBdfdffj");
		airlineDetails.setIcaoCode("AABfddfdffui");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);

		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("AFGdffYTFfgffgfdfghdfGtH7532");
		origin.setAirportICAO("A123t1FGYfgfhfdfddfgTf5H52");
		origin.setAirportName("AFKJHK7FG5fhTddfdffgffgH312");
		AirportDTO originAirport = airportService.addAirport(origin);

		AirportDTO destination = new AirportDTO();
		destination.setAirportIATA("DLIT2fdfgdfdfhFfgFGH");
		destination.setAirportICAO("DLYHUfhdffdfFGfgTG3H12");
		destination.setAirportName("De1FDGfhdfffdfgT2H");
		AirportDTO destinationAirport = airportService.addAirport(destination);

		RouteDTO routeDTO = new RouteDTO();
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("AtHFfgfhdfgdffG531-DfddfffgfhgIGHT312");
		// routeDTO.setRouteId(routeId);
		RouteDTO route = routeService.addRoute(routeDTO);

		Set<RouteDTO> routesDTO = new HashSet<RouteDTO>();
		routesDTO.add(route);
		RouteGroupDTO routeGroupDetails = new RouteGroupDTO();
		routeGroupDetails.setRouteGroupName(route.getName());
		// routeGroupDetails.setRoutes(routes);
		routeGroupDetails.setRoutes(routesDTO);
		RouteGroupDTO routeGroupDTO = routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(),
				routeGroupDetails);

		SeatingClassDTO classDetails = new SeatingClassDTO();
		classDetails.setName("Busidsess23xdsfdfdfddd4f15371");
		SeatingClassDTO seatingClassDTO = seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails);

		/*
		 * BundleForRouteGroupDTO bundleForRouteGroupDetails =new
		 * BundleForRouteGroupDTO;
		 * bundleForRouteGroupDetails.setBundleId(bundleId);
		 * bundleForRouteGroupDetails.setPpaFee(ppaFee);
		 * bundleForRouteGroupDetails.setPpvFee(ppvFee);
		 * bundleForRouteGroupDetails.setBundleForRouteGroupId(
		 * bundleForRouteGroupId);
		 * bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.
		 * getRouteGroupId());
		 * bundleForRouteGroupDetails.setSeatingClassId(seatingClassId);
		 */

		BundleDTO bundleDetails = new BundleDTO();
		bundleDetails.setBundleName("First1");

		Long creationDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setCreationDate(creationDate);

		Long lastModifiedDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setLastModifiedDate(lastModifiedDate);

		Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings = new HashSet<>();
		bundleDetails.setSeatingClassPriceMappings(seatingClassPriceMappings);

		BundleDTO bundleDTO = bundleService.addBundle(airlineDTO.getAirlineInternalId(), bundleDetails);

		BundleForRouteGroupDTO bundleForRouteGroupDetails = new BundleForRouteGroupDTO();
		bundleForRouteGroupDetails.setBundleId(bundleDTO.getBundleId());
		bundleForRouteGroupDetails.setPpaFee(300.0);
		bundleForRouteGroupDetails.setPpvFee(500.0);
		bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.getRouteGroupId());
		bundleForRouteGroupDetails.setSeatingClassId(seatingClassDTO.getSeatingClassInternalId());

		BundleForRouteGroupDTO bundleForRouteGroupDTO = bundleForRouteGroupService
				.addBundleForRouteGroup(airlineDTO.getAirlineInternalId(), bundleForRouteGroupDetails);

		Assert.assertEquals(routeGroupDTO.getRouteGroupName(), routeGroupDetails.getRouteGroupName());
		Assert.assertNotNull(routeGroupDTO.getRouteGroupId());
		// Assert.assertNotNull(route.getRouteId());

		Query.from(BundleForRouteGroup.class).where("id = ?", bundleForRouteGroupDTO.getBundleForRouteGroupId()).first()
				.delete();
		Query.from(Bundle.class).where("id = ?", bundleDTO.getBundleId()).first().delete();
		Query.from(SeatingClass.class).where("id = ?", seatingClassDTO.getSeatingClassInternalId()).first().delete();
		Query.from(RouteGroup.class).where("id = ?", routeGroupDTO.getRouteGroupId()).first().delete();
		Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
		Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
		Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();

		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();

	}

	@Test
	public void getBundleForRouteGroupTest()
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException {
		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("fSicejddfdfdet1yy");
		airlineDetails.setWebsite("https://www.dggofgdffyntu.com");
		airlineDetails.setCompanyName("Sicejefdft1dfui");
		// airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("903df6f8f9");
		airlineDetails.setIataCode("BBdfdffj");
		airlineDetails.setIcaoCode("AABfddfdffui");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);

		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("AFGdffYTFfgffgfdfghdfGtH7532");
		origin.setAirportICAO("A123t1FGYfgfhfdfddfgTf5H52");
		origin.setAirportName("AFKJHK7FG5fhTddfdffgffgH312");
		AirportDTO originAirport = airportService.addAirport(origin);

		AirportDTO destination = new AirportDTO();
		destination.setAirportIATA("DLIT2fdfgdfdfhFfgFGH");
		destination.setAirportICAO("DLYHUfhdffdfFGfgTG3H12");
		destination.setAirportName("De1FDGfhdfffdfgT2H");
		AirportDTO destinationAirport = airportService.addAirport(destination);

		RouteDTO routeDTO = new RouteDTO();
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("AtHFfgfhdfgdffG531-DfddfffgfhgIGHT312");
		// routeDTO.setRouteId(routeId);
		RouteDTO route = routeService.addRoute(routeDTO);

		Set<RouteDTO> routesDTO = new HashSet<RouteDTO>();
		routesDTO.add(route);
		RouteGroupDTO routeGroupDetails = new RouteGroupDTO();
		routeGroupDetails.setRouteGroupName(route.getName());
		// routeGroupDetails.setRoutes(routes);
		routeGroupDetails.setRoutes(routesDTO);
		RouteGroupDTO routeGroupDTO = routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(),
				routeGroupDetails);

		SeatingClassDTO classDetails = new SeatingClassDTO();
		classDetails.setName("Busidsess23xdsfdfdfddd4f15371");
		SeatingClassDTO seatingClassDTO = seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails);

		/*
		 * BundleForRouteGroupDTO bundleForRouteGroupDetails =new
		 * BundleForRouteGroupDTO;
		 * bundleForRouteGroupDetails.setBundleId(bundleId);
		 * bundleForRouteGroupDetails.setPpaFee(ppaFee);
		 * bundleForRouteGroupDetails.setPpvFee(ppvFee);
		 * bundleForRouteGroupDetails.setBundleForRouteGroupId(
		 * bundleForRouteGroupId);
		 * bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.
		 * getRouteGroupId());
		 * bundleForRouteGroupDetails.setSeatingClassId(seatingClassId);
		 */

		BundleDTO bundleDetails = new BundleDTO();
		bundleDetails.setBundleName("First1");

		Long creationDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setCreationDate(creationDate);

		Long lastModifiedDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setLastModifiedDate(lastModifiedDate);

		Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings = new HashSet<>();
		bundleDetails.setSeatingClassPriceMappings(seatingClassPriceMappings);

		BundleDTO bundleDTO = bundleService.addBundle(airlineDTO.getAirlineInternalId(), bundleDetails);

		BundleForRouteGroupDTO bundleForRouteGroupDetails = new BundleForRouteGroupDTO();
		bundleForRouteGroupDetails.setBundleId(bundleDTO.getBundleId());
		bundleForRouteGroupDetails.setPpaFee(300.0);
		bundleForRouteGroupDetails.setPpvFee(500.0);
		bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.getRouteGroupId());
		bundleForRouteGroupDetails.setSeatingClassId(seatingClassDTO.getSeatingClassInternalId());

		BundleForRouteGroupDTO bundleForRouteGroupDTO = bundleForRouteGroupService
				.addBundleForRouteGroup(airlineDTO.getAirlineInternalId(), bundleForRouteGroupDetails);

		// List<RouteDTO> routeDTOList = routeService.getRoutes();

		List<BundleForRouteGroupDTO> bundleForRouteGroupDTOList = bundleForRouteGroupService
				.getAll(airlineDTO.getAirlineInternalId());

		/*
		 * for(BundleForRouteGroup bundleForRouteGroupDTOList :
		 * bundleForRouteGroupDTOLists){
		 * list.add(populateBundleForRouteGroupDTO(bundleForRouteGroupDTOList));
		 * }
		 */

		Assert.assertTrue(bundleForRouteGroupDTOList.size() > 0);

		Query.from(BundleForRouteGroup.class).where("id = ?", bundleForRouteGroupDTO.getBundleForRouteGroupId()).first()
				.delete();
		Query.from(Bundle.class).where("id = ?", bundleDTO.getBundleId()).first().delete();
		Query.from(SeatingClass.class).where("id = ?", seatingClassDTO.getSeatingClassInternalId()).first().delete();
		Query.from(RouteGroup.class).where("id = ?", routeGroupDTO.getRouteGroupId()).first().delete();
		Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
		Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
		Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();

		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();

	}

	@Test
	public void getBundleForRouteGroupByIdTest()
			throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException {
		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("fSicejddfdfdet1yy");
		airlineDetails.setWebsite("https://www.dggofgdffyntu.com");
		airlineDetails.setCompanyName("Sicejefdft1dfui");
		// airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("903df6f8f9");
		airlineDetails.setIataCode("BBdfdffj");
		airlineDetails.setIcaoCode("AABfddfdffui");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);

		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("AFGdffYTFfgffgfdfghdfGtH7532");
		origin.setAirportICAO("A123t1FGYfgfhfdfddfgTf5H52");
		origin.setAirportName("AFKJHK7FG5fhTddfdffgffgH312");
		AirportDTO originAirport = airportService.addAirport(origin);

		AirportDTO destination = new AirportDTO();
		destination.setAirportIATA("DLIT2fdfgdfdfhFfgFGH");
		destination.setAirportICAO("DLYHUfhdffdfFGfgTG3H12");
		destination.setAirportName("De1FDGfhdfffdfgT2H");
		AirportDTO destinationAirport = airportService.addAirport(destination);

		RouteDTO routeDTO = new RouteDTO();
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("AtHFfgfhdfgdffG531-DfddfffgfhgIGHT312");
		// routeDTO.setRouteId(routeId);
		RouteDTO route = routeService.addRoute(routeDTO);

		Set<RouteDTO> routesDTO = new HashSet<RouteDTO>();
		routesDTO.add(route);
		RouteGroupDTO routeGroupDetails = new RouteGroupDTO();
		routeGroupDetails.setRouteGroupName(route.getName());
		// routeGroupDetails.setRoutes(routes);
		routeGroupDetails.setRoutes(routesDTO);
		RouteGroupDTO routeGroupDTO = routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(),
				routeGroupDetails);

		SeatingClassDTO classDetails = new SeatingClassDTO();
		classDetails.setName("Busidsess23xdsfdfdfddd4f15371");
		SeatingClassDTO seatingClassDTO = seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails);

		/*
		 * BundleForRouteGroupDTO bundleForRouteGroupDetails =new
		 * BundleForRouteGroupDTO;
		 * bundleForRouteGroupDetails.setBundleId(bundleId);
		 * bundleForRouteGroupDetails.setPpaFee(ppaFee);
		 * bundleForRouteGroupDetails.setPpvFee(ppvFee);
		 * bundleForRouteGroupDetails.setBundleForRouteGroupId(
		 * bundleForRouteGroupId);
		 * bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.
		 * getRouteGroupId());
		 * bundleForRouteGroupDetails.setSeatingClassId(seatingClassId);
		 */

		BundleDTO bundleDetails = new BundleDTO();
		bundleDetails.setBundleName("First1");

		Long creationDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setCreationDate(creationDate);

		Long lastModifiedDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setLastModifiedDate(lastModifiedDate);

		Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings = new HashSet<>();
		bundleDetails.setSeatingClassPriceMappings(seatingClassPriceMappings);

		BundleDTO bundleDTO = bundleService.addBundle(airlineDTO.getAirlineInternalId(), bundleDetails);

		BundleForRouteGroupDTO bundleForRouteGroupDetails = new BundleForRouteGroupDTO();
		bundleForRouteGroupDetails.setBundleId(bundleDTO.getBundleId());
		bundleForRouteGroupDetails.setPpaFee(300.0);
		bundleForRouteGroupDetails.setPpvFee(500.0);
		bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.getRouteGroupId());
		bundleForRouteGroupDetails.setSeatingClassId(seatingClassDTO.getSeatingClassInternalId());

		// List<BundleForRouteGroupDTO> bundleForRouteGroupDTOList
		// =bundleForRouteGroupService.getAll(airlineDTO.getAirlineInternalId());

		BundleForRouteGroupDTO bundleForRouteGroupDTO = bundleForRouteGroupService
				.addBundleForRouteGroup(airlineDTO.getAirlineInternalId(), bundleForRouteGroupDetails);
		BundleForRouteGroupDTO bundleForRouteGroupDTO1 = bundleForRouteGroupService.getBundleForRouteGroupById(
				airlineDTO.getAirlineInternalId(), bundleForRouteGroupDTO.getBundleForRouteGroupId());
		Assert.assertNotNull(bundleForRouteGroupDTO1.getBundleForRouteGroupId());
		Query.from(BundleForRouteGroup.class).where("id = ?", bundleForRouteGroupDTO.getBundleForRouteGroupId()).first()
				.delete();
		Query.from(Bundle.class).where("id = ?", bundleDTO.getBundleId()).first().delete();
		Query.from(SeatingClass.class).where("id = ?", seatingClassDTO.getSeatingClassInternalId()).first().delete();
		Query.from(RouteGroup.class).where("id = ?", routeGroupDTO.getRouteGroupId()).first().delete();
		Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
		Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
		Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();

		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}

	@Test
	public void updateRouteGroupTest()
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException {
		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("fSicejddfdfdet1yy");
		airlineDetails.setWebsite("https://www.dggofgdffyntu.com");
		airlineDetails.setCompanyName("Sicejefdft1dfui");
		// airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("903df6f8f9");
		airlineDetails.setIataCode("BBdfdffj");
		airlineDetails.setIcaoCode("AABfddfdffui");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);

		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("AFGdffYTFfgffgfdfghdfGtH7532");
		origin.setAirportICAO("A123t1FGYfgfhfdfddfgTf5H52");
		origin.setAirportName("AFKJHK7FG5fhTddfdffgffgH312");
		AirportDTO originAirport = airportService.addAirport(origin);

		AirportDTO destination = new AirportDTO();
		destination.setAirportIATA("DLIT2fdfgdfdfhFfgFGH");
		destination.setAirportICAO("DLYHUfhdffdfFGfgTG3H12");
		destination.setAirportName("De1FDGfhdfffdfgT2H");
		AirportDTO destinationAirport = airportService.addAirport(destination);

		RouteDTO routeDTO = new RouteDTO();
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("AtHFfgfhdfgdffG531-DfddfffgfhgIGHT312");
		// routeDTO.setRouteId(routeId);
		RouteDTO route = routeService.addRoute(routeDTO);

		Set<RouteDTO> routesDTO = new HashSet<RouteDTO>();
		routesDTO.add(route);
		RouteGroupDTO routeGroupDetails = new RouteGroupDTO();
		routeGroupDetails.setRouteGroupName(route.getName());
		// routeGroupDetails.setRoutes(routes);
		routeGroupDetails.setRoutes(routesDTO);
		RouteGroupDTO routeGroupDTO = routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(),
				routeGroupDetails);

		SeatingClassDTO classDetails = new SeatingClassDTO();
		classDetails.setName("Busidsess23xdsfdfdfddd4f15371");
		SeatingClassDTO seatingClassDTO = seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails);

		/*
		 * BundleForRouteGroupDTO bundleForRouteGroupDetails =new
		 * BundleForRouteGroupDTO;
		 * bundleForRouteGroupDetails.setBundleId(bundleId);
		 * bundleForRouteGroupDetails.setPpaFee(ppaFee);
		 * bundleForRouteGroupDetails.setPpvFee(ppvFee);
		 * bundleForRouteGroupDetails.setBundleForRouteGroupId(
		 * bundleForRouteGroupId);
		 * bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.
		 * getRouteGroupId());
		 * bundleForRouteGroupDetails.setSeatingClassId(seatingClassId);
		 */

		BundleDTO bundleDetails = new BundleDTO();
		bundleDetails.setBundleName("First1");

		Long creationDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setCreationDate(creationDate);

		Long lastModifiedDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setLastModifiedDate(lastModifiedDate);

		Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings = new HashSet<>();
		bundleDetails.setSeatingClassPriceMappings(seatingClassPriceMappings);

		BundleDTO bundleDTO = bundleService.addBundle(airlineDTO.getAirlineInternalId(), bundleDetails);

		BundleForRouteGroupDTO bundleForRouteGroupDetails = new BundleForRouteGroupDTO();
		bundleForRouteGroupDetails.setBundleId(bundleDTO.getBundleId());
		bundleForRouteGroupDetails.setPpaFee(300.0);
		bundleForRouteGroupDetails.setPpvFee(500.0);
		bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.getRouteGroupId());
		bundleForRouteGroupDetails.setSeatingClassId(seatingClassDTO.getSeatingClassInternalId());

		// List<BundleForRouteGroupDTO> bundleForRouteGroupDTOList
		// =bundleForRouteGroupService.getAll(airlineDTO.getAirlineInternalId());

		BundleForRouteGroupDTO bundleForRouteGroupDTO = bundleForRouteGroupService
				.addBundleForRouteGroup(airlineDTO.getAirlineInternalId(), bundleForRouteGroupDetails);

		// AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("AFGdffYTFfgfffghjfgfdfghdfGtH7532");
		origin.setAirportICAO("A123t1FGYfgfhfdfghfghffddfgTf5H52");
		origin.setAirportName("AFKJHK7FG5fhTddgfhfhfdffgffgH312");
		// AirportDTO originAirport = airportService.addAirport(origin);

		// AirportDTO destination = new AirportDTO();
		destination.setAirportIATA("DLIT2fhdfdfgdfdfhFfgFGH");
		destination.setAirportICAO("DLYHUfdfhdhhdffdfFGfgTG3H12");
		destination.setAirportName("De1FDGfdfhdfhhdfffdfgT2H");
		// AirportDTO destinationAirport =
		// airportService.addAirport(destination);

		// RouteDTO routeDTO = new RouteDTO();
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("AtHFfgfhdfdfgfsdggdffG531-dfgdfgDfddfffgfhgIGHT312");
		// routeDTO.setRouteId(routeId);
		// RouteDTO route = routeService.addRoute(routeDTO);

		// Set<RouteDTO> routesDTO=new HashSet<RouteDTO>();
		// routesDTO.add(route);
		// RouteGroupDTO routeGroupDetails=new RouteGroupDTO();
		routeGroupDetails.setRouteGroupName(route.getName());
		// routeGroupDetails.setRoutes(routes);
		routeGroupDetails.setRoutes(routesDTO);
		// RouteGroupDTO routeGroupDTO
		// =routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(),
		// routeGroupDetails);

		// SeatingClassDTO classDetails=new SeatingClassDTO();
		classDetails.setName("Busidsess23xdsfdffghfghdfdfddd4f15371");
		// SeatingClassDTO seatingClassDTO
		// =seatingClassService.addClass(airlineDTO.getAirlineInternalId(),
		// classDetails) ;

		/*
		 * BundleForRouteGroupDTO bundleForRouteGroupDetails =new
		 * BundleForRouteGroupDTO;
		 * bundleForRouteGroupDetails.setBundleId(bundleId);
		 * bundleForRouteGroupDetails.setPpaFee(ppaFee);
		 * bundleForRouteGroupDetails.setPpvFee(ppvFee);
		 * bundleForRouteGroupDetails.setBundleForRouteGroupId(
		 * bundleForRouteGroupId);
		 * bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.
		 * getRouteGroupId());
		 * bundleForRouteGroupDetails.setSeatingClassId(seatingClassId);
		 */

		// BundleDTO bundleDetails =new BundleDTO();
		bundleDetails.setBundleName("First17");

		// Long creationDate=new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setCreationDate(creationDate);

		// Long lastModifiedDate=new
		// Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setLastModifiedDate(lastModifiedDate);

		// Set<SeatingClassPriceMappingDTO>seatingClassPriceMappings = new
		// HashSet<>();
		bundleDetails.setSeatingClassPriceMappings(seatingClassPriceMappings);

		// BundleDTO bundleDTO =
		// bundleService.addBundle(airlineDTO.getAirlineInternalId(),
		// bundleDetails);

		// BundleForRouteGroupDTO bundleForRouteGroupDetails =new
		// BundleForRouteGroupDTO();
		bundleForRouteGroupDetails.setBundleId(bundleDTO.getBundleId());
		bundleForRouteGroupDetails.setPpaFee(3000.0);
		bundleForRouteGroupDetails.setPpvFee(5000.0);
		bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.getRouteGroupId());
		bundleForRouteGroupDetails.setSeatingClassId(seatingClassDTO.getSeatingClassInternalId());
		// routeGroupDetails.setRouteGroupName("DDFdfLI-HYBDdfFD");
		RouteGroupDTO routeGroupdto = routeGroupService.updateRouteGroup(airlineDTO.getAirlineInternalId(),
				routeGroupDTO.getRouteGroupId(), routeGroupDetails);
		Assert.assertEquals(routeGroupdto.getRouteGroupName(), routeGroupDetails.getRouteGroupName());

		Query.from(BundleForRouteGroup.class).where("id = ?", bundleForRouteGroupDTO.getBundleForRouteGroupId()).first()
				.delete();
		Query.from(Bundle.class).where("id = ?", bundleDTO.getBundleId()).first().delete();
		Query.from(SeatingClass.class).where("id = ?", seatingClassDTO.getSeatingClassInternalId()).first().delete();
		Query.from(RouteGroup.class).where("id = ?", routeGroupDTO.getRouteGroupId()).first().delete();
		Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
		Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
		Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();

		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}

	@Test
	public void deleteBundleForRouteGroupTest()
			throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException {
		AirlineDTO airlineDetails = new AirlineDTO();
		airlineDetails.setAirlineName("fSicejddfdfdet1yy");
		airlineDetails.setWebsite("https://www.dggofgdffyntu.com");
		airlineDetails.setCompanyName("Sicejefdft1dfui");
		// airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("903df6f8f9");
		airlineDetails.setIataCode("BBdfdffj");
		airlineDetails.setIcaoCode("AABfddfdffui");
		airlineDetails.setActivate(true);
		Date startDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate = new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate = new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);

		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("AFGdffYTFfgffgfdfghdfGtH7532");
		origin.setAirportICAO("A123t1FGYfgfhfdfddfgTf5H52");
		origin.setAirportName("AFKJHK7FG5fhTddfdffgffgH312");
		AirportDTO originAirport = airportService.addAirport(origin);

		AirportDTO destination = new AirportDTO();
		destination.setAirportIATA("DLIT2fdfgdfdfhFfgFGH");
		destination.setAirportICAO("DLYHUfhdffdfFGfgTG3H12");
		destination.setAirportName("De1FDGfhdfffdfgT2H");
		AirportDTO destinationAirport = airportService.addAirport(destination);

		RouteDTO routeDTO = new RouteDTO();
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("AtHFfgfhdfgdffG531-DfddfffgfhgIGHT312");
		// routeDTO.setRouteId(routeId);
		RouteDTO route = routeService.addRoute(routeDTO);

		Set<RouteDTO> routesDTO = new HashSet<RouteDTO>();
		routesDTO.add(route);
		RouteGroupDTO routeGroupDetails = new RouteGroupDTO();
		routeGroupDetails.setRouteGroupName(route.getName());
		// routeGroupDetails.setRoutes(routes);
		routeGroupDetails.setRoutes(routesDTO);
		RouteGroupDTO routeGroupDTO = routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(),
				routeGroupDetails);

		SeatingClassDTO classDetails = new SeatingClassDTO();
		classDetails.setName("Busidsess23xdsfdfdfddd4f15371");
		SeatingClassDTO seatingClassDTO = seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails);

		/*
		 * BundleForRouteGroupDTO bundleForRouteGroupDetails =new
		 * BundleForRouteGroupDTO;
		 * bundleForRouteGroupDetails.setBundleId(bundleId);
		 * bundleForRouteGroupDetails.setPpaFee(ppaFee);
		 * bundleForRouteGroupDetails.setPpvFee(ppvFee);
		 * bundleForRouteGroupDetails.setBundleForRouteGroupId(
		 * bundleForRouteGroupId);
		 * bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.
		 * getRouteGroupId());
		 * bundleForRouteGroupDetails.setSeatingClassId(seatingClassId);
		 */

		BundleDTO bundleDetails = new BundleDTO();
		bundleDetails.setBundleName("First1");

		Long creationDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setCreationDate(creationDate);

		Long lastModifiedDate = new Long(Calendar.getInstance().getTimeInMillis());
		bundleDetails.setLastModifiedDate(lastModifiedDate);

		Set<SeatingClassPriceMappingDTO> seatingClassPriceMappings = new HashSet<>();
		bundleDetails.setSeatingClassPriceMappings(seatingClassPriceMappings);

		BundleDTO bundleDTO = bundleService.addBundle(airlineDTO.getAirlineInternalId(), bundleDetails);

		BundleForRouteGroupDTO bundleForRouteGroupDetails = new BundleForRouteGroupDTO();
		bundleForRouteGroupDetails.setBundleId(bundleDTO.getBundleId());
		bundleForRouteGroupDetails.setPpaFee(300.0);
		bundleForRouteGroupDetails.setPpvFee(500.0);
		bundleForRouteGroupDetails.setRouteGroupId(routeGroupDTO.getRouteGroupId());
		bundleForRouteGroupDetails.setSeatingClassId(seatingClassDTO.getSeatingClassInternalId());

		// List<BundleForRouteGroupDTO> bundleForRouteGroupDTOList
		// =bundleForRouteGroupService.getAll(airlineDTO.getAirlineInternalId());

		BundleForRouteGroupDTO bundleForRouteGroupDTO = bundleForRouteGroupService
				.addBundleForRouteGroup(airlineDTO.getAirlineInternalId(), bundleForRouteGroupDetails);

		boolean result = bundleForRouteGroupService.deleteBundleForRouteGroup(airlineDTO.getAirlineInternalId(),
				bundleForRouteGroupDTO.getBundleForRouteGroupId());

		Assert.assertTrue(result);
		// Query.from(BundleForRouteGroup.class).where("id = ?",
		// bundleForRouteGroupDTO.getBundleForRouteGroupId()).first().delete();
		Query.from(Bundle.class).where("id = ?", bundleDTO.getBundleId()).first().delete();
		Query.from(SeatingClass.class).where("id = ?", seatingClassDTO.getSeatingClassInternalId()).first().delete();
		Query.from(RouteGroup.class).where("id = ?", routeGroupDTO.getRouteGroupId()).first().delete();
		Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
		Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
		Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();

		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}

	/**
	 * method for delete RouteGroup exception
	 * 
	 * @throws ObjectNotFoundException
	 *//*
		 * @Test(expected = ObjectNotFoundException.class) public void
		 * deleteRouteGroupExceptionTest() throws ObjectNotFoundException {
		 * boolean result = routeGroupService.deleteRouteGroup("airlineId",
		 * "routeGroupId"); Assert.assertTrue(result);
		 * 
		 * }
		 */

}
