package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.Route;
import com.thales.ecms.model.RouteGroup;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.model.dto.LopaDTO;
import com.thales.ecms.model.dto.RouteDTO;
import com.thales.ecms.model.dto.RouteGroupDTO;
import com.thales.ecms.service.RouteGroupService;



/**
 * 
 * @author ashish
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class RouteGroupServiceTest {
	
	@Autowired
	private RouteService routeService;
	
	@Autowired
	private AirportService airportService;

	@Autowired
	private RouteGroupService routeGroupService;
	
	@Autowired
	private AirlineService airlineService;
	
	@Before
	public void setup() {
		
	}

	/**
	 * Method for add RouteGroup test
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
@Test
public void addRouteGroupTest() throws ObjectExistsException, BadRequestException, ObjectNotFoundException, DuplicateEntryException{
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtrefrtgdtgtdfdwer1yy");
	airlineDetails.setWebsite("https://www.agfordfgdgtywerntu.com");
	airlineDetails.setCompanyName("aSicerdftdgdgjet1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("yy690rdftddfdf3689");
	airlineDetails.setIataCode("hBrfghdfdftBj");
	airlineDetails.setIcaoCode("yArtdfgdfdffABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	
	
	AirportDTO origin = new AirportDTO();
	origin.setAirportIATA("AFGdffYTFfgfghdfGtH7532");
	origin.setAirportICAO("A123t1FGYfgfhfdfgTf5H52");
	origin.setAirportName("AFKJHK7FG5fhTdffgH312");
	AirportDTO originAirport = airportService.addAirport(origin);
	
	AirportDTO destination = new AirportDTO();
	destination.setAirportIATA("DLIT2fdfhFfgFGH");
	destination.setAirportICAO("DLYHUfhdfFGfgTG3H12");
	destination.setAirportName("De1FDGfhdffgT2H");
	AirportDTO destinationAirport = airportService.addAirport(destination);
	
	RouteDTO routeDTO = new RouteDTO();
	routeDTO.setDestination(destinationAirport);
	routeDTO.setOrigin(originAirport);
	routeDTO.setName("AtHFfgfhdfG531-DfdffhgIGHT312");
	//routeDTO.setRouteId(routeId);
	RouteDTO route = routeService.addRoute(routeDTO);
	
	Set<RouteDTO> routesDTO=new HashSet<RouteDTO>();
	routesDTO.add(route);
	RouteGroupDTO routeGroupDetails=new RouteGroupDTO();
	routeGroupDetails.setRouteGroupName(route.getName());
	//routeGroupDetails.setRoutes(routes);
	routeGroupDetails.setRoutes(routesDTO);
	
	RouteGroupDTO routeGroupDTO =routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(), routeGroupDetails);
	
	    Assert.assertEquals(routeGroupDTO.getRouteGroupName(), routeGroupDetails.getRouteGroupName());
	    Assert.assertNotNull(routeGroupDTO.getRouteGroupId());
	 //   Assert.assertNotNull(route.getRouteId());
	
	    Query.from(RouteGroup.class).where("id = ?",routeGroupDTO.getRouteGroupId()).first().delete();
	    Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
		Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
		Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();
		
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	

}




@Test
public void getRouteGroupsTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtrefrtgdtgfgfgtdfdwer1yy");
	airlineDetails.setWebsite("https://www.agfordfgfgfgfdgtywerntu.com");
	airlineDetails.setCompanyName("aSicerdftdgdgfggjet1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("yy690rdftddfgfgfdf3689");
	airlineDetails.setIataCode("hBrfghdfdfgftBj");
	airlineDetails.setIcaoCode("yArtdfgdffgfgdffABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	
	AirportDTO origin = new AirportDTO();
	origin.setAirportIATA("AFG1dsgdedfgRTYsdFGrwFfgfghjGFGerwUdsfddfft7532");
	origin.setAirportICAO("A123t1g5erdRTYgddGsFGUggfghjFGdfdff552");
	origin.setAirportName("AFGAN5dgerdfgdwrfFFGFFghjGfgfGGUHddfft675312");
	AirportDTO originAirport = airportService.addAirport(origin);
	
	AirportDTO destination = new AirportDTO();
	destination.setAirportIATA("DLI5tddd6wrdfFGHhgjFgfgGGgUedfrdffdfgg5312");
	destination.setAirportICAO("DLf55ddd6fwerFGdghjFDGFfgffUdfgerfdfdg312");
	destination.setAirportName("Del5dfdh7fwerFGHgjFGdHgfgfgJfdUf3er5i12");
	AirportDTO destinationAirport = airportService.addAirport(destination);
	
	RouteDTO routeDTO = new RouteDTO();
	routeDTO.setDestination(destinationAirport);
	routeDTO.setOrigin(originAirport);
	routeDTO.setName("AFG5grfyFGhFGdfwrGrdUgjHfgJrt531-DereFGFgfgfghjFGLFUGHJ5ddfhwerwftddgff5dI312");
	//routeDTO.setRouteId(routeId);
	RouteDTO route = routeService.addRoute(routeDTO);
	
	Set<RouteDTO> routesDTO=new HashSet<RouteDTO>();
	routesDTO.add(route);
	RouteGroupDTO routeGroupDetails=new RouteGroupDTO();
	routeGroupDetails.setRouteGroupName(route.getName());
	//routeGroupDetails.setRoutes(routes);
	routeGroupDetails.setRoutes(routesDTO);
	
	RouteGroupDTO routeGroupDTO =routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(), routeGroupDetails);
	//List<RouteDTO> routeDTOList = routeService.getRoutes();
	
	List<RouteGroupDTO> routeGroupDTOList =routeGroupService.getRouteGroups(airlineDTO.getAirlineInternalId());
    Assert.assertTrue(routeGroupDTOList.size()>0);
    
    Query.from(RouteGroup.class).where("id = ?",routeGroupDTO.getRouteGroupId()).first().delete();
    Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
	Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
	Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();
	
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
}

@Test
public void getRouteGroupByIdTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtrefrtgdtgfgfgtdfdwer1yy");
	airlineDetails.setWebsite("https://www.agfordfgfgfgfdgtywerntu.com");
	airlineDetails.setCompanyName("aSicerdftdgdgfggjet1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("yy690rdftddfgfgfdf3689");
	airlineDetails.setIataCode("hBrfghdfdfgftBj");
	airlineDetails.setIcaoCode("yArtdfgdffgfgdffABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	AirportDTO origin = new AirportDTO();
	origin.setAirportIATA("AFGdffYTFfgfghGtfgH7532");
	origin.setAirportICAO("A123t1FGYfgfhffggTf5H52");
	origin.setAirportName("AFKJHK7FG5fhTfgfgH312");
	AirportDTO originAirport = airportService.addAirport(origin);
	
	AirportDTO destination = new AirportDTO();
	destination.setAirportIATA("DLIT2fhFfgfgFGH");
	destination.setAirportICAO("DLYHUfhFfgGfgTG3H12");
	destination.setAirportName("De1FDGffghfgT2H");
	AirportDTO destinationAirport = airportService.addAirport(destination);
	
	RouteDTO routeDTO = new RouteDTO();
	routeDTO.setDestination(destinationAirport);
	routeDTO.setOrigin(originAirport);
	routeDTO.setName("AtHFfgffghG531-DfffghgIGHT312");
	//routeDTO.setRouteId(routeId);
	RouteDTO route = routeService.addRoute(routeDTO);
	
	Set<RouteDTO> routesDTO=new HashSet<RouteDTO>();
	routesDTO.add(route);
	RouteGroupDTO routeGroupDetails=new RouteGroupDTO();
	routeGroupDetails.setRouteGroupName(route.getName());
	//routeGroupDetails.setRoutes(routes);
	routeGroupDetails.setRoutes(routesDTO);
	
	RouteGroupDTO routeGroupDTO =routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(), routeGroupDetails);
	RouteGroupDTO RouteGroupDTO=routeGroupService.getRouteGroupById(airlineDTO.getAirlineInternalId(), routeGroupDTO.getRouteGroupId());
    Assert.assertNotNull(RouteGroupDTO.getRouteGroupId());
    
    Query.from(RouteGroup.class).where("id = ?",routeGroupDTO.getRouteGroupId()).first().delete();
    Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
	Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
	Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();
	
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
}

@Test
public void updateRouteGroupTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtrefrtgdtgfgfgtdfdwer1yy");
	airlineDetails.setWebsite("https://www.agfordfgfgfgfdgtywerntu.com");
	airlineDetails.setCompanyName("aSicerdftdgdgfggjet1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("yy690rdftddfgfgfdf3689");
	airlineDetails.setIataCode("hBrfghdfdfgftBj");
	airlineDetails.setIcaoCode("yArtdfgdffgfgdffABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	AirportDTO origin = new AirportDTO();
	origin.setAirportIATA("AFGdffYTFGfgtgjtguyH7532");
	origin.setAirportICAO("A123t1FGYghggjgTf5H52");
	origin.setAirportName("AFKJHK7FG5hfgjgfTH312");
	AirportDTO originAirport = airportService.addAirport(origin);
	
	AirportDTO destination = new AirportDTO();
	destination.setAirportIATA("DLITgh2FfgjgFGH");
	destination.setAirportICAO("DLYHUFGggjfghTG3H12");
	destination.setAirportName("De1FghDfgGT2H");
	AirportDTO destinationAirport = airportService.addAirport(destination);
	
	RouteDTO routeDTO = new RouteDTO();
	routeDTO.setDestination(destinationAirport);
	routeDTO.setOrigin(originAirport);
	routeDTO.setName("AthHfgFgjG531-DIGggjhHfgT312");
	//routeDTO.setRouteId(routeId);
	RouteDTO route = routeService.addRoute(routeDTO);
	
	Set<RouteDTO> routesDTO=new HashSet<RouteDTO>();
	routesDTO.add(route);
	RouteGroupDTO routeGroupDetails=new RouteGroupDTO();
	routeGroupDetails.setRouteGroupName(route.getName());
	//routeGroupDetails.setRoutes(routes);
	routeGroupDetails.setRoutes(routesDTO);
	
	RouteGroupDTO routeGroupDTO =routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(), routeGroupDetails);
	

	//AirportDTO origin = new AirportDTO();
	origin.setAirportIATA("AFGdffgfgfhDFgjt7532");
	origin.setAirportICAO("A123t1fgghDgjFG552");
	origin.setAirportName("AFKJHK7gffhgj53DF12");
	//AirportDTO originAirport = airportService.addAirport(origin);
	
	//AirportDTO destination = new AirportDTO();
	destination.setAirportIATA("DLDgghgjgFI2");
	destination.setAirportICAO("DLDfgFgjYhHUG312");
	destination.setAirportName("DeDgghfghF12");
	//AirportDTO destinationAirport = airportService.addAirport(destination);
	
//	RouteDTO routeDTO = new RouteDTO();
	routeDTO.setDestination(destinationAirport);
	routeDTO.setOrigin(originAirport);
	routeDTO.setName("AtFghtyfh531-DIDtyFfghgh312");
	//routeDTO.setRouteId(routeId);
//	RouteDTO route = routeService.addRoute(routeDTO);
	
	//Set<RouteDTO> routesDTO=new HashSet<RouteDTO>();
	//routesDTO.add(route);
//	RouteGroupDTO routeGroupDetails=new RouteGroupDTO();
	routeGroupDetails.setRouteGroupName(route.getName());
	//routeGroupDetails.setRoutes(routes);
//	routeGroupDetails.setRoutes(routesDTO);
	

	//routeGroupDetails.setRouteGroupName("DDFdfLI-HYBDdfFD");
	RouteGroupDTO routeGroupdto=routeGroupService.updateRouteGroup(airlineDTO.getAirlineInternalId(), routeGroupDTO.getRouteGroupId(), routeGroupDetails);
    Assert.assertEquals(routeGroupdto.getRouteGroupName(), routeGroupDetails.getRouteGroupName());
    
    Query.from(RouteGroup.class).where("id = ?",routeGroupDTO.getRouteGroupId()).first().delete();
    Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
	Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
	Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();
	
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
    
}

@Test
public void deleteRouteGroupTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException{
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("aSicejtrefrtgdtgfgfgtdfdwer1yy");
	airlineDetails.setWebsite("https://www.agfordfgfgfgfdgtywerntu.com");
	airlineDetails.setCompanyName("aSicerdftdgdgfggjet1ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("yy690rdftddfgfgfdf3689");
	airlineDetails.setIataCode("hBrfghdfdfgftBj");
	airlineDetails.setIcaoCode("yArtdfgdffgfgdffABui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);

	AirportDTO origin = new AirportDTO();
	origin.setAirportIATA("AFG1dsgdedfgRTYsdrwFghfgGGHerwdHsfddfft7532");
	origin.setAirportICAO("A123t1g5erdRTYgddsFGghfgdfdGHffH552");
	origin.setAirportName("AFGAN5dgerdfgdwrfFFGgfghGHGHddfft6H75312");
	AirportDTO originAirport = airportService.addAirport(origin);
	
	AirportDTO destination = new AirportDTO();
	destination.setAirportIATA("DLI5tddd6wrdfFGHFGgfghggGHedfrdHffdfgg5312");
	destination.setAirportICAO("DLf55ddd6fwerFGdGFhfgfGHdfgeHrfdfdg312");
	destination.setAirportName("Del5dfdh7fwerFGHghFfgGdfGHdf3Her5i12");
	AirportDTO destinationAirport = airportService.addAirport(destination);
	
	RouteDTO routeDTO = new RouteDTO();
	routeDTO.setDestination(destinationAirport);
	routeDTO.setOrigin(originAirport);
	routeDTO.setName("AFG5grfyFGhFGdfwrrdfgGHghHrt531-DereFghGGHLFHGfg5ddfhwerwftddgff5dI312");
	//routeDTO.setRouteId(routeId);
	RouteDTO route = routeService.addRoute(routeDTO);
	
	Set<RouteDTO> routesDTO=new HashSet<RouteDTO>();
	routesDTO.add(route);
	RouteGroupDTO routeGroupDetails=new RouteGroupDTO();
	routeGroupDetails.setRouteGroupName(route.getName());
	//routeGroupDetails.setRoutes(routes);
	routeGroupDetails.setRoutes(routesDTO);
	
	RouteGroupDTO routeGroupDTO =routeGroupService.addRouteGroup(airlineDTO.getAirlineInternalId(), routeGroupDetails);
	
	boolean result=routeGroupService.deleteRouteGroup(airlineDTO.getAirlineInternalId(), routeGroupDTO.getRouteGroupId());
	
	  Assert.assertTrue(result);
	 
	 	    Query.from(Route.class).where("id = ?", route.getRouteId()).first().delete();
		Query.from(Airport.class).where("id = ?", originAirport.getAirportId()).first().delete();
		Query.from(Airport.class).where("id = ?", destinationAirport.getAirportId()).first().delete();
		
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	 
}

/**
 * method for delete RouteGroup exception
 * 
 * @throws ObjectNotFoundException
 */
@Test(expected = ObjectNotFoundException.class)
public void deleteRouteGroupExceptionTest() throws ObjectNotFoundException {
	boolean result = routeGroupService.deleteRouteGroup("airlineId", "routeGroupId");
	Assert.assertTrue(result);
	
}
	


}
