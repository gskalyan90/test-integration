package com.thales.ecms.service;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.Route;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.model.dto.RouteDTO;

import junit.framework.Assert;

/**
 * 
 * @author shuklar
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class RouteServiceTest {
	@Autowired
	RouteService routeService;
	@Autowired
	AirportService airportService;

	@Before
	public void setup() {

	}

	/**
	 * Method for add route test
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test
	public void addRouteTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		AirportDTO destination = new AirportDTO();
		RouteDTO routeDTO = new RouteDTO();
		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("IATA");
		origin.setAirportICAO("ICAO");
		origin.setAirportName("India Airport");
		AirportDTO originAirport = airportService.addAirport(origin);
		destination.setAirportIATA("IATA1");
		destination.setAirportICAO("IACO1");
		destination.setAirportName("China Airport");
		AirportDTO destinationAirport = airportService.addAirport(destination);
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("Ind-Chn");
		RouteDTO route = routeService.addRoute(routeDTO);
		Assert.assertEquals(route.getName(), routeDTO.getName());
		Assert.assertNotNull(route.getRouteId());
		routeService.deleteRoute(route.getRouteId());
		airportService.deleteAirport(originAirport.getAirportId());
		airportService.deleteAirport(destinationAirport.getAirportId());
	}

	/**
	 * Method for add route exception
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test(expected = BadRequestException.class)
	public void addRouteExceptionTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		RouteDTO routeDTO = new RouteDTO();
		routeDTO.setName("D-LKO");
		routeService.addRoute(routeDTO);
		routeDTO.setName("D-LKO");
		routeService.addRoute(routeDTO);
	}

	/**
	 * Method for get route test
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test
	public void getRoutesTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		AirportDTO destination = new AirportDTO();
		RouteDTO routeDTO = new RouteDTO();
		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("BHIM");
		origin.setAirportICAO("PAY");
		origin.setAirportName("DelhiAirport");
		AirportDTO originAirport = airportService.addAirport(origin);
		destination.setAirportIATA("BOIX");
		destination.setAirportICAO("ICM");
		destination.setAirportName("Lucknow_Airport");
		AirportDTO destinationAirport = airportService.addAirport(destination);
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("D-L");
		RouteDTO route = routeService.addRoute(routeDTO);
		List<RouteDTO> routeDTOList = routeService.getRoutes();
		Assert.assertTrue(routeDTOList.size() > 0);
		routeService.deleteRoute(route.getRouteId());
		airportService.deleteAirport(originAirport.getAirportId());
		airportService.deleteAirport(destinationAirport.getAirportId());
	}

	/**
	 * method for get route by Id
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test
	public void getRouteByIdTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		AirportDTO destination = new AirportDTO();
		RouteDTO routeDTO = new RouteDTO();
		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("originIATA2");
		origin.setAirportICAO("originICAO2");
		origin.setAirportName("originName2");
		AirportDTO originAirport = airportService.addAirport(origin);
		destination.setAirportIATA("destinatrionIATA2");
		destination.setAirportICAO("destinatrionICAO2");
		destination.setAirportName("detinationName2");
		AirportDTO destinationAirport = airportService.addAirport(destination);
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("origin-destination2");
		RouteDTO routeDetails = routeService.addRoute(routeDTO);
		RouteDTO route = routeService.getRouteById(routeDetails.getRouteId());
		Assert.assertNotNull(route);
		routeService.deleteRoute(route.getRouteId());
		airportService.deleteAirport(originAirport.getAirportId());
		airportService.deleteAirport(destinationAirport.getAirportId());

	}

	/**
	 * Method for update route test
	 * 
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	@Test
	public void updateRouteTest() throws ObjectExistsException, BadRequestException, ObjectNotFoundException {

		AirportDTO destination = new AirportDTO();
		RouteDTO routeDTO = new RouteDTO();
		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("IATA");
		origin.setAirportICAO("ICAO");
		origin.setAirportName("India Airport");
		AirportDTO originAirport = airportService.addAirport(origin);
		destination.setAirportIATA("IATA1");
		destination.setAirportICAO("IACO1");
		destination.setAirportName("China Airport");
		AirportDTO destinationAirport = airportService.addAirport(destination);
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("Ind-Chn");
		RouteDTO route = routeService.addRoute(routeDTO);
		origin.setAirportIATA("BfkM");
		origin.setAirportICAO("1234");
		origin.setAirportName("bombayport");
		AirportDTO newOriginAirport = airportService.addAirport(origin);
		destination.setAirportIATA("DLI1");
		destination.setAirportICAO("DLI2");
		destination.setAirportName("Delhihiopfort1");
		AirportDTO newDestinationAirport = airportService.addAirport(destination);
		routeDTO.setDestination(newDestinationAirport);
		routeDTO.setOrigin(newOriginAirport);
		routeDTO.setName("HYB-DdrgtofLI1");
		RouteDTO updatedRoute = routeService.updateRoute(route.getRouteId(), routeDTO);
		Assert.assertNotNull(updatedRoute);
		routeService.deleteRoute(updatedRoute.getRouteId());
		airportService.deleteAirport(originAirport.getAirportId());
		airportService.deleteAirport(destinationAirport.getAirportId());
		airportService.deleteAirport(newDestinationAirport.getAirportId());
		airportService.deleteAirport(newOriginAirport.getAirportId());
	}

	/**
	 * Method for update route exception
	 * 
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 */
	@Test(expected = ObjectNotFoundException.class)
	public void updateRouteExceptionTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException {
		RouteDTO routeDetails = new RouteDTO();
		routeDetails.setName("No Route");
		routeService.updateRoute("NorouteId", routeDetails);

	}

	/**
	 * Method for delete route test
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test
	public void deleteRouteTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		AirportDTO destination = new AirportDTO();
		RouteDTO routeDTO = new RouteDTO();
		AirportDTO origin = new AirportDTO();
		origin.setAirportIATA("IATA");
		origin.setAirportICAO("ICAO");
		origin.setAirportName("India Airport");
		AirportDTO originAirport = airportService.addAirport(origin);
		destination.setAirportIATA("IATA1");
		destination.setAirportICAO("IACO1");
		destination.setAirportName("China Airport");
		AirportDTO destinationAirport = airportService.addAirport(destination);
		routeDTO.setDestination(destinationAirport);
		routeDTO.setOrigin(originAirport);
		routeDTO.setName("Ind-Chn");
		RouteDTO route = routeService.addRoute(routeDTO);
		String result = routeService.deleteRoute(route.getRouteId());
		Assert.assertEquals(result, "Route deleted successfully");
		airportService.deleteAirport(originAirport.getAirportId());
		airportService.deleteAirport(destinationAirport.getAirportId());

	}

	/**
	 * Method for delete route exception
	 * 
	 * @throws ObjectNotFoundException
	 */
	@Test(expected = ObjectNotFoundException.class)
	public void deleteRouteExceptionTest() throws ObjectNotFoundException {
		String result = routeService.deleteRoute("routeIdd123");
		Assert.assertEquals(result, "Route is not exist");
	}

}
